#include "Colibrih.h"

class App : public Colibri::Application
{
private: 
	std::shared_ptr<Colibri::Model> triangleModel;
	std::shared_ptr<Colibri::UniformBufferDescription> mvpUboDesc;
	std::shared_ptr<Colibri::ShaderDescription> vert;
	std::shared_ptr<Colibri::ShaderDescription> frag;
	std::shared_ptr<Colibri::GraphicsPipeline> pipeline;
	double d = 0.0;

public:
	App() 
		: Colibri::Application({ "Colibri-Sanbdbox", 1, 0, 0 })
	{
	}

	void initialize() override
	{
		std::shared_ptr<Colibri::Window> window = Colibri::Window::builder()
			.width(Colibri::Engine::getConfig().getResolutionX())
			.height(Colibri::Engine::getConfig().getResolutionY())
			.title("Sandbox")
			.build();

		engine->getWindowManager().addWindow(window);
		engine->getWindowManager().setActiveWindow(window);

		std::shared_ptr<Colibri::Renderer> renderer = Colibri::Renderer::builder()
			.window(window)
			.build();

		engine->getRendererManager().setRenderer(renderer);

		triangleModel = std::make_shared<Colibri::Model>(std::move(Colibri::Model {
			// Vertices.
			{
				Colibri::ColoredVertex { Colibri::Vec3f { -0.5f, -0.5f, 0.0f }, Colibri::Vec3f { 1.0f, 0.0f, 0.0f } }, // Top left
				Colibri::ColoredVertex { Colibri::Vec3f {  0.5f, -0.5f, 0.0f }, Colibri::Vec3f { 0.0f, 1.0f, 0.0f } }, // Top right
				Colibri::ColoredVertex { Colibri::Vec3f {  0.5f,  0.5f, 0.0f }, Colibri::Vec3f { 0.0f, 0.0f, 1.0f } }, // Bottom right
				Colibri::ColoredVertex { Colibri::Vec3f { -0.5f,  0.5f, 0.0f }, Colibri::Vec3f { 1.0f, 1.0f, 1.0f } }  // Bottom left
			},
			// Indices.
			{
				0, 1, 2, 2, 3, 0
			}
			}));

		struct MvpUboLayout {
			alignas(16) Colibri::Mat4f mvp;
		};
		mvpUboDesc = Colibri::UniformBufferDescription::builder()
			.binding(Colibri::UniformBufferBinding { 0, 1, Colibri::ShaderStage::vertex })
			.sizeInBytes(sizeof(MvpUboLayout))
			.fillBuffer([&](void* buffer, size_t sizeInBytes, const Colibri::Entity& entity)
				{
					//const auto& mvp = entity.getTransform().getMvp();
					d += engine->getTimer().getDeltaS() * 1.0;
					MvpUboLayout data;
					data.mvp = Colibri::Mat4f::makeIdentity();
					data.mvp[3][0] = (float)std::sin(d);
					assert(sizeof(data) <= sizeInBytes);
					memcpy(buffer, &data, sizeof(data));
				})
			.build();

		vert = Colibri::ShaderDescription::builder()
			.type(Colibri::ShaderType::vertex)
			.addSource("res\\Shaders\\Triangle.vert.spv")
			.addUniformBufferDescription(mvpUboDesc)
			.build();

		frag = Colibri::ShaderDescription::builder()
			.type(Colibri::ShaderType::fragment)
			.addSource("res\\Shaders\\Triangle.frag.spv")
			.build();

		pipeline = Colibri::GraphicsPipeline::builder()
			.renderer(renderer)
			.vertexShader(vert)
			.fragmentShader(frag)
			.vertexInputDescription(Colibri::ColoredVertex::defaultVertexInputDescription)
			.build();

		Colibri::Entity triangle { triangleModel, pipeline };

		engine->getEcs().addEntity(triangle);
	}

	void onUpdate() override
	{
	}

	void terminate() override
	{
		pipeline.reset();
		frag.reset();
		vert.reset();
		mvpUboDesc.reset();
		triangleModel.reset();
	}
};

Colibri::Application* Colibri::createApplication()
{
	return new App();
}
