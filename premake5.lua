--
-- Premake does currently not support defining the C++ language level to be C++20.
-- The language level of all projects must therefore be adjusted manually after workspace generation.
-- A fix is already merged and should be available in the next release: Alpha 16
--


workspace "Colibri"
	architecture "x86_64"
	configurations {
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "Colibri"
	location "Colibri"
	kind "StaticLib"
	language "C++"

	--targetdir ("bin/%{prj.name}/" .. outputdir)
	--objdir ("bin-int/%{prj.name}/" .. outputdir)
	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")
	
	files {
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
		"%{prj.name}/**.cpp", -- picks up prcompiled header
		"%{prj.name}/**.h" -- picks up prcompiled header
	}

	includedirs {
		os.getenv("VULKAN_SDK") .. "/Include",
		"libraries/glfw-3.3.2.bin.WIN64/include",
		"libraries/json/include",
		"libraries/boost_1_73_0",
		"%{prj.name}" -- finds the precompiled header
	}

	pchsource "%{prj.name}/pch.cpp" -- for vs only: must be real path!
	pchheader "pch.h"
	
	filter "system:window"
		cppdialect "C++latest"
		staticruntime "On"
		systemversion "latest"
		defines {
			"COLIBRI_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines {
			"COLIBRI_DEBUG",
			"COLIBRI_NOT_RELEASE",
			"COLIBRI_NOT_DIST"
		}
		symbols "On"
		optimize "Off"

	filter "configurations:Release"
		defines {
			"COLIBRI_RELEASE",
			"COLIBRI_NOT_DEBUG",
			"COLIBRI_NOT_DIST"
		}
		symbols "Off"
		optimize "Speed"

	filter "configurations:Dist"
		defines {
			"COLIBRI_DIST",
			"COLIBRI_NOT_DEBUG",
			"COLIBRI_NOT_RELEASE"
		}
		symbols "Off"
		optimize "Speed"

project "ColibriTests"
	location "ColibriTests"
	kind "ConsoleApp"
	language "C++"

	--targetdir ("bin/%{prj.name}/" .. outputdir)
	--objdir ("bin-int/%{prj.name}/" .. outputdir)
	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files {
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
		--"%{prj.name}/packages.config",
		"%{prj.name}/**.cpp", -- picks up prcompiled header
		"%{prj.name}/**.h" -- picks up prcompiled header
	}

	includedirs {
		os.getenv("VULKAN_SDK") .. "/Include",
		"libraries/glfw-3.3.2.bin.WIN64/include",
		"libraries/json/include",
		"libraries/boost_1_73_0",
		"Colibri/src",
		"%{prj.name}" -- finds the precompiled header
	}

	libdirs {
		"C:/VulkanSDK/1.2.135.0/Lib",
		"libraries/glfw-3.3.2.bin.WIN64/lib-vc2019",
		"libraries/boost_1_73_0/stage/lib"
	}

	links {
		"Colibri",
		"vulkan-1.lib",
		"glfw3.lib"
	}
	
	nuget {
		"Microsoft.googletest.v140.windesktop.msvcstl.static.rt-dyn:1.8.1.3"
	}

	pchsource "%{prj.name}/pch.cpp" -- for vs only: must be real path!
	pchheader "pch.h"

	filter "system:window"
		cppdialect "C++latest"
		staticruntime "On"
		systemversion "latest"
		defines {
			"COLIBRI_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines {
			"COLIBRI_DEBUG",
			"COLIBRI_NOT_RELEASE",
			"COLIBRI_NOT_DIST"
		}
		symbols "On"
		optimize "Off"

	filter "configurations:Release"
		defines {
			"COLIBRI_RELEASE",
			"COLIBRI_NOT_DEBUG",
			"COLIBRI_NOT_DIST"
		}
		symbols "Off"
		optimize "Speed"

	filter "configurations:Dist"
		defines {
			"COLIBRI_DIST",
			"COLIBRI_NOT_DEBUG",
			"COLIBRI_NOT_RELEASE"
		}
		symbols "Off"
		optimize "Speed"

project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	language "C++"
	icon "%{prj.name}/res/Icon/Colibri.ico"

	--targetdir ("bin/%{prj.name}/" .. outputdir)
	--objdir ("bin-int/%{prj.name}/" .. outputdir)
	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	files {
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
		"%{prj.name}/resource.h",
		"%{prj.name}/%{prj.name}.aps",
		"%{prj.name}/%{prj.name}.rc"
	}

	includedirs {
		os.getenv("VULKAN_SDK") .. "/Include",
		"libraries/glfw-3.3.2.bin.WIN64/include",
		"libraries/json/include",
		"libraries/boost_1_73_0",
		"Colibri/src"
	}
	
	libdirs {
		"C:/VulkanSDK/1.2.135.0/Lib",
		"libraries/glfw-3.3.2.bin.WIN64/lib-vc2019",
		"libraries/boost_1_73_0/stage/lib"
	}

	links {
		"Colibri",
		"vulkan-1.lib",
		"glfw3.lib"
	}

	filter "system:window"
		cppdialect "C++latest"
		staticruntime "On"
		systemversion "latest"
		defines	{
			"COLIBRI_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines {
			"COLIBRI_DEBUG",
			"COLIBRI_NOT_RELEASE",
			"COLIBRI_NOT_DIST"
		}
		symbols "On"
		optimize "Off"

	filter "configurations:Release"
		defines {
			"COLIBRI_RELEASE",
			"COLIBRI_NOT_DEBUG",
			"COLIBRI_NOT_DIST"
		}
		symbols "Off"
		optimize "Speed"

	filter "configurations:Dist"
		defines {
			"COLIBRI_DIST",
			"COLIBRI_NOT_DEBUG",
			"COLIBRI_NOT_RELEASE"
		}
		symbols "Off"
		optimize "Speed"
