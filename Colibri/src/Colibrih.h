#pragma once

/* 
 * -------------------------------------------------------------------
 * This file is intended to be included in a client application. 
 * Colibri will never include this file by itself!
 * -------------------------------------------------------------------
 */

#include "../pch.h"

#include "Colibri/Core/Engine.h"
#include "Colibri/Core/Application.h"

#ifdef COLIBRI_EXCLUDE_ENTRY_POINT
#else
#include "Colibri/Core/EntryPoint.h"
#endif

#include "Colibri/Math/Math.h"

#include "Colibri/Observer/Pattern.h"

#include "Colibri/Tools/Logger.h"
#include "Colibri/Tools/Timer.h"


#include "Colibri/Graphics/Window/Window.h"
