#pragma once

#ifndef _VULKAN_PHYSICAL_DEVICES_
#define _VULKAN_PHYSICAL_DEVICES_

#include "../PlatformLibraries.h"
#include "VulkanInstance.h"
#include "VulkanSurface.h"
#include "VulkanPhysicalDevice.h"

COLIBRI_BEGIN

class VulkanPhysicalDevices
{
private:
	std::vector<VulkanPhysicalDevice> devices;

public:
	VulkanPhysicalDevices(const VulkanInstance& vulkanInstance,
						  const VulkanSurface& surface);

	const std::vector<VulkanPhysicalDevice>& getDevices() const;

	void print() const;
};

COLIBRI_END

#endif // _VULKAN_PHYSICAL_DEVICES_
