#pragma once

#ifndef _VULKAN_INSTANCE_H_
#define _VULKAN_INSTANCE_H_

#include "../../Core/Application.h"
#include "../PlatformLibraries.h"
#include "VulkanExtensionsInfo.h"

COLIBRI_BEGIN

class VulkanInstance
{
private:
	VkInstance instance;
	bool useValidationLayers = false;
	const std::vector<const char*> requestedValidationLayers = { "VK_LAYER_KHRONOS_validation", "VK_LAYER_LUNARG_standard_validation" };

public:
	VulkanInstance(const ApplicationInfo& applicationInfo, const VulkanExtensionsInfo& extensionsInfo);
	~VulkanInstance();
	VulkanInstance(const VulkanInstance& other) = delete;
	VulkanInstance(VulkanInstance&& other) noexcept;
	VulkanInstance& operator=(const VulkanInstance& other) = delete;
	VulkanInstance& operator=(VulkanInstance&& other) noexcept;

	bool isUsingValidationLayers() const { return useValidationLayers; }
	const VkInstance* const getInstance() const { return &instance; }

private:
	std::vector<const char*> computeRequiredExtensions();
	std::vector<VkLayerProperties> getAvailableValidationLayers();
	std::vector<const char*> checkValidationLayerSupport(const std::vector<const char*>& requestedValidationLaysers);
};

COLIBRI_END

#endif // _VULKAN_INSTANCE_H_
