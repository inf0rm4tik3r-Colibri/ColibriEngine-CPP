#include "pch.h"
# include "VulkanUniformBuffer.h"

COLIBRI_BEGIN

VulkanUniformBuffer::VulkanUniformBuffer(size_t sizeInBytes, bool hostVisible)
	: VulkanBuffer(
		sizeInBytes,
		hostVisible ? VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT : VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		hostVisible ? VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT : VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		hostVisible
	)
{
}

VulkanUniformBuffer::~VulkanUniformBuffer()
{
}

COLIBRI_END
