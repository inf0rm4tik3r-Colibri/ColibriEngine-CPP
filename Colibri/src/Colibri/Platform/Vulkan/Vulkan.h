#pragma once

#ifndef _VULKAN_H_
#define _VULKAN_H_

#include "../../Core/Application.h"
#include "VulkanInstance.h"
#include "VulkanDebugCallback.h"
#include "VulkanExtensionsInfo.h"
#include "VulkanPhysicalDevice.h"
#include "VulkanPhysicalDevices.h"
#include "VulkanQueueFamilies.h"
#include "VulkanLogicalDevice.h"

COLIBRI_BEGIN

class Vulkan {

private:
	static std::unique_ptr<VulkanExtensionsInfo> extensionsInfo;
	static std::unique_ptr<VulkanInstance> instance;
	static std::unique_ptr<VulkanDebugCallback> debugCallback;
	static std::unique_ptr<VulkanPhysicalDevices> physicalDevices;
	static const VulkanPhysicalDevice* physicalDevice;
	static std::unique_ptr<VulkanQueueFamilies> physicalDeviceQueueFamilies;
	static std::unique_ptr<VulkanLogicalDevice> logicalDevice;

public:
	static void initialize(const ApplicationInfo& appInfo);
	static void terminate();

	static void pickPhysicalDevice(const std::vector<std::string>& requiredDeviceExtensions,
								   const VulkanSurface& surface);

	static const VulkanExtensionsInfo& getExtensionsInfo() { return *extensionsInfo; }
	static const VulkanInstance& getInstance() { return *instance; }
	static const VulkanPhysicalDevice* getPhysicalDevice() { return physicalDevice; }
	static const VulkanLogicalDevice* getLogicalDevice() { return logicalDevice.get(); }

};

COLIBRI_END

#endif // _VULKAN_H_
