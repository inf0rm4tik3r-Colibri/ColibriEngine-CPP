#pragma once

#ifndef _VULKAN_DESCRIPTOR_SET_LAYOUT_H_
#define _VULKAN_DESCRIPTOR_SET_LAYOUT_H_

#include "../PlatformLibraries.h"

COLIBRI_BEGIN

class VulkanDescriptorSetLayout
{
protected:
	uint64_t id;
	VkDescriptorSetLayout descriptorSetLayout;
	VkDescriptorSetLayoutBinding uniformLayoutBinding;

public:
	VulkanDescriptorSetLayout();
	~VulkanDescriptorSetLayout();

	VulkanDescriptorSetLayout(const VulkanDescriptorSetLayout& other) = delete;
	VulkanDescriptorSetLayout(VulkanDescriptorSetLayout&& other) noexcept;
	VulkanDescriptorSetLayout& operator=(const VulkanDescriptorSetLayout& other) = delete;
	VulkanDescriptorSetLayout& operator=(VulkanDescriptorSetLayout&& other) noexcept;

	inline uint64_t getId() const { return id; }
	const VkDescriptorSetLayout& getVkDescriptorSetLayout() const;
	virtual const VkDescriptorSetLayoutBinding& getVkDescriptorSetLayoutBinding() const = 0;
};

COLIBRI_END

#endif // _VULKAN_DESCRIPTOR_SET_LAYOUT_H_