#pragma once

#ifndef _VULKAN_SHADER_MODULE_H_
#define _VULKAN_SHADER_MODULE_H_

#include "../PlatformLibraries.h"
#include "Vulkan.h"

COLIBRI_BEGIN

class VulkanShaderModule
{
private:
	VkShaderModule shaderModule;

public:
	// TODO: Pass more than one shader to (for example) create a module of combined fragment shaders with different entrypoints for different behaviour. 
	explicit VulkanShaderModule(const std::vector<char>& shaderByteCode);
	~VulkanShaderModule();

	const VkShaderModule& getVkShaderModule() const;
};

COLIBRI_END

#endif // _VULKAN_SHADER_MODULE_H_
