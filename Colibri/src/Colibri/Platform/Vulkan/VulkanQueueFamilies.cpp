#include "pch.h"
#include "VulkanQueueFamilies.h"

COLIBRI_BEGIN

VulkanQueueFamilies::VulkanQueueFamilies(const VulkanPhysicalDevice& physicalDevice, 
										 const VulkanSurface& surface)
{
	LOG_INFO << "Finding queue families for: \"" << physicalDevice.getName() << "\"";

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice.getVkDevice(), &queueFamilyCount, nullptr);

	queueFamilies = std::vector<VkQueueFamilyProperties>(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice.getVkDevice(), &queueFamilyCount, queueFamilies.data());

	int i = 0;
	for (const VkQueueFamilyProperties& queueFamily : queueFamilies) {
		LOG_INFO << "\t(" << i << ") flags: " << queueFamily.queueFlags << ", count: " << queueFamily.queueCount;
		if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			LOG_INFO << "\t(" << i << ") - is graphics family.";
			if (graphicsFamily == std::nullopt)
			{
				graphicsFamily = i;
			}
		}
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice.getVkDevice(), i, surface.getVkSurface(), &presentSupport);
		if (presentSupport)
		{
			LOG_INFO << "\t(" << i << ") - is present family.";
			if (presentFamily == std::nullopt)
			{
				presentFamily = i;
			}
		}
		if (queueFamily.queueFlags & VK_QUEUE_TRANSFER_BIT) {
			LOG_INFO << "\t(" << i << ") - is transfer family.";
			if (transferFamily == std::nullopt)
			{
				transferFamily = i;
			}
		}
		i++;
	}

	if (!graphicsFamily.has_value())
	{
		throw COLIBRI_EXCEPTION("Unable to find graphics queue family.");
	}
	if (!transferFamily.has_value())
	{
		throw COLIBRI_EXCEPTION("Unable to find transfer queue family.");
	}
	if (!presentFamily.has_value())
	{
		throw COLIBRI_EXCEPTION("Unable to find present queue family.");
	}
	if (graphicsFamily.value() != presentFamily.value()) // TODO: relax this!!!
	{
		throw COLIBRI_EXCEPTION("Graphics and present family must be the same!");
	}

	LOG_INFO << "\t" << graphicsFamily.value() << " - Chosen as graphics family!";
	LOG_INFO << "\t" << transferFamily.value() << " - Chosen as transfer family!";
	LOG_INFO << "\t" << presentFamily.value() << " - Chosen as present family!";
}

bool VulkanQueueFamilies::hasGraphicsFamily() const
{
	return graphicsFamily.has_value();
}

const std::optional<uint32_t>& VulkanQueueFamilies::getGraphicsFamily() const
{
	return graphicsFamily;
}

std::optional<VkQueueFamilyProperties> VulkanQueueFamilies::getGraphicsFamilyProperties() const
{
	return hasGraphicsFamily()
		? std::make_optional<VkQueueFamilyProperties>(queueFamilies.at(*graphicsFamily))
		: std::nullopt;
}

bool VulkanQueueFamilies::hasTransferFamily() const
{
	return transferFamily.has_value();
}

const std::optional<uint32_t>& VulkanQueueFamilies::getTransferFamily() const
{
	return transferFamily;
}

std::optional<VkQueueFamilyProperties> VulkanQueueFamilies::getTransferFamilyProperties() const
{
	return hasTransferFamily()
		? std::make_optional<VkQueueFamilyProperties>(queueFamilies.at(*transferFamily))
		: std::nullopt;
}

bool VulkanQueueFamilies::hasPresentFamily() const
{
	return presentFamily.has_value();
}

const std::optional<uint32_t>& VulkanQueueFamilies::getPresentFamily() const
{
	return presentFamily;
}

std::optional<VkQueueFamilyProperties> VulkanQueueFamilies::getPresentFamilyProperties() const
{
	return hasPresentFamily()
		? std::make_optional<VkQueueFamilyProperties>(queueFamilies.at(*presentFamily))
		: std::nullopt;
}

COLIBRI_END
