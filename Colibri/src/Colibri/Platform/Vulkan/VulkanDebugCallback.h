#pragma once

#ifndef _VULKAN_DEBUG_CALLBACK_H_
#define _VULKAN_DEBUG_CALLBACK_H_

#include "../PlatformLibraries.h"

COLIBRI_BEGIN

class VulkanDebugCallback
{
private:
    const VulkanInstance& vulkanInstance;
    VkDebugUtilsMessengerEXT debugMessenger;

public:
    static const bool verboseMessages = VULKAN_LOG_VERBOSE;

    VulkanDebugCallback(const VulkanInstance& vulkanInstance)
        :vulkanInstance(vulkanInstance)
    {
        VkDebugUtilsMessengerCreateInfoEXT createInfo = getDefaultDebugMessengerCreateInfo();
        if (CreateDebugUtilsMessengerEXT(*vulkanInstance.getInstance(), &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
            LOG_ERROR << "Failed to set up a debug messenger!";
            throw std::runtime_error("Failed to set up a debug messenger!");
        }
        LOG_INFO << "Created Vulkan debug callback.";
    }

    ~VulkanDebugCallback()
    {
        LOG_INFO << "Destroyed Vulkan debug callback.";
        DestroyDebugUtilsMessengerEXT(*vulkanInstance.getInstance(), debugMessenger, nullptr);
    }

    static VkDebugUtilsMessengerCreateInfoEXT getDefaultDebugMessengerCreateInfo()
    {
        VkDebugUtilsMessengerCreateInfoEXT createInfo {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        uint32_t severityFlag = 0;
        if (VULKAN_LOG_GENERAL)
        {
            severityFlag |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;
        }
        if (VULKAN_LOG_INFO)
        {
            severityFlag |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;
        }
        if (VULKAN_LOG_WARN)
        {
            severityFlag |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
        }
        if (VULKAN_LOG_ERROR)
        {
            severityFlag |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        }
        createInfo.messageSeverity = severityFlag;
        /*
        createInfo.messageSeverity = 
             VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT // Could be ommitted to greately thin out the received messages.
           | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT    // Could be ommitted to greately thin out the received messages.
           | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
           | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        */
        createInfo.messageType =
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        createInfo.pfnUserCallback = VulkanDebugCallback::debugCallback;
        createInfo.pUserData = nullptr; // Optional
        return createInfo;
    }

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData) 
    {
        /*
         * VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT: Diagnostic message.
         * VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: Informational message like the creation of a resource.
         * VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: Message about behavior that is not necessarily an error, but very likely a bug in your application.
         * VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: Message about behavior that is invalid and may cause crashes.
         */
        switch (messageSeverity)
        {
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
                LOG_TRACE;
                if (verboseMessages) {
                    LOG << "(Diagnostic message): ";
                }
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
                LOG_INFO;
                if (verboseMessages) {
                    LOG << "(Informational): ";
                }
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
                LOG_WARN;
                if (verboseMessages) {
                    LOG << "(Found behaviour that is not necessarily an error, but very likely a bug in your application): ";
                }
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
                LOG_ERROR;
                if (verboseMessages) {
                    LOG << "(Severe error or found behaviour that is invalid and may cause crashes): ";
                }
                break;
            default:
                LOG_ERROR << "Unknown message severity!";
                break;
        }

        LOG << "Vulkan validation layer: ";

        /*
         * VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT: Some event has happened that is unrelated to the specification or performance.
         * VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT: Something has happened that violates the specification or indicates a possible mistake.
         * VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT: Potential non-optimal use of Vulkan.
         */
        switch (messageType)
        {
            case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
                LOG << (verboseMessages 
                        ? "GENERAL (Some event has happened that is unrelated to the specification or performance): "
                        : "GENERAL: "
                        );
                break;
            case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
                LOG << (verboseMessages
                        ? "VALIDATION (Something has happened that violates the specification or indicates a possible mistake): "
                        : "VALIDATION: "
                        );
                break;
            case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
                LOG << (verboseMessages
                        ? "PERFORMANCE (Potential non-optimal use of Vulkan): "
                        : "PERFORMANCE: "
                        );
                break;
            default:
                LOG << "Unknown message type!";
                break;
        }

        LOG << pCallbackData->pMessage;

        return VK_FALSE; // Only return true if we want to test the validation layer!
    }

};

COLIBRI_END

#endif // _VULKAN_DEBUG_CALLBACK_H_
