#pragma once

#ifndef _VULKAN_DESCRIPTOR_POOL_H_
#define _VULKAN_DESCRIPTOR_POOL_H_

#include "../PlatformLibraries.h"
#include "VulkanDescriptorSet.h"
#include "VulkanDescriptorSetLayout.h"

COLIBRI_BEGIN

class VulkanDescriptorPool
{
private:
	VkDescriptorPool descriptorPool;

public:
	VulkanDescriptorPool(VkDescriptorType descriptorType, size_t size);
	virtual ~VulkanDescriptorPool();

	VulkanDescriptorPool(const VulkanDescriptorPool& other) = delete;
	VulkanDescriptorPool(VulkanDescriptorPool&& other) noexcept;
	VulkanDescriptorPool& operator=(const VulkanDescriptorPool& other) = delete;
	VulkanDescriptorPool& operator=(VulkanDescriptorPool&& other) noexcept;

	VulkanDescriptorSet allocateDescriptorSet(const VulkanDescriptorSetLayout& descriptorSetLayout) const;
	std::vector<VulkanDescriptorSet> allocateDescriptorSets(const VulkanDescriptorSetLayout& descriptorSetLayout, size_t times) const;
	std::vector<VulkanDescriptorSet> allocateDescriptorSets(const std::vector<VulkanDescriptorSetLayout>& descriptorSetLayouts) const;

	const VkDescriptorPool& getVkDescriptorPool() const;

private:
	std::vector<VulkanDescriptorSet> allocateDescriptorSetsByVkLayouts(const std::vector<VkDescriptorSetLayout>& vkLayouts) const;
};

COLIBRI_END

#endif // _VULKAN_DESCRIPTOR_POOL_H_