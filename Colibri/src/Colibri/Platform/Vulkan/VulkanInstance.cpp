#include "pch.h"
#include "../../Core/Engine.h"
#include "VulkanInstance.h"
#include "VulkanDebugCallback.h"

COLIBRI_BEGIN

VulkanInstance::VulkanInstance(const ApplicationInfo& appInfo, const VulkanExtensionsInfo& extensionsInfo)
{
    VkApplicationInfo vkAppInfo {};
    vkAppInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    vkAppInfo.pApplicationName = appInfo.appName.c_str();
    vkAppInfo.applicationVersion = VK_MAKE_VERSION(appInfo.appVersionMajor, appInfo.appVersionMinor, appInfo.appVersionPatch);
    vkAppInfo.pEngineName = Engine::EngineName().c_str();
    vkAppInfo.engineVersion = VK_MAKE_VERSION(
        Engine::ColibriVersionMajor,
        Engine::ColibriVersionMinor,
        Engine::ColibriVersionPatch
    );
    vkAppInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo createInfo {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &vkAppInfo;
    
    // Validation layers...
#ifdef COLIBRI_NOT_DEBUG
    LOG_INFO << "Skipping validation layers.";
    useValidationLayers = false;
    createInfo.enabledLayerCount = 0;
    createInfo.ppEnabledLayerNames = nullptr;
#else
    LOG_INFO << "Enabling validation layers.";
    LOG_INFO << "Requested validation layers: " << requestedValidationLayers;
    useValidationLayers = true;
    const std::vector<const char*> checkedValidationLayers = checkValidationLayerSupport(requestedValidationLayers);
    LOG_INFO << "Checked validation layers: " << checkedValidationLayers;
    VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo; // Should not be destroyed before vkCreateInstance is called.
    if (!checkedValidationLayers.empty()) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(checkedValidationLayers.size());
        createInfo.ppEnabledLayerNames = checkedValidationLayers.data();
        // This allows us to debug the creation of this vulkan instance.
        debugMessengerCreateInfo = VulkanDebugCallback::getDefaultDebugMessengerCreateInfo();
        createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*) &debugMessengerCreateInfo;
    }
    else {
        LOG_WARN << "None of the requested validation layers were supported.";
        createInfo.enabledLayerCount = 0;
        createInfo.ppEnabledLayerNames = nullptr;
    }
#endif

    // Extensions...
    LOG_INFO << "Enabling extensions.";
    const std::vector<const char*> requiredExtensions = computeRequiredExtensions();
    LOG_INFO << "Required extension: " << requiredExtensions;
    bool allAvailable = extensionsInfo.allAvailable(requiredExtensions);
    if (!allAvailable) {
        LOG_ERROR << "Not all required Vulkan extensions are available!";
        throw std::runtime_error("Not all required Vulkan extensions are available!");
    }
    LOG_INFO << "Required extension are all supported.";
    createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
    createInfo.ppEnabledExtensionNames = requiredExtensions.data();

    if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
        LOG_ERROR << "Failed to create Vulkan instance!";
        throw std::runtime_error("Failed to create Vulkan instance!");
    }
}

VulkanInstance::~VulkanInstance()
{
    if (instance != nullptr)
    {
        vkDestroyInstance(instance, nullptr);
        LOG_INFO << "Destroyed Vulkan instance.";
    }
}

VulkanInstance::VulkanInstance(VulkanInstance&& other) noexcept
    : instance(std::move(other.instance)),
    useValidationLayers(std::move(other.useValidationLayers))
{
    other.instance = nullptr;
}

VulkanInstance& VulkanInstance::operator=(VulkanInstance&& other) noexcept
{
    instance = std::move(other.instance);
    useValidationLayers = std::move(other.useValidationLayers);
    other.instance = nullptr;
    return *this;
}

std::vector<const char*> VulkanInstance::computeRequiredExtensions()
{
    uint32_t glfwRequiredExtensionCount = 0;
    const char** glfwRequiredExtensionNames = glfwGetRequiredInstanceExtensions(&glfwRequiredExtensionCount);

    std::vector<const char*> requiredExtensions(glfwRequiredExtensionNames, glfwRequiredExtensionNames + glfwRequiredExtensionCount);
    if (useValidationLayers) {
        requiredExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    return requiredExtensions;
}

std::vector<VkLayerProperties> VulkanInstance::getAvailableValidationLayers()
{
    uint32_t availableLayerCount;
    vkEnumerateInstanceLayerProperties(&availableLayerCount, nullptr);

    std::vector<VkLayerProperties> availableLayers(availableLayerCount);
    vkEnumerateInstanceLayerProperties(&availableLayerCount, availableLayers.data());

    return availableLayers;
}

std::vector<const char*> VulkanInstance::checkValidationLayerSupport(const std::vector<const char*>& requestedValidationLaysers)
{
    std::vector<VkLayerProperties> availableValidationLayers = VulkanInstance::getAvailableValidationLayers();
    std::vector<const char*> checkedValidationLayers;
    for (const char* layerName : requestedValidationLaysers) {
        bool foundAvailableLayer = false;
        for (const VkLayerProperties& layerProperties : availableValidationLayers) {
            if (strcmp(layerName, layerProperties.layerName) == 0) {
                foundAvailableLayer = true;
                LOG_INFO << "Validation layer with name \"" << layerName << "\" is supported!";
                checkedValidationLayers.push_back(layerName);
                break;
            }
        }
        if (!foundAvailableLayer) {
            LOG_WARN << "Validation layer with name \"" << layerName << "\" is not supported! Ignoring it...";
        }
    }
    return checkedValidationLayers;
}

COLIBRI_END
