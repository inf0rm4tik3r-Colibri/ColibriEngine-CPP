#include "pch.h"
#include "VulkanIndexBuffer.h"
#include "Vulkan.h"
#include "VulkanMemory.h"

COLIBRI_BEGIN

VulkanIndexBuffer::VulkanIndexBuffer(size_t size, size_t sizePerIndex, bool hostVisible)
	: VulkanBuffer(
		size * sizePerIndex,
		hostVisible ? VK_BUFFER_USAGE_INDEX_BUFFER_BIT : VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		hostVisible ? VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT : VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		hostVisible
	)
{
}

VulkanIndexBuffer::~VulkanIndexBuffer()
{
}

void VulkanIndexBuffer::fill(const std::initializer_list<uint32_t>& indices)
{
	VulkanBuffer::fill<uint32_t>(indices);
}

COLIBRI_END
