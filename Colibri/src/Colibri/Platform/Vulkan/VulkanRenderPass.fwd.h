#pragma once

#ifndef _VULKAN_RENDER_PASS_FWD_H_
#define _VULKAN_RENDER_PASS_FWD_H_

COLIBRI_BEGIN

class VulkanRenderPass;

COLIBRI_END

#endif // _VULKAN_RENDER_PASS_FWD_H_
