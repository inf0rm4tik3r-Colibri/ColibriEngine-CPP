#include "pch.h"
#include "VulkanUniformDescriptorSetLayout.h"
#include "Vulkan.h"

COLIBRI_BEGIN

VulkanUniformDescriptorSetLayout::VulkanUniformDescriptorSetLayout(const uint64_t uniformBufferDescriptionId, 
                                                                   const UniformBufferBinding& uniformBufferbinding)
    : VulkanDescriptorSetLayout(), uniformBufferDescriptionId(uniformBufferDescriptionId)
{
    VkShaderStageFlags shaderStageFlags = 0;
    for (const ShaderStage& shaderStage : uniformBufferbinding.shaderStages)
    {
        switch (shaderStage) {
            case ShaderStage::vertex:
                shaderStageFlags |= VK_SHADER_STAGE_VERTEX_BIT;
                break;
            case ShaderStage::compute:
                shaderStageFlags |= VK_SHADER_STAGE_COMPUTE_BIT;
                break;
            case ShaderStage::tesselationControl:
                shaderStageFlags |= VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
                break;
            case ShaderStage::tesselationEvaluation:
                shaderStageFlags |= VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
                break;
            case ShaderStage::fragment:
                shaderStageFlags |= VK_SHADER_STAGE_FRAGMENT_BIT;
                break;
        }
    }

    uniformLayoutBinding = VkDescriptorSetLayoutBinding {};
    uniformLayoutBinding.binding = uniformBufferbinding.binding;
    uniformLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniformLayoutBinding.descriptorCount = uniformBufferbinding.count;
    uniformLayoutBinding.stageFlags = shaderStageFlags;
    uniformLayoutBinding.pImmutableSamplers = nullptr; // Only relevant for image sampling related descriptors.

    VkDescriptorSetLayoutCreateInfo layoutInfo {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = 1;
    layoutInfo.pBindings = &uniformLayoutBinding;

    if (vkCreateDescriptorSetLayout(Vulkan::getLogicalDevice()->getVkDevice(), &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS)
    {
        throw COLIBRI_EXCEPTION("Failed to create descriptor set layout!");
    }
    LOG_INFO << "Created vulkan descriptor set layout.";
}

VulkanUniformDescriptorSetLayout::~VulkanUniformDescriptorSetLayout()
{
}

VulkanUniformDescriptorSetLayout::VulkanUniformDescriptorSetLayout(VulkanUniformDescriptorSetLayout&& other) noexcept
    : VulkanDescriptorSetLayout(std::move(other)), 
      uniformBufferDescriptionId(std::move(other.uniformBufferDescriptionId))
{
}

VulkanUniformDescriptorSetLayout& VulkanUniformDescriptorSetLayout::operator=(VulkanUniformDescriptorSetLayout&& other) noexcept
{
    VulkanDescriptorSetLayout::operator=(std::move(other));
    uniformBufferDescriptionId = std::move(other.uniformBufferDescriptionId);
    return *this;
}

uint64_t VulkanUniformDescriptorSetLayout::getUniformBufferDescriptionId() const
{
    return uniformBufferDescriptionId;
}

const VkDescriptorSetLayoutBinding& VulkanUniformDescriptorSetLayout::getVkDescriptorSetLayoutBinding() const
{
    return uniformLayoutBinding;
}

COLIBRI_END
