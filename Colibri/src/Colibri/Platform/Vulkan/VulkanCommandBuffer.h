#pragma once

#ifndef _VULKAN_COMMAND_BUFFER_H_
#define _VULKAN_COMMAND_BUFFER_H_

#include "../PlatformLibraries.h"
#include "VulkanCommandBufferPool.fwd.h"
#include "VulkanCommandBufferPool.h"

COLIBRI_BEGIN

class VulkanCommandBuffer
{
private:
	VkCommandBuffer commandBuffer;
	std::weak_ptr<const VulkanCommandBufferPool> origin;

public:
	explicit VulkanCommandBuffer(VkCommandBuffer commandBuffer,
								 const std::shared_ptr<const VulkanCommandBufferPool>& origin) noexcept;
	~VulkanCommandBuffer();
	VulkanCommandBuffer(const VulkanCommandBuffer& other) = delete;
	VulkanCommandBuffer(VulkanCommandBuffer&& other) noexcept;
	VulkanCommandBuffer& operator=(const VulkanCommandBuffer& other) = delete;
	VulkanCommandBuffer& operator=(VulkanCommandBuffer&& other) noexcept;

	/*
	 * If the command buffer was already recorded once, then a call to startRecording will implicitly reset it.
	 * It is not possible to append commands to a buffer at a later time.
	 */
	void startRecording();
	void stopRecording();

	const VkCommandBuffer& getVkCommandBuffer() const;
};

COLIBRI_END

#endif // _VULKAN_COMMAND_BUFFER_H_