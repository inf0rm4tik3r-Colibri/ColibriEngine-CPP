#include "pch.h"
#include "Vulkan.h"
#include "VulkanSwapchainSupportDetails.h"
#include "VulkanCommandBufferPool.h"
#include "VulkanMemory.h"
#include "../../Graphics/Window/Window.h"
#include "../../Graphics/Window/DummyWindow.h"
#include "../../Graphics/Window/WindowBuilder.h"

COLIBRI_BEGIN

std::unique_ptr<VulkanExtensionsInfo> Vulkan::extensionsInfo = nullptr;
std::unique_ptr<VulkanInstance> Vulkan::instance = nullptr;
std::unique_ptr<VulkanDebugCallback> Vulkan::debugCallback = nullptr;
std::unique_ptr<VulkanPhysicalDevices> Vulkan::physicalDevices = nullptr;
const VulkanPhysicalDevice* Vulkan::physicalDevice = nullptr;
std::unique_ptr<VulkanQueueFamilies> Vulkan::physicalDeviceQueueFamilies = nullptr;
std::unique_ptr<VulkanLogicalDevice> Vulkan::logicalDevice = nullptr;

void Vulkan::initialize(const ApplicationInfo& appInfo)
{
	// Get information about available extensions.
	extensionsInfo = std::make_unique<VulkanExtensionsInfo>();
	extensionsInfo->print();

	// Create a Vulkan instance.
	instance = std::make_unique<VulkanInstance>(appInfo, *extensionsInfo);

	// Initialize the debug callback.
	if (instance->isUsingValidationLayers())
	{
		debugCallback = std::make_unique<VulkanDebugCallback>(*instance);
	}

	// Create a dummy window to test surface creation and device capabilities.
	// Automatically destroyed at the end of this scope.
	DummyWindow window;
	VulkanSurface surface(*instance, &window);

	// List physical devices and pickt the best one.
	// We pass our dummy surface to check for presentation support.
	// Each physical device already has its available queue families listed.
	physicalDevices = std::make_unique<VulkanPhysicalDevices>(*instance, surface);
	if (physicalDevices->getDevices().size() == 0)
	{
		throw std::runtime_error("Failed to find a GPU with Vulkan support!");
	}
	physicalDevices->print();

	static std::vector<std::string> requiredDeviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

	pickPhysicalDevice(requiredDeviceExtensions, surface);

	// Create the logical device.
	logicalDevice = std::make_unique<VulkanLogicalDevice>(*physicalDevice, requiredDeviceExtensions);

	// Create the commad pool used for memory transfer operations.
	VulkanMemory::transferCommandBufferPool = std::make_unique<VulkanCommandBufferPool>(physicalDevice->getQueueFamilies().getTransferFamily().value());
}

void Vulkan::terminate()
{
	// As these pointers are static, they do not get destroyed and we have to do that manually!
	VulkanMemory::transferCommandBufferPool.reset();
	logicalDevice.reset();
	physicalDevices.reset();
	debugCallback.reset();
	instance.reset();
	extensionsInfo.reset();
}

void Vulkan::pickPhysicalDevice(const std::vector<std::string>& requiredDeviceExtensions,
								const VulkanSurface& surface)
{
	LOG_INFO << "Picking a physical device...";
	
	// Ordered map to automatically sort candidates by increasing score.
	std::multimap<int, const VulkanPhysicalDevice*> candidates;

	for (const VulkanPhysicalDevice& device : physicalDevices->getDevices())
	{
		// Lets fetch some swap chain support information to better rate the device.
		VulkanSwapchainSupportDetails swapchainSupportDetails(&device, surface);
		swapchainSupportDetails.print();

		int score = device.rateDeviceSuitability(requiredDeviceExtensions, swapchainSupportDetails);
		candidates.insert(std::make_pair(score, &device));
		LOG_INFO << "Device \"" << device.getName() << "\" scored: " << score;
	}

	// Check if the best candidate is suitable at all.
	if (candidates.rbegin()->first > 0)
	{
		physicalDevice = candidates.rbegin()->second;
		LOG_INFO << "Picked device: " << physicalDevice->getName();
		return;
	}

	throw COLIBRI_EXCEPTION("Failed to find a suitable GPU!");
}

COLIBRI_END
