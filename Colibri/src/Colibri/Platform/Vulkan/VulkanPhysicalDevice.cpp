#include "pch.h"
#include "VulkanPhysicalDevice.h"

COLIBRI_BEGIN

VulkanPhysicalDevice::VulkanPhysicalDevice(
	const VkPhysicalDevice& device,
	const VkPhysicalDeviceProperties& properties,
	const VkPhysicalDeviceFeatures& features,
	const VkPhysicalDeviceMemoryProperties& memProperties,
	const std::vector<VkExtensionProperties>& extensionProperties,
	const VulkanSurface& surface)
	: device(device), 
	properties(properties), 
	features(features),
	memProperties(memProperties),
	extensionProperties(extensionProperties),
	queueFamilies(std::make_unique<VulkanQueueFamilies>(*this, surface))
{
}

const char* VulkanPhysicalDevice::getName() const
{
	return properties.deviceName;
}

bool VulkanPhysicalDevice::isDiscreteGPU() const
{
	return properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

bool VulkanPhysicalDevice::areRequiredDeviceExtensionsSupported(const std::vector<std::string>& requiredDeviceExtensions) const {
	std::set<std::string> requiredExtensionsSet(requiredDeviceExtensions.begin(), requiredDeviceExtensions.end());
	for (const auto& extension : extensionProperties)
	{
		requiredExtensionsSet.erase(extension.extensionName);
	}
	if (!requiredExtensionsSet.empty())
	{
		for (const auto& unsupported : requiredExtensionsSet) {
			LOG_INFO << "Unsupported device extensions: " << unsupported;
		}
		return false;
	}
	return true;
}

int VulkanPhysicalDevice::rateDeviceSuitability(const std::vector<std::string>& requiredDeviceExtensions,
												const VulkanSwapchainSupportDetails& swapchainSupportDetails) const
{
	int score = 0;

	// Discrete GPUs have a significant performance advantage!
	if (isDiscreteGPU())
	{
		score += 10'000;
	}

	// Maximum possible size of textures affects graphics quality.
	score += properties.limits.maxImageDimension2D;

	// Application can't function without geometry shaders.
	if (!features.geometryShader)
	{
		LOG_INFO << "Device not suitable: Does not allow geometry shaders.";
		return 0;
	}

	// We cannot work with a GPU which is not able to accept graphics commands.
	if (!queueFamilies->getGraphicsFamily())
	{
		LOG_INFO << "Device not suitable: No graphics queue family available.";
		return 0;
	}

	// We cannot work with a GPU which is not able to present images.
	if (!queueFamilies->hasPresentFamily())
	{
		LOG_INFO << "Device not suitable: No present queue family available.";
		return 0;
	}

	// Some extensions must be supported!
	if (!areRequiredDeviceExtensionsSupported(requiredDeviceExtensions))
	{
		LOG_INFO << "Device not suitable: Not all required device extensions are supported.";
		return 0;
	}

	if (swapchainSupportDetails.getFormats().empty())
	{
		LOG_INFO << "Device not suitable: No swapchain formats available.";
		return 0;
	}

	if (swapchainSupportDetails.getPresentModes().empty())
	{
		LOG_INFO << "Device not suitable: No present modes available.";
		return 0;
	}

	return score;
}

const VkPhysicalDevice& VulkanPhysicalDevice::getVkDevice() const
{
	return device;
}

const VkPhysicalDeviceProperties& VulkanPhysicalDevice::getVkDeviceProperties() const
{
	return properties;
}

const VkPhysicalDeviceFeatures& VulkanPhysicalDevice::getVkDeviceFeatures() const
{
	return features;
}

const VkPhysicalDeviceMemoryProperties& VulkanPhysicalDevice::getVkDeviceMemoryProperties() const
{
	return memProperties;
}

const VulkanQueueFamilies& VulkanPhysicalDevice::getQueueFamilies() const
{
	return *queueFamilies;
}

const std::vector<VkExtensionProperties>& VulkanPhysicalDevice::getExtensionProperties() const
{
	return extensionProperties;
}

void VulkanPhysicalDevice::print() const
{
	LOG_DEBUG << "Physical device: ";
	printProperties();
	printLimits();
	printSparseProperties();
	printFeatures();
	printMemoryProperties();
	printExtensionProperties();
}

void VulkanPhysicalDevice::printProperties() const
{
	LOG_DEBUG << "\t" << "Properties:";
	LOG_DEBUG << "\t\t" << "apiVersion: " << properties.apiVersion;
	LOG_DEBUG << "\t\t" << "deviceID: " << properties.deviceID;
	LOG_DEBUG << "\t\t" << "deviceName: " << properties.deviceName;
	LOG_DEBUG << "\t\t" << "deviceType: " << properties.deviceType;
	LOG_DEBUG << "\t\t" << "deviceType-isDiscreteGPU: " << isDiscreteGPU();
	LOG_DEBUG << "\t\t" << "driverVersion: " << properties.driverVersion;
	LOG_DEBUG << "\t\t" << "pipelineCacheUUID: " << properties.pipelineCacheUUID;
	LOG_DEBUG << "\t\t" << "vendorID: " << properties.vendorID;
}

void VulkanPhysicalDevice::printLimits() const
{
	LOG_DEBUG << "\t" << "Limits:";
	LOG_DEBUG << "\t\t" << "maxImageDimension1D: " << properties.limits.maxImageDimension1D;
	LOG_DEBUG << "\t\t" << "maxImageDimension2D: " << properties.limits.maxImageDimension2D;
	LOG_DEBUG << "\t\t" << "maxImageDimension3D: " << properties.limits.maxImageDimension3D;
	LOG_DEBUG << "\t\t" << "maxImageDimensionCube: " << properties.limits.maxImageDimensionCube;
	LOG_DEBUG << "\t\t" << "maxImageArrayLayers: " << properties.limits.maxImageArrayLayers;
	LOG_DEBUG << "\t\t" << "maxTexelBufferElements: " << properties.limits.maxTexelBufferElements;
	LOG_DEBUG << "\t\t" << "maxUniformBufferRange: " << properties.limits.maxUniformBufferRange;
	LOG_DEBUG << "\t\t" << "maxStorageBufferRange: " << properties.limits.maxStorageBufferRange;
	LOG_DEBUG << "\t\t" << "maxPushConstantsSize: " << properties.limits.maxPushConstantsSize;
	LOG_DEBUG << "\t\t" << "maxMemoryAllocationCount: " << properties.limits.maxMemoryAllocationCount;
	LOG_DEBUG << "\t\t" << "maxSamplerAllocationCount: " << properties.limits.maxSamplerAllocationCount;
	LOG_DEBUG << "\t\t" << "bufferImageGranularity: " << properties.limits.bufferImageGranularity;
	LOG_DEBUG << "\t\t" << "sparseAddressSpaceSize: " << properties.limits.sparseAddressSpaceSize;
	LOG_DEBUG << "\t\t" << "maxBoundDescriptorSets: " << properties.limits.maxBoundDescriptorSets;
	LOG_DEBUG << "\t\t" << "maxPerStageDescriptorSamplers: " << properties.limits.maxPerStageDescriptorSamplers;
	LOG_DEBUG << "\t\t" << "maxPerStageDescriptorUniformBuffers: " << properties.limits.maxPerStageDescriptorUniformBuffers;
	LOG_DEBUG << "\t\t" << "maxPerStageDescriptorStorageBuffers: " << properties.limits.maxPerStageDescriptorStorageBuffers;
	LOG_DEBUG << "\t\t" << "maxPerStageDescriptorSampledImages: " << properties.limits.maxPerStageDescriptorSampledImages;
	LOG_DEBUG << "\t\t" << "maxPerStageDescriptorStorageImages: " << properties.limits.maxPerStageDescriptorStorageImages;
	LOG_DEBUG << "\t\t" << "maxPerStageDescriptorInputAttachments: " << properties.limits.maxPerStageDescriptorInputAttachments;
	LOG_DEBUG << "\t\t" << "maxPerStageResources: " << properties.limits.maxPerStageResources;
	LOG_DEBUG << "\t\t" << "maxDescriptorSetSamplers: " << properties.limits.maxDescriptorSetSamplers;
	LOG_DEBUG << "\t\t" << "maxDescriptorSetUniformBuffers: " << properties.limits.maxDescriptorSetUniformBuffers;
	LOG_DEBUG << "\t\t" << "maxDescriptorSetUniformBuffersDynamic: " << properties.limits.maxDescriptorSetUniformBuffersDynamic;
	LOG_DEBUG << "\t\t" << "maxDescriptorSetStorageBuffers: " << properties.limits.maxDescriptorSetStorageBuffers;
	LOG_DEBUG << "\t\t" << "maxDescriptorSetStorageBuffersDynamic: " << properties.limits.maxDescriptorSetStorageBuffersDynamic;
	LOG_DEBUG << "\t\t" << "maxDescriptorSetSampledImages: " << properties.limits.maxDescriptorSetSampledImages;
	LOG_DEBUG << "\t\t" << "maxDescriptorSetStorageImages: " << properties.limits.maxDescriptorSetStorageImages;
	LOG_DEBUG << "\t\t" << "maxDescriptorSetInputAttachments: " << properties.limits.maxDescriptorSetInputAttachments;
	LOG_DEBUG << "\t\t" << "maxVertexInputAttributes: " << properties.limits.maxVertexInputAttributes;
	LOG_DEBUG << "\t\t" << "maxVertexInputBindings: " << properties.limits.maxVertexInputBindings;
	LOG_DEBUG << "\t\t" << "maxVertexInputAttributeOffset: " << properties.limits.maxVertexInputAttributeOffset;
	LOG_DEBUG << "\t\t" << "maxVertexInputBindingStride: " << properties.limits.maxVertexInputBindingStride;
	LOG_DEBUG << "\t\t" << "maxVertexOutputComponents: " << properties.limits.maxVertexOutputComponents;
	LOG_DEBUG << "\t\t" << "maxTessellationGenerationLevel: " << properties.limits.maxTessellationGenerationLevel;
	LOG_DEBUG << "\t\t" << "maxTessellationPatchSize: " << properties.limits.maxTessellationPatchSize;
	LOG_DEBUG << "\t\t" << "maxTessellationControlPerVertexInputComponents: " << properties.limits.maxTessellationControlPerVertexInputComponents;
	LOG_DEBUG << "\t\t" << "maxTessellationControlPerVertexOutputComponents: " << properties.limits.maxTessellationControlPerVertexOutputComponents;
	LOG_DEBUG << "\t\t" << "maxTessellationControlPerPatchOutputComponents: " << properties.limits.maxTessellationControlPerPatchOutputComponents;
	LOG_DEBUG << "\t\t" << "maxTessellationControlTotalOutputComponents: " << properties.limits.maxTessellationControlTotalOutputComponents;
	LOG_DEBUG << "\t\t" << "maxTessellationEvaluationInputComponents: " << properties.limits.maxTessellationEvaluationInputComponents;
	LOG_DEBUG << "\t\t" << "maxTessellationEvaluationOutputComponents: " << properties.limits.maxTessellationEvaluationOutputComponents;
	LOG_DEBUG << "\t\t" << "maxGeometryShaderInvocations: " << properties.limits.maxGeometryShaderInvocations;
	LOG_DEBUG << "\t\t" << "maxGeometryInputComponents: " << properties.limits.maxGeometryInputComponents;
	LOG_DEBUG << "\t\t" << "maxGeometryOutputComponents: " << properties.limits.maxGeometryOutputComponents;
	LOG_DEBUG << "\t\t" << "maxGeometryOutputVertices: " << properties.limits.maxGeometryOutputVertices;
	LOG_DEBUG << "\t\t" << "maxGeometryTotalOutputComponents: " << properties.limits.maxGeometryTotalOutputComponents;
	LOG_DEBUG << "\t\t" << "maxFragmentInputComponents: " << properties.limits.maxFragmentInputComponents;
	LOG_DEBUG << "\t\t" << "maxFragmentOutputAttachments: " << properties.limits.maxFragmentOutputAttachments;
	LOG_DEBUG << "\t\t" << "maxFragmentDualSrcAttachments: " << properties.limits.maxFragmentDualSrcAttachments;
	LOG_DEBUG << "\t\t" << "maxFragmentCombinedOutputResources: " << properties.limits.maxFragmentCombinedOutputResources;
	LOG_DEBUG << "\t\t" << "maxComputeSharedMemorySize: " << properties.limits.maxComputeSharedMemorySize;
	LOG_DEBUG << "\t\t" << "maxComputeWorkGroupCount[0]: " << properties.limits.maxComputeWorkGroupCount[0];
	LOG_DEBUG << "\t\t" << "maxComputeWorkGroupCount[1]: " << properties.limits.maxComputeWorkGroupCount[1];
	LOG_DEBUG << "\t\t" << "maxComputeWorkGroupCount[2]: " << properties.limits.maxComputeWorkGroupCount[2];
	LOG_DEBUG << "\t\t" << "maxComputeWorkGroupInvocations: " << properties.limits.maxComputeWorkGroupInvocations;
	LOG_DEBUG << "\t\t" << "maxComputeWorkGroupSize[0]: " << properties.limits.maxComputeWorkGroupSize[0];
	LOG_DEBUG << "\t\t" << "maxComputeWorkGroupSize[1]: " << properties.limits.maxComputeWorkGroupSize[1];
	LOG_DEBUG << "\t\t" << "maxComputeWorkGroupSize[2]: " << properties.limits.maxComputeWorkGroupSize[2];
	LOG_DEBUG << "\t\t" << "subPixelPrecisionBits: " << properties.limits.subPixelPrecisionBits;
	LOG_DEBUG << "\t\t" << "subTexelPrecisionBits: " << properties.limits.subTexelPrecisionBits;
	LOG_DEBUG << "\t\t" << "mipmapPrecisionBits: " << properties.limits.mipmapPrecisionBits;
	LOG_DEBUG << "\t\t" << "maxDrawIndexedIndexValue: " << properties.limits.maxDrawIndexedIndexValue;
	LOG_DEBUG << "\t\t" << "maxDrawIndirectCount: " << properties.limits.maxDrawIndirectCount;
	LOG_DEBUG << "\t\t" << "maxSamplerLodBias: " << properties.limits.maxSamplerLodBias;
	LOG_DEBUG << "\t\t" << "maxSamplerAnisotropy: " << properties.limits.maxSamplerAnisotropy;
	LOG_DEBUG << "\t\t" << "maxViewports: " << properties.limits.maxViewports;
	LOG_DEBUG << "\t\t" << "maxViewportDimensions[0]: " << properties.limits.maxViewportDimensions[0];
	LOG_DEBUG << "\t\t" << "maxViewportDimensions[1]: " << properties.limits.maxViewportDimensions[1];
	LOG_DEBUG << "\t\t" << "viewportBoundsRange[0]: " << properties.limits.viewportBoundsRange[0];
	LOG_DEBUG << "\t\t" << "viewportBoundsRange[1]: " << properties.limits.viewportBoundsRange[1];
	LOG_DEBUG << "\t\t" << "viewportSubPixelBits: " << properties.limits.viewportSubPixelBits;
	LOG_DEBUG << "\t\t" << "minMemoryMapAlignment: " << properties.limits.minMemoryMapAlignment;
	LOG_DEBUG << "\t\t" << "minTexelBufferOffsetAlignment: " << properties.limits.minTexelBufferOffsetAlignment;
	LOG_DEBUG << "\t\t" << "minUniformBufferOffsetAlignment: " << properties.limits.minUniformBufferOffsetAlignment;
	LOG_DEBUG << "\t\t" << "minStorageBufferOffsetAlignment: " << properties.limits.minStorageBufferOffsetAlignment;
	LOG_DEBUG << "\t\t" << "minTexelOffset: " << properties.limits.minTexelOffset;
	LOG_DEBUG << "\t\t" << "maxTexelOffset: " << properties.limits.maxTexelOffset;
	LOG_DEBUG << "\t\t" << "minTexelGatherOffset: " << properties.limits.minTexelGatherOffset;
	LOG_DEBUG << "\t\t" << "maxTexelGatherOffset: " << properties.limits.maxTexelGatherOffset;
	LOG_DEBUG << "\t\t" << "minInterpolationOffset: " << properties.limits.minInterpolationOffset;
	LOG_DEBUG << "\t\t" << "maxInterpolationOffset: " << properties.limits.maxInterpolationOffset;
	LOG_DEBUG << "\t\t" << "subPixelInterpolationOffsetBits: " << properties.limits.subPixelInterpolationOffsetBits;
	LOG_DEBUG << "\t\t" << "maxFramebufferWidth: " << properties.limits.maxFramebufferWidth;
	LOG_DEBUG << "\t\t" << "maxFramebufferHeight: " << properties.limits.maxFramebufferHeight;
	LOG_DEBUG << "\t\t" << "maxFramebufferLayers: " << properties.limits.maxFramebufferLayers;
	LOG_DEBUG << "\t\t" << "framebufferColorSampleCounts: " << properties.limits.framebufferColorSampleCounts;
	LOG_DEBUG << "\t\t" << "framebufferDepthSampleCounts: " << properties.limits.framebufferDepthSampleCounts;
	LOG_DEBUG << "\t\t" << "framebufferStencilSampleCounts: " << properties.limits.framebufferStencilSampleCounts;
	LOG_DEBUG << "\t\t" << "framebufferNoAttachmentsSampleCounts: " << properties.limits.framebufferNoAttachmentsSampleCounts;
	LOG_DEBUG << "\t\t" << "maxColorAttachments: " << properties.limits.maxColorAttachments;
	LOG_DEBUG << "\t\t" << "sampledImageColorSampleCounts: " << properties.limits.sampledImageColorSampleCounts;
	LOG_DEBUG << "\t\t" << "sampledImageIntegerSampleCounts: " << properties.limits.sampledImageIntegerSampleCounts;
	LOG_DEBUG << "\t\t" << "sampledImageDepthSampleCounts: " << properties.limits.sampledImageDepthSampleCounts;
	LOG_DEBUG << "\t\t" << "sampledImageStencilSampleCounts: " << properties.limits.sampledImageStencilSampleCounts;
	LOG_DEBUG << "\t\t" << "storageImageSampleCounts: " << properties.limits.storageImageSampleCounts;
	LOG_DEBUG << "\t\t" << "maxSampleMaskWords: " << properties.limits.maxSampleMaskWords;
	LOG_DEBUG << "\t\t" << "timestampComputeAndGraphics: " << properties.limits.timestampComputeAndGraphics;
	LOG_DEBUG << "\t\t" << "timestampPeriod: " << properties.limits.timestampPeriod;
	LOG_DEBUG << "\t\t" << "maxClipDistances: " << properties.limits.maxClipDistances;
	LOG_DEBUG << "\t\t" << "maxCullDistances: " << properties.limits.maxCullDistances;
	LOG_DEBUG << "\t\t" << "maxCombinedClipAndCullDistances: " << properties.limits.maxCombinedClipAndCullDistances;
	LOG_DEBUG << "\t\t" << "discreteQueuePriorities: " << properties.limits.discreteQueuePriorities;
	LOG_DEBUG << "\t\t" << "pointSizeRange[0]: " << properties.limits.pointSizeRange[0];
	LOG_DEBUG << "\t\t" << "pointSizeRange[1]: " << properties.limits.pointSizeRange[1];
	LOG_DEBUG << "\t\t" << "lineWidthRange[0]: " << properties.limits.lineWidthRange[0];
	LOG_DEBUG << "\t\t" << "lineWidthRange[1]: " << properties.limits.lineWidthRange[1];
	LOG_DEBUG << "\t\t" << "pointSizeGranularity: " << properties.limits.pointSizeGranularity;
	LOG_DEBUG << "\t\t" << "lineWidthGranularity: " << properties.limits.lineWidthGranularity;
	LOG_DEBUG << "\t\t" << "strictLines: " << properties.limits.strictLines;
	LOG_DEBUG << "\t\t" << "standardSampleLocations: " << properties.limits.standardSampleLocations;
	LOG_DEBUG << "\t\t" << "optimalBufferCopyOffsetAlignment: " << properties.limits.optimalBufferCopyOffsetAlignment;
	LOG_DEBUG << "\t\t" << "optimalBufferCopyRowPitchAlignment: " << properties.limits.optimalBufferCopyRowPitchAlignment;
	LOG_DEBUG << "\t\t" << "nonCoherentAtomSize: " << properties.limits.nonCoherentAtomSize;
}

void VulkanPhysicalDevice::printSparseProperties() const
{
	LOG_DEBUG << "\t\t" << "Sparse properties:";
	LOG_DEBUG << "\t\t\t" << "limits: " << properties.limits.minTexelBufferOffsetAlignment;
}

void VulkanPhysicalDevice::printFeatures() const
{
	LOG_DEBUG << "\t" << "Features: ";
	LOG_DEBUG << "\t\t" << "alphaToOne: " << features.alphaToOne;
	LOG_DEBUG << "\t\t" << "depthBiasClamp: " << features.depthBiasClamp;
	LOG_DEBUG << "\t\t" << "depthBounds: " << features.depthBounds;
	LOG_DEBUG << "\t\t" << "depthClamp: " << features.depthClamp;
	LOG_DEBUG << "\t\t" << "drawIndirectFirstInstance: " << features.drawIndirectFirstInstance;
	LOG_DEBUG << "\t\t" << "dualSrcBlend: " << features.dualSrcBlend;
	LOG_DEBUG << "\t\t" << "fillModeNonSolid: " << features.fillModeNonSolid;
	LOG_DEBUG << "\t\t" << "fragmentStoresAndAtomics: " << features.fragmentStoresAndAtomics;
	LOG_DEBUG << "\t\t" << "fullDrawIndexUint32: " << features.fullDrawIndexUint32;
	LOG_DEBUG << "\t\t" << "geometryShader: " << features.geometryShader;
	LOG_DEBUG << "\t\t" << "imageCubeArray: " << features.imageCubeArray;
	LOG_DEBUG << "\t\t" << "independentBlend: " << features.independentBlend;
	LOG_DEBUG << "\t\t" << "inheritedQueries: " << features.inheritedQueries;
	LOG_DEBUG << "\t\t" << "largePoints: " << features.largePoints;
	LOG_DEBUG << "\t\t" << "logicOp: " << features.logicOp;
	LOG_DEBUG << "\t\t" << "multiDrawIndirect: " << features.multiDrawIndirect;
	LOG_DEBUG << "\t\t" << "multiViewport: " << features.multiViewport;
	LOG_DEBUG << "\t\t" << "occlusionQueryPrecise: " << features.occlusionQueryPrecise;
	LOG_DEBUG << "\t\t" << "pipelineStatisticsQuery: " << features.pipelineStatisticsQuery;
	LOG_DEBUG << "\t\t" << "robustBufferAccess: " << features.robustBufferAccess;
	LOG_DEBUG << "\t\t" << "samplerAnisotropy: " << features.samplerAnisotropy;
	LOG_DEBUG << "\t\t" << "sampleRateShading: " << features.sampleRateShading;
	LOG_DEBUG << "\t\t" << "shaderClipDistance: " << features.shaderClipDistance;
	LOG_DEBUG << "\t\t" << "shaderCullDistance: " << features.shaderCullDistance;
	LOG_DEBUG << "\t\t" << "shaderFloat64: " << features.shaderFloat64;
	LOG_DEBUG << "\t\t" << "shaderImageGatherExtended: " << features.shaderImageGatherExtended;
	LOG_DEBUG << "\t\t" << "alphaToOneshaderInt16 " << features.shaderInt16;
	LOG_DEBUG << "\t\t" << "shaderInt64: " << features.shaderInt64;
	LOG_DEBUG << "\t\t" << "shaderResourceMinLod: " << features.shaderResourceMinLod;
	LOG_DEBUG << "\t\t" << "shaderResourceResidency: " << features.shaderResourceResidency;
	LOG_DEBUG << "\t\t" << "shaderSampledImageArrayDynamicIndexing: " << features.shaderSampledImageArrayDynamicIndexing;
	LOG_DEBUG << "\t\t" << "shaderStorageBufferArrayDynamicIndexing: " << features.shaderStorageBufferArrayDynamicIndexing;
	LOG_DEBUG << "\t\t" << "shaderStorageImageArrayDynamicIndexing: " << features.shaderStorageImageArrayDynamicIndexing;
	LOG_DEBUG << "\t\t" << "shaderStorageImageExtendedFormats: " << features.shaderStorageImageExtendedFormats;
	LOG_DEBUG << "\t\t" << "shaderStorageImageMultisample: " << features.shaderStorageImageMultisample;
	LOG_DEBUG << "\t\t" << "shaderStorageImageReadWithoutFormat: " << features.shaderStorageImageReadWithoutFormat;
	LOG_DEBUG << "\t\t" << "shaderStorageImageWriteWithoutFormat: " << features.shaderStorageImageWriteWithoutFormat;
	LOG_DEBUG << "\t\t" << "shaderTessellationAndGeometryPointSize: " << features.shaderTessellationAndGeometryPointSize;
	LOG_DEBUG << "\t\t" << "shaderUniformBufferArrayDynamicIndexing: " << features.shaderUniformBufferArrayDynamicIndexing;
	LOG_DEBUG << "\t\t" << "sparseBinding: " << features.sparseBinding;
	LOG_DEBUG << "\t\t" << "sparseResidency2Samples: " << features.sparseResidency2Samples;
	LOG_DEBUG << "\t\t" << "sparseResidency4Samples: " << features.sparseResidency4Samples;
	LOG_DEBUG << "\t\t" << "sparseResidency8Samples: " << features.sparseResidency8Samples;
	LOG_DEBUG << "\t\t" << "sparseResidency16Samples: " << features.sparseResidency16Samples;
	LOG_DEBUG << "\t\t" << "sparseResidencyAliased: " << features.sparseResidencyAliased;
	LOG_DEBUG << "\t\t" << "sparseResidencyBuffer: " << features.sparseResidencyBuffer;
	LOG_DEBUG << "\t\t" << "sparseResidencyImage2D: " << features.sparseResidencyImage2D;
	LOG_DEBUG << "\t\t" << "sparseResidencyImage3D: " << features.sparseResidencyImage3D;
	LOG_DEBUG << "\t\t" << "tessellationShader: " << features.tessellationShader;
	LOG_DEBUG << "\t\t" << "alphaToOnetextureCompressionASTC_LDR " << features.textureCompressionASTC_LDR;
	LOG_DEBUG << "\t\t" << "textureCompressionBC: " << features.textureCompressionBC;
	LOG_DEBUG << "\t\t" << "textureCompressionETC2: " << features.textureCompressionETC2;
	LOG_DEBUG << "\t\t" << "variableMultisampleRate: " << features.variableMultisampleRate;
	LOG_DEBUG << "\t\t" << "vertexPipelineStoresAndAtomics: " << features.vertexPipelineStoresAndAtomics;
	LOG_DEBUG << "\t\t" << "wideLines: " << features.wideLines;
}

void VulkanPhysicalDevice::printMemoryProperties() const
{
	LOG_DEBUG << "\t" << "Memory properties: ";
	LOG_DEBUG << "\t\t" << "memoryTypeCount: " << memProperties.memoryTypeCount;
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
	{
		LOG_DEBUG << "\t\t" << "memoryType - " << i;
		LOG_DEBUG << "\t\t\t" << "heapIndex: " << memProperties.memoryTypes[i].heapIndex;
		LOG_DEBUG << "\t\t\t" << "propertyFlags: " << memProperties.memoryTypes[i].propertyFlags;
	}
	LOG_DEBUG << "\t\t" << "memoryHeapCount: " << memProperties.memoryHeapCount;
	for (uint32_t i = 0; i < memProperties.memoryHeapCount; i++)
	{
		LOG_DEBUG << "\t\t" << "memoryHeap - " << i;
		LOG_DEBUG << "\t\t\t" << "size: " << memProperties.memoryHeaps[i].size;
		LOG_DEBUG << "\t\t\t" << "flags: " << memProperties.memoryHeaps[i].flags;
	}
}

void VulkanPhysicalDevice::printExtensionProperties() const
{
	LOG_DEBUG << "\t" << "Extension properties: ";
	for (const VkExtensionProperties props : extensionProperties)
	{
		LOG_DEBUG << "\t\t" << props.extensionName << " (" << props.specVersion << ")";
	}
}

COLIBRI_END
