#pragma once

#ifndef _VULKAN_SWAPCHAIN_H_
#define _VULKAN_SWAPCHAIN_H_

#include "../PlatformLibraries.h"
#include "VulkanSwapchainSupportDetails.h"
#include "VulkanRenderPass.fwd.h"
#include "VulkanRenderPass.h"

COLIBRI_BEGIN

class VulkanSwapchain
{
private:
	VulkanSwapchainSupportDetails supportDetails;

	VkSurfaceFormatKHR surfaceFormat;
	VkPresentModeKHR presentMode;
	VkExtent2D extent;

	VkSwapchainKHR swapchain;
	std::vector<VkImage> images;
	std::vector<VkImageView> imageViews;
	std::vector<VkFramebuffer> framebuffers;

public:
	VulkanSwapchain(const VulkanPhysicalDevice* device,
					const VulkanSurface& surface);
	~VulkanSwapchain();
	VulkanSwapchain(const VulkanSwapchain& other) = delete;
	VulkanSwapchain(VulkanSwapchain&& other) noexcept;
	VulkanSwapchain& operator=(const VulkanSwapchain& other) = delete;
	VulkanSwapchain& operator=(VulkanSwapchain&& other) noexcept;

	void createFramebuffers(const std::shared_ptr<VulkanRenderPass> renderPass);
	void destroyFramebuffers();

	const VkSwapchainKHR& getVkSwapchain() const { return swapchain; }
	const std::vector<VkImage>& getVkImages() const { return images; }
	const std::vector<VkImageView>& getVkImageViews() const { return imageViews; }
	const std::vector<VkFramebuffer>& getVkFramebuffers() const { return framebuffers; }
	const VkSurfaceFormatKHR& getSurfaceFormat() const { return surfaceFormat; }
	const VkPresentModeKHR& getPresentMode() const { return presentMode; }
	const VkExtent2D& getExtent() const { return extent; }

private:
	const VkSurfaceFormatKHR& pickOptimalSurfaceFormat() const;
	const VkPresentModeKHR& pickOptimalPresentMode() const;
	const VkExtent2D pickOptimalExtent() const;
};

COLIBRI_END

#endif // _VULKAN_SWAPCHAIN_H_
