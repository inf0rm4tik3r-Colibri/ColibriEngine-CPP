#pragma once

#ifndef _VULKAN_EXTENSIONS_INFO_
#define _VULKAN_EXTENSIONS_INFO_

#include "../PlatformLibraries.h"

COLIBRI_BEGIN

class VulkanExtensionsInfo
{

private:
	uint32_t extensionCount;
	std::vector<VkExtensionProperties> supportedExtensions;

public:
	VulkanExtensionsInfo()
	{
		extensionCount = 0;
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
		supportedExtensions = std::vector<VkExtensionProperties>(extensionCount);
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, supportedExtensions.data());
	}

	bool allAvailable(const std::vector<const char*>& requiredExtensions) const
	{
		for (auto required : requiredExtensions) {
			bool found = false;
			for (auto supported : supportedExtensions) {
				if (strcmp(required, supported.extensionName) == 0) {
					found = true;
					break;
				}
			}
			if (!found) {
				LOG_WARN << "Required Vulkan extension is not supported: " << required;
				return false;
			}
		}
		return true;
	}

	void print() const
	{
		LOG_INFO << "Supported Vulkan extensions (" << extensionCount << "): ";
		for (auto supported : supportedExtensions)
		{
			LOG_INFO << "(SpecVersion: " << supported.specVersion << ")\t" << supported.extensionName;
		}
	}
};

COLIBRI_END

#endif // _VULKAN_EXTENSIONS_INFO_
