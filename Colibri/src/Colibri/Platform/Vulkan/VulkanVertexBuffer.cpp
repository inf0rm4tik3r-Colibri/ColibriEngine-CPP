#include "pch.h"
#include "VulkanVertexBuffer.h"
#include "Vulkan.h"
#include "VulkanMemory.h"

COLIBRI_BEGIN

VulkanVertexBuffer::VulkanVertexBuffer(size_t size, size_t sizePerVertex, bool hostVisible)
	: VulkanBuffer(
		size* sizePerVertex, 
		hostVisible ? VK_BUFFER_USAGE_VERTEX_BUFFER_BIT : VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		hostVisible ? VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT : VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		hostVisible
	)
{
}

VulkanVertexBuffer::~VulkanVertexBuffer()
{
}

void VulkanVertexBuffer::fill(const std::initializer_list<ColoredVertex>& vertices)
{
	VulkanBuffer::fill<ColoredVertex>(vertices);
}

COLIBRI_END
