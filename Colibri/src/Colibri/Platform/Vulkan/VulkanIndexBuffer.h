#pragma once

#ifndef _VULKAN_INDEX_BUFFER_H_
#define _VULKAN_INDEX_BUFFER_H_

#include "../PlatformLibraries.h"
#include "../../Graphics/Renderer/IndexBuffer.h"
#include "../../Graphics/Model/Vertex.h"
#include "VulkanBuffer.h"

COLIBRI_BEGIN

class VulkanIndexBuffer : public VulkanBuffer, public IndexBuffer
{
public:
	VulkanIndexBuffer(size_t size, size_t sizePerIndex, bool hostVisible = false);
	~VulkanIndexBuffer();

	void fill(const std::initializer_list<uint32_t>& indices) override;
};

COLIBRI_END

#endif // _VULKAN_INDEX_BUFFER_H_
