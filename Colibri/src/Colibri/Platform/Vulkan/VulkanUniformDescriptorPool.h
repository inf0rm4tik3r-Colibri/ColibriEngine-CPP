#pragma once

#ifndef _VULKAN_UNIFORM_DESCRIPTOR_POOL_H_
#define _VULKAN_UNIFORM_DESCRIPTOR_POOL_H_

#include "../PlatformLibraries.h"
#include "VulkanDescriptorPool.h"

COLIBRI_BEGIN

class VulkanUniformDescriptorPool : public VulkanDescriptorPool
{
public:
	VulkanUniformDescriptorPool(size_t size);
	~VulkanUniformDescriptorPool();
};

COLIBRI_END

#endif // _VULKAN_UNIFORM_DESCRIPTOR_POOL_H_
