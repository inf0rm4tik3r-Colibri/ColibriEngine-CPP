#include "pch.h"
#include "VulkanLogicalDevice.h"

COLIBRI_BEGIN

VulkanLogicalDevice::VulkanLogicalDevice(const VulkanPhysicalDevice& physicalDevice, const std::vector<std::string>& requiredDeviceExtensions)
{
	LOG_INFO << "Creating a logical device: ";

	if (!physicalDevice.getQueueFamilies().hasGraphicsFamily())
	{
		throw COLIBRI_EXCEPTION("Graphics family is not present!");
	}
	if (!physicalDevice.getQueueFamilies().hasTransferFamily())
	{
		throw COLIBRI_EXCEPTION("Transfer family is not present!");
	}
	if (!physicalDevice.getQueueFamilies().hasPresentFamily())
	{
		throw COLIBRI_EXCEPTION("Present family is not present!");
	}

	std::set<uint32_t> uniqueQueueFamilies = {
		physicalDevice.getQueueFamilies().getGraphicsFamily().value(),
		physicalDevice.getQueueFamilies().getPresentFamily().value(),
		physicalDevice.getQueueFamilies().getPresentFamily().value(),
	};

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;

	float queuePriority = 1.0f;
	for (uint32_t queueFamily : uniqueQueueFamilies) {
		VkDeviceQueueCreateInfo queueCreateInfo {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	// The device features we want to use. Check with physicalDevice.
	VkPhysicalDeviceFeatures deviceFeatures {};

	VkDeviceCreateInfo createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pEnabledFeatures = &deviceFeatures;
	// Validation layers are configured through the Vulkan instance!

	// Request all the required device extensions. 
	// We would not have reached this point if the given physical device would not support them.
	createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredDeviceExtensions.size());
	std::vector<const char*> cReqNames;
	for (const auto& name : requiredDeviceExtensions)
	{
		cReqNames.push_back(name.c_str());
	}
	createInfo.ppEnabledExtensionNames = cReqNames.data();
	LOG_INFO << "Enabled extensions: " << cReqNames;

	// Now create the logical device.
	if (vkCreateDevice(physicalDevice.getVkDevice(), &createInfo, nullptr, &device) != VK_SUCCESS) {
		throw std::runtime_error("Failed to create a logical device!");
	}

	LOG_INFO << "Logical device created.";

	// And get handles to the queues we specified before.
	// NOTE: Device queues are automatically freed up with the logical device, so they must not be destroyed manually.
	vkGetDeviceQueue(device, physicalDevice.getQueueFamilies().getGraphicsFamily().value(), 0, &graphicsQueue);
	vkGetDeviceQueue(device, physicalDevice.getQueueFamilies().getTransferFamily().value(), 0, &transferQueue);
	vkGetDeviceQueue(device, physicalDevice.getQueueFamilies().getPresentFamily().value(), 0, &presentQueue);

	LOG_INFO << "Device queues acquired.";
}

VulkanLogicalDevice::~VulkanLogicalDevice()
{
	vkDestroyDevice(device, nullptr);
	LOG_INFO << "Destroyed Vulkan logical device.";
}

const VkDevice& VulkanLogicalDevice::getVkDevice() const
{
	return device;
}

const VkQueue& VulkanLogicalDevice::getGraphicsQueue() const
{
	return graphicsQueue;
}

const VkQueue& VulkanLogicalDevice::getTransferQueue() const
{
	return transferQueue;
}

const VkQueue& VulkanLogicalDevice::getPresentQueue() const
{
	return presentQueue;
}

COLIBRI_END
