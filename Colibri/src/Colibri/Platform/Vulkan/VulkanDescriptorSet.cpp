#include "pch.h"
#include "VulkanDescriptorSet.h"

COLIBRI_BEGIN

VulkanDescriptorSet::VulkanDescriptorSet(VkDescriptorSet descriptorSet)
	: descriptorSet(descriptorSet)
{
}

VulkanDescriptorSet::~VulkanDescriptorSet()
{
}

const VkDescriptorSet& VulkanDescriptorSet::getVkDescriptorSet() const
{
	return descriptorSet;
}

COLIBRI_END
