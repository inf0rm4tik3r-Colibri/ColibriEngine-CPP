#include "pch.h"
#include "VulkanBuffer.h"
#include "VulkanMemory.h"

COLIBRI_BEGIN

VulkanBuffer::VulkanBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, bool mappable)
	: mappable(mappable)
{
	bufferCreateInfo = VkBufferCreateInfo {};
	bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;

	// The size of the buffer in bytes.
	bufferCreateInfo.size = size;

	// It is possible to specify multiple purposes using a bitwise or.
	bufferCreateInfo.usage = usage;

	// Buffers can be owned by a specific queue family or be shared between multiple at the same time.
	// The buffer will only be used from the graphics queue, so we stick to exclusive access.
	bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE; // TODO: 
	/*
	
	The buffer copy command requires a queue family that supports transfer operations, which is indicated using VK_QUEUE_TRANSFER_BIT.
	The good news is that any queue family with VK_QUEUE_GRAPHICS_BIT or VK_QUEUE_COMPUTE_BIT capabilities already implicitly support VK_QUEUE_TRANSFER_BIT operations.
	The implementation is not required to explicitly list it in queueFlags in those cases.

	If you like a challenge, then you can still try to use a different queue family specifically for transfer operations. It will require you to make the following modifications to your program:

	Modify QueueFamilyIndices and findQueueFamilies to explicitly look for a queue family with the VK_QUEUE_TRANSFER_BIT bit, but not the VK_QUEUE_GRAPHICS_BIT.
	Modify createLogicalDevice to request a handle to the transfer queue
	Create a second command pool for command buffers that are submitted on the transfer queue family
	Change the sharingMode of resources to be VK_SHARING_MODE_CONCURRENT and specify both the graphics and transfer queue families
	Submit any transfer commands like vkCmdCopyBuffer (which we'll be using in this chapter) to the transfer queue instead of the graphics queue
	
	*/

	// To configure sparse memory.
	bufferCreateInfo.flags = 0;

	if (vkCreateBuffer(Vulkan::getLogicalDevice()->getVkDevice(), &bufferCreateInfo, nullptr, &buffer) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to create a buffer!");
	}

	LOG_INFO << "Created Vulkan buffer.";

	/*
	 * The VkMemoryRequirements struct has three fields:
	 * - size: The size of the required amount of memory in bytes, may differ from bufferInfo.size.
	 * - alignment: The offset in bytes where the buffer begins in the allocated region of memory, depends on bufferInfo.usage and bufferInfo.flags.
	 * - memoryTypeBits: Bit field of the memory types that are suitable for the buffer.
	 */
	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(Vulkan::getLogicalDevice()->getVkDevice(), buffer, &memRequirements);

	VkMemoryAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = VulkanMemory::findMemoryType(memRequirements.memoryTypeBits, properties);

	if (vkAllocateMemory(Vulkan::getLogicalDevice()->getVkDevice(), &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to allocate buffer memory!");
	}

	LOG_INFO << "Acquired buffer memory.";

	// The fourth parameter is the offset within the region of memory.
	// If the offset is non-zero, then it is required to be divisible by memRequirements.alignment.
	vkBindBufferMemory(Vulkan::getLogicalDevice()->getVkDevice(), buffer, bufferMemory, 0);

	LOG_INFO << "Bound the acquired memory to the buffer.";
}

VulkanBuffer::~VulkanBuffer()
{
	vkDestroyBuffer(Vulkan::getLogicalDevice()->getVkDevice(), buffer, nullptr);
	LOG_INFO << "Destroyed vulkan buffer.";

	vkFreeMemory(Vulkan::getLogicalDevice()->getVkDevice(), bufferMemory, nullptr);
	LOG_INFO << "Freed vulkan buffer memory.";
}

void VulkanBuffer::copyTo(VulkanBuffer& dstBuffer, VkDeviceSize size) const
{
	VkCommandBufferAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = VulkanMemory::transferCommandBufferPool->getVkCommandPool();
	allocInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(Vulkan::getLogicalDevice()->getVkDevice(), &allocInfo, &commandBuffer);

	VkCommandBufferBeginInfo beginInfo {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	VkBufferCopy copyRegion {};
	copyRegion.srcOffset = 0; // Optional
	copyRegion.dstOffset = 0; // Optional
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, buffer, dstBuffer.getVkBuffer(), 1, &copyRegion);

	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	vkQueueSubmit(Vulkan::getLogicalDevice()->getTransferQueue(), 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(Vulkan::getLogicalDevice()->getTransferQueue());

	vkFreeCommandBuffers(Vulkan::getLogicalDevice()->getVkDevice(), VulkanMemory::transferCommandBufferPool->getVkCommandPool(), 1, &commandBuffer);
}

void VulkanBuffer::mapMemory(void** data)
{
	assert(mappable);
	vkMapMemory(Vulkan::getLogicalDevice()->getVkDevice(), bufferMemory, 0, bufferCreateInfo.size, 0, data);
}

void VulkanBuffer::unmapMemory()
{
	assert(mappable);
	vkUnmapMemory(Vulkan::getLogicalDevice()->getVkDevice(), bufferMemory);
}

const size_t VulkanBuffer::getSizeInBytes() const
{
	return bufferCreateInfo.size;
}

const VkBuffer VulkanBuffer::getVkBuffer() const
{
	return buffer;
}

COLIBRI_END
