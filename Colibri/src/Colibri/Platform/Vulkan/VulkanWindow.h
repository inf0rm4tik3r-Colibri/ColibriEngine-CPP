#pragma once

#ifndef _VULKAN_WINDOW_H_
#define _VULKAN_WINDOW_H_

#include "../PlatformLibraries.h"
#include "../../Graphics/Window/Window.h"
#include "../../Observer/Pattern.h"
#include "VulkanInstance.h"
#include "VulkanSurface.h"
#include "VulkanSwapchain.h"

COLIBRI_BEGIN

class VulkanWindow : public Window
{
private:
	VulkanSurface surface; // Order is important!
	VulkanSwapchain swapchain;
	bool swapchainCreated = false;

public:
	Subject<> swapchainRecreationFinished;

public:
	VulkanWindow(unsigned int width, unsigned int height, const std::string& title) noexcept;
	~VulkanWindow();
	VulkanWindow(const VulkanWindow& other) noexcept = delete;
	VulkanWindow(VulkanWindow&& other) noexcept;
	VulkanWindow& operator=(const VulkanWindow& other) = delete;
	VulkanWindow& operator=(VulkanWindow&& other) = delete;

	// void onFramebufferResize(int width, int height) override;

	void recreateSwapchain();

	const VulkanSurface& getSurface() const { return surface; }
	const VulkanSwapchain& getSwapchain() const { return swapchain; }
	VulkanSwapchain& getSwapchain() { return swapchain; }
};

COLIBRI_END

#endif // _VULKAN_WINDOW_H_
