#pragma once

#ifndef _VULKAN_MEMORY_H_
#define _VULKAN_MEMORY_H_

#include "../PlatformLibraries.h"
#include "VulkanCommandBufferPool.h"

COLIBRI_BEGIN

class VulkanMemory
{
public:
	static std::unique_ptr<VulkanCommandBufferPool> transferCommandBufferPool;

	static uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

};

COLIBRI_END

#endif // _VULKAN_MEMORY_H_
