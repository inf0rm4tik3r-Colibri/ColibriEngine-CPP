#include "pch.h"
#include "VulkanVertexDescription.h"

COLIBRI_BEGIN

inline VkFormat VulkanVertexDescription::vertexFormatToVkFormat(VertexAttributeFormat format)
{
	switch (format)
	{
		case VertexAttributeFormat::R32G32_SFLOAT:
			return VK_FORMAT_R32G32_SFLOAT;
		case VertexAttributeFormat::R32G32B32_SFLOAT:
			return VK_FORMAT_R32G32B32_SFLOAT;
		default:
			throw COLIBRI_EXCEPTION("Unable to convert format");
	}
}

VulkanVertexDescription::VulkanVertexDescription(const VertexInputDescription& vertexInputDescription) noexcept
{
	for (const VertexInputBindingDescription& desc : vertexInputDescription.bindingDescriptions)
	{
		VkVertexInputBindingDescription bindingDescription {};
		// The binding parameter specifies the index of the binding in the array of bindings.
		bindingDescription.binding = desc.binding;
		// The stride parameter specifies the number of bytes from one entry to the next.
		bindingDescription.stride = desc.stride;
		/*
		* VK_VERTEX_INPUT_RATE_VERTEX: Move to the next data entry after each vertex.
		* VK_VERTEX_INPUT_RATE_INSTANCE: Move to the next data entry after each instance. -> when using instanced rendering
		*/
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		bindingDescriptions.push_back(bindingDescription);
	}

	for (const VertexInputAttributeDescription& desc : vertexInputDescription.attributeDescriptions)
	{
		VkVertexInputAttributeDescription attributeDescription {};
		// The binding parameter tells Vulkan from which binding the per - vertex data comes.
		attributeDescription.binding = desc.binding;
		// The location parameter references the location directive of the input in the vertex shader.
		attributeDescription.location = desc.location;
		// The format parameter describes the type of data for the attribute.
		attributeDescription.format = VulkanVertexDescription::vertexFormatToVkFormat(desc.format);
		// The format parameter implicitly defines the byte size of attribute data 
		// and the offset parameter specifies the number of bytes since the start of the per-vertex data to read from.
		attributeDescription.offset = desc.offset;

		attributeDescriptions.push_back(attributeDescription);
	}
}

VulkanVertexDescription::~VulkanVertexDescription() noexcept
{
}

const std::vector<VkVertexInputBindingDescription>& VulkanVertexDescription::getVkBindingDescriptions() const
{
	return bindingDescriptions;
}

const std::vector<VkVertexInputAttributeDescription>& VulkanVertexDescription::getVkAttributeDescriptions() const
{
	return attributeDescriptions;
}

COLIBRI_END
