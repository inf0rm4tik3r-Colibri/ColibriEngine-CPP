#include "pch.h"
#include "VulkanCommandBuffer.h"
#include "Vulkan.h"

COLIBRI_BEGIN

VulkanCommandBuffer::VulkanCommandBuffer(VkCommandBuffer commandBuffer,
										 const std::shared_ptr<const VulkanCommandBufferPool>& origin) noexcept
	: commandBuffer(commandBuffer),
	  origin(origin)
{
	LOG_INFO << "Create Vulkan command buffer: " << reinterpret_cast<int64_t>(commandBuffer);
}

VulkanCommandBuffer::~VulkanCommandBuffer()
{
	if (commandBuffer != nullptr)
	{
		if (auto pool = origin.lock())
		{
			vkFreeCommandBuffers(Vulkan::getLogicalDevice()->getVkDevice(), pool->getVkCommandPool(), 1, &commandBuffer);
			LOG_INFO << "Freed Vulkan command buffer.";
		}
		else
		{
			LOG_INFO << "Vulkan command buffer got out of scope at a time where its origin-pool was already destroyed.";
		}
	}
}

VulkanCommandBuffer::VulkanCommandBuffer(VulkanCommandBuffer&& other) noexcept
	: commandBuffer(std::move(other.commandBuffer)),
	origin(std::move(other.origin))
{
	other.commandBuffer = nullptr;
}

VulkanCommandBuffer& VulkanCommandBuffer::operator=(VulkanCommandBuffer&& other) noexcept
{
	commandBuffer = std::move(other.commandBuffer);
	origin = std::move(other.origin);
	other.commandBuffer = nullptr;
	return *this;
}

void VulkanCommandBuffer::startRecording()
{
	VkCommandBufferBeginInfo beginInfo {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	/*
	 * The flags parameter specifies how we're going to use the command buffer.
	 * VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT: 
	 *	Specifies that each recording of the command buffer will only be submitted once, and the command buffer will be reset and recorded again between each submission.
     * VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT:
	 *	Specifies that a secondary command buffer is considered to be entirely inside a render pass. If this is a primary command buffer, then this bit is ignored.
     * VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT:
	 *	 Specifies that a command buffer can be resubmitted to a queue while it is in the pending state, and recorded into multiple primary command buffers.
     */
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT; // 0 = Optional
	beginInfo.pInheritanceInfo = nullptr; // Optional, only relevant for secondary command buffers.

	if (vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS)
	{
		throw COLIBRI_EXCEPTION("Failed to begin recording command buffer!");
	}
	// LOG_INFO << "Started command buffer recording.";
}

void VulkanCommandBuffer::stopRecording()
{
	if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
	{
		throw COLIBRI_EXCEPTION("failed to record command buffer!");
	}
	// LOG_INFO << "Stoped command buffer recording.";
}

const VkCommandBuffer& VulkanCommandBuffer::getVkCommandBuffer() const
{
	return commandBuffer;
}

COLIBRI_END
