#pragma once

#ifndef _VULKAN_DESCRIPTOR_SET_H_
#define _VULKAN_DESCRIPTOR_SET_H_

#include "../PlatformLibraries.h"

COLIBRI_BEGIN

/*
 * Descriptor sets must not be cleaned up explicitely. They are freed whenever their pool is destroyed.
 */
class VulkanDescriptorSet
{
private:
	VkDescriptorSet descriptorSet;

public:
	VulkanDescriptorSet(VkDescriptorSet descriptorSet);
	~VulkanDescriptorSet();

	const VkDescriptorSet& getVkDescriptorSet() const;
};

COLIBRI_END

#endif // _VULKAN_DESCRIPTOR_SET_H_