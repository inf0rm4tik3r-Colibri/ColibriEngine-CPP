#include "pch.h"
#include "VulkanRenderPass.h"

COLIBRI_BEGIN

// ----
// TODO: Content for VkRenderPassCreateInfo must be dynamic!
// -----

VulkanRenderPass::VulkanRenderPass(const VulkanSwapchain& swapchain)
{
	// Define one color attachment. (For the images from the swap chain!)
	VkAttachmentDescription colorAttachment {};
	colorAttachment.format = swapchain.getSurfaceFormat().format;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT; // Because no multisampling!

	// The loadOp and storeOp apply to color and depth data!												 
	/*
	 * VK_ATTACHMENT_LOAD_OP_LOAD: Preserve the existing contents of the attachment.
	 * VK_ATTACHMENT_LOAD_OP_CLEAR: Clear the values to a constant at the start.
	 * VK_ATTACHMENT_LOAD_OP_DONT_CARE: Existing contents are undefined; We don't care about them.
	 */
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	/*
	 * VK_ATTACHMENT_STORE_OP_STORE: Rendered contents will be stored in memory and can be read later.
	 * VK_ATTACHMENT_STORE_OP_DONT_CARE: Contents of the framebuffer will be undefined after the rendering operation.
	 */
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

	// stencilLoadOp / stencilStoreOp apply to stencil data!
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

	/*
	* Common layout types:
	* VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL: Images used as color attachment.
	* VK_IMAGE_LAYOUT_PRESENT_SRC_KHR: Images to be presented in the swap chain.
	* VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL: Images to be used as destination for a memory copy operation.
	*/
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; // LAYOUT_UNDEFINED -> Content may not be preserved. But we clar anyway.
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference colorAttachmentRef {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	/*
	 * Other possible attachments:
	 * pInputAttachments: Attachments that are read from a shader.
	 * pResolveAttachments: Attachments used for multisampling color attachments.
	 * pDepthStencilAttachment: Attachment for depth and stencil data.
	 * pPreserveAttachments: Attachments that are not used by this subpass, but for which the data must be preserved.
	 */
	subpass.pColorAttachments = &colorAttachmentRef;

	VkRenderPassCreateInfo renderPassInfo {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = 1;
	renderPassInfo.pAttachments = &colorAttachment;
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;

	VkSubpassDependency dependency {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;

	if (vkCreateRenderPass(Vulkan::getLogicalDevice()->getVkDevice(), &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to create render pass!");
	}

	LOG_INFO << "Created Vulkan render pass.";
}

VulkanRenderPass::~VulkanRenderPass()
{
	if (renderPass != nullptr)
	{
		vkDestroyRenderPass(Vulkan::getLogicalDevice()->getVkDevice(), renderPass, nullptr);
		renderPass = nullptr;
		LOG_INFO << "Destroyed Vulkan render pass.";
	}
}

VulkanRenderPass::VulkanRenderPass(VulkanRenderPass&& other) noexcept
	: renderPass(std::move(other.renderPass))
{
	other.renderPass = nullptr;
}

VulkanRenderPass& VulkanRenderPass::operator=(VulkanRenderPass&& other) noexcept
{
	renderPass = std::move(other.renderPass);
	other.renderPass = nullptr;
	return *this;
}

bool VulkanRenderPass::isDegenerate() const
{
	return renderPass == nullptr;
}

const VkRenderPass VulkanRenderPass::getVkRenderPass() const
{
	return renderPass;
}

COLIBRI_END
