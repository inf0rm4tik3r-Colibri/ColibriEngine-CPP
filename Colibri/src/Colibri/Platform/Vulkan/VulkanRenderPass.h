#pragma once

#ifndef _VULKAN_RENDER_PASS_H_
#define _VULKAN_RENDER_PASS_H_

#include "../PlatformLibraries.h"
#include "Vulkan.h"
#include "VulkanSwapchain.fwd.h"
#include "VulkanSwapchain.h"

COLIBRI_BEGIN

class VulkanRenderPass
{
private:
	VkRenderPass renderPass;

public:
	VulkanRenderPass(const VulkanSwapchain& swapchain);
	~VulkanRenderPass();
	VulkanRenderPass(const VulkanRenderPass& other) = delete;
	VulkanRenderPass(VulkanRenderPass&& other) noexcept;
	VulkanRenderPass& operator=(const VulkanRenderPass& other) = delete;
	VulkanRenderPass& operator=(VulkanRenderPass&& other) noexcept;

	bool isDegenerate() const;
	const VkRenderPass getVkRenderPass() const;
};

COLIBRI_END

#endif // _VULKAN_RENDER_PASS_H_
