#pragma once

#ifndef _VULKAN_COMMAND_POOL_H_
#define _VULKAN_COMMAND_POOL_H_

#include "../PlatformLibraries.h"
#include "VulkanCommandBuffer.fwd.h"
#include "VulkanCommandBuffer.h"

COLIBRI_BEGIN

class VulkanCommandBufferPool : public std::enable_shared_from_this<VulkanCommandBufferPool>
{
private:
	VkCommandPool commandPool;

public:
	VulkanCommandBufferPool(uint32_t queueFamilyIndex);
	~VulkanCommandBufferPool();
	VulkanCommandBufferPool(const VulkanCommandBufferPool& other) = delete;
	VulkanCommandBufferPool(VulkanCommandBufferPool&& other) noexcept;
	VulkanCommandBufferPool& operator=(const VulkanCommandBufferPool& other) = delete;
	VulkanCommandBufferPool& operator=(VulkanCommandBufferPool&& other) noexcept;

	std::vector<VulkanCommandBuffer> createCommandBuffers(uint32_t size) const;
	const VkCommandPool& getVkCommandPool() const;
};

COLIBRI_END

#endif // _VULKAN_COMMAND_POOL_H_