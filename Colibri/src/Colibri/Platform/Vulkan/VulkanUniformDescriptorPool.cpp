#include "pch.h"
#include "VulkanUniformDescriptorPool.h"

COLIBRI_BEGIN

VulkanUniformDescriptorPool::VulkanUniformDescriptorPool(size_t size)
	: VulkanDescriptorPool(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, size)
{
}

VulkanUniformDescriptorPool::~VulkanUniformDescriptorPool()
{
}

COLIBRI_END
