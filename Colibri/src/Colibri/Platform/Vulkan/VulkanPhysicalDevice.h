#pragma once

#ifndef _VULKAN_PHYSICAL_DEVICE_H_
#define _VULKAN_PHYSICAL_DEVICE_H_

#include "../PlatformLibraries.h"
#include "VulkanInstance.h"
#include "VulkanSurface.h"
#include "VulkanPhysicalDevice.fwd.h"
#include "VulkanQueueFamilies.fwd.h"
#include "VulkanQueueFamilies.h"
#include "VulkanSwapchainSupportDetails.fwd.h"
#include "VulkanSwapchainSupportDetails.h"

COLIBRI_BEGIN

class VulkanPhysicalDevice
{

private:
	VkPhysicalDevice device;
	VkPhysicalDeviceProperties properties;
	VkPhysicalDeviceFeatures features; 
	VkPhysicalDeviceMemoryProperties memProperties;
	std::vector<VkExtensionProperties> extensionProperties;
	std::unique_ptr<VulkanQueueFamilies> queueFamilies;

public:
	VulkanPhysicalDevice(const VkPhysicalDevice& device,
						 const VkPhysicalDeviceProperties& properties,
						 const VkPhysicalDeviceFeatures& features,
						 const VkPhysicalDeviceMemoryProperties& memProperties,
						 const std::vector<VkExtensionProperties>& extensionProperties,
						 const VulkanSurface& surface);

	const char* getName() const;
	bool isDiscreteGPU() const;
	bool areRequiredDeviceExtensionsSupported(const std::vector<std::string>& requiredDeviceExtensions) const;
	int rateDeviceSuitability(const std::vector<std::string>& requiredExtensions,
							  const VulkanSwapchainSupportDetails& swapchainSupportDetails) const;

	const VkPhysicalDevice& getVkDevice() const;
	const VkPhysicalDeviceProperties& getVkDeviceProperties() const;
	const VkPhysicalDeviceFeatures& getVkDeviceFeatures() const;
	const VkPhysicalDeviceMemoryProperties& getVkDeviceMemoryProperties() const;
	const VulkanQueueFamilies& getQueueFamilies() const;
	const std::vector<VkExtensionProperties>& getExtensionProperties() const;

	void print() const;

private:
	void printProperties() const;
	void printLimits() const;
	void printSparseProperties() const;
	void printFeatures() const;
	void printMemoryProperties() const;
	void printExtensionProperties() const;
};

COLIBRI_END

#endif // _VULKAN_PHYSICAL_DEVICE_H_
