#pragma once

#ifndef _VULKAN_LOGICAL_DEVICE_H_
#define _VULKAN_LOGICAL_DEVICE_H_

#include "../PlatformLibraries.h"
#include "VulkanPhysicalDevice.h"

COLIBRI_BEGIN

class VulkanLogicalDevice
{
private:
	VkDevice device;
	VkQueue graphicsQueue;
	VkQueue transferQueue;
	VkQueue presentQueue;

public:
	VulkanLogicalDevice(const VulkanPhysicalDevice& physicalDevice, const std::vector<std::string>& requiredDeviceExtensions);
	~VulkanLogicalDevice();

	const VkDevice& getVkDevice() const;
	const VkQueue& getGraphicsQueue() const;
	const VkQueue& getTransferQueue() const;
	const VkQueue& getPresentQueue() const;
};

COLIBRI_END

#endif // _VULKAN_LOGICAL_DEVICE_H_
