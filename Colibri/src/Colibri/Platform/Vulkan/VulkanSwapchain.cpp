#include "pch.h"
#include "../../Core/Engine.h"
#include "VulkanSwapchain.h"
#include "Vulkan.h"

COLIBRI_BEGIN

VulkanSwapchain::VulkanSwapchain(const VulkanPhysicalDevice* device,
								 const VulkanSurface& surface)
	: supportDetails(device, surface)
{
	LOG_INFO << "Creating Vulkan swapchain.";
	supportDetails.print();

	LOG_INFO << "Picking best format, presentation mode and extent:";
	surfaceFormat = pickOptimalSurfaceFormat();
	LOG_INFO << "Chose format: " << VulkanSurface::formatToString(surfaceFormat.format) << " (" << surfaceFormat.format << ")";
	LOG_INFO << "Chose colorSpace: " << VulkanSurface::colorSpaceToString(surfaceFormat.colorSpace) << " (" << surfaceFormat.colorSpace << ")";
	presentMode = pickOptimalPresentMode();
	LOG_INFO << "Chose present mode: " << VulkanSurface::presentModeToString(presentMode) << " (" << presentMode << ")";
	extent = pickOptimalExtent();
	LOG_INFO << "Chose extent: " << Vec2i(extent.width, extent.height);

	uint32_t minImageCount = supportDetails.getCapabilities().minImageCount;
	uint32_t maxImageCount = supportDetails.getCapabilities().maxImageCount;
	// Prefer 3 images, else one over min count to reduce the time we wait on the driver.
	// Result is always > minImageCount, so no check needed.
	uint32_t imageCount = std::max(minImageCount + 1, 3u);
	// Max image count can be 0. Then there can be as many images as we want and we dont need to do a range check.
	if (maxImageCount > 0 && imageCount > maxImageCount) {
		imageCount = maxImageCount;
	}
	LOG_INFO << "Chose image count: " << imageCount;

	// Check that the device can really work with the given surface:
	VkBool32 supported;
	vkGetPhysicalDeviceSurfaceSupportKHR(device->getVkDevice(), 
										 device->getQueueFamilies().getPresentFamily().value(), 
										 surface.getVkSurface(), 
										 &supported);
	if (supported == VK_FALSE)
	{
		throw UNSUPPORTED_OPERATION_EXCEPTION("Physical device does not support presentation to the given surface!");
	}

	// Finally create the swapchain :)

	VkSwapchainCreateInfoKHR createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	// The surface to which this swapchain should be tied.
	createInfo.surface = surface.getVkSurface();
	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	// If stereoscopic 3D rendering is desired, this might be changed to 2!
	createInfo.imageArrayLayers = 1;
	// We will render directly to the images. Use VK_IMAGE_USAGE_TRANSFER_DST_BIT for offscreen images where memory copy is used.
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	
	/*
	VK_SHARING_MODE_EXCLUSIVE: An image is owned by one queue family at a time and ownership must be explicitly transfered before using it in another queue family. This option offers the best performance.
	VK_SHARING_MODE_CONCURRENT: Images can be used across multiple queue families without explicit ownership transfers.
	*/
	uint32_t queueFamilyIndices[] = { 
		device->getQueueFamilies().getGraphicsFamily().value(), 
		device->getQueueFamilies().getPresentFamily().value()
	};
	if (device->getQueueFamilies().getGraphicsFamily().value() != device->getQueueFamilies().getPresentFamily().value()) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
		LOG_INFO << "Using: VK_SHARING_MODE_CONCURRENT";
	}
	else {
		// Using exclusice mode with differen queue families would result in the need of manual ownership transfers...
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0; // Optional
		createInfo.pQueueFamilyIndices = nullptr; // Optional
		LOG_INFO << "Using: VK_SHARING_MODE_CONCURRENT";
	}
	// Spacial transformations are possible. Like horizontal flip or 90 degree rotation.
	// currentTransform is default and means "pls nothing special".
	createInfo.preTransform = supportDetails.getCapabilities().currentTransform;
	// Should be used for blending with other windows in the window system.
	// VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR means "simply ignore alpha".
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = presentMode;
	// TRUE = We do not care about color of obsured pixels. Possibly obscured by another window.
	createInfo.clipped = VK_TRUE;
	// Must be set when the swapchain gets recreated! Later...
	createInfo.oldSwapchain = VK_NULL_HANDLE;
	
	if (vkCreateSwapchainKHR(Vulkan::getLogicalDevice()->getVkDevice(), &createInfo, nullptr, &swapchain) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to create swapchain!");
	}
	LOG_INFO << "Created Vulkan swapchain.";

	// The implementation is allowed to create a swapchain with more images then we specified.
	uint32_t realImageCount;
	vkGetSwapchainImagesKHR(Vulkan::getLogicalDevice()->getVkDevice(), swapchain, &realImageCount, nullptr);
	images = std::vector<VkImage>(realImageCount);
	vkGetSwapchainImagesKHR(Vulkan::getLogicalDevice()->getVkDevice(), swapchain, &realImageCount, images.data());
	LOG_INFO << "Acquired " << realImageCount << " swapchain images.";

	imageViews = std::vector<VkImageView>(realImageCount);
	for (uint32_t i = 0; i < realImageCount; i++)
	{
		VkImageViewCreateInfo createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = images[i];
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.format = surfaceFormat.format;
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;
		if (vkCreateImageView(Vulkan::getLogicalDevice()->getVkDevice(), &createInfo, nullptr, &imageViews[i]) != VK_SUCCESS) {
			throw COLIBRI_EXCEPTION("Failed to create swapchain image view!");
		}
	}
	LOG_INFO << "Created Vulkan swapchain image views.";
}

VulkanSwapchain::~VulkanSwapchain()
{
	// This swapchain got moved if swapchain is set to nullptr.
	// Every cleanup is then up to the new instance.
	if (swapchain != nullptr)
	{
		destroyFramebuffers();
		if (imageViews.size() > 0)
		{
			for (auto imageView : imageViews) {
				vkDestroyImageView(Vulkan::getLogicalDevice()->getVkDevice(), imageView, nullptr);
			}
			LOG_INFO << "Destroyed Vulkan swapchain image views.";
		}
		vkDestroySwapchainKHR(Vulkan::getLogicalDevice()->getVkDevice(), swapchain, nullptr);
		LOG_INFO << "Destroyed Vulkan swapchain.";
	}
}

VulkanSwapchain::VulkanSwapchain(VulkanSwapchain&& other) noexcept
	: supportDetails(std::move(other.supportDetails)),
	surfaceFormat(std::move(other.surfaceFormat)),
	presentMode(std::move(other.presentMode)),
	extent(std::move(other.extent)),
	swapchain(std::move(other.swapchain)),
	images(std::move(other.images)),
	imageViews(std::move(other.imageViews)),
	framebuffers(std::move(other.framebuffers))
{
	other.swapchain = nullptr;
	LOG_INFO << "Move constructed Vulkan swapchain.";
}

VulkanSwapchain& VulkanSwapchain::operator=(VulkanSwapchain&& other) noexcept
{
	supportDetails = std::move(other.supportDetails);
	surfaceFormat = std::move(other.surfaceFormat);
	presentMode = std::move(other.presentMode);
	extent = std::move(other.extent);
	swapchain = std::move(other.swapchain);
	images = std::move(other.images);
	imageViews = std::move(other.imageViews);
	framebuffers = std::move(other.framebuffers);
	other.swapchain = nullptr;
	return *this;
	LOG_INFO << "Move assigned Vulkan swapchain.";
}

void VulkanSwapchain::createFramebuffers(const std::shared_ptr<VulkanRenderPass> renderPass)
{
	framebuffers.resize(imageViews.size());

	for (size_t i = 0; i < imageViews.size(); i++) {
		VkImageView attachments[] = {
			imageViews[i]
		};

		VkFramebufferCreateInfo framebufferInfo {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		assert(!renderPass->isDegenerate());
		framebufferInfo.renderPass = renderPass->getVkRenderPass();
		framebufferInfo.attachmentCount = 1;
		framebufferInfo.pAttachments = attachments;
		framebufferInfo.width = extent.width;
		framebufferInfo.height = extent.height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer(Vulkan::getLogicalDevice()->getVkDevice(), &framebufferInfo, nullptr, &framebuffers[i]) != VK_SUCCESS) {
			throw COLIBRI_EXCEPTION("Failed to create framebuffer!");
		}
		LOG_INFO << "Created Vulkan swapchain framebuffer: " << reinterpret_cast<uint64_t>(framebuffers[i]);
	}
}

void VulkanSwapchain::destroyFramebuffers()
{
	for (const auto& framebuffer : framebuffers)
	{
		vkDestroyFramebuffer(Vulkan::getLogicalDevice()->getVkDevice(), framebuffer, nullptr);
	}
	LOG_INFO << "Destroyed Vulkan swapchain framebuffers.";
}

const VkSurfaceFormatKHR& VulkanSwapchain::pickOptimalSurfaceFormat() const
{
	const std::vector<VkSurfaceFormatKHR>& formats = supportDetails.getFormats();
	for (const auto& format : formats)
	{
		if (format.format == VK_FORMAT_B8G8R8A8_SRGB
			&& format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
		{
			return format;
		}
	}
	if (formats.size() == 0) {
		throw UNSUPPORTED_OPERATION_EXCEPTION("Cannot pick optimal surface format as none are available!");
	}
	// For now, the first format should suffice.
	return formats[0];
}

const VkPresentModeKHR& VulkanSwapchain::pickOptimalPresentMode() const
{
	/*
	VK_PRESENT_MODE_IMMEDIATE_KHR: Images submitted by your application are transferred to the screen right away, which may result in tearing.
	VK_PRESENT_MODE_FIFO_KHR: The swap chain is a queue where the display takes an image from the front of the queue when the display is refreshed and the program inserts rendered images at the back of the queue. If the queue is full then the program has to wait. This is most similar to vertical sync as found in modern games. The moment that the display is refreshed is known as "vertical blank".
	VK_PRESENT_MODE_FIFO_RELAXED_KHR: This mode only differs from the previous one if the application is late and the queue was empty at the last vertical blank. Instead of waiting for the next vertical blank, the image is transferred right away when it finally arrives. This may result in visible tearing.
	VK_PRESENT_MODE_MAILBOX_KHR: This is another variation of the second mode. Instead of blocking the application when the queue is full, the images that are already queued are simply replaced with the newer ones. This mode can be used to implement triple buffering, which allows you to avoid tearing with significantly less latency issues than standard vertical sync that uses double buffering.
	*/
	const std::vector<VkPresentModeKHR>& presentModes = supportDetails.getPresentModes();

	// Prefer a synced mode. 
	if (Engine::getConfig().isVSync())
	{
		// Lets prefer mailbox mode.
		for (const auto& presentMode : presentModes) {
			if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return presentMode;
			}
		}
		// Use fifo as fallback. Should always be available.
		for (const auto& presentMode : presentModes) {
			if (presentMode == VK_PRESENT_MODE_FIFO_KHR) {
				return presentMode;
			}
		}
	}
	// Use immediate mode for maximum throughput.
	for (const auto& presentMode : presentModes) {
		if (presentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
			return presentMode;
		}
	}
	if (presentModes.size() == 0) {
		throw UNSUPPORTED_OPERATION_EXCEPTION("Cannot pick optimal present mode as none are available!");
	}
	// Fall back to the first present mode as last resort.
	return presentModes[0];
}

const VkExtent2D VulkanSwapchain::pickOptimalExtent() const
{
	const VkSurfaceCapabilitiesKHR& capabilities = supportDetails.getCapabilities();
	if (capabilities.currentExtent.width != UINT32_MAX) {
		return capabilities.currentExtent;
	}
	return VkExtent2D {
		std::max(capabilities.minImageExtent.width, capabilities.maxImageExtent.width),
		std::max(capabilities.minImageExtent.height, capabilities.maxImageExtent.height)
	};
}

COLIBRI_END

