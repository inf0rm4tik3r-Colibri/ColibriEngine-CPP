#pragma once

#ifndef _VULKAN_QUEUE_FAMILIES_H_
#define _VULKAN_QUEUE_FAMILIES_H_

#include "../PlatformLibraries.h"
#include "VulkanQueueFamilies.fwd.h"
#include "VulkanPhysicalDevice.fwd.h"
#include "VulkanPhysicalDevice.h"
#include "VulkanSurface.h"

COLIBRI_BEGIN

class VulkanQueueFamilies
{
private:
	std::vector<VkQueueFamilyProperties> queueFamilies;

	// Indices into the queueFamilies vector!
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> transferFamily;
	std::optional<uint32_t> presentFamily;

public:
	VulkanQueueFamilies(const VulkanPhysicalDevice& physicalDevice,
						const VulkanSurface& surface);

	bool hasGraphicsFamily() const;
	const std::optional<uint32_t>& getGraphicsFamily() const;
	std::optional<VkQueueFamilyProperties> getGraphicsFamilyProperties() const;

	bool hasTransferFamily() const;
	const std::optional<uint32_t>& getTransferFamily() const;
	std::optional<VkQueueFamilyProperties> getTransferFamilyProperties() const;

	bool hasPresentFamily() const;
	const std::optional<uint32_t>& getPresentFamily() const;
	std::optional<VkQueueFamilyProperties> getPresentFamilyProperties() const;
};

COLIBRI_END

#endif // _VULKAN_QUEUE_FAMILIES_H_
