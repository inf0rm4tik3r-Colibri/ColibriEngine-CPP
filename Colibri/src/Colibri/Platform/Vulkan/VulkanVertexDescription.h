#pragma once

#ifndef _VULKAN_VERTEX_DESCRIPTION_H_
#define _VULKAN_VERTEX_DESCRIPTION_H_

#include "../PlatformLibraries.h"
#include "../../Graphics/Model/Vertex.h"

COLIBRI_BEGIN

class VulkanVertexDescription
{
private:
	std::vector<VkVertexInputBindingDescription> bindingDescriptions;
	std::vector<VkVertexInputAttributeDescription> attributeDescriptions;

public:
	static VkFormat vertexFormatToVkFormat(VertexAttributeFormat format);

	VulkanVertexDescription(const VertexInputDescription& vertexInputDescription) noexcept;
	~VulkanVertexDescription() noexcept;

	const std::vector<VkVertexInputBindingDescription>& getVkBindingDescriptions() const;
	const std::vector<VkVertexInputAttributeDescription>& getVkAttributeDescriptions() const;
};

COLIBRI_END

#endif // _VULKAN_VERTEX_DESCRIPTION_H_
