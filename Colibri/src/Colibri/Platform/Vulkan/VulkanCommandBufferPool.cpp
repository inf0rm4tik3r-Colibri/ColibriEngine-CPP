#include "pch.h"
#include "VulkanCommandBufferPool.h"
#include "Vulkan.h"

COLIBRI_BEGIN

VulkanCommandBufferPool::VulkanCommandBufferPool(uint32_t queueFamilyIndex)
{
	VkCommandPoolCreateInfo poolInfo {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndex;
	/*
	 * VK_COMMAND_POOL_CREATE_TRANSIENT_BIT: Hint that command buffers are rerecorded with new commands very often (may change memory allocation behavior).
     * VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT: Allow command buffers to be rerecorded individually, without this flag they all have to be reset together.
     */
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT | VK_COMMAND_POOL_CREATE_TRANSIENT_BIT; // 0 = Optional

	if (vkCreateCommandPool(Vulkan::getLogicalDevice()->getVkDevice(), &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to create Vulkan command pool!");
	}
	LOG_INFO << "Created Vulkan command pool: " << reinterpret_cast<uint64_t>(commandPool);
}

VulkanCommandBufferPool::~VulkanCommandBufferPool()
{
	if (commandPool != nullptr)
	{
		vkDestroyCommandPool(Vulkan::getLogicalDevice()->getVkDevice(), commandPool, nullptr);
		LOG_INFO << "Destroyed Vulkan command pool.";
	}
}

VulkanCommandBufferPool::VulkanCommandBufferPool(VulkanCommandBufferPool&& other) noexcept
	: commandPool(std::move(other.commandPool))
{
	other.commandPool = nullptr;
}

VulkanCommandBufferPool& VulkanCommandBufferPool::operator=(VulkanCommandBufferPool&& other) noexcept
{
	commandPool = std::move(other.commandPool);
	other.commandPool = nullptr;
	return *this;
}

std::vector<VulkanCommandBuffer> VulkanCommandBufferPool::createCommandBuffers(uint32_t size) const
{
	VkCommandBufferAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = size;

	std::vector<VkCommandBuffer> vkCommandBuffers(size);
	if (vkAllocateCommandBuffers(Vulkan::getLogicalDevice()->getVkDevice(), &allocInfo, vkCommandBuffers.data()) != VK_SUCCESS)
	{
		throw COLIBRI_EXCEPTION("Failed to allocate Vulkan command buffers!");
	}

	std::vector<VulkanCommandBuffer> commandBuffers;
	for (const auto& vkCommandBuffer : vkCommandBuffers)
	{
		commandBuffers.push_back(VulkanCommandBuffer { vkCommandBuffer, shared_from_this() });
	}
	return std::move(commandBuffers);
}

const VkCommandPool& VulkanCommandBufferPool::getVkCommandPool() const
{
	return commandPool;
}

COLIBRI_END
