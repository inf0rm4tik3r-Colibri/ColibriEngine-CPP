#pragma once

#ifndef _VULKAN_BUFFER_H_
#define _VULKAN_BUFFER_H_

#include "../PlatformLibraries.h"
#include "Vulkan.h"

COLIBRI_BEGIN

class VulkanBuffer
{
private:
	VkBufferCreateInfo bufferCreateInfo;
	VkBuffer buffer;
	VkDeviceMemory bufferMemory;
	bool mappable;

public:
	VulkanBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, bool mappable);
	virtual ~VulkanBuffer();

	template<typename T>
	void fill(const std::initializer_list<T>& elements)
	{
		if (mappable)
		{
			void* data;
			mapMemory(&data);
			for (const T& elem : elements)
			{
				memcpy(data, &elem, sizeof(elem));
				data = static_cast<T*>(data) + 1;
			}
			unmapMemory();
			LOG_INFO << "Filled vertex buffer by mapping it.";
		}
		else
		{
			// Create and fill host visible staging buffer;
			VkDeviceSize bufferSize = elements.size() * sizeof(T);
			VulkanBuffer staging(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, true);
			void* data;
			staging.mapMemory(&data);
			for (const T& elem : elements)
			{
				memcpy(data, &elem, sizeof(elem));
				data = static_cast<T*>(data) + 1;
			}
			staging.unmapMemory();
			// Copy staging buffer to current.
			staging.copyTo(*this, bufferSize);
			LOG_INFO << "Filled vertex buffer by copying a staging buffer.";
		}
	}

	template<typename T>
	void fill(const std::vector<T>& elements)
	{
		VkDeviceSize size = elements.size() * sizeof(T);
		if (mappable)
		{
			void* data;
			mapMemory(&data);
			memcpy(data, elements.data(), size);
			unmapMemory();
			LOG_INFO << "Filled vertex buffer by mapping it.";
		}
		else
		{
			// Create and fill host visible staging buffer;
			VulkanBuffer staging(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
			void* data;
			staging.mapMemory(&data);
			memcpy(data, elements.data(), size);
			staging.unmapMemory();
			// Copy staging buffer to current.
			staging.copyTo(*this, size);
			LOG_INFO << "Filled vertex buffer by copying a staging buffer.";
		}
	}

	/*
	 * Copies the contents of this buffer to the destination buffer.
	 */
	void copyTo(VulkanBuffer& dstBuffer, VkDeviceSize size) const;

	void mapMemory(void** data);
	void unmapMemory();

	const size_t getSizeInBytes() const;
	const VkBuffer getVkBuffer() const;
};

COLIBRI_END

#endif // _VULKAN_BUFFER_H_
