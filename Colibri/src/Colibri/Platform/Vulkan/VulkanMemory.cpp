#include "pch.h"
#include "VulkanMemory.h"
#include "Vulkan.h"

COLIBRI_BEGIN

std::unique_ptr<VulkanCommandBufferPool> VulkanMemory::transferCommandBufferPool = nullptr;

uint32_t VulkanMemory::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
    /*
     * The typeFilter parameter will be used to specify the bit field of memory types that are suitable.
     * The maximum memory type count should be 32.
     * That means that we can find the index of a suitable memory type by simply iterating over them and checking if the corresponding bit is set to 1.
     */
    const VkPhysicalDeviceMemoryProperties& memProperties = Vulkan::getPhysicalDevice()->getVkDeviceMemoryProperties();
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if (typeFilter & (1 << i)) {
            // The memory type with index i is valid candidate.
            // Lets check if it statisfies our properties requirements.
            if ((memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
                // The memory type is suitable for our needs.
                return i;
            }
        }
    }
    throw COLIBRI_EXCEPTION("Failed to find a suitable vulkan memory type!");
}

COLIBRI_END
