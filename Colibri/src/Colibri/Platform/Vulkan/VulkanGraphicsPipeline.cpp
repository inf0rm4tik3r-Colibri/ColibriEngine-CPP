#include "pch.h"
#include "VulkanGraphicsPipeline.h"

COLIBRI_BEGIN

VulkanGraphicsPipeline::VulkanGraphicsPipeline(const std::shared_ptr<VulkanWindow>& window,
											   const std::shared_ptr<VulkanRenderPass>& renderPass,
											   const std::vector<std::shared_ptr<ShaderDescription>>& shaderDescriptions,
											   const std::unique_ptr<VulkanVertexDescription> vertexDesc)
	: GraphicsPipeline(shaderDescriptions), renderPass(renderPass)
{
	std::vector<std::shared_ptr<VulkanShaderModule>> shaderModules;
	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

	// Construct shader stage create infos which are later used in pipeline creation.
	for (const std::shared_ptr<ShaderDescription> shaderDescription : shaderDescriptions)
	{
		//  Accumulate the source code of all referenced shader sources.
		std::vector<char> shaderSourceCode;
		for (const std::string& sourceLocation : shaderDescription->getSources())
		{
			std::vector<char> temp = File(sourceLocation).readBinary();
			shaderSourceCode.insert(shaderSourceCode.end(), temp.begin(), temp.end());
		}

		// Create a shade module from that source code.
		std::shared_ptr<VulkanShaderModule> shaderModule = std::make_unique<VulkanShaderModule>(shaderSourceCode);
		shaderModules.push_back(shaderModule);

		// Create the shader stage create info which references the shader module and specifies the stage of the shader and its entry point.
		VkPipelineShaderStageCreateInfo shaderStageCreateInfo {};
		shaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		switch (shaderDescription->getType())
		{
			case ShaderType::vertex:
				shaderStageCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
				break;
			case ShaderType::compute:
				shaderStageCreateInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
				break;
			case ShaderType::tesselationControl:
				shaderStageCreateInfo.stage = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
				break;
			case ShaderType::tesselationEvaluation:
				shaderStageCreateInfo.stage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
				break;
			case ShaderType::fragment:
				shaderStageCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
				break;
		}
		shaderStageCreateInfo.module = shaderModule->getVkShaderModule();
		shaderStageCreateInfo.pName = shaderDescription->getEntryPoint().c_str();
		shaderStageCreateInfo.pSpecializationInfo = nullptr; // Const data can be set here!
		shaderStages.push_back(shaderStageCreateInfo);

		// Create the "descriptor set layouts" for all uniform buffers described by the current shader.
		// These are stored in this pipeline obect so that they can be used later to construct the actual uniform descriptor sets!
		for (const std::shared_ptr<UniformBufferDescription>& ubDescription : shaderDescription->getUniformBufferDescriptions())
		{
			uniformDescriptorSetLayouts.push_back(std::move(VulkanUniformDescriptorSetLayout(ubDescription->getId(), ubDescription->getBinding())));
		}
	}

	// Collect descriptor set layouts that should be used in the pipeline layout which is yet to be sconstructed.
	std::vector<VkDescriptorSetLayout> vkDescriptorSetLayouts;
	{
		// Add the uniform layouts computed aboeve.
		for (const VulkanUniformDescriptorSetLayout& uniformDescriptorSetLayout : uniformDescriptorSetLayouts)
		{
			vkDescriptorSetLayouts.push_back(uniformDescriptorSetLayout.getVkDescriptorSetLayout());
		}
	}

	// Construct pipeline layout using the known descriptor set layouts.
	VkPipelineLayoutCreateInfo pipelineLayoutInfo {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = static_cast<uint32_t>(vkDescriptorSetLayouts.size()); // Optional
	pipelineLayoutInfo.pSetLayouts = vkDescriptorSetLayouts.data(); // Optional
	pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
	pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

	if (vkCreatePipelineLayout(Vulkan::getLogicalDevice()->getVkDevice(), &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to create Vulkan pipeline layout!");
	}
	LOG_INFO << "Created Vulkan graphics pipeline layout.";

	VkPipelineVertexInputStateCreateInfo vertexInputState {};
	vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	if (vertexDesc == nullptr)
	{
		vertexInputState.vertexBindingDescriptionCount = 0;
		vertexInputState.pVertexBindingDescriptions = nullptr;
		vertexInputState.vertexAttributeDescriptionCount = 0;
		vertexInputState.pVertexAttributeDescriptions = nullptr;
	}
	else
	{
		vertexInputState.vertexBindingDescriptionCount = static_cast<uint32_t>(vertexDesc->getVkBindingDescriptions().size());
		vertexInputState.pVertexBindingDescriptions = vertexDesc->getVkBindingDescriptions().data();
		vertexInputState.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexDesc->getVkAttributeDescriptions().size());
		vertexInputState.pVertexAttributeDescriptions = vertexDesc->getVkAttributeDescriptions().data();
	}

	VkPipelineInputAssemblyStateCreateInfo inputAssemblyState {};
	inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssemblyState.primitiveRestartEnable = VK_FALSE;

	// Region of the framebuffer the output will go to.
	VkViewport viewport {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	// We stick to the size of the swapchin images, as tey will be used as the framebuffers.
	viewport.width = (float)window->getSwapchain().getExtent().width;
	viewport.height = (float)window->getSwapchain().getExtent().height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	// Region of pixels that will actually receive values. (Filter!)
	VkRect2D scissor {};
	scissor.offset = { 0, 0 };
	scissor.extent = window->getSwapchain().getExtent();

	// Combine viewport and scissor.
	VkPipelineViewportStateCreateInfo viewportState {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	// Configuration of the rasterizer: Depth testing, face culling, wireframe mode, ...
	VkPipelineRasterizationStateCreateInfo rasterizerState {};
	rasterizerState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	// If VK_TRUE, fragments outside near and far planes are clamped and not discarded. Needs GPU feature.
	rasterizerState.depthClampEnable = VK_FALSE;
	// If VK_TRUE, all fragments are discarded. -> No output to framebuffers.
	rasterizerState.rasterizerDiscardEnable = VK_FALSE;
	// VK_POLYGON_MODE_FILL: fill the area of the polygon with fragments
	// VK_POLYGON_MODE_LINE: polygon edges are drawn as lines
	// VK_POLYGON_MODE_POINT : polygon vertices are drawn as points
	// Modes other than fill require a GPU feature.
	rasterizerState.polygonMode = VK_POLYGON_MODE_FILL;
	// If >1, than wideLines feature must be enabled!
	rasterizerState.lineWidth = 1.0f;
	rasterizerState.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizerState.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizerState.depthBiasEnable = VK_FALSE;
	rasterizerState.depthBiasConstantFactor = 0.0f; // Optional
	rasterizerState.depthBiasClamp = 0.0f; // Optional
	rasterizerState.depthBiasSlopeFactor = 0.0f; // Optional

	// Configure Multisampling. Required a GPU feature.
	VkPipelineMultisampleStateCreateInfo multisampleState {};
	multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampleState.sampleShadingEnable = VK_FALSE;
	multisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampleState.minSampleShading = 1.0f; // Optional
	multisampleState.pSampleMask = nullptr; // Optional
	multisampleState.alphaToCoverageEnable = VK_FALSE; // Optional
	multisampleState.alphaToOneEnable = VK_FALSE; // Optional

	// VkPipelineDepthStencilStateCreateInfo depthStencil = {};

	// Per framebuffer blending configuration.
	VkPipelineColorBlendAttachmentState colorBlendAttachment {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional
	// Settings for blending of transparent fragments with already current values.
	// colorBlendAttachment.blendEnable = VK_TRUE;
	// colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	// colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	// colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	// colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	// colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	// colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	// Global blending state.
	VkPipelineColorBlendStateCreateInfo colorBlendingState {};
	colorBlendingState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendingState.logicOpEnable = VK_FALSE;
	colorBlendingState.logicOp = VK_LOGIC_OP_COPY; // Optional
	colorBlendingState.attachmentCount = 1;
	colorBlendingState.pAttachments = &colorBlendAttachment;
	colorBlendingState.blendConstants[0] = 0.0f; // Optional
	colorBlendingState.blendConstants[1] = 0.0f; // Optional
	colorBlendingState.blendConstants[2] = 0.0f; // Optional
	colorBlendingState.blendConstants[3] = 0.0f; // Optional

	// Some options can be changed dynamically without recreating the pipeling:
	// size of the viewport, line width, blend constants, ...
	VkDynamicState dynamicStates[] = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR
	};

	VkPipelineDynamicStateCreateInfo dynamicState {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.pNext = nullptr;
	dynamicState.flags = 0;
	dynamicState.dynamicStateCount = 2;
	dynamicState.pDynamicStates = dynamicStates;

	VkGraphicsPipelineCreateInfo pipelineInfo {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineInfo.pStages = shaderStages.data();
	pipelineInfo.pVertexInputState = &vertexInputState;
	pipelineInfo.pInputAssemblyState = &inputAssemblyState;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizerState;
	pipelineInfo.pMultisampleState = &multisampleState;
	pipelineInfo.pDepthStencilState = nullptr; // Optional
	pipelineInfo.pColorBlendState = &colorBlendingState;
	pipelineInfo.pDynamicState = &dynamicState; // Optional
	pipelineInfo.layout = pipelineLayout; // Vulkan handle here!
	pipelineInfo.renderPass = renderPass->getVkRenderPass(); // Vulkan handle here!
	pipelineInfo.subpass = 0; // INDEX TO THE SUBPASS WHERE THIS RENDER PIPELINE WILL BE USED!
	// Pipelines can be derived from existing pipeline with which they have much in common! 
	// -> Greater create and swithching performance between pipelines of the same parent.
	//pipelineInfo.flags = VK_PIPELINE_CREATE_DERIVATIVE_BIT;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
	pipelineInfo.basePipelineIndex = -1; // Optional

	// TODO: Use the pipeline cache!
	if (vkCreateGraphicsPipelines(Vulkan::getLogicalDevice()->getVkDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipeline) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to create Vulkan graphics pipeline!");
	}
	LOG_INFO << "Created Vulkan graphics pipeline.";
}

VulkanGraphicsPipeline::~VulkanGraphicsPipeline()
{
	if (pipeline != nullptr)
	{
		vkDestroyPipeline(Vulkan::getLogicalDevice()->getVkDevice(), pipeline, nullptr);
		pipeline = nullptr;
		LOG_INFO << "Destroyed Vulkan graphics pipeline.";
	}
	if (pipelineLayout != nullptr)
	{
		vkDestroyPipelineLayout(Vulkan::getLogicalDevice()->getVkDevice(), pipelineLayout, nullptr);
		pipelineLayout = nullptr;
		LOG_INFO << "Destroyed Vulkan graphics pipeline layout.";
	}
}

VulkanGraphicsPipeline::VulkanGraphicsPipeline(VulkanGraphicsPipeline&& other) noexcept
	: GraphicsPipeline(std::move(other)),
	  renderPass(std::move(other.renderPass)),
	  pipeline(std::move(other.pipeline)),
	  pipelineLayout(std::move(other.pipelineLayout))
{
	other.pipeline = nullptr;
	other.pipelineLayout = nullptr;
}

VulkanGraphicsPipeline& VulkanGraphicsPipeline::operator=(VulkanGraphicsPipeline&& other) noexcept
{
	GraphicsPipeline::operator=(std::move(other));
	renderPass = std::move(other.renderPass);
	pipeline = std::move(other.pipeline);
	pipelineLayout = std::move(other.pipelineLayout);
	other.pipeline = nullptr;
	other.pipelineLayout = nullptr;
	return *this;
}

void VulkanGraphicsPipeline::bind(const VulkanCommandBuffer& commandBuffer) const
{
	/* TODO: Do we have to recreate the pipeline?
	if (renderPass->isDegenerate())
	{
		LOG_INFO << "Render pass is degenerate.";
	}
	*/

	// TODO: Currently fixed to VK_PIPELINE_BIND_POINT_GRAPHICS. But we must not always be a graphics pipeline!
	vkCmdBindPipeline(commandBuffer.getVkCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
}

const VkPipeline& VulkanGraphicsPipeline::getVkPipeline() const
{
	return pipeline;
}

const VkPipelineLayout& VulkanGraphicsPipeline::getVkPipelineLayout() const
{
	return pipelineLayout;
}

const std::vector<VulkanUniformDescriptorSetLayout>& VulkanGraphicsPipeline::getUniformDescriptorSetLayouts() const
{
	return uniformDescriptorSetLayouts;
}

COLIBRI_END
