#include "pch.h"
#include "VulkanWindow.h"
#include "Vulkan.h"

COLIBRI_BEGIN

VulkanWindow::VulkanWindow(unsigned int width, unsigned int height, const std::string& title) noexcept
	: Window(width, height, title),
	surface(Vulkan::getInstance(), this),
	swapchain(Vulkan::getPhysicalDevice(), surface)
{
	swapchainCreated = true;
	
	Window::frameBufferResize.subscribe(
		[this](int width, int height)
		{
			LOG_INFO << "Framebuffer resize: w=" << width << ", h=" << height;
			if (width == 0 || height == 0)
			{
				LOG_INFO << "Cannot recreate swapchain as width or height is zero and zero sized framebuffers are not allowed!";
				LOG_INFO << "Application should stop rendering!";
			}
			else
			{
				recreateSwapchain();
			}
		}
	);
										
	LOG_INFO << "Created Vulkan window.";
}

VulkanWindow::~VulkanWindow()
{
}

VulkanWindow::VulkanWindow(VulkanWindow&& other) noexcept
	: Window(std::move(other)),
	surface(std::move(other.surface)),
	swapchain(std::move(other.swapchain))
{
	LOG_INFO << "Moved Vulkan window.";
}

/*
void VulkanWindow::onFramebufferResize(int width, int height)
{
	LOG_INFO << "Framebuffer resize: w=" << width << ", h=" << height;
	recreateSwapchain();
}
*/

void VulkanWindow::recreateSwapchain()
{
	// We can only destroy our swapchain, if it is not already destroyed.
	// We must not try to create a swapchain while it is being created.
	if (swapchainCreated)
	{
		swapchainCreated = false;
		// Rendering must stop!
		Window::stopRendering.next(true);

		// No operations must go on for us to correctly recreate the swapchain.
		vkDeviceWaitIdle(Vulkan::getLogicalDevice()->getVkDevice());

		swapchain.~VulkanSwapchain();
		swapchain = VulkanSwapchain(Vulkan::getPhysicalDevice(), surface);

		// Inform any observers that the swapchain of this window got recreated!
		swapchainRecreationFinished.next();
		// Inform any observers that rendering now may continue!
		Window::stopRendering.next(false);
		// The swapchain is recreated.
		// New calls to this method will recreate the swapchain again.
		swapchainCreated = true;

		// TODO check if swapchain is correct. Otherwise recreate. ???
	}
}

COLIBRI_END
