#pragma once

#ifndef _VULKAN_SURFACE_H_
#define _VULKAN_SURFACE_H_

#include "../PlatformLibraries.h"
#include "../../Graphics/Window/Window.h"
#include "VulkanInstance.h"

COLIBRI_BEGIN

class VulkanSurface
{
private:
	const VulkanInstance& instance;
	VkSurfaceKHR surface;

public:
	VulkanSurface(const VulkanInstance& instance, const Window* const window);
	~VulkanSurface();
	VulkanSurface(const VulkanSurface& other) = delete;
	VulkanSurface(VulkanSurface&& other) noexcept;
	VulkanSurface& operator=(const VulkanSurface& other) = delete;
	VulkanSurface& operator=(VulkanSurface&& other) = delete;

	const VkSurfaceKHR& getVkSurface() const { return surface; }

	static const char* formatToString(const VkFormat format);
	static const char* colorSpaceToString(const VkColorSpaceKHR colorSpace);
	static const char* presentModeToString(const VkPresentModeKHR presentMode);
};

COLIBRI_END

#endif // _VULKAN_SURFACE_H_
