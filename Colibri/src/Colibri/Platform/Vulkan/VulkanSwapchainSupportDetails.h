#pragma once

#ifndef _VULKAN_SWAPCHAIN_SUPPORT_DETAILS_H_
#define _VULKAN_SWAPCHAIN_SUPPORT_DETAILS_H_

#include "../PlatformLibraries.h"
#include "VulkanPhysicalDevice.h"

COLIBRI_BEGIN

class VulkanSwapchainSupportDetails
{
private:
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;

public:
    VulkanSwapchainSupportDetails(const VulkanPhysicalDevice* device,
                                  const VulkanSurface& surface) noexcept;

    const VkSurfaceCapabilitiesKHR& getCapabilities() const { return capabilities; }
    const std::vector<VkSurfaceFormatKHR>& getFormats() const { return formats; }
    const std::vector<VkPresentModeKHR>& getPresentModes() const { return presentModes; }

    void print() const;
    void printCapabilities() const;
    void printFormats() const;
    void printPresentModes() const;
};

COLIBRI_END

#endif // _VULKAN_SWAPCHAIN_SUPPORT_DETAILS_H_
