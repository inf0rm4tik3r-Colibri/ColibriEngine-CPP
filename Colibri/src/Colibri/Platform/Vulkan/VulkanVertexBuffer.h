#pragma once

#ifndef _VULKAN_VERTEX_BUFFER_H_
#define _VULKAN_VERTEX_BUFFER_H_

#include "../PlatformLibraries.h"
#include "../../Graphics/Model/Vertex.h"
#include "../../Graphics/Renderer/VertexBuffer.h"
#include "VulkanBuffer.h"

COLIBRI_BEGIN

class VulkanVertexBuffer : public VulkanBuffer, public VertexBuffer
{
public:
	VulkanVertexBuffer(size_t size, size_t sizePerVertex, bool hostVisible = false);
	~VulkanVertexBuffer();

	void fill(const std::initializer_list<ColoredVertex>& vertices) override;
};

COLIBRI_END

#endif // _VULKAN_VERTEX_BUFFER_H_
