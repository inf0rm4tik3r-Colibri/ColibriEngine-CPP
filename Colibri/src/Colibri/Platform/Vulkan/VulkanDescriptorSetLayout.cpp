#include "pch.h"
#include "VulkanDescriptorSetLayout.h"
#include "Vulkan.h"

COLIBRI_BEGIN

VulkanDescriptorSetLayout::VulkanDescriptorSetLayout()
    : id(IDS::genRand64())
{
}

VulkanDescriptorSetLayout::~VulkanDescriptorSetLayout()
{
    if (descriptorSetLayout != nullptr)
    {
        vkDestroyDescriptorSetLayout(Vulkan::getLogicalDevice()->getVkDevice(), descriptorSetLayout, nullptr);
        descriptorSetLayout = nullptr;
        LOG_INFO << "Destroyed vulkan descriptor set layout.";
    }
}

VulkanDescriptorSetLayout::VulkanDescriptorSetLayout(VulkanDescriptorSetLayout&& other) noexcept
    : id(std::move(other.id)),
      descriptorSetLayout(std::move(other.descriptorSetLayout)),
      uniformLayoutBinding(std::move(other.uniformLayoutBinding))
{
    other.descriptorSetLayout = nullptr;
}

VulkanDescriptorSetLayout& VulkanDescriptorSetLayout::operator=(VulkanDescriptorSetLayout&& other) noexcept
{
    this->~VulkanDescriptorSetLayout();
    id = std::move(other.id);
    descriptorSetLayout = std::move(other.descriptorSetLayout);
    uniformLayoutBinding = std::move(other.uniformLayoutBinding);
    other.descriptorSetLayout = nullptr;
    return *this;
}

const VkDescriptorSetLayout& VulkanDescriptorSetLayout::getVkDescriptorSetLayout() const
{
	return descriptorSetLayout;
}

COLIBRI_END
