#pragma once

#ifndef _VULKAN_UNIFORM_BUFFER_H_
#define _VULKAN_UNIFORM_BUFFER_H_

#include "../PlatformLibraries.h"
#include "VulkanBuffer.h"

COLIBRI_BEGIN

class VulkanUniformBuffer : public VulkanBuffer
{
public:
	VulkanUniformBuffer(size_t sizeInBytes, bool hostVisible = true);
	~VulkanUniformBuffer();
};

COLIBRI_END

#endif // _VULKAN_UNIFORM_BUFFER_H_
