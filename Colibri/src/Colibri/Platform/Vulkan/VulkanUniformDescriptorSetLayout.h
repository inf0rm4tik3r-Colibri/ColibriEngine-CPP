#pragma once

#ifndef _VULKAN_UNIFORM_BUFFER_DESCRIPTOR_SET_LAYOUT_H_
#define _VULKAN_UNIFORM_BUFFER_DESCRIPTOR_SET_LAYOUT_H_

#include "../PlatformLibraries.h"
#include "../../Graphics/Shader/ShaderStage.h"
#include "../../Graphics/Shader/UniformBufferBinding.h"
#include "VulkanDescriptorSetLayout.h"

COLIBRI_BEGIN

class VulkanUniformDescriptorSetLayout : public VulkanDescriptorSetLayout
{
private:
	uint64_t uniformBufferDescriptionId;

public:
	VulkanUniformDescriptorSetLayout(const uint64_t uniformBufferDescriptionId,
									 const UniformBufferBinding& uniformBufferbinding);
	~VulkanUniformDescriptorSetLayout();

	VulkanUniformDescriptorSetLayout(const VulkanUniformDescriptorSetLayout& other) = delete;
	VulkanUniformDescriptorSetLayout(VulkanUniformDescriptorSetLayout&& other) noexcept;
	VulkanUniformDescriptorSetLayout& operator=(const VulkanUniformDescriptorSetLayout& other) = delete;
	VulkanUniformDescriptorSetLayout& operator=(VulkanUniformDescriptorSetLayout&& other) noexcept;

	uint64_t getUniformBufferDescriptionId() const;

	const VkDescriptorSetLayoutBinding& getVkDescriptorSetLayoutBinding() const override;
};

COLIBRI_END

#endif // _VULKAN_UNIFORM_BUFFER_DESCRIPTOR_SET_LAYOUT_H_
