#pragma once

#ifndef _VULKAN_GRAPHICS_PIPELINE_H_
#define _VULKAN_GRAPHICS_PIPELINE_H_

#include "../PlatformLibraries.h"
#include "../../Graphics/Shader/ShaderDescription.h"
#include "../../Graphics/Renderer/GraphicsPipeline.fwd.h"
#include "../../Graphics/Renderer/GraphicsPipeline.h"
#include "VulkanCommandBuffer.h"
#include "VulkanShaderModule.h"
#include "VulkanRenderPass.h"
#include "VulkanVertexDescription.h"
#include "VulkanUniformDescriptorSetLayout.h"
#include "VulkanWindow.h"

COLIBRI_BEGIN

class VulkanGraphicsPipeline : public GraphicsPipeline
{
private:
	VkPipeline pipeline;
	VkPipelineLayout pipelineLayout;

	/*
	 * The render pass this pipeline was created with. Can get degenerate! Pipeline must then berecreated.
	 */
	std::shared_ptr<VulkanRenderPass> renderPass;

	/*
	 * The uniform descriptor set layout which got computed before pipeline creation.
	 * Must be used to construct the actual descriptor sets.
	 */
	std::vector<VulkanUniformDescriptorSetLayout> uniformDescriptorSetLayouts;

public:
	/*
	 * Vertex description data may be null.
	 */
	VulkanGraphicsPipeline(const std::shared_ptr<VulkanWindow>& window,
						   const std::shared_ptr<VulkanRenderPass>& renderPass, 
						   const std::vector<std::shared_ptr<ShaderDescription>>& shaderDescriptions,
						   const std::unique_ptr<VulkanVertexDescription> vertexDesc);
	~VulkanGraphicsPipeline();

	VulkanGraphicsPipeline(const VulkanGraphicsPipeline& other) = delete;
	VulkanGraphicsPipeline(VulkanGraphicsPipeline&& other) noexcept;
	VulkanGraphicsPipeline& operator=(const VulkanGraphicsPipeline& other) = delete;
	VulkanGraphicsPipeline& operator=(VulkanGraphicsPipeline&& other) noexcept;

	void bind(const VulkanCommandBuffer& commandBuffer) const;

	const VkPipeline& getVkPipeline() const;
	const VkPipelineLayout& getVkPipelineLayout() const;
	const std::vector<VulkanUniformDescriptorSetLayout>& getUniformDescriptorSetLayouts() const;
};

COLIBRI_END

#endif // _VULKAN_GRAPHICS_PIPELINE_H_
