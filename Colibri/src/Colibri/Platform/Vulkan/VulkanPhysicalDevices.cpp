#include "pch.h"
#include "VulkanPhysicalDevices.h"

COLIBRI_BEGIN

VulkanPhysicalDevices::VulkanPhysicalDevices(const VulkanInstance& vulkanInstance,
											 const VulkanSurface& surface)
{
	// Load information about all physical devices.
	// Physical device information is automatically released when the Vulkan instance is destroyed.
	LOG_INFO << "Loading physical devices...";
	uint32_t physicalDeviceCount = 0;
	vkEnumeratePhysicalDevices(*vulkanInstance.getInstance(), &physicalDeviceCount, nullptr);
	std::vector<VkPhysicalDevice> vkPhysicalDevices = std::vector<VkPhysicalDevice>(physicalDeviceCount);
	vkEnumeratePhysicalDevices(*vulkanInstance.getInstance(), &physicalDeviceCount, vkPhysicalDevices.data());

	devices = std::vector<VulkanPhysicalDevice>();
	devices.reserve(physicalDeviceCount);
	for (VkPhysicalDevice device : vkPhysicalDevices)
	{
		VkPhysicalDeviceProperties properties;
		vkGetPhysicalDeviceProperties(device, &properties);

		VkPhysicalDeviceFeatures features;
		vkGetPhysicalDeviceFeatures(device, &features);

		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(device, &memProperties);

		uint32_t extensionCount = 0;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
		std::vector<VkExtensionProperties> extensionProperties(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, extensionProperties.data());

		devices.emplace_back(VulkanPhysicalDevice(device, properties, features, memProperties, extensionProperties, surface));
	}
}

const std::vector<VulkanPhysicalDevice>& VulkanPhysicalDevices::getDevices() const
{
	return devices;
}

void VulkanPhysicalDevices::print() const
{
	for (const VulkanPhysicalDevice& device : devices)
	{
		device.print();
	}
}

COLIBRI_END
