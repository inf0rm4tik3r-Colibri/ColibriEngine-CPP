#include "pch.h"
#include "VulkanSwapChainSupportDetails.h"

COLIBRI_BEGIN

VulkanSwapchainSupportDetails::VulkanSwapchainSupportDetails(const VulkanPhysicalDevice* device,
															 const VulkanSurface& surface) noexcept
{
	// Load the device<->surface capabilities.
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device->getVkDevice(), surface.getVkSurface(), &capabilities);

	// Load the available surface formats. 
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device->getVkDevice(), surface.getVkSurface(), &formatCount, nullptr);
	if (formatCount != 0) {
		formats = std::vector<VkSurfaceFormatKHR>(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device->getVkDevice(), surface.getVkSurface(), &formatCount, formats.data());
	}

	// Load the available present modes.
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device->getVkDevice(), surface.getVkSurface(), &presentModeCount, nullptr);
	if (formatCount != 0) {
		presentModes = std::vector<VkPresentModeKHR>(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device->getVkDevice(), surface.getVkSurface(), &presentModeCount, presentModes.data());
	}
}

void VulkanSwapchainSupportDetails::print() const
{
	LOG_DEBUG << "Swapchain support details: ";
	printCapabilities();
	printFormats();
	printPresentModes();
}

void VulkanSwapchainSupportDetails::printCapabilities() const
{
	LOG_DEBUG << "\t" << "Capabilities:";
	LOG_DEBUG << "\t\t" << "currentExtent: " << capabilities.currentExtent.width << " x " << capabilities.currentExtent.height;
	LOG_DEBUG << "\t\t" << "currentTransform: " << capabilities.currentTransform;
	LOG_DEBUG << "\t\t" << "maxImageArrayLayers: " << capabilities.maxImageArrayLayers;
	LOG_DEBUG << "\t\t" << "maxImageCount: " << capabilities.maxImageCount;
	LOG_DEBUG << "\t\t" << "maxImageExtent: " << capabilities.maxImageExtent.width << " x " << capabilities.maxImageExtent.height;
	LOG_DEBUG << "\t\t" << "minImageCount: " << capabilities.minImageCount;
	LOG_DEBUG << "\t\t" << "minImageExtent: " << capabilities.minImageExtent.width << " x " << capabilities.minImageExtent.height;
	LOG_DEBUG << "\t\t" << "supportedCompositeAlpha: " << capabilities.supportedCompositeAlpha;
	LOG_DEBUG << "\t\t" << "supportedTransforms: " << capabilities.supportedTransforms;
	LOG_DEBUG << "\t\t" << "supportedUsageFlags: " << capabilities.supportedUsageFlags;
}

void VulkanSwapchainSupportDetails::printFormats() const
{
	LOG_DEBUG << "\t" << "Formats:";
	int i = 1;
	for (const auto& format : formats)
	{
		LOG_DEBUG << "\t\t" << "(" << i << ")";
		LOG_DEBUG << "\t\t\t" << "format: " << VulkanSurface::formatToString(format.format) << " (" << format.format << ")";
		LOG_DEBUG << "\t\t\t" << "colorSpace: " << VulkanSurface::colorSpaceToString(format.colorSpace) << " (" << format.colorSpace << ")";
		i++;
	}
}

void VulkanSwapchainSupportDetails::printPresentModes() const
{
	LOG_DEBUG << "\t" << "Present modes:";
	for (const auto& presentMode : presentModes)
	{
		LOG_DEBUG << "\t\t" << VulkanSurface::presentModeToString(presentMode) << " (" << presentMode << ")";
	}
}

COLIBRI_END
