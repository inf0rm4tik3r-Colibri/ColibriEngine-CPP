#include "pch.h"
#include "VulkanDescriptorPool.h"
#include "Vulkan.h"

COLIBRI_BEGIN

VulkanDescriptorPool::VulkanDescriptorPool(VkDescriptorType descriptorType, size_t size)
{
	VkDescriptorPoolSize poolSize {};
	poolSize.type = descriptorType;
	poolSize.descriptorCount = static_cast<uint32_t>(size);

	VkDescriptorPoolCreateInfo poolInfo {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = 1;
	poolInfo.pPoolSizes = &poolSize;
	poolInfo.maxSets = static_cast<uint32_t>(size); // Maximum number of descriptor sets that may be allocated.

	if (vkCreateDescriptorPool(Vulkan::getLogicalDevice()->getVkDevice(), &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to create Vulkan descriptor pool!");
	}
	LOG_INFO << "Created Vulkan descriptor pool " << reinterpret_cast<uint64_t>(descriptorPool) << " of size " << size;
}

VulkanDescriptorPool::~VulkanDescriptorPool()
{
	if (descriptorPool != nullptr)
	{
		vkDestroyDescriptorPool(Vulkan::getLogicalDevice()->getVkDevice(), descriptorPool, nullptr);
		descriptorPool = nullptr;
		LOG_INFO << "Destroyed Vulkan descriptor pool.";
	}
}

VulkanDescriptorPool::VulkanDescriptorPool(VulkanDescriptorPool&& other) noexcept
	: descriptorPool(std::move(other.descriptorPool))
{
	other.descriptorPool = nullptr;
}

VulkanDescriptorPool& VulkanDescriptorPool::operator=(VulkanDescriptorPool&& other) noexcept
{
	this->~VulkanDescriptorPool();
	descriptorPool = std::move(other.descriptorPool);
	other.descriptorPool = nullptr;
	return *this;
}

VulkanDescriptorSet VulkanDescriptorPool::allocateDescriptorSet(const VulkanDescriptorSetLayout& descriptorSetLayout) const
{
	VkDescriptorSetAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = &descriptorSetLayout.getVkDescriptorSetLayout();

	std::vector<VkDescriptorSet> vkDescriptorSets;
	vkDescriptorSets.resize(1);
	if (vkAllocateDescriptorSets(Vulkan::getLogicalDevice()->getVkDevice(), &allocInfo, vkDescriptorSets.data()) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to allocate Vulkan descriptor set!");
	}
	return VulkanDescriptorSet { vkDescriptorSets[0] };
}

std::vector<VulkanDescriptorSet> VulkanDescriptorPool::allocateDescriptorSets(const VulkanDescriptorSetLayout& descriptorSetLayout, size_t times) const
{
	VkDescriptorSetLayout vkLayout = descriptorSetLayout.getVkDescriptorSetLayout();
	std::vector<VkDescriptorSetLayout> vkLayouts(times, vkLayout);
	return allocateDescriptorSetsByVkLayouts(vkLayouts);
}

std::vector<VulkanDescriptorSet> VulkanDescriptorPool::allocateDescriptorSets(const std::vector<VulkanDescriptorSetLayout>& descriptorSetLayouts) const
{
	std::vector<VkDescriptorSetLayout> vkLayouts;
	for (const VulkanDescriptorSetLayout& descriptorSetLayout : descriptorSetLayouts)
	{
		vkLayouts.push_back(descriptorSetLayout.getVkDescriptorSetLayout());
	}
	return allocateDescriptorSetsByVkLayouts(vkLayouts);
}

std::vector<VulkanDescriptorSet> VulkanDescriptorPool::allocateDescriptorSetsByVkLayouts(const std::vector<VkDescriptorSetLayout>& vkLayouts) const
{
	VkDescriptorSetAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(vkLayouts.size());
	allocInfo.pSetLayouts = vkLayouts.data();

	std::vector<VkDescriptorSet> vkDescriptorSets;
	vkDescriptorSets.resize(vkLayouts.size());
	if (vkAllocateDescriptorSets(Vulkan::getLogicalDevice()->getVkDevice(), &allocInfo, vkDescriptorSets.data()) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to allocate Vulkan descriptor sets!");
	}

	std::vector<VulkanDescriptorSet> descriptorSets;
	for (const VkDescriptorSet& vkDescriptorSet : vkDescriptorSets)
	{
		descriptorSets.push_back(VulkanDescriptorSet { vkDescriptorSet });
	}
	return descriptorSets;
}

const VkDescriptorPool& VulkanDescriptorPool::getVkDescriptorPool() const
{
	return descriptorPool;
}

COLIBRI_END
