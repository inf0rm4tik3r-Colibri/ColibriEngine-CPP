#include "pch.h"
#include "VulkanShaderModule.h"

COLIBRI_BEGIN

VulkanShaderModule::VulkanShaderModule(const std::vector<char>& shaderByteCode)
{
	VkShaderModuleCreateInfo createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = shaderByteCode.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(shaderByteCode.data());
	if (vkCreateShaderModule(Vulkan::getLogicalDevice()->getVkDevice(), &createInfo, nullptr, &shaderModule) != VK_SUCCESS) {
		throw COLIBRI_EXCEPTION("Failed to create Vulkan shader module!");
	}
	LOG_INFO << "Created Vulkan shader module.";
}

VulkanShaderModule::~VulkanShaderModule()
{
	vkDestroyShaderModule(Vulkan::getLogicalDevice()->getVkDevice(), shaderModule, nullptr);
	LOG_INFO << "Destroyed Vulkan shader module.";
}

const VkShaderModule& VulkanShaderModule::getVkShaderModule() const
{
	return shaderModule;
}

COLIBRI_END
