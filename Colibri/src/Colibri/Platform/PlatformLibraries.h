#pragma once

#ifndef _PLATFORM_LIBRARIES_H_
#define _PLATFORM_LIBRARIES_H_

// Boost-Stacktrace must be included before GLFW to avoid a macro redefinition!
// That is done through our precompiled header.

#include <vulkan/vulkan.h>

#include <GLFW/glfw3.h>

#include "Vulkan/VulkanFunctionDef.h"

#endif // _PLATFORM_LIBRARIES_H_
