#pragma once

#ifndef _COLIBRI_EXCEPTION_H_
#define _COLIBRI_EXCEPTION_H_

#define NOMINMAX
#define BOOST_STACKTRACE_NO_NOMINMAX
#include <boost/stacktrace.hpp>

COLIBRI_BEGIN

class ColibriException : public std::runtime_error
{
public:
    explicit ColibriException(const char* file, int line, const std::string& message, const std::exception* causedBy = nullptr)
        : std::runtime_error(processMessage(file, line, message.c_str())),
        stacktrace(boost::stacktrace::stacktrace()),
        causedBy(causedBy)
    {}

    explicit ColibriException(const char* file, int line, const char* message, const std::exception* causedBy = nullptr)
        : std::runtime_error(processMessage(file, line, message)),
        stacktrace(boost::stacktrace::stacktrace()),
        causedBy(causedBy)
    {}

    virtual std::string getName() const
    {
        return "ColibriException";
    }

    const std::exception* getCausedBy() const
    {
        return causedBy;
    }

    const boost::stacktrace::stacktrace& getStacktrace() const
    {
        return stacktrace;
    }

private:
    const std::exception* causedBy = nullptr;
    const boost::stacktrace::stacktrace stacktrace;

    const std::string processMessage(const char* file, int line, const char* message)
    {
        return std::string(file)
            .append(":")
            .append(std::to_string(line))
            .append(" - ")
            .append(message);
    }
};

#define COLIBRI_EXCEPTION(message) ColibriException(__FILE__, __LINE__, message)
#define COLIBRI_EXCEPTION_WC(message, causedBy) ColibriException(__FILE__, __LINE__, message, causedBy)

COLIBRI_END

#endif // _COLIBRI_EXCEPTION_H_
