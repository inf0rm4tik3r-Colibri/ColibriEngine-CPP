#pragma once

#ifndef _UNSUPPORTED_OPERATION_EXCEPTION_H_
#define _UNSUPPORTED_OPERATION_EXCEPTION_H_

#include "../StaticConfig.h"
#include "../Tools/String.h"
#include "ColibriException.h"

COLIBRI_BEGIN

class UnsupportedOperationException : public ColibriException
{
public:
    explicit UnsupportedOperationException(const char* file, int line, const std::string& message, const std::exception* causedBy = nullptr)
        : ColibriException(file, line, message.c_str(), causedBy)
    {}

    explicit UnsupportedOperationException(const char* file, int line, const char* message, const std::exception* causedBy = nullptr)
        : ColibriException(file, line, message, causedBy)
    {}

    std::string getName()
    {
        return std::string("UnsupportedOperationException");
    }
};

#define UNSUPPORTED_OPERATION_EXCEPTION(message) UnsupportedOperationException(__FILE__, __LINE__, message)
#define UNSUPPORTED_OPERATION_EXCEPTION_WC(message, causedBy) UnsupportedOperationException(__FILE__, __LINE__, message, causedBy)

COLIBRI_END

#endif // _UNSUPPORTED_OPERATION_EXCEPTION_H_
