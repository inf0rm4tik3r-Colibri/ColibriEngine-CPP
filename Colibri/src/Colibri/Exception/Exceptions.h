#pragma once

#ifndef _EXCEPTIONS_H_
#define _EXCEPTIONS_H_

#include "ColibriException.h"
#include "IllegalArgumentException.h"
#include "IllegalStateException.h"
#include "UnsupportedOperationException.h"

#endif // _EXCEPTIONS_H_
