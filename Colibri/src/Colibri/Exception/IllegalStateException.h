#pragma once

#ifndef _ILLEGAL_STATE_EXCEPTION_H_
#define _ILLEGAL_STATE_EXCEPTION_H_

#include "ColibriException.h"

COLIBRI_BEGIN

class IllegalStateException : public ColibriException
{
public:
    explicit IllegalStateException(const char* file, int line, const std::string& message, const std::exception* causedBy = nullptr)
        : ColibriException(file, line, message.c_str(), causedBy)
    {}

    explicit IllegalStateException(const char* file, int line, const char* message, const std::exception* causedBy = nullptr)
        : ColibriException(file, line, message, causedBy)
    {}

    std::string getName()
    {
        return std::string("IllegalStateException");
    }
};

#define ILLEGAL_STATE_EXCEPTION(message) IllegalStateException(__FILE__, __LINE__, message)
#define ILLEGAL_STATE_EXCEPTION_WC(message, causedBy) IllegalStateException(__FILE__, __LINE__, message, causedBy)

COLIBRI_END

#endif // _ILLEGAL_STATE_EXCEPTION_H_
