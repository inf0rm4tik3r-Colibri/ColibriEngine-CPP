#pragma once

#ifndef _ILLEGAL_ARGUMENT_EXCEPTION_H_
#define _ILLEGAL_ARGUMENT_EXCEPTION_H_

#include "ColibriException.h"

COLIBRI_BEGIN

class IllegalArgumentException : public ColibriException
{
public:
    explicit IllegalArgumentException(const char* file, int line, const std::string& message, const std::exception* causedBy = nullptr)
        : ColibriException(file, line, message.c_str(), causedBy)
    {}

    explicit IllegalArgumentException(const char* file, int line, const char* message, const std::exception* causedBy = nullptr)
        : ColibriException(file, line, message, causedBy)
    {}

    std::string getName()
    {
        return std::string("IllegalArgumentException");
    }
};

#define ILLEGAL_ARGUMENT_EXCEPTION(message) IllegalArgumentException(__FILE__, __LINE__, message)
#define ILLEGAL_ARGUMENT_EXCEPTION_WC(message, causedBy) IllegalArgumentException(__FILE__, __LINE__, message, causedBy)

COLIBRI_END

#endif // _ILLEGAL_ARGUMENT_EXCEPTION_H_
