#pragma once

#ifndef _OBSERVER_H_
#define _OBSERVER_H_

#include "Observer.fwd.h"
#include "Subject.fwd.h"
#include "Subject.h"

COLIBRI_BEGIN

template<typename... T>
class Observer
{
public:
	virtual void next(T... value) const = 0;
	virtual void error(T... error) const = 0;
	virtual void complete() const = 0;
};

COLIBRI_END

#endif // _OBSERVER_H_
