#pragma once

#ifndef _OBSERVER_FWD_H_
#define _OBSERVER_FWD_H_

COLIBRI_BEGIN

template<typename... T>
class Observer;

COLIBRI_END

#endif // _OBSERVER_FWD_H_
