#pragma once

#ifndef _OBSERVABLE_H_
#define _OBSERVABLE_H_

#include "Observable.fwd.h"
#include "Subject.fwd.h"
#include "Subject.h"
#include "Observer.fwd.h"
#include "Observer.h"

COLIBRI_BEGIN

template<typename... T>
class Observable
{
private:
    const std::function<void(const Observer<T...>&)> m_subscribe;

protected:
    bool completed = false;

public:
    explicit Observable(const std::function<void(const Observer<T...>&)>&& subscribe) noexcept
        : m_subscribe(subscribe)
    {
    }

    /*
     * Subscribe with a custom observer implementation.
     */
    virtual void subscribe(const std::shared_ptr<Observer<T...>> observer)
    {
        m_subscribe(*observer.get());
    }
    
    /*
     * Subscribe with a next callback.
     */
    virtual std::shared_ptr<Observer<T...>> subscribe(std::function<void(T...)>&& nextImpl)
    {
        std::shared_ptr<Observer<T...>> observer = createObserver(std::forward<std::function<void(T...)>>(nextImpl));
        m_subscribe(*observer.get());
        return observer;
    }

    /*
     * Subscribe with a next, error and complete callback.
     */
    virtual std::shared_ptr<Observer<T...>> subscribe(std::function<void(T...)>&& nextImpl,
                                                   std::function<void(T...)>&& errorImpl,
                                                   std::function<void()>&& completeImpl)
    {
        std::shared_ptr<Observer<T...>> observer = createObserver(std::forward<std::function<void(T...)>>(nextImpl),
                                                               std::forward<std::function<void(T...)>>(errorImpl),
                                                               std::forward<std::function<void()>>(completeImpl));
        m_subscribe(*observer.get());
        return observer;
    }

protected:
    std::shared_ptr<Observer<T...>> createObserver(std::function<void(T...)>&& nextImpl)
    {
        class PartialObserverImpl : public Observer<T...>
        {
        public:
            const std::function<void(T...)> m_NextImpl;

            PartialObserverImpl(std::function<void(T...)>&& nextImpl)
                : Observer<T...>(),
                m_NextImpl(std::move(nextImpl))
            {}

            void next(T... value) const override
            {
                m_NextImpl(value...);
            }
            void error(T... value) const override
            {
            }
            void complete() const override
            {
            }
        };
        return std::make_shared<PartialObserverImpl>(std::forward<std::function<void(T...)>>(nextImpl));
    }

    std::shared_ptr<Observer<T...>> createObserver(std::function<void(T...)>&& nextImpl,
                                                std::function<void(T...)>&& errorImpl,
                                                std::function<void()>&& completeImpl)
    {
        class CompleteObserverImpl : public Observer<T...>
        {
        public:
            const std::function<void(T...)> m_NextImpl;
            const std::function<void(T...)> m_ErrorImpl;
            const std::function<void()> m_CompleteImpl;

            CompleteObserverImpl(std::function<void(T...)>&& nextImpl,
                                 std::function<void(T...)>&& errorImpl,
                                 std::function<void()>&& completeImpl)
                : Observer<T...>(),
                m_NextImpl(std::move(nextImpl)),
                m_ErrorImpl(std::move(errorImpl)),
                m_CompleteImpl(std::move(completeImpl))
            {}

            void next(T... value) const override
            {
                m_NextImpl(value...);
            }
            void error(T... value) const override
            {
                m_ErrorImpl(value...);
            }
            void complete() const override
            {
                m_CompleteImpl();
            }
        };
        return std::make_shared<CompleteObserverImpl>(std::forward<std::function<void(T...)>>(nextImpl),
                                                      std::forward<std::function<void(T...)>>(errorImpl),
                                                      std::forward<std::function<void()>>(completeImpl));
    }
};

COLIBRI_END

#endif // _OBSERVABLE_H_
