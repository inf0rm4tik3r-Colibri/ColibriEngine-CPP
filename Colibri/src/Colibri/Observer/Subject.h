#pragma once

#ifndef _SUBJECT_H_
#define _SUBJECT_H_

#include "Subject.fwd.h"
#include "Observer.fwd.h"
#include "Observer.h"
#include "Observable.fwd.h"
#include "Observable.h"

COLIBRI_BEGIN

template<typename... T>
class Subject : public Observable<T...>
{
private:
    std::vector<std::shared_ptr<Observer<T...>>> observers;

public:
    // A subject is an observable which does not provide a meaningful subscribe function.
    // A subject is an event emitter and will multicast to all registered observers.
    Subject() noexcept
        : Observable<T...>([](const Observer<T...>&) {}) // TODO: performance?
    {
    }

    ~Subject() noexcept
    {
        observers.clear();
    }

    /*
     * Subscribe with a custom observer implementation.
     */
    void subscribe(const std::shared_ptr<Observer<T...>> observer) override
    {
        assert(std::find(observers.begin(), observers.end(), observer) == observers.end());
        observers.push_back(observer);
    }

    /*
     * Subscribe with a next callback.
     */
    std::shared_ptr<Observer<T...>> subscribe(std::function<void(T...)>&& nextImpl) override
    {
        std::shared_ptr<Observer<T...>> observer = std::move(Observable<T...>::createObserver(std::forward<std::function<void(T...)>>(nextImpl)));
        observers.push_back(observer);
        return observer;
    }

    /*
     * Subscribe with a next, error and complete callback.
     */
    std::shared_ptr<Observer<T...>> subscribe(std::function<void(T...)>&& nextImpl,
                                           std::function<void(T...)>&& errorImpl,
                                           std::function<void()>&& completeImpl) override
    {
        std::shared_ptr<Observer<T...>> observer = std::move(Observable<T...>::createObserver(std::forward<std::function<void(T...)>>(nextImpl),
                                                                                        std::forward<std::function<void(T...)>>(errorImpl),
                                                                                        std::forward<std::function<void()>>(completeImpl)));
        observers.push_back(observer);
        return observer;
    }

    /*
     * Removes the specified observer from this subjects list of subscribed observers.
     * The given observer must be subscribed to this subject.
     */
    void unsubscribe(const std::shared_ptr<Observer<T...>>& observer)
    {
        assert(std::find(observers.begin(), observers.end(), observer) != observers.end());
        observers.erase(std::remove(observers.begin(), observers.end(), observer), observers.end());
    }

    /*
     * Broadcasts a new value to all observers.
     */
    void next(T... value)
    {
        if (!Observable<T...>::completed)
        {
            for (const std::shared_ptr<Observer<T...>>& observer : observers)
            {
                observer.get()->next(value...);
            }
        }
    }

    /*
     * Broadcasts an error to all observers.
     * After an error, this subject is completed an no more values and no "error" or "complete" call will be broadcasted.
     */
    void error(T... error)
    {
        if (!Observable<T...>::completed)
        {
            for (const std::shared_ptr<Observer<T...>>& observer : observers)
            {
                observer.get()->error(error...);
            }
            Observable<T...>::completed = true;
        }
    }

    /*
     * Broadcasts the completed signal to all observers.
     * After that signal, this subject is completed an no more values and no "error" or "complete" call will be broadcasted.
     */
    void complete()
    {
        if (!Observable<T...>::completed)
        {
            for (const std::shared_ptr<Observer<T...>>& observer : observers)
            {
                observer.get()->complete();
            }
            Observable<T...>::completed = true;
        }
    }
};

COLIBRI_END

#endif // _SUBJECT_H_
