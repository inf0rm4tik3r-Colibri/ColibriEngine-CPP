#pragma once

#ifndef _SUBJECT_FWD_H_
#define _SUBJECT_FWD_H_

COLIBRI_BEGIN

template<typename... T>
class Subject;

COLIBRI_END

#endif // _SUBJECT_FWD_H_
