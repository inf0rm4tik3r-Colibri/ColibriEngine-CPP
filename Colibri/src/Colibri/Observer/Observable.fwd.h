#pragma once

#ifndef _OBSERVABLE_FWD_H_
#define _OBSERVABLE_FWD_H_

COLIBRI_BEGIN

template<typename... T>
class Observable;

COLIBRI_END

#endif // _OBSERVABLE_FWD_H_
