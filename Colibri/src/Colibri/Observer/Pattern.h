#pragma once

#ifndef _OBSERVABLE_PATTERN_H
#define _OBSERVABLE_PATTERN_H

#include "Observable.h"
#include "Subject.h"
#include "Observer.h"

#endif // _OBSERVABLE_PATTERN_H
