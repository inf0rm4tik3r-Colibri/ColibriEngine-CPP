#pragma once

#ifndef _MATH_TYPES_H_
#define _MATH_TYPES_H_

COLIBRI_BEGIN

template<size_t S>
using StaticVectorB = StaticVector<S, bool, false, true>;

template<size_t S>
using StaticVectorI = StaticVector<S, int, 0, 1>;

template<size_t S>
using StaticVectorL = StaticVector<S, long, 0L, 1L>;

template<size_t S>
using StaticVectorF = StaticVector<S, float, 0.0f, 1.0f>;

template<size_t S>
using StaticVectorD = StaticVector<S, double, 0.0, 1.0>;

using Vec1b = StaticVectorB<1>;
using Vec1i = StaticVectorI<1>;
using Vec1l = StaticVectorL<1>;
using Vec1f = StaticVectorF<1>;
using Vec1d = StaticVectorD<1>;

using Vec2b = StaticVectorB<2>;
using Vec2i = StaticVectorI<2>;
using Vec2l = StaticVectorL<2>;
using Vec2f = StaticVectorF<2>;
using Vec2d = StaticVectorD<2>;

using Vec3b = StaticVectorB<3>;
using Vec3i = StaticVectorI<3>;
using Vec3l = StaticVectorL<3>;
using Vec3f = StaticVectorF<3>;
using Vec3d = StaticVectorD<3>;

using Vec4b = StaticVectorB<4>;
using Vec4i = StaticVectorI<4>;
using Vec4l = StaticVectorL<4>;
using Vec4f = StaticVectorF<4>;
using Vec4d = StaticVectorD<4>;

template<size_t R, size_t C>
using StaticMatrixB = StaticMatrix<R, C, bool, false, true>;

template<size_t R, size_t C>
using StaticMatrixI = StaticMatrix<R, C, int, 0, 1>;

template<size_t R, size_t C>
using StaticMatrixL = StaticMatrix<R, C, long, 0L, 1L>;

template<size_t R, size_t C>
using StaticMatrixF = StaticMatrix<R, C, float, 0.0f, 1.0f>;

template<size_t R, size_t C>
using StaticMatrixD = StaticMatrix<R, C, double, 0.0, 1.0>;

using Mat1b = StaticMatrixB<1, 1>;
using Mat1i = StaticMatrixI<1, 1>;
using Mat1l = StaticMatrixL<1, 1>;
using Mat1f = StaticMatrixF<1, 1>;
using Mat1d = StaticMatrixD<1, 1>;

using Mat2b = StaticMatrixB<2, 2>;
using Mat2i = StaticMatrixI<2, 2>;
using Mat2l = StaticMatrixL<2, 2>;
using Mat2f = StaticMatrixF<2, 2>;
using Mat2d = StaticMatrixD<2, 2>;

using Mat3b = StaticMatrixB<3, 3>;
using Mat3i = StaticMatrixI<3, 3>;
using Mat3l = StaticMatrixL<3, 3>;
using Mat3f = StaticMatrixF<3, 3>;
using Mat3d = StaticMatrixD<3, 3>;

using Mat4b = StaticMatrixB<4, 4>;
using Mat4i = StaticMatrixI<4, 4>;
using Mat4l = StaticMatrixL<4, 4>;
using Mat4f = StaticMatrixF<4, 4>;
using Mat4d = StaticMatrixD<4, 4>;

using QuaternionF = Quaternion<float, 0.0f, 1.0f>;
using QuaternionD = Quaternion<double, 0.0, 1.0>;

COLIBRI_END

#endif // _MATH_TYPES_H_
