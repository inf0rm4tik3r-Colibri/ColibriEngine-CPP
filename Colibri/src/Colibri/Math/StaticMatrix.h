#pragma once

#ifndef _MATH_STATIC_MATRIX_H_
#define _MATH_STATIC_MATRIX_H_

#include "StaticVector.h"

COLIBRI_BEGIN

template<size_t R, size_t C, typename T, T T_ZERO, T T_ONE>
class StaticMatrix {
public:
	/* DATA */

	T data[C][R];

	/* CONSTRUCTORS */

	StaticMatrix<R, C, T, T_ZERO, T_ONE>() noexcept
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] = T_ZERO;
			}
		}
	}

	// Raw value constructors.
	StaticMatrix<R, C, T, T_ZERO, T_ONE>(const T m00 /* Row 1 */) noexcept
	{
		static_assert(R == 1 && C == 1, "Constructor only applicable for 1x1 matrices!");
		// Column 1
		data[0][0] = m00;
	}

	StaticMatrix<R, C, T, T_ZERO, T_ONE>(const T m00, const T m10, /* Row 1 */
										 const T m01, const T m11  /* Row 2 */) noexcept
	{
		static_assert(R == 2 && C == 2, "Constructor only applicable for 2x2 matrices!");
		// Column 1
		data[0][0] = m00;
		data[0][1] = m01;
		// Column 2
		data[1][0] = m10;
		data[1][1] = m11;
	}

	StaticMatrix<R, C, T, T_ZERO, T_ONE>(const T m00, const T m10, const T m20, /* Row 1 */
										 const T m01, const T m11, const T m21, /* Row 2 */
										 const T m02, const T m12, const T m22  /* Row 3 */) noexcept
	{
		static_assert(R == 3 && C == 3, "Constructor only applicable for 3x3 matrices!");
		// Column 1
		data[0][0] = m00;
		data[0][1] = m01;
		data[0][2] = m02;
		// Column 2
		data[1][0] = m10;
		data[1][1] = m11;
		data[1][2] = m12;
		// Column 3
		data[2][0] = m20;
		data[2][1] = m21;
		data[2][2] = m22;
	}

	StaticMatrix<R, C, T, T_ZERO, T_ONE>(const T m00, const T m10, const T m20, const T m30, /* Column 1 */
										 const T m01, const T m11, const T m21, const T m31, /* Column 2 */
										 const T m02, const T m12, const T m22, const T m32, /* Column 3 */
										 const T m03, const T m13, const T m23, const T m33  /* Column 4 */) noexcept
	{
		static_assert(R == 4 && C == 4, "Constructor only applicable for 4x4 matrices!");
		// Column 1
		data[0][0] = m00;
		data[0][1] = m01;
		data[0][2] = m02;
		data[0][3] = m03;
		// Column 2
		data[1][0] = m10;
		data[1][1] = m11;
		data[1][2] = m12;
		data[1][3] = m13;
		// Column 3
		data[2][0] = m20;
		data[2][1] = m21;
		data[2][2] = m22;
		data[2][3] = m23;
		// Column 4
		data[3][0] = m30;
		data[3][1] = m31;
		data[3][2] = m32;
		data[3][3] = m33;
	}

	// Construct from column definitions.
	StaticMatrix<R, C, T, T_ZERO, T_ONE>(const StaticVector<2, T, T_ZERO, T_ONE>& c1, /* Column 1 */
										 const StaticVector<2, T, T_ZERO, T_ONE>& c2  /* Column 2 */) noexcept
	{
		static_assert(R == 2 && C == 2, "Constructor only applicable for 2x2 matrices!");
		// Column 1
		data[0][0] = c1.x();
		data[0][1] = c1.y();
		// Column 2
		data[1][0] = c2.x();
		data[1][1] = c2.y();
	}

	StaticMatrix<R, C, T, T_ZERO, T_ONE>(const StaticVector<3, T, T_ZERO, T_ONE>& c1, /* Column 1 */
										 const StaticVector<3, T, T_ZERO, T_ONE>& c2, /* Column 2 */
										 const StaticVector<3, T, T_ZERO, T_ONE>& c3  /* Column 3 */) noexcept
	{
		static_assert(R == 3 && C == 3, "Constructor only applicable for 3x3 matrices!");
		// Column 1
		data[0][0] = c1.x();
		data[0][1] = c1.y();
		data[0][2] = c1.z();
		// Column 2
		data[1][0] = c2.x();
		data[1][1] = c2.y();
		data[1][2] = c2.z();
		// Column 3
		data[2][0] = c3.x();
		data[2][1] = c3.y();
		data[2][2] = c3.z();
	}

	StaticMatrix<R, C, T, T_ZERO, T_ONE>(const StaticVector<4, T, T_ZERO, T_ONE>& c1, /* Column 1 */
										 const StaticVector<4, T, T_ZERO, T_ONE>& c2, /* Column 2 */
										 const StaticVector<4, T, T_ZERO, T_ONE>& c3, /* Column 3 */
										 const StaticVector<4, T, T_ZERO, T_ONE>& c4  /* Column 4 */) noexcept
	{
		static_assert(R == 4 && C == 4, "Constructor only applicable for 4x4 matrices!");
		// Column 1
		data[0][0] = c1.x();
		data[0][1] = c1.y();
		data[0][2] = c1.z();
		data[0][3] = c1.w();
		// Column 2
		data[1][0] = c2.x();
		data[1][1] = c2.y();
		data[1][2] = c2.z();
		data[1][3] = c2.w();
		// Column 3
		data[2][0] = c3.x();
		data[2][1] = c3.y();
		data[2][2] = c3.z();
		data[2][3] = c3.w();
		// Column 4
		data[3][0] = c4.x();
		data[3][1] = c4.y();
		data[3][2] = c4.z();
		data[3][3] = c4.w();
	}

	// Copy constructor.
	StaticMatrix<R, C, T, T_ZERO, T_ONE>(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& other) noexcept
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] = other.data[c][r];
			}
		}
	}

	// Move constructor.
	StaticMatrix<R, C, T, T_ZERO, T_ONE>(StaticMatrix<R, C, T, T_ZERO, T_ONE>&& other) noexcept
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] = std::move(other.data[c][r]);
			}
		}
	}

	/* INITIALIZERS */
	
	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& initZero()
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] = T_ZERO;
			}
		}
		return *this;
	}

	inline static StaticMatrix<R, C, T, T_ZERO, T_ONE> makeZero()
	{
		return StaticMatrix<R, C, T, T_ZERO, T_ONE>().initZero();
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& initIdentity()
	{
		static_assert(R == C, "Identity initialization only available on squared matrices.");
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] = c == r ? T_ONE : T_ZERO;
			}
		}
		return *this;
	}
	
	inline static StaticMatrix<R, C, T, T_ZERO, T_ONE> makeIdentity()
	{
		static_assert(R == C, "Identity initialization only available on squared matrices.");
		return StaticMatrix<R, C, T, T_ZERO, T_ONE>().initIdentity();
	}
	
	inline StaticMatrix<4, 4, T, T_ZERO, T_ONE>& initBias()
	{
		static_assert(R == 4 && C == 4, "Biasd matrix is only defined for 4x4 matrices.");
		static_assert(std::is_floating_point<T>::value, "Biasd matrix is only defined for floating point data.");
		T half = (T)(T_ONE / 2.0);
		// Column 1
		data[0][0] = half;
		data[0][1] = T_ZERO;
		data[0][2] = T_ZERO;
		data[0][3] = T_ZERO;
		// Column 2
		data[1][0] = T_ZERO;
		data[1][1] = half;
		data[1][2] = T_ZERO;
		data[1][3] = T_ZERO;
		// Column 3
		data[2][0] = T_ZERO;
		data[2][1] = T_ZERO;
		data[2][2] = half;
		data[2][3] = T_ZERO;
		// Column 4
		data[3][0] = half;
		data[3][1] = half;
		data[3][2] = half;
		data[3][3] = T_ONE;
		return *this;
	}

	inline static StaticMatrix<4, 4, T, T_ZERO, T_ONE> makeBias()
	{
		static_assert(R == 4 && C == 4, "Biasd matrix is only defined for 4x4 matrices.");
		static_assert(std::is_floating_point<T>::value, "Biasd matrix is only defined for floating point data.");
		return StaticMatrix<4, 4, T, T_ZERO, T_ONE>().initBias();
	}

	inline StaticMatrix<4, 4, T, T_ZERO, T_ONE>& initTranslation(const StaticVector<3, T, T_ZERO, T_ONE>& translation)
	{
		static_assert(R == 4 && C == 4, "Translation matrix is only defined for 4x4 matrices.");
		// Column 1
		data[0][0] = T_ONE;
		data[0][1] = T_ZERO;
		data[0][2] = T_ZERO;
		data[0][3] = T_ZERO;
		// Column 2
		data[1][0] = T_ZERO;
		data[1][1] = T_ONE;
		data[1][2] = T_ZERO;
		data[1][3] = T_ZERO;
		// Column 3
		data[2][0] = T_ZERO;
		data[2][1] = T_ZERO;
		data[2][2] = T_ONE;
		data[2][3] = T_ZERO;
		// Column 4
		data[3][0] = translation.x();
		data[3][1] = translation.y();
		data[3][2] = translation.z();
		data[3][3] = T_ONE;
		return *this;
	}

	inline static StaticMatrix<4, 4, T, T_ZERO, T_ONE> makeTranslation(const StaticVector<3, T, T_ZERO, T_ONE>& translation)
	{
		static_assert(R == 4 && C == 4, "Translation matrix is only defined for 4x4 matrices.");
		return StaticMatrix<4, 4, T, T_ZERO, T_ONE>().initTranslation(translation);
	}

	// Rotation.
	inline StaticMatrix<3, 3, T, T_ZERO, T_ONE>& initRotationX(const double angleAroundX)
	{
		static_assert(R == 3 && C == 3, "Basix rotation matrix is only defined for 3x3 matrices.");
		const double angleAroundXInRad = Math::toRadians(angleAroundX);
		const double sinX = std::sin(angleAroundXInRad);
		const double cosX = std::cos(angleAroundXInRad);
		return set({
			 T_ONE,  T_ZERO,  T_ZERO,
			T_ZERO,    cosX,    sinX,
			T_ZERO,   -sinX,    cosX
		});
	}

	inline StaticMatrix<3, 3, T, T_ZERO, T_ONE>& initRotationY(const double angleAroundY)
	{
		static_assert(R == 3 && C == 3, "Basix rotation matrix is only defined for 3x3 matrices.");
		const double angleAroundYInRad = Math::toRadians(angleAroundY);
		const double sinY = std::sin(angleAroundYInRad);
		const double cosY = std::cos(angleAroundYInRad);
		return set({
			  cosY,  T_ZERO,   -sinY,
			T_ZERO,   T_ONE,  T_ZERO,
			  sinY,  T_ZERO,    cosY
		});
	}

	inline StaticMatrix<3, 3, T, T_ZERO, T_ONE>& initRotationZ(const double angleAroundZ)
	{
		static_assert(R == 3 && C == 3, "Basix rotation matrix is only defined for 3x3 matrices.");
		const double angleAroundZInRad = Math::toRadians(angleAroundZ);
		const double sinZ = std::sin(angleAroundZInRad);
		const double cosZ = std::cos(angleAroundZInRad);
		return set({
			  cosZ,   -sinZ,  T_ZERO,
			  sinZ,    cosZ,  T_ZERO,
			T_ZERO,  T_ZERO,   T_ONE
		});
	}

	inline StaticMatrix<3, 3, T, T_ZERO, T_ONE>& initRotation(const double angleAroundX,
															  const double angleAroundY,
															  const double angleAroundZ)
	{
		static_assert(R == 3 && C == 3, "Basix rotation matrix is only defined for 3x3 matrices.");
		const double angleAroundXInRad = Math::toRadians(angleAroundX);
		const double angleAroundYInRad = Math::toRadians(angleAroundY);
		const double angleAroundZInRad = Math::toRadians(angleAroundZ);

		const double sinX = std::sin(angleAroundXInRad);
		const double cosX = std::cos(angleAroundXInRad);
		const double sinY = std::sin(angleAroundYInRad);
		const double cosY = std::cos(angleAroundYInRad);
		const double sinZ = std::sin(angleAroundZInRad);
		const double cosZ = std::cos(angleAroundZInRad);

		return set({
			 cosY * cosZ,						cosY * -sinZ,						-sinY,
             sinX * sinY * cosZ + cosX * sinZ,	sinX * sinY * -sinZ + cosX * cosZ,	sinX * cosY,
			 cosX * sinY * cosZ - sinX * sinZ,	cosX * sinY * -sinZ - sinX * cosZ,	cosX * cosY
		});
	}

	inline StaticMatrix<3, 3, T, T_ZERO, T_ONE>& initRotation(const StaticVector<3, T, T_ZERO, T_ONE>& axis,
															  const double angle)
	{
		// 											 (+0,  -Rz, +Ry)
		// M = r*rT + cos(a) * (I - r*rT) + sin(a) * (+Rz, +0,  -Rx)
		//											 (-Ry, +Rx, +0 )

		const StaticVector<3, T, T_ZERO, T_ONE> normalizedAxis = axis.normalized();

		const StaticMatrix<3, 3, T, T_ZERO, T_ONE> axis
		{
			        0, -axis.z(), +axis.y(),
			+axis.z(),         0, -axis.x(),
			-axis.y(), +axis.x(),         0
		};

		const double xx = normalizedAxis.x() * normalizedAxis.x();
		const double xy = normalizedAxis.x() * normalizedAxis.y();
		const double xz = normalizedAxis.x() * normalizedAxis.z();
		const double yy = normalizedAxis.y() * normalizedAxis.y();
		const double yz = normalizedAxis.y() * normalizedAxis.z();
		const double zz = normalizedAxis.z() * normalizedAxis.z();

		// r*r^T
		set({
			xx, xy, xz,
			xy, yy, yz,
			xz, yz, zz
		});

		const double angleInRad = Math::toRadians(angle);
		const double sin = std::sin(angleInRad);
		const double cos = std::cos(angleInRad);

		const StaticMatrix<3, 3, T, T_ZERO, T_ONE> term1 = StaticMatrix<3, 3, T, T_ZERO, T_ONE>().initIdentity().minus(*this).times(cos);
		const StaticMatrix<3, 3, T, T_ZERO, T_ONE> term2 = axis.times(sin);

		add(term1).add(term2);

		return this;
	}

	/* OPERATORS */

	// Copy assignment.
	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& operator=(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& other) noexcept
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] = other.data[c][r];
			}
		}
		return *this;
	}

	// Move assignment.
	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& operator=(StaticMatrix<R, C, T, T_ZERO, T_ONE>&& other) noexcept
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] = std::move(other.data[c][r]);
			}
		}
		return *this;
	}

	// Subscript.
	inline StaticVector<R, T, T_ZERO, T_ONE>& operator[](int columnIndex)
	{
		return (*reinterpret_cast<StaticVector<R, T, T_ZERO, T_ONE>*>(data[columnIndex]));
	}

	inline const StaticVector<R, T, T_ZERO, T_ONE>& operator[](int columnIndex) const
	{
		return (*reinterpret_cast<const StaticVector<R, T, T_ZERO, T_ONE>*>(data[columnIndex]));
	}

	/*
	inline ArrayAccessProxy<T> operator[](int index)
	{
		return ArrayAccessProxy<T>(data[index]);
	}
	*/

	// Equality
	inline friend bool operator==(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& a,
								  const StaticMatrix<R, C, T, T_ZERO, T_ONE>& b)
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				if (a.data[c][r] != b.data[c][r])
				{
					return false;
				}
			}
		}
		return true;
	}

	inline friend bool operator!=(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& a,
								  const StaticMatrix<R, C, T, T_ZERO, T_ONE>& b)
	{
		return !(a == b);
	}

	// Addition.
	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> add(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& other) const
	{
		StaticMatrix<R, C, T, T_ZERO, T_ONE> result;
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				result.data[c][r] = data[c][r] + other.data[c][r];
			}
		}
		return result;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> operator+(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& rhs) const
	{
		return add(rhs);
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> add(const T& other) const
	{
		StaticMatrix<R, C, T, T_ZERO, T_ONE> result;
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				result.data[c][r] = data[c][r] + other;
			}
		}
		return result;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> operator+(const T& rhs) const
	{
		return add(rhs);
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& addInplace(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& other) const
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] += other.data[c][r];
			}
		}
		return *this;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& operator+=(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& rhs) const
	{
		return addInplace(rhs);
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& addInplace(const T& other) const
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] += other;
			}
		}
		return *this;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& operator+=(const T& rhs) const
	{
		return addInplace(rhs);
	}

	// Subtraction.
	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> subtract(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& other) const
	{
		StaticMatrix<R, C, T, T_ZERO, T_ONE> result;
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				result.data[c][r] = data[c][r] - other.data[c][r];
			}
		}
		return result;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> operator-(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& rhs) const
	{
		return subtract(rhs);
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> subtract(const T& other) const
	{
		StaticMatrix<R, C, T, T_ZERO, T_ONE> result;
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				result.data[c][r] = data[c][r] - other;
			}
		}
		return result;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> operator-(const T& rhs) const
	{
		return subtract(rhs);
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& subtractInplace(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& other) const
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] -= other.data[c][r];
			}
		}
		return *this;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& operator-=(const StaticMatrix<R, C, T, T_ZERO, T_ONE>& rhs) const
	{
		return subtractInplace(rhs);
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& subtractInplace(const T& other) const
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] -= other;
			}
		}
		return *this;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& operator-=(const T& rhs) const
	{
		return subtractInplace(rhs);
	}

	// Scalar multiplication.
	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> multiply(const T& other) const
	{
		StaticMatrix<R, C, T, T_ZERO, T_ONE> result;
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				result.data[c][r] = data[c][r] * other;
			}
		}
		return result;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE> operator*(const T& rhs) const
	{
		return multiply(rhs);
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& multiplyInplace(const T& other) const
	{
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				data[c][r] *= other;
			}
		}
		return *this;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& operator*=(const T& rhs) const
	{
		return multiplyInplace(rhs);
	}

	// Matrx Matrix multiplication.
	template<size_t R2, size_t C2>
	inline StaticMatrix<R, C2, T, T_ZERO, T_ONE> multiply(const StaticMatrix<R2, C2, T, T_ZERO, T_ONE>& other) const
	{
		static_assert(C == R2, "StaticMatrixrices are not eligible for multiplication.");

		if constexpr (R == C && R2 == C2 && R == 2)
		{
			return StaticMatrix<2, 2, T, T_ZERO, T_ONE>(
				data[0][0] * other.data[0][0] + data[1][0] * other.data[0][1], /* m00 */
				data[0][0] * other.data[1][0] + data[1][0] * other.data[1][1], /* m10 */
				data[0][1] * other.data[0][0] + data[1][1] * other.data[0][1], /* m01 */
				data[0][1] * other.data[1][0] + data[1][1] * other.data[1][1]  /* m11 */
			);
		}
		else if constexpr (R == C && R2 == C2 && R == 3)
		{
			return StaticMatrix<3, 3, T, T_ZERO, T_ONE>(
				data[0][0] * other.data[0][0] + data[1][0] * other.data[0][1] + data[2][0] * other.data[0][2], /* m00 */
				data[0][0] * other.data[1][0] + data[1][0] * other.data[1][1] + data[2][0] * other.data[1][2], /* m10 */
				data[0][0] * other.data[2][0] + data[1][0] * other.data[2][1] + data[2][0] * other.data[2][2], /* m20 */
				data[0][1] * other.data[0][0] + data[1][1] * other.data[0][1] + data[2][1] * other.data[0][2], /* m01 */
				data[0][1] * other.data[1][0] + data[1][1] * other.data[1][1] + data[2][1] * other.data[1][2], /* m11 */
				data[0][1] * other.data[2][0] + data[1][1] * other.data[2][1] + data[2][1] * other.data[2][2], /* m21 */
				data[0][2] * other.data[0][0] + data[1][2] * other.data[0][1] + data[2][2] * other.data[0][2], /* m02 */
				data[0][2] * other.data[1][0] + data[1][2] * other.data[1][1] + data[2][2] * other.data[1][2], /* m12 */
				data[0][2] * other.data[2][0] + data[1][2] * other.data[2][1] + data[2][2] * other.data[2][2]  /* m22 */
			);
		}
		else if constexpr (R == C && R2 == C2 && R == 4)
		{
			return StaticMatrix<4, 4, T, T_ZERO, T_ONE>(
				data[0][0] * other.data[0][0] + data[1][0] * other.data[0][1] + data[2][0] * other.data[0][2] + data[3][0] * other.data[0][3], /* m00 */
				data[0][0] * other.data[1][0] + data[1][0] * other.data[1][1] + data[2][0] * other.data[1][2] + data[3][0] * other.data[1][3], /* m10 */
				data[0][0] * other.data[2][0] + data[1][0] * other.data[2][1] + data[2][0] * other.data[2][2] + data[3][0] * other.data[2][3], /* m20 */
				data[0][0] * other.data[3][0] + data[1][0] * other.data[3][1] + data[2][0] * other.data[3][2] + data[3][0] * other.data[3][3], /* m30 */
				data[0][1] * other.data[0][0] + data[1][1] * other.data[0][1] + data[2][1] * other.data[0][2] + data[3][1] * other.data[0][3], /* m01 */
				data[0][1] * other.data[1][0] + data[1][1] * other.data[1][1] + data[2][1] * other.data[1][2] + data[3][1] * other.data[1][3], /* m11 */
				data[0][1] * other.data[2][0] + data[1][1] * other.data[2][1] + data[2][1] * other.data[2][2] + data[3][1] * other.data[2][3], /* m21 */
				data[0][1] * other.data[3][0] + data[1][1] * other.data[3][1] + data[2][1] * other.data[3][2] + data[3][1] * other.data[3][3], /* m31 */
				data[0][2] * other.data[0][0] + data[1][2] * other.data[0][1] + data[2][2] * other.data[0][2] + data[3][2] * other.data[0][3], /* m02 */
				data[0][2] * other.data[1][0] + data[1][2] * other.data[1][1] + data[2][2] * other.data[1][2] + data[3][2] * other.data[1][3], /* m12 */
				data[0][2] * other.data[2][0] + data[1][2] * other.data[2][1] + data[2][2] * other.data[2][2] + data[3][2] * other.data[2][3], /* m22 */
				data[0][2] * other.data[3][0] + data[1][2] * other.data[3][1] + data[2][2] * other.data[3][2] + data[3][2] * other.data[3][3], /* m32 */
				data[0][3] * other.data[0][0] + data[1][3] * other.data[0][1] + data[2][3] * other.data[0][2] + data[3][3] * other.data[0][3], /* m03 */
				data[0][3] * other.data[1][0] + data[1][3] * other.data[1][1] + data[2][3] * other.data[1][2] + data[3][3] * other.data[1][3], /* m13 */
				data[0][3] * other.data[2][0] + data[1][3] * other.data[2][1] + data[2][3] * other.data[2][2] + data[3][3] * other.data[2][3], /* m23 */
				data[0][3] * other.data[3][0] + data[1][3] * other.data[3][1] + data[2][3] * other.data[3][2] + data[3][3] * other.data[3][3]  /* m33 */
			);
		}
		else
		{
			StaticMatrix<R, C2, T, T_ZERO, T_ONE> result;
			for (int c = 0; c < C2; c++) {
				for (int r = 0; r < R; r++) {
					T intermediate = 0;
					for (int r2 = 0; r2 < R2; r2++) {
						intermediate += data[r2][r] * other.data[c][r2];
					}
					result.data[c][r] = intermediate;
				}
			}
			return result;
		}
	}

	template<size_t R2, size_t C2>
	inline StaticMatrix<R, C2, T, T_ZERO, T_ONE> operator*(const StaticMatrix<R2, C2, T, T_ZERO, T_ONE>& rhs) const
	{
		return multiply(rhs);
	}

	template<size_t R2, size_t C2>
	inline StaticMatrix<R, C2, T, T_ZERO, T_ONE>& multiplyInplace(const StaticMatrix<R2, C2, T, T_ZERO, T_ONE>& other) const
	{
		static_assert(C == R2, "StaticMatrixrices are not eligible for multiplication.");
		StaticMatrix<R, C2, T, T_ZERO, T_ONE> result = this * other;
		this = result;
		return *this;
	}

	template<size_t R2, size_t C2>
	inline StaticMatrix<R, C2, T, T_ZERO, T_ONE>& operator*=(const StaticMatrix<R2, C2, T, T_ZERO, T_ONE>& rhs) const
	{
		return multiply(rhs);
	}

	// Matrix Vector multiplication: Columns of matrix must equal size/rows of vector => StaticVector of lenght C.
	inline StaticVector<C, T, T_ZERO, T_ONE> multiply(const StaticVector<C, T, T_ZERO, T_ONE>& vector) const
	{
		if constexpr (C == 2)
		{
			return StaticVector<2, T, T_ZERO, T_ONE>(
				data[0][0] * vector.x() + data[1][0] * vector.y(),
				data[0][1] * vector.x() + data[1][1] * vector.y()
			);
		} 
		else if constexpr (C == 3)
		{
			return StaticVector<3, T, T_ZERO, T_ONE>(
				data[0][0] * vector.x() + data[1][0] * vector.y() + data[2][0] * vector.z(),
				data[0][1] * vector.x() + data[1][1] * vector.y() + data[2][1] * vector.z(),
				data[0][2] * vector.x() + data[1][2] * vector.y() + data[2][2] * vector.z()
			);
		}
		else if constexpr (C == 4)
		{
			return StaticVector<4, T, T_ZERO, T_ONE>(
				data[0][0] * vector.x() + data[1][0] * vector.y() + data[2][0] * vector.z() + data[3][0] * vector.w(),
				data[0][1] * vector.x() + data[1][1] * vector.y() + data[2][1] * vector.z() + data[3][1] * vector.w(),
				data[0][2] * vector.x() + data[1][2] * vector.y() + data[2][2] * vector.z() + data[3][2] * vector.w(),
				data[0][3] * vector.x() + data[1][3] * vector.y() + data[2][3] * vector.z() + data[3][3] * vector.w()
			);
		}
		else
		{
			StaticVector<C, T, T_ZERO, T_ONE> result;
			for (int r = 0; r < R; r++) {
				T intermediate = 0;
				for (int c = 0; c < C; c++) {
					intermediate += data[c][r] * vector.data[c];
				}
				result.data[r] = intermediate;
			}
			return result;
		}
	}

	inline StaticVector<C, T, T_ZERO, T_ONE> operator*(const StaticVector<C, T, T_ZERO, T_ONE>& rhs) const
	{
		return multiply(rhs);
	}

	// Transpose.
	inline StaticMatrix<C, R, T, T_ZERO, T_ONE> transpose() const
	{
		StaticMatrix<C, R, T, T_ZERO, T_ONE> result;
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				result.data[r][c] = data[c][r];
			}
		}
		return result;
	}

	// Submatrix.
	inline StaticMatrix<R - 1, C - 1, T, T_ZERO, T_ONE> submatrix(unsigned int excludedCol,
																  unsigned int excludedRow) const
	{
		StaticMatrix<R - 1, C - 1, T, T_ZERO, T_ONE> sub;
		int srcCol = 0;
		for (int c = 0; c < C - 1; c++)
		{
			// The current column is the one which is to be excluded.
			if (c == excludedCol)
			{
				srcCol++;
			}
			int srcRow = 0;
			for (int r = 0; r < R - 1; r++)
			{
				// The current row is the one which is to be excluded.
				if (r == excludedRow)
				{
					srcRow++;
				}
				sub.data[c][r] = data[srcCol][srcRow];
				srcRow++;
			}
			srcCol++;
		}
		return sub;
	}

	// Determinant.
	inline double determinant() const
	{
		static_assert(R == C, "Determinant is only defined for squared matrices.");
		if constexpr (R == 1)
		{
			return (double)data[0][0];
		}
		else if constexpr (R == 2)
		{
			return (double)data[0][0] * (double)data[1][1] - (double)data[1][0] * (double)data[0][1];
		}
		else if constexpr (R == 3)
		{
			return ((double)data[0][0] * ((double)data[1][1] * (double)data[2][2] - (double)data[1][2] * (double)data[2][1])
				  + (double)data[0][1] * ((double)data[1][2] * (double)data[2][0] - (double)data[1][0] * (double)data[2][2])
				  + (double)data[0][2] * ((double)data[1][0] * (double)data[2][1] - (double)data[1][1] * (double)data[2][0]));
		}
		else if constexpr (R == 4)
		{
			// TODO: This is just the hand written form of the general approach below...
			// Lets calculate the determinant by eliminating row 4.
			double determinant = 0.0;
			// Elimination of the 1st column.
			StaticMatrix<3, 3, T, T_ZERO, T_ONE> sub1
			{
				data[1][0], data[2][0], data[3][0],
				data[1][1], data[2][1], data[3][1],
				data[1][2], data[2][2], data[3][2],
			};
			determinant -= ((double)data[0][3] * sub1.determinant());
			// Elimination of the 2nd column.
			StaticMatrix<3, 3, T, T_ZERO, T_ONE> sub2
			{
				data[0][0], data[2][0], data[3][0],
				data[0][1], data[2][1], data[3][1],
				data[0][2], data[2][2], data[3][2],
			};
			determinant += ((double)data[1][3] * sub2.determinant());
			// Elimination of the 3rd column.
			StaticMatrix<3, 3, T, T_ZERO, T_ONE> sub3
			{
				data[0][0], data[1][0], data[3][0],
				data[0][1], data[1][1], data[3][1],
				data[0][2], data[1][2], data[3][2],
			};
			determinant -= ((double)data[2][3] * sub3.determinant());
			// Elimination of the 4th column.
			StaticMatrix<3, 3, T, T_ZERO, T_ONE> sub4 
			{
				data[0][0], data[1][0], data[2][0],
				data[0][1], data[1][1], data[2][1],
				data[0][2], data[1][2], data[2][2],
			};
			determinant += ((double)data[3][3] * sub4.determinant());
			return determinant;
		}
		else
		{
			// Lets calculate the determinant by eliminating the last row.
			double determinant = 0.0;
			// i determines the column which is to be eliminated.
			for (int i = 0; i < C; i++)
			{
				double summand = ((double)data[i][R - 1] * submatrix(i, R - 1).determinant());
				// Use i (the column which is eliminated) and R-1 (the row which is eliminated) to calculate the sign.
				// The plus 1 inverts the modulo result.
				int sign = (((i + R - 1) + 1) % 2) * 2 - 1;
				determinant += sign * summand;
			}
			return determinant;
		}
	}

	// Inverse.
	inline StaticMatrix<C, R, T, T_ZERO, T_ONE> inverse() const
	{
		static_assert(C == R, "Inverse is only defined for s quare matrices!");
		if constexpr (C == 1)
		{
			return StaticMatrix<C, R, T, T_ZERO, T_ONE>
			{
				(T)1.0 / data[0][0]
			};
		}
		else if constexpr (C == 2)
		{
			double invDet = 1.0 / determinant();
			return StaticMatrix<C, R, T, T_ZERO, T_ONE>
			{
				+(T)invDet * data[1][1], -(T)invDet * data[1][0],
				-(T)invDet * data[0][1], +(T)invDet * data[0][0]
			};
		}
		else if constexpr (C == 3)
		{
			// TODO: enforce double
			const StaticVector<3, T, T_ZERO, T_ONE>& a = operator[](0);
			const StaticVector<3, T, T_ZERO, T_ONE>& b = operator[](1);
			const StaticVector<3, T, T_ZERO, T_ONE>& c = operator[](2);

			const StaticVector<3, T, T_ZERO, T_ONE> r0 = b.cross(c);
			const StaticVector<3, T, T_ZERO, T_ONE> r1 = c.cross(a);
			const StaticVector<3, T, T_ZERO, T_ONE> r2 = a.cross(b);

			T invDet = (T)1.0 / r2.dot(c);

			return StaticMatrix<C, R, T, T_ZERO, T_ONE>
			{
				r0.x() * invDet, r0.y() * invDet, r0.z() * invDet,
				r1.x() * invDet, r1.y() * invDet, r1.z() * invDet,
				r2.x() * invDet, r2.y() * invDet, r2.z() * invDet
			};
		}
		else if constexpr (C == 4)
		{
			// TODO: enforce double
			const StaticVector<3, T, T_ZERO, T_ONE>& a = reinterpret_cast<const StaticVector<3, T, T_ZERO, T_ONE>&>(operator[](0));
			const StaticVector<3, T, T_ZERO, T_ONE>& b = reinterpret_cast<const StaticVector<3, T, T_ZERO, T_ONE>&>(operator[](1));
			const StaticVector<3, T, T_ZERO, T_ONE>& c = reinterpret_cast<const StaticVector<3, T, T_ZERO, T_ONE>&>(operator[](2));
			const StaticVector<3, T, T_ZERO, T_ONE>& d = reinterpret_cast<const StaticVector<3, T, T_ZERO, T_ONE>&>(operator[](3));

			const T& x = data[0][3];
			const T& y = data[1][3];
			const T& z = data[2][3];
			const T& w = data[3][3];

			StaticVector<3, T, T_ZERO, T_ONE> s = a.cross(b);
			StaticVector<3, T, T_ZERO, T_ONE> t = c.cross(d);
			StaticVector<3, T, T_ZERO, T_ONE> u = a * y - b * x;
			StaticVector<3, T, T_ZERO, T_ONE> v = c * w - d * z;

			const T invDet = (T)1.0 / (s.dot(v) + t.dot(u));
			s *= invDet;
			t *= invDet;
			u *= invDet;
			v *= invDet;

			const StaticVector<3, T, T_ZERO, T_ONE> r0 = b.cross(v) + t * y;
			const StaticVector<3, T, T_ZERO, T_ONE> r1 = v.cross(a) - t * x;
			const StaticVector<3, T, T_ZERO, T_ONE> r2 = d.cross(u) + s * w;
			const StaticVector<3, T, T_ZERO, T_ONE> r3 = u.cross(c) - s * z;

			return StaticMatrix<C, R, T, T_ZERO, T_ONE>
			{
				r0.x(), r0.y(), r0.z(), -b.dot(t),
				r1.x(), r1.y(), r1.z(),  a.dot(t),
				r2.x(), r2.y(), r2.z(), -d.dot(s),
				r3.x(), r3.y(), r3.z(),  c.dot(s)
			};
		}
		else
		{
			// This is pretty inefficient...
			StaticMatrix<C, R, T, T_ZERO, T_ONE> cof;
			double invDet = 1.0 / determinant();
			for (int c = 0; c < C; c++) {
				for (int r = 0; r < R; r++) {
					int sign = (((c + r - 1) + 1) % 2) * 2 - 1;
					cof.data[c][r] = sign * submatrix(c, r).determinant();
				}
			}
			return cof.transpose() * (T)invDet;
		}
	}

	/* GETTER */

	inline StaticVector<C, T, T_ZERO, T_ONE> getMajorDiagonal() const
	{
		static_assert(C == R, "Major diagonal only defined for square matrices!");
		StaticVector<C, T, T_ZERO, T_ONE> result;
		for (int c = 0; c < C; c++) {
			for (int r = 0; r < R; r++) {
				if (c == r)
				{
					result.data[c] = data[c][r];
				}
			}
		}
		return result;
	}

	inline StaticVector<R, T, T_ZERO, T_ONE> getColumn(int columnIndex) const
	{
		StaticVector<R, T, T_ZERO, T_ONE> result;
		for (int r = 0; r < R; r++) {
			result.data[r] = data[columnIndex][r];
		}
		return result;
	}

	inline StaticVector<C, T, T_ZERO, T_ONE> getRow(int rowIndex) const
	{
		StaticVector<R, T, T_ZERO, T_ONE> result;
		for (int c = 0; c < C; c++) {
			result.data[c] = data[c][rowIndex];
		}
		return result;
	}

	/* SETTER */

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& set(const std::initializer_list<T> elements)
	{
		ASSERT_MSG(elements.size() == R * C, "Initializer list did not contain R * C elements!");
		int r = 0;
		int c = 0;
		for (const T& element : elements) {
			data[c][r] = element;
			c++;
			if (c == C)
			{
				r++;
				c = 0;
			}
		}
		return *this;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& setColumn(int columnIndex, const StaticVector<R, T, T_ZERO, T_ONE>& column)
	{
		for (int r = 0; r < R; r++) {
			data[columnIndex][r] = column[r];
		}
		return *this;
	}

	inline StaticMatrix<R, C, T, T_ZERO, T_ONE>& setRow(int rowIndex, const StaticVector<C, T, T_ZERO, T_ONE>& row)
	{
		for (int c = 0; c < C; c++) {
			data[c][rowIndex] = row[c];
		}
		return *this;
	}

	/* MISCELLANEOUS */

	inline std::string toString() const
	{
		std::ostringstream ss;
		ss << "StaticMatrix" << R << C << TypeName<T>::getShort() << " {" << std::endl;
		// Inefficient data access, as layout in memory is column major. But ok for toString.
		for (int r = 0; r < R; r++) {
			ss << "\t";
			for (int c = 0; c < C; c++) {
				ss << data[c][r] << ", ";
			}
			ss << std::endl;
		}
		ss << "}";
		return ss.str();
	}

	inline friend std::ostream& operator<<(std::ostream& os, const StaticMatrix<R, C, T, T_ZERO, T_ONE>& mat)
	{
		return os << mat.toString();
	}

	inline friend Logger& operator<<(Logger& logger, const StaticMatrix<R, C, T, T_ZERO, T_ONE>& mat)
	{
		return logger << mat.toString();
	}
};

COLIBRI_END

#endif // _MATH_STATIC_MATRIX_H_
