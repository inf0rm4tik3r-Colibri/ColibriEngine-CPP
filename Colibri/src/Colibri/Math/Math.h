#pragma once

#ifndef _MATH_H_
#define _MATH_H_

COLIBRI_BEGIN

namespace Math
{

// Fallback, in case M_PI doesn't exist.
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef M_E
#define M_E 2.71828182845904523536
#endif

/*
 * PI.
 */
static const double PI = M_PI;

/*
 * E.
 */
static const double E = M_E;

/*
 * E.
 */
static const double DEGREES_TO_RADIANS = M_PI / 180.0;
static const double RADIANS_TO_DEGREES = 180.0 / M_PI;

/*
 * Average sea level pressure in [kPa].
 */
static const int EARTH_AVG_SEA_LEVEL_PRESSURE = 101325;

/*
 * Molar mass of Earth's air in [kg/mol].
 */
static const double EARTH_AIR_MOLAR_MASS = 0.02896;

/*
 * Earth's gravitational acceleration in [m/s^2].
 */
static const double EARTH_GRAVITATIONAL_ACCELERATION = 9.807;

/*
 * Universal gas constant in [(N*m)/(mol*kg)].
 */
static const double EARTH_UNIVERSAL_GAS_CONSTANT = 8.3143;

/*
 * Specific gas constant for dry air in [J/(kg*K)].
 */
static const double EARTH_GAS_CONSTANT_DRY_AIR = 287.05;

/*
 * A base temperature.
 */
static const double BASE_TEMPERATURE = 15.0;

/*
 * Refraction values for specific materials.
 * You may use these constants as the eta parameter in the {@link StdVec3f#refract(StdVec3f, float)} method.
 */
static const double
REFRACTION_VACUUM = 1.0000,
REFRACTION_AIR = 1.0003,
REFRACTION_ICE = 1.3100,
REFRACTION_WATER = 1.3330,
REFRACTION_GASOLINE = 1.3980,
REFRACTION_GLASS = 1.5500,
REFRACTION_SAPPHIRE = 1.7700,
REFRACTION_DIAMOND = 2.4190;

// Conversion functions to make the code below more readable.
inline double toRadians(double degrees) {
    return degrees * DEGREES_TO_RADIANS;
}

inline double toDegrees(double radians) {
    return radians * RADIANS_TO_DEGREES;
}

inline int wrapAround(int value, int delta, int minval, int maxval)
{
    const int mod = maxval + 1 - minval;
    value += delta - minval;
    value += (1 - value / mod) * mod;
    return value % mod + minval;
}

} // namespace Math

COLIBRI_END

#endif // _MATH_H_
