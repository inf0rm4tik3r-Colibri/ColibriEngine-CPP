#pragma once

#ifndef _MATH_STATIC_VECTOR_H_
#define _MATH_STATIC_VECTOR_H_

COLIBRI_BEGIN

template<size_t S, typename T, T T_ZERO, T T_ONE>
class StaticVector {
public:
	/* DATA */

	T data[S];

	/* CONSTRUCTORS */

	StaticVector<S, T, T_ZERO, T_ONE>() noexcept
	{
		/*
		for (int i = 0; i < S; i++) {
			data[i] = T_ZERO;
		}
		*/
	}

	StaticVector<S, T, T_ZERO, T_ONE>(const T& x) noexcept
	{
		static_assert(S == 1, "Constructor only applicable to vectors of lenght 1!");
		data[0] = x;
	}

	StaticVector<S, T, T_ZERO, T_ONE>(const T& x, const T& y) noexcept
	{
		static_assert(S == 2, "Constructor only applicable to vectors of lenght 2!");
		data[0] = x;
		data[1] = y;
	}

	StaticVector<S, T, T_ZERO, T_ONE>(const T& x, const T& y, const T& z) noexcept
	{
		static_assert(S == 3, "Constructor only applicable to vectors of lenght 3!");
		data[0] = x;
		data[1] = y;
		data[2] = z;
	}

	StaticVector<S, T, T_ZERO, T_ONE>(const T& x, const T& y, const T& z, const T& w) noexcept
	{
		static_assert(S == 4, "Constructor only applicable to vectors of lenght 4!");
		data[0] = x;
		data[1] = y;
		data[2] = z;
		data[3] = w;
	}

	// 3D Swizzle constructors.
	StaticVector<S, T, T_ZERO, T_ONE>(const StaticVector<2, T, T_ZERO, T_ONE>& xy, const T& z) noexcept
	{
		static_assert(S == 3, "Constructor only applicable to vectors of lenght 3!");
		data[0] = xy.x();
		data[1] = xy.y();
		data[2] = z;
	}

	StaticVector<S, T, T_ZERO, T_ONE>(const T& x, const StaticVector<2, T, T_ZERO, T_ONE>& yz) noexcept
	{
		static_assert(S == 3, "Constructor only applicable to vectors of lenght 3!");
		data[0] = x;
		data[1] = yz.x();
		data[2] = yz.y();
	}

	// 4D Swizzle constructors.
	StaticVector<S, T, T_ZERO, T_ONE>(const StaticVector<3, T, T_ZERO, T_ONE>& xyz, const T& w) noexcept
	{
		static_assert(S == 4, "Constructor only applicable to vectors of lenght 4!");
		data[0] = xyz.x();
		data[1] = xyz.y();
		data[2] = xyz.z();
		data[3] = w;
	}

	StaticVector<S, T, T_ZERO, T_ONE>(const T& x, const StaticVector<3, T, T_ZERO, T_ONE>& yzw) noexcept
	{
		static_assert(S == 4, "Constructor only applicable to vectors of lenght 4!");
		data[0] = x;
		data[1] = yzw.x();
		data[2] = yzw.y();
		data[3] = yzw.z();
	}

	StaticVector<S, T, T_ZERO, T_ONE>(const StaticVector<2, T, T_ZERO, T_ONE>& xy, 
									  const StaticVector<2, T, T_ZERO, T_ONE>& zw) noexcept
	{
		static_assert(S == 4, "Constructor only applicable to vectors of lenght 4!");
		data[0] = xy.x();
		data[1] = xy.y();
		data[2] = zw.x();
		data[3] = zw.y();
	}

	// Copy constructor.
	StaticVector<S, T, T_ZERO, T_ONE>(const StaticVector<S, T, T_ZERO, T_ONE>& other) noexcept
	{
		for (int i = 0; i < S; i++) {
			data[i] = other.data[i];
		}
	}

	// Move constructor.
	StaticVector<S, T, T_ZERO, T_ONE>(StaticVector<S, T, T_ZERO, T_ONE>&& other) noexcept
	{
		for (int i = 0; i < S; i++) {
			data[i] = std::move(other.data[i]);
		}
	}

	/* INITIALIZERS */
	inline StaticVector<S, T, T_ZERO, T_ONE>& initZero()
	{
		for (int i = 0; i < S; i++) {
			data[i] = T_ZERO;
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& initOne()
	{
		for (int i = 0; i < S; i++) {
			data[i] = T_ONE;
		}
		return *this;
	}

	/* OPERATORS */

	// Copy assignment.
	inline StaticVector<S, T, T_ZERO, T_ONE>& operator=(const StaticVector<S, T, T_ZERO, T_ONE>& other) noexcept
	{
		for (int i = 0; i < S; i++) {
			data[i] = other.data[i];
		}
		return *this;
	}

	// Move assignment.
	inline StaticVector<S, T, T_ZERO, T_ONE>& operator=(StaticVector<S, T, T_ZERO, T_ONE>&& other) noexcept
	{
		for (int i = 0; i < S; i++) {
			data[i] = std::move(other.data[i]);
		}
		return *this;
	}

	// Equality.
	inline friend bool operator==(const StaticVector<S, T, T_ZERO, T_ONE>& a,
								  const StaticVector<S, T, T_ZERO, T_ONE>& b)
	{
		if constexpr (S == 1)
		{
			return a.data[0] == b.data[0];
		}
		else if constexpr (S == 2)
		{
			return a.data[0] == b.data[0]
				&& a.data[1] == b.data[1];
		}
		else if constexpr (S == 3)
		{
			return a.data[0] == b.data[0] 
				&& a.data[1] == b.data[1]
				&& a.data[2] == b.data[2];
		}
		else if constexpr (S == 4)
		{
			return a.data[0] == b.data[0]
				&& a.data[1] == b.data[1]
				&& a.data[2] == b.data[2]
				&& a.data[3] == b.data[3];
		}
		else
		{
			for (int i = 0; i < S; i++) {
				if (a.data[i] != b.data[i])
				{
					return false;
				}
			}
			return true;
		}
	}

	inline friend bool operator!=(const StaticVector<S, T, T_ZERO, T_ONE>& a,
								  const StaticVector<S, T, T_ZERO, T_ONE>& b)
	{
		return !(a == b);
	}

	// Comparison.
	inline friend bool operator<(const StaticVector<S, T, T_ZERO, T_ONE>& a,
								  const StaticVector<S, T, T_ZERO, T_ONE>& b)
	{
		// Is (a < s)?
		for (int i = 0; i < S; i++) {
			if (a.data[i] < b.data[i])
			{
				return true;
			}
			else if (a.data[i] > b.data[i])
			{
				return false;
			}
			// continue...
		}
		return false;
	}

	inline friend bool operator<=(const StaticVector<S, T, T_ZERO, T_ONE>& a,
								  const StaticVector<S, T, T_ZERO, T_ONE>& b)
	{
		return a < b || a == b;
	}

	inline friend bool operator>(const StaticVector<S, T, T_ZERO, T_ONE>& a,
								  const StaticVector<S, T, T_ZERO, T_ONE>& b)
	{
		// Is (a > s)?
		for (int i = 0; i < S; i++) {
			if (a.data[i] > b.data[i])
			{
				return true;
			}
			else if (a.data[i] < b.data[i])
			{
				return false;
			}
			// continue...
		}
		return false;
	}

	inline friend bool operator>=(const StaticVector<S, T, T_ZERO, T_ONE>& a,
								  const StaticVector<S, T, T_ZERO, T_ONE>& b)
	{
		return a > b || a == b;
	}

	// Subscript.
	inline const T& at(int index) const
	{
		ASSERT_MSG(index >= 0 && index < S, "Index out of range!");
		return data[index];
	}

	inline const T& operator[](int index) const
	{
		return at(index);
	}

	inline T& at(int index)
	{
		ASSERT_MSG(index >= 0 && index < S, "Index out of range!");
		return data[index];
	}

	inline T& operator[](int index)
	{
		return at(index);
	}

	// Addition.
	inline StaticVector<S, T, T_ZERO, T_ONE> add(const StaticVector<S, T, T_ZERO, T_ONE>& other) const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = data[i] + other.data[i];
		}
		return result;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator+(const StaticVector<S, T, T_ZERO, T_ONE>& rhs) const
	{
		return add(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> add(const T& other) const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = data[i] + other;
		}
		return result;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator+(const T& rhs) const
	{
		return add(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& addInplace(const StaticVector<S, T, T_ZERO, T_ONE>& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] += other.data[i];
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& operator+=(const StaticVector<S, T, T_ZERO, T_ONE>& rhs)
	{
		return addInplace(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& addInplace(const T& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] += other;
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& operator+=(const T& rhs)
	{
		return addInplace(rhs);
	}

	// Subtraction.
	inline StaticVector<S, T, T_ZERO, T_ONE> subtract(const StaticVector<S, T, T_ZERO, T_ONE>& other) const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = data[i] - other.data[i];
		}
		return result;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator-(const StaticVector<S, T, T_ZERO, T_ONE>& rhs) const
	{
		return subtract(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> subtract(const T& other) const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = data[i] - other;
		}
		return result;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator-(const T& rhs) const
	{
		return subtract(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& subtractInplace(const StaticVector<S, T, T_ZERO, T_ONE>& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] -= other.data[i];
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& operator-=(const StaticVector<S, T, T_ZERO, T_ONE>& rhs)
	{
		return subtractInplace(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& subtractInplace(const T& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] -= other;
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& operator-=(const T& rhs)
	{
		return subtractInplace(rhs);
	}

	// Multiplication.
	inline StaticVector<S, T, T_ZERO, T_ONE> multiply(const StaticVector<S, T, T_ZERO, T_ONE>& other) const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = data[i] * other.data[i];
		}
		return result;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator*(const StaticVector<S, T, T_ZERO, T_ONE>& rhs) const
	{
		return multiply(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> multiply(const T& other) const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = data[i] * other;
		}
		return result;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator*(const T& rhs) const
	{
		return multiply(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& multiplyInplace(const StaticVector<S, T, T_ZERO, T_ONE>& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] *= other.data[i];
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& operator*=(const StaticVector<S, T, T_ZERO, T_ONE>& rhs)
	{
		return multiplyInplace(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& multiplyInplace(const T& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] *= other;
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& operator*=(const T& rhs)
	{
		return multiplyInplace(rhs);
	}

	// Division.
	inline StaticVector<S, T, T_ZERO, T_ONE> divide(const StaticVector<S, T, T_ZERO, T_ONE>& other) const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = data[i] / other.data[i];
		}
		return result;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator/(const StaticVector<S, T, T_ZERO, T_ONE>& rhs) const
	{
		return divide(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> divide(const T& other) const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = data[i] / other;
		}
		return result;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator/(const T& rhs) const
	{
		return divide(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& divideInplace(const StaticVector<S, T, T_ZERO, T_ONE>& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] /= other.data[i];
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& operator/=(const StaticVector<S, T, T_ZERO, T_ONE>& rhs)
	{
		return divideInplace(rhs);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& divideInplace(const T& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] /= other;
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& operator/=(const T& rhs)
	{
		return divideInplace(rhs);
	}

	// Negation.
	inline StaticVector<S, T, T_ZERO, T_ONE>& negate()
	{
		for (int i = 0; i < S; i++) {
			data[i] = -data[i];
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> operator-()
	{
		return negate();
	}

	// Length.
	inline double length() const // TODO: Always a double? Should this be based on type?
	{
		double sumOfSquares = 0;
		for (int i = 0; i < S; i++) {
			sumOfSquares += data[i] * data[i];
		}
		return sqrt(sumOfSquares);
	}

	// Normalization.
	inline StaticVector<S, T, T_ZERO, T_ONE>& normalize()
	{
		double length = StaticVector<S, T, T_ZERO, T_ONE>::length();
		if (length == 0) {
			return *this;
		}
		for (int i = 0; i < S; i++) {
			data[i] /= (T)length;// TODO: Look into: Only allow length / normalize if T is a number type
		}
		return *this;
	}
	
	inline StaticVector<S, T, T_ZERO, T_ONE> normalized() const
	{
		return StaticVector<S, T, T_ZERO, T_ONE>(*this).normalize();
	}

	// Dot.
	inline const T dot(const StaticVector<S, T, T_ZERO, T_ONE>& a,
					   const StaticVector<S, T, T_ZERO, T_ONE>& b)
	{
		return a.dot(b);
	}

	inline T dot(const StaticVector<S, T, T_ZERO, T_ONE>& other) const
	{
		if constexpr (S == 3)
		{
			return data[0] * other.data[0]
				 + data[1] * other.data[1]
				 + data[2] * other.data[2];
		}
		else if constexpr (S == 4)
		{
			return data[0] * other.data[0]
				 + data[1] * other.data[1]
				 + data[2] * other.data[2]
				 + data[3] * other.data[3];
		}
		else
		{
			T result = T_ZERO;
			for (int i = 0; i < S; i++) {
				result += data[i] * other.data[i];
			}
			return result;
		}
	}

	// Cross.
	inline static StaticVector<S, T, T_ZERO, T_ONE> cross(const StaticVector<S, T, T_ZERO, T_ONE>& a,
												          const StaticVector<S, T, T_ZERO, T_ONE>& b)
	{
		static_assert(S == 3, "Cross product is only defined for 3D vectors! (7D not yet implemented...)");
		return StaticVector<S, T, T_ZERO, T_ONE> {
			a.data[1] * b.data[2] - a.data[2] * b.data[1],
			a.data[2] * b.data[0] - a.data[0] * b.data[2],
			a.data[0] * b.data[1] - a.data[1] * b.data[0]
		};
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> cross(const StaticVector<S, T, T_ZERO, T_ONE>& b) const
	{
		static_assert(S == 3, "Cross product is only defined for 3D vectors! (7D not yet implemented...)");
		return StaticVector<S, T, T_ZERO, T_ONE> {
			data[1] * b.data[2] - data[2] * b.data[1],
			data[2] * b.data[0] - data[0] * b.data[2],
			data[0] * b.data[1] - data[1] * b.data[0]
		};
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& crossInplace(const StaticVector<S, T, T_ZERO, T_ONE>& other)
	{
		static_assert(S == 3, "Cross product is only defined for 3D vectors! (7D not yet implemented...)");
		T x = data[1] * other.data[2] - data[2] * other.data[1];
		T y = data[2] * other.data[0] - data[0] * other.data[2];
		T z = data[0] * other.data[1] - data[1] * other.data[0];
		data[0] = x;
		data[1] = y;
		data[2] = z;
		return *this;
	}

	// Invert.
	inline StaticVector<S, T, T_ZERO, T_ONE>& invert()
	{
		for (int i = 0; i < S; i++) {
			data[i] = -data[i];
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> asInverted() const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = -data[i];
		}
		return result;
	}

	// Absolute.
	inline StaticVector<S, T, T_ZERO, T_ONE>& abs()
	{
		for (int i = 0; i < S; i++) {
			data[i] = std::abs(data[i]);
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& absolute()
	{
		return abs();
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> asAbsolute() const
	{
		StaticVector<S, T, T_ZERO, T_ONE> result;
		for (int i = 0; i < S; i++) {
			result.data[i] = -data[i];
		}
		return result;
	}

	// Angle.
	inline double angleRadians(const StaticVector<S, T, T_ZERO, T_ONE>& other) const
	{
		double fromLength = length();
		double toLength = other.length();
		ASSERT_MSG(
			fromLength != 0.0 && toLength != 0.0, 
			"Both vectors need a length other than 0! A vector of length 0 has no orientation, and therefore no angel to another vector."
		);
		return std::acos(dot(other) / (fromLength * toLength));
	}

	inline double angleDegrees(const StaticVector<S, T, T_ZERO, T_ONE>& other) const
	{
		return Math::toDegrees(angleRadians(other));
	}

	inline double angle(const StaticVector<S, T, T_ZERO, T_ONE>& other) const
	{
		return angleDegrees(other);
	}

	// Project.
	inline StaticVector<S, T, T_ZERO, T_ONE> prorject(const StaticVector<3, T, T_ZERO, T_ONE>& onto) const
	{
		static_assert(S == 3, "Projection is only defined for 3D vectors!");
		// Division by onto.dot(onto) is necessary, becouse we do not know if onto is normalized.
		// Calculation could be sped up when we make this an assumption.
		return (onto * (this->dot(onto) / onto.dot(onto)));
	}
	
	inline StaticVector<S, T, T_ZERO, T_ONE>& prorjectInplace(const StaticVector<3, T, T_ZERO, T_ONE>& onto)
	{
		static_assert(S == 3, "Projection is only defined for 3D vectors!");
		// Division by onto.dot(onto) is necessary, becouse we do not know if onto is normalized.
		// Calculation could be sped up when we make this an assumption.
		return set(onto * (this->dot(onto) / onto.dot(onto)));
	}

	// Reject.
	inline StaticVector<S, T, T_ZERO, T_ONE> reject(const StaticVector<3, T, T_ZERO, T_ONE>& from) const
	{
		static_assert(S == 3, "Projection is only defined for 3D vectors!");
		// Division by from.dot(from) is necessary, becouse we do not know if from is normalized.
		// Calculation could be sped up when we make this an assumption.
		return (*this - (from * (this->dot(from) / from.dot(from))));
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& rejectInplace(const StaticVector<3, T, T_ZERO, T_ONE>& from)
	{
		static_assert(S == 3, "Projection is only defined for 3D vectors!");
		// Division by from.dot(from) is necessary, becouse we do not know if from is normalized.
		// Calculation could be sped up when we make this an assumption.
		return (*this -= (from * (this->dot(from) / from.dot(from))));
	}

	// TODO: Only defined for floating point number types!

	// Reflect.
	inline StaticVector<S, T, T_ZERO, T_ONE>& reflectInplace(const StaticVector<3, T, T_ZERO, T_ONE>& normalizedNormal)
	{
		static_assert(S == 3, "Reflection is only defined for 3D vectors!");
		ASSERT_MSG(std::abs(1 - normalizedNormal.length()) < 0.01, "Normal vector was not normalized.");

		// r = d - n * (2 * (d o n))
		T nx = normalizedNormal.x();
		T ny = normalizedNormal.y();
		T nz = normalizedNormal.z();
		T c = (T)2.0 * dot(normalizedNormal);
		StaticVector<S, T, T_ZERO, T_ONE> nc(
			nx * c,
			ny * c,
			nz * c
		);
		return *this -= nc;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> reflect(const StaticVector<3, T, T_ZERO, T_ONE>& normalizedNormal) const
	{
		return StaticVector<S, T, T_ZERO, T_ONE>(*this).reflectInplace(normalizedNormal);
	}

	// Refract.
	inline StaticVector<S, T, T_ZERO, T_ONE>& refractInplace(const StaticVector<3, T, T_ZERO, T_ONE>& normalizedNormal, double eta)
	{
		static_assert(S == 3, "Refraction is only defined for 3D vectors!");
		ASSERT_MSG(std::abs(1 - length()) < 0.01, "This vector was not normalized before.");
		ASSERT_MSG(std::abs(1 - normalizedNormal.length()) < 0.01, "Normal vector was not normalized.");

		// Store the dot product for later calculations.
		T dotNormal = dot(normalizedNormal);
		double k = 1.0 - eta * eta * (1.0 - (double)dotNormal * (double)dotNormal);

		// A negative k would result in a reflection of the incoming vector... and the sqrt of k would then be NaN.
		if (k < 0.0) {
			LOG_WARN << "Unable to calculate refraction vector. K is negative. Leaving this vector unchanged.";
			return *this;
		}

		double factor = eta * dotNormal + std::sqrt(k);
		auto factoredNormal = normalizedNormal * (T)factor;
		*this *= (T)eta;
		*this -= factoredNormal;
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> refract(const StaticVector<S, T, T_ZERO, T_ONE>& normalizedNormal, double eta) const
	{
		return StaticVector<S, T, T_ZERO, T_ONE>(*this).refractInplace(normalizedNormal, eta);
	}

	// Lerp.
	inline StaticVector<S, T, T_ZERO, T_ONE> lerp(const StaticVector<S, T, T_ZERO, T_ONE>& target, double lerpFactor) const
	{
		ASSERT_MSG(
			!(lerpFactor < 0 || lerpFactor > 1), 
			"Lerp factor argument was not in the [0,...,1] range!"
		);

		return lerpFree(target, lerpFactor);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE> lerpFree(const StaticVector<S, T, T_ZERO, T_ONE>& target, double lerpFactor) const
	{
		auto result = target - *this;
		result *= (T)lerpFactor;
		result += *this;
		return result;
	}

	// Minimum.
	inline const T& min() const
	{
		if constexpr (S == 1)
		{
			return data[0];
		}
		else if constexpr (S == 2)
		{
			return std::min(data[0], data[1]);
		}
		else if constexpr (S == 3)
		{
			return std::min(
				data[0], 
				std::min(
					data[1], 
					data[2]
				)
			);
		}
		else if constexpr (S == 4)
		{
			return std::min(
				data[0], 
				std::min(
					data[1], 
					std::min(
						data[2], 
						data[3]
					)
				)
			);
		}
		else
		{
			T min = data[0];
			for (int i = 1; i < S; i++) {
				if (data[i] < min)
				{
					min = data[i];
				}
			}
			return min;
		}
	}

	// Maximum.
	inline const T& max() const
	{
		if constexpr (S == 1)
		{
			return data[0];
		}
		else if constexpr (S == 2)
		{
			return std::max(data[0], data[1]);
		}
		else if constexpr (S == 3)
		{
			return std::max(
				data[0],
				std::max(
					data[1],
					data[2]
				)
			);
		}
		else if constexpr (S == 4)
		{
			return std::max(
				data[0],
				std::max(
					data[1],
					std::max(
						data[2],
						data[3]
					)
				)
			);
		}
		else
		{
			T max = data[0];
			for (int i = 1; i < S; i++) {
				if (data[i] > max)
				{
					max = data[i];
				}
			}
			return max;
		}
	}

	/* GETTER */

	inline const T& x() const
	{
		static_assert(S >= 1, "StaticVectortor is not long enough!");
		return data[0];
	}

	inline const T& r() const
	{
		return x();
	}

	inline const T& y() const
	{
		static_assert(S >= 2, "StaticVectortor is not long enough!");
		return data[1];
	}

	inline const T& g() const
	{
		return y();
	}

	inline const T& z() const
	{
		static_assert(S >= 3, "StaticVectortor is not long enough!");
		return data[2];
	}

	inline const T& b() const
	{
		return z();
	}

	inline const T& w() const
	{
		static_assert(S >= 4, "StaticVectortor is not long enough!");
		return data[3];
	}

	inline const T& a() const
	{
		return w();
	}

	inline StaticVector<2, T, T_ZERO, T_ONE> xy() const
	{
		static_assert(S >= 2, "StaticVectortor is not long enough!");
		return StaticVector<2, T, T_ZERO, T_ONE>(data[0], data[1]);
	}

	inline StaticVector<2, T, T_ZERO, T_ONE> yz() const
	{
		static_assert(S >= 3, "StaticVectortor is not long enough!");
		return StaticVector<2, T, T_ZERO, T_ONE>(data[1], data[2]);
	}

	inline StaticVector<2, T, T_ZERO, T_ONE> xz() const
	{
		static_assert(S >= 3, "StaticVectortor is not long enough!");
		return StaticVector<2, T, T_ZERO, T_ONE>(data[0], data[2]);
	}

	inline StaticVector<3, T, T_ZERO, T_ONE> xyz() const
	{
		static_assert(S >= 4, "StaticVectortor is not long enough!");
		return StaticVector<3, T, T_ZERO, T_ONE>(data[0], data[1], data[2]);
	}

	inline StaticVector<3, T, T_ZERO, T_ONE> rgb() const
	{
		return xyz();
	}

	/* SETTER */
	
	inline StaticVector<S, T, T_ZERO, T_ONE>& set(const StaticVector<S, T, T_ZERO, T_ONE>& other)
	{
		for (int i = 0; i < S; i++) {
			data[i] = other.data[i];
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& set(const std::initializer_list<T>& elements)
	{
		ASSERT_MSG((elements.size() == S), "Too many or not enough values in initializer list!");
		int i = 0;
		for (const T& element : elements) {
			data[i] = element;
			i++;
		}
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& set(int index, const T& x)
	{
		assert_msg(index < S, "Index out of range!");
		data[index] = x;
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& x(const T& x)
	{
		static_assert(S >= 1, "StaticVectortor is not long enough!");
		data[0] = x;
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& r(const T& r)
	{
		return x(r);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& y(const T& y)
	{
		static_assert(S >= 2, "StaticVectortor is not long enough!");
		data[1] = y;
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& g(const T& g)
	{
		return y(g);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& z(const T& z)
	{
		static_assert(S >= 3, "StaticVectortor is not long enough!");
		data[2] = z;
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& b(const T& b)
	{
		return z(b);
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& w(const T& w)
	{
		static_assert(S >= 4, "StaticVectortor is not long enough!");
		data[3] = w;
		return *this;
	}

	inline StaticVector<S, T, T_ZERO, T_ONE>& a(const T& a)
	{
		return w(a);
	}

	/* MISCELLANEOUS */

	inline std::string toString() const
	{
		std::ostringstream ss;
		ss << "StaticVector" << S << TypeName<T>::getShort() << " {";
		for (int i = 0; i < S - 1; i++) {
			ss << data[i] << ", ";
		}
		ss << data[S - 1];
		ss << "}";
		return ss.str();
	}

	inline friend std::ostream& operator<<(std::ostream& os, const StaticVector<S, T, T_ZERO, T_ONE>& vec)
	{
		return os << vec.toString();
	}

	inline friend Logger& operator<<(Logger& logger, const StaticVector<S, T, T_ZERO, T_ONE>& vec)
	{
		return logger << vec.toString();
	}
};

COLIBRI_END

#endif // _MATH_STATIC_VECTOR_H_
