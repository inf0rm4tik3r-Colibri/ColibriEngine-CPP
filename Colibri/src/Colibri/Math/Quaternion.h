#pragma once

#ifndef _QUATERNION_H_
#define _QUATERNION_H_

#include "StaticVector.h"

COLIBRI_BEGIN

template<typename T, T T_ZERO, T T_ONE>
class Quaternion
{
public:
	T x; // vector part: x
	T y; // vector part: y
	T z; // vector part: z
	T w; // scalar part

	Quaternion<T, T_ZERO, T_ONE>() = default;

	Quaternion<T, T_ZERO, T_ONE>(const T& x, const T& y, const T& z, const T& w) noexcept
		: x(x), y(y), z(z), w(w)
	{
	}

	Quaternion<T, T_ZERO, T_ONE>(const StaticVector<3, T, T_ZERO, T_ONE>& vectorPart, const T& scalarPart) noexcept
		: x(vectorPart.x()), y(vectorPart.y()), z(vectorPart.z()), w(scalarPart)
	{
	}

	// Copy constructor.
	Quaternion<T, T_ZERO, T_ONE>(const Quaternion<T, T_ZERO, T_ONE>& other) noexcept
		: x(other.x()), y(other.y()), z(other.z()), w(other.w())
	{
	}

	// Move constructor.
	Quaternion<T, T_ZERO, T_ONE>(Quaternion<T, T_ZERO, T_ONE>&& other) noexcept
		: x(std::move(other.x())), y(std::move(other.y())), z(std::move(other.z())), w(std::move(other.w()))
	{
	}

	/* OPERATORS */

	// Copy assignment operator.
	inline Quaternion<T, T_ZERO, T_ONE>& operator=(const Quaternion<T, T_ZERO, T_ONE>& other) noexcept
	{
		x = other.x();
		y = other.y();
		z = other.z();
		w = other.w();
		return *this;
	}

	// Move assignment opoerator.
	inline Quaternion<T, T_ZERO, T_ONE>& operator=(Quaternion<T, T_ZERO, T_ONE>&& other) noexcept
	{
		x = std::move(other.x());
		y = std::move(other.y());
		z = std::move(other.z());
		w = std::move(other.w());
		return *this;
	}

	// Subscript.
	inline T& operator[](int index)
	{
		static_assert(index >= 0 && index < 4, "Index out of range [0..3]!");
		return ((&x)[index]);
	}

	inline const T& operator[](int index) const
	{
		static_assert(index >= 0 && index < 4, "Index out of range [0..3]!");
		return ((&x)[index]);
	}

	/* GETTER */

	inline const StaticVector<3, T, T_ZERO, T_ONE>& vectorPart() const
	{
		return (reinterpret_cast<StaticVector<3, T, T_ZERO, T_ONE>&>(x));
	}

	inline const StaticVector<3, T, T_ZERO, T_ONE>& scalarPart() const
	{
		return w;
	}

	/* SETTER */

};

COLIBRI_END

#endif // _QUATERNION_H_
