#include "pch.h"
#include "Model.h"

COLIBRI_BEGIN

Model::Model(const std::initializer_list<ColoredVertex>& vertices,
			 const std::initializer_list<uint32_t>& indices) noexcept
{
	vertexInputDescription = ColoredVertex::defaultVertexInputDescription;

	// Vertex buffer creation.
	vertexCount = vertices.size();
	vertexBuffer = VertexBuffer::create(vertices.size(), ColoredVertex::size);
	vertexBuffer->fill(vertices);

	// Index buffer creation.
	indexCount = indices.size();
	indexBuffer = IndexBuffer::create(indices.size(), sizeof(uint32_t));
	indexBuffer->fill(indices);
}

Model::~Model() noexcept
{
	LOG_INFO << "Destroyed model.";
}

Model::Model(Model&& other) noexcept
{
	vertexInputDescription = std::move(other.vertexInputDescription);
	vertexCount = std::move(other.vertexCount);
	vertexBuffer = std::move(other.vertexBuffer);
	indexCount = std::move(other.indexCount);
	indexBuffer = std::move(other.indexBuffer);
}

Model& Model::operator=(Model&& other) noexcept
{
	vertexInputDescription = std::move(other.vertexInputDescription);
	vertexCount = std::move(other.vertexCount);
	vertexBuffer = std::move(other.vertexBuffer);
	indexCount = std::move(other.indexCount);
	indexBuffer = std::move(other.indexBuffer);
	return *this;
}

size_t Model::getVertexCount() const
{
	return vertexCount;
}

size_t Model::getIndexCount() const
{
	return indexCount;
}

const VertexBuffer& Model::getVertexBuffer() const
{
	return *vertexBuffer;
}

const IndexBuffer& Model::getIndexBuffer() const
{
	return *indexBuffer;
}

const VertexInputDescription& Model::getVertexInputDescription() const
{
	return vertexInputDescription;
}

COLIBRI_END
