#pragma once

#ifndef _VERTEX_H_
#define _VERTEX_H_

COLIBRI_BEGIN

enum class VertexAttributeFormat
{
	R32G32_SFLOAT,
	R32G32B32_SFLOAT
};

/*
 * A binding description.
 */
class VertexInputBindingDescription
{
public:
	uint32_t binding;
	uint32_t stride;

	VertexInputBindingDescription(uint32_t binding, uint32_t stride);
};

/*
 * An attribute description.
 */
class VertexInputAttributeDescription
{
public:
	uint32_t binding;
	uint32_t location;
	VertexAttributeFormat format;
	uint32_t offset;

	VertexInputAttributeDescription(uint32_t binding, uint32_t location, VertexAttributeFormat format, uint32_t offset);
};

/*
 * Collection of binding and attribute descriptions.
 */
class VertexInputDescription
{
public:
	std::vector<VertexInputBindingDescription> bindingDescriptions;
	std::vector<VertexInputAttributeDescription> attributeDescriptions;

	VertexInputDescription() = default;
	VertexInputDescription(std::initializer_list<VertexInputBindingDescription> bindingDescriptions,
						   std::initializer_list<VertexInputAttributeDescription> attributeDescriptions);
};

/*
 * Basic vertex - Only position
 */
class Vertex
{
public:
	Vec3f position;

	static const size_t size;
	static const VertexInputDescription defaultVertexInputDescription;
};

/*
 * Colored vertex - Position and color
 */
class ColoredVertex : public Vertex
{
public:
	Vec3f color;

	static const size_t size;
	static const VertexInputDescription defaultVertexInputDescription;
};

COLIBRI_END

#endif // _VERTEX_H_