#pragma once

#ifndef _MODEL_H_
#define _MODEL_H_

#include "Vertex.h"
#include "..\Renderer\VertexBuffer.h"
#include "..\Renderer\IndexBuffer.h"

COLIBRI_BEGIN

class Model
{
private:
	size_t vertexCount;
	size_t indexCount;
	std::unique_ptr<VertexBuffer> vertexBuffer;
	std::unique_ptr<IndexBuffer> indexBuffer;
	VertexInputDescription vertexInputDescription;

public:
	Model(const std::initializer_list<ColoredVertex>& vertices,
		  const std::initializer_list<uint32_t>& indices) noexcept;
	// TODO: Create model by reading file input.
	~Model() noexcept;

	Model(const Model& other) = delete;
	Model(Model&& other) noexcept;
	Model& operator=(const Model& other) = delete;
	Model& operator=(Model&& other) noexcept;

	size_t getVertexCount() const;
	size_t getIndexCount() const;
	const VertexBuffer& getVertexBuffer() const;
	const IndexBuffer& getIndexBuffer() const;
	const VertexInputDescription& getVertexInputDescription() const;

};

COLIBRI_END

#endif // _MODEL_H_
