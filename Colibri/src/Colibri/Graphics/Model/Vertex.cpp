#include "pch.h"
#include "Vertex.h"

COLIBRI_BEGIN

VertexInputBindingDescription::VertexInputBindingDescription(uint32_t binding, uint32_t stride)
	: binding(binding),
	  stride(stride)
{}

VertexInputAttributeDescription::VertexInputAttributeDescription(uint32_t binding, uint32_t location, VertexAttributeFormat format, uint32_t offset)
	: binding(binding),
	  location(location),
	  format(format),
	  offset(offset)
{}

VertexInputDescription::VertexInputDescription(std::initializer_list<VertexInputBindingDescription> bindingDescriptions,
											   std::initializer_list<VertexInputAttributeDescription> attributeDescriptions)
	: bindingDescriptions(bindingDescriptions),
	  attributeDescriptions(attributeDescriptions)
{}

const size_t Vertex::size = sizeof(Vertex);
const VertexInputDescription Vertex::defaultVertexInputDescription = {
	{ 
		VertexInputBindingDescription(0, static_cast<uint32_t>(Vertex::size))
	},
	{
		VertexInputAttributeDescription(0, 0, VertexAttributeFormat::R32G32B32_SFLOAT, offsetof(Vertex, position))
	}
};

const size_t ColoredVertex::size = sizeof(ColoredVertex);
const VertexInputDescription ColoredVertex::defaultVertexInputDescription = {
	{
		VertexInputBindingDescription(0, static_cast<uint32_t>(ColoredVertex::size))
	},
	{ 
		VertexInputAttributeDescription(0, 0, VertexAttributeFormat::R32G32B32_SFLOAT, offsetof(ColoredVertex, position)),
		VertexInputAttributeDescription(0, 1, VertexAttributeFormat::R32G32B32_SFLOAT, offsetof(ColoredVertex, color))
	}
};

COLIBRI_END
