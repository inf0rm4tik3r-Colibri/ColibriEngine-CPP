#include "pch.h"
#include "GraphicsPipeline.h"

COLIBRI_BEGIN

GraphicsPipeline::GraphicsPipeline(const std::vector<std::shared_ptr<ShaderDescription>>& shaderDescriptions)
	: id(IDS::genRand64()), shaderDescriptions(shaderDescriptions)
{
}

GraphicsPipeline::GraphicsPipeline(GraphicsPipeline&& other) noexcept
    : id(std::move(other.id)), shaderDescriptions(std::move(other.shaderDescriptions))
{
}

GraphicsPipeline& GraphicsPipeline::operator=(GraphicsPipeline&& other) noexcept
{
    id = std::move(other.id);
    shaderDescriptions = std::move(other.shaderDescriptions);
    return *this;
}

GraphicsPipelineBuilder GraphicsPipeline::builder()
{
    return GraphicsPipelineBuilder();
}

inline bool operator==(const GraphicsPipeline& a, const GraphicsPipeline& b)
{
    return a.getId() == b.getId();
}

COLIBRI_END
