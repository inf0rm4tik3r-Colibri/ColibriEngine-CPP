#include "pch.h"
#include "VertexBuffer.h"
#include "../../Core/Engine.h"
#include "../../Platform/Vulkan/VulkanVertexBuffer.h"

COLIBRI_BEGIN

std::unique_ptr<VertexBuffer> VertexBuffer::create(size_t size, size_t sizePerVertex)
{
	switch (Engine::getConfig().getGraphicsApi())
	{
		case GraphicsApi::vulkan:
			return std::make_unique<VulkanVertexBuffer>(size, sizePerVertex);

		case GraphicsApi::opengl:
			throw UNSUPPORTED_OPERATION_EXCEPTION("OpenGL vertex buffers are not jet implemented.");

		default:
			throw UNSUPPORTED_OPERATION_EXCEPTION("Graphics api does not suporrt vertex buffers.");
	}
}

VertexBuffer::~VertexBuffer()
{
	LOG_INFO << "VertexBuffer base destructor.";
}

COLIBRI_END