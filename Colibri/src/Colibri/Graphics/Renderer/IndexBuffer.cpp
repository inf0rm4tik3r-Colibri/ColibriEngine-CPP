#include "pch.h"
#include "IndexBuffer.h"
#include "../../Core/Engine.h"
#include "../../Platform/Vulkan/VulkanIndexBuffer.h"

COLIBRI_BEGIN

std::unique_ptr<IndexBuffer> IndexBuffer::create(size_t size, size_t sizePerIndex)
{
	switch (Engine::getConfig().getGraphicsApi())
	{
		case GraphicsApi::vulkan:
			return std::make_unique<VulkanIndexBuffer>(size, sizePerIndex);

		case GraphicsApi::opengl:
			throw UNSUPPORTED_OPERATION_EXCEPTION("OpenGL index buffers are not jet implemented.");

		default:
			throw UNSUPPORTED_OPERATION_EXCEPTION("Graphics api does not suporrt index buffers.");
	}
}

IndexBuffer::~IndexBuffer()
{
	LOG_INFO << "IndexBuffer base destructor.";
}

COLIBRI_END