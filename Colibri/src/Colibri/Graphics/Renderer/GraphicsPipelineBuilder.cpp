#include "pch.h"
#include "GraphicsPipelineBuilder.h"
#include "../../Core/Engine.h"
#include "../../Platform/Vulkan/VulkanGraphicsPipeline.h"
#include "VulkanRenderer.h"

COLIBRI_BEGIN

void GraphicsPipelineBuilder::check()
{
	if (initBits != 0) {
		throw ILLEGAL_STATE_EXCEPTION(formatRequiredAttributesMessage());
	}
}

std::string GraphicsPipelineBuilder::formatRequiredAttributesMessage() {
	std::vector<std::string> attributes {};
	if ((initBits & init_bit_renderer) != 0) attributes.push_back("renderer");
	if ((initBits & init_bit_vertex_shader) != 0) attributes.push_back("vertex shader");
	if ((initBits & init_bit_fragment_shader) != 0) attributes.push_back("fragment shader");
	if ((initBits & init_bit_vertex_input_description) != 0) attributes.push_back("vertex input description");
	return std::string("Cannot build graphics pipeline, some of required attributes are not set ")
		.append(string::toString(attributes, ", "));
}

GraphicsPipelineBuilder::GraphicsPipelineBuilder()
{
	// Default initialization.
}

std::unique_ptr<GraphicsPipeline> GraphicsPipelineBuilder::build()
{
#ifdef COLIBRI_DEBUG
	check();
#endif
	std::vector<std::shared_ptr<ShaderDescription>> shaderDescriptions;
	if (_vertexShader != nullptr)
	{
		shaderDescriptions.push_back(_vertexShader);
	}
	if (_computeShader != nullptr)
	{
		shaderDescriptions.push_back(_computeShader);
	}
	if (_tesselationControlShader != nullptr)
	{
		shaderDescriptions.push_back(_tesselationControlShader);
	}
	if (_tesselationEvaluationShader != nullptr)
	{
		shaderDescriptions.push_back(_tesselationEvaluationShader);
	}
	if (_fragmentShader != nullptr)
	{
		shaderDescriptions.push_back(_fragmentShader);
	}

	switch (Engine::getConfig().getGraphicsApi())
	{
		case GraphicsApi::vulkan:
		{
			VulkanRenderer* vulkanRenderer = dynamic_cast<VulkanRenderer*>(_renderer.get());
			return std::make_unique<VulkanGraphicsPipeline>(
				vulkanRenderer->getVulkanWindow(), 
				vulkanRenderer->getRenderPass(), 
				shaderDescriptions,
				std::make_unique<VulkanVertexDescription>(_vertexInputDescription)
			);
		}
		case GraphicsApi::opengl:
			LOG_ERROR << "OpenGL is not yet supported.";
			throw UNSUPPORTED_OPERATION_EXCEPTION("Cannot create graphics pipeline with unsupported graphics api: OpenGL.");
			break;
		default:
			throw COLIBRI_EXCEPTION("Unknown graphics api.");
	}
}

GraphicsPipelineBuilder GraphicsPipelineBuilder::renderer(const std::shared_ptr<Renderer>& renderer)
{
	_renderer = renderer;
	initBits &= ~init_bit_renderer;
	return *this;
}

GraphicsPipelineBuilder GraphicsPipelineBuilder::vertexShader(const std::shared_ptr<ShaderDescription>& vertexShader)
{
	_vertexShader = vertexShader;
	initBits &= ~init_bit_vertex_shader;
	return *this;
}

GraphicsPipelineBuilder GraphicsPipelineBuilder::computeShader(const std::shared_ptr<ShaderDescription>& computeShader)
{
	_computeShader = computeShader;
	return *this;
}

GraphicsPipelineBuilder GraphicsPipelineBuilder::tesselationControlShader(const std::shared_ptr<ShaderDescription>& tesselationControlShader)
{
	_tesselationControlShader = tesselationControlShader;
	return *this;
}

GraphicsPipelineBuilder GraphicsPipelineBuilder::tesselationEvaluationShader(const std::shared_ptr<ShaderDescription>& tesselationEvaluationShader)
{
	_tesselationEvaluationShader = tesselationEvaluationShader;
	return *this;
}

GraphicsPipelineBuilder GraphicsPipelineBuilder::fragmentShader(const std::shared_ptr<ShaderDescription>& fragmentShader)
{
	_fragmentShader = fragmentShader;
	initBits &= ~init_bit_fragment_shader;
	return *this;
}

GraphicsPipelineBuilder GraphicsPipelineBuilder::vertexInputDescription(const VertexInputDescription& vertexInputDescription)
{
	_vertexInputDescription = vertexInputDescription;
	initBits &= ~init_bit_vertex_input_description;
	return *this;
}

COLIBRI_END
