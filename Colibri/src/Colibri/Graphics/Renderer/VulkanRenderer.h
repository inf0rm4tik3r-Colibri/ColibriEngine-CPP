#pragma once

#ifndef _VULKAN_RENDERER_H_
#define _VULKAN_RENDERER_H_

#include "../../Components/EntityComponentSystem.h"
#include "../../Exception/Exceptions.h"
#include "../../Platform/Vulkan/Vulkan.h"
#include "../../Platform/Vulkan/VulkanWindow.h"
#include "../../Platform/Vulkan/VulkanRenderPass.h"
#include "../../Platform/Vulkan/VulkanUniformDescriptorPool.h"
#include "../../Platform/Vulkan/VulkanDescriptorSet.h"
#include "../../Platform/Vulkan/VulkanGraphicsPipeline.h"
#include "../../Platform/Vulkan/VulkanCommandBufferPool.h"
#include "../../Platform/Vulkan/VulkanCommandBuffer.h"
#include "../../Platform/Vulkan/VulkanUniformBuffer.h"
#include "../Window/Window.h"
#include "Renderer.h"

COLIBRI_BEGIN

class VulkanRenderer : public Renderer
{
private:
	std::shared_ptr<VulkanWindow> window;
	std::shared_ptr<VulkanRenderPass> renderPass;

	std::shared_ptr<VulkanCommandBufferPool> graphicsCommandBufferPool;
	std::vector<VulkanCommandBuffer> commandBuffers;

	std::unique_ptr<VulkanUniformDescriptorPool> uniformDescriptorPool;
	std::unordered_map<uint64_t, std::vector<std::shared_ptr<VulkanDescriptorSet>>> uniformDescriptorSets;
	std::unordered_map<uint64_t, std::vector<std::shared_ptr<VulkanUniformBuffer>>> uniformBuffers;

	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inFlightFences;
	std::vector<VkFence> imagesInFlight;
	size_t currentFrame = 0;

	int maxFramesInFlight = 2;

public:
	VulkanRenderer(std::shared_ptr<Window>& baseWindow);
	~VulkanRenderer();
	VulkanRenderer(const VulkanRenderer& other) = delete;
	VulkanRenderer(VulkanRenderer&& other) = delete;
	VulkanRenderer& operator=(const VulkanRenderer& other) = delete;
	VulkanRenderer& operator=(VulkanRenderer&& other) = delete;

	void render(const EntityComponentSystem& ecs) override;
	bool beginFrame(uint32_t* imageIndex);
	void endFrame(uint32_t imageIndex);

	void waitIdle() override;

	Window* getWindow() const override;
	const std::shared_ptr<VulkanWindow>& getVulkanWindow() const;
	const std::shared_ptr<VulkanRenderPass>& getRenderPass() const;
};

COLIBRI_END

#endif // _VULKAN_RENDERER_H_
