#pragma once

#ifndef _GRAPHICS_PIPELINE_H_
#define _GRAPHICS_PIPELINE_H_

#include "../Shader/ShaderDescription.fwd.h"
#include "../Shader/ShaderDescription.h"
#include "GraphicsPipeline.fwd.h"
#include "GraphicsPipelineBuilder.fwd.h"
#include "GraphicsPipelineBuilder.h"

COLIBRI_BEGIN

class GraphicsPipeline
{
protected:
	uint64_t id;
	std::vector<std::shared_ptr<ShaderDescription>> shaderDescriptions;

public:
	GraphicsPipeline(const std::vector<std::shared_ptr<ShaderDescription>>& shaderDescriptions);
	virtual ~GraphicsPipeline() = default;

	GraphicsPipeline(const GraphicsPipeline& other) = delete;
	GraphicsPipeline(GraphicsPipeline&& other) noexcept;
	GraphicsPipeline& operator=(const GraphicsPipeline& other) = delete;
	GraphicsPipeline& operator=(GraphicsPipeline&& other) noexcept;

	static GraphicsPipelineBuilder builder();

	inline friend bool operator==(const GraphicsPipeline& a,
								  const GraphicsPipeline& b);

	inline uint64_t getId() const { return id; }
	inline const std::vector<std::shared_ptr<ShaderDescription>>& getShaderDescriptions() const { return shaderDescriptions; }
};

COLIBRI_END

#endif // _GRAPHICS_PIPELINE_H_
