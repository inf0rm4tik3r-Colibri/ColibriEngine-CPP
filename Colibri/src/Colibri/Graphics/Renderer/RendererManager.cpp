#include "pch.h"
#include "RendererManager.h"

COLIBRI_BEGIN

void RendererManager::waitAllIdle() const
{
	for (const auto& renderer : renderers)
	{
		renderer.second->waitIdle();
	}
}

void RendererManager::setRenderer(std::shared_ptr<Renderer> renderer)
{
	renderers[renderer->getWindow()] = renderer;
}

Renderer* RendererManager::getRenderer(Window* window)
{
	return renderers[window].get();
}

COLIBRI_END
