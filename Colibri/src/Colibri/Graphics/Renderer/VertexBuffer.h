#pragma once

#ifndef _VERTEX_BUFFER_H_
#define _VERTEX_BUFFER_H_

#include "../Model/Vertex.h"

COLIBRI_BEGIN

class VertexBuffer
{
public:
	static std::unique_ptr<VertexBuffer> create(size_t size, size_t sizePerVertex);

	virtual ~VertexBuffer();

	virtual void fill(const std::initializer_list<ColoredVertex>& vertices) = 0;
};

COLIBRI_END

#endif // _VERTEX_BUFFER_H_
