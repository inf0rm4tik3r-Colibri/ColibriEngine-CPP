#pragma once

#ifndef _GRAPHICS_PIPELINE_BUILDER_H_
#define _GRAPHICS_PIPELINE_BUILDER_H_

#include "GraphicsPipelineBuilder.fwd.h"
#include "GraphicsPipeline.fwd.h"
#include "GraphicsPipeline.h"
#include "Renderer.fwd.h"
#include "Renderer.h"
#include "../Shader/ShaderDescription.fwd.h"
#include "../Shader/ShaderDescription.h"
#include "../Model/Vertex.h"

COLIBRI_BEGIN

class GraphicsPipelineBuilder
{

private:
	static const constexpr long init_bit_renderer = 1L;
	static const constexpr long init_bit_vertex_shader = 2L;
	static const constexpr long init_bit_fragment_shader = 4L;
	static const constexpr long init_bit_vertex_input_description = 8L;
	long initBits = 15L; // Heighes bit value * 2 - 1

	// TODO: Performance impact of storing shared pointer copies?
	std::shared_ptr<Renderer> _renderer;
	std::shared_ptr<ShaderDescription> _vertexShader;
	std::shared_ptr<ShaderDescription> _computeShader;
	std::shared_ptr<ShaderDescription> _tesselationControlShader;
	std::shared_ptr<ShaderDescription> _tesselationEvaluationShader;
	std::shared_ptr<ShaderDescription> _fragmentShader;
	VertexInputDescription _vertexInputDescription;

	void check();
	std::string formatRequiredAttributesMessage();

public:
	GraphicsPipelineBuilder();

	std::unique_ptr<GraphicsPipeline> build();
	GraphicsPipelineBuilder renderer(const std::shared_ptr<Renderer>& renderer);
	GraphicsPipelineBuilder vertexShader(const std::shared_ptr<ShaderDescription>& vertexShader);
	GraphicsPipelineBuilder computeShader(const std::shared_ptr<ShaderDescription>& computeShader);
	GraphicsPipelineBuilder tesselationControlShader(const std::shared_ptr<ShaderDescription>& tesselationControlShader);
	GraphicsPipelineBuilder tesselationEvaluationShader(const std::shared_ptr<ShaderDescription>& tesselationEvaluationShader);
	GraphicsPipelineBuilder fragmentShader(const std::shared_ptr<ShaderDescription>& fragmentShader);
	GraphicsPipelineBuilder vertexInputDescription(const VertexInputDescription& vertexInputDescription);
};

COLIBRI_END

#endif // _GRAPHICS_PIPELINE_BUILDER_H_