#pragma once

#ifndef _RENDERER_BUILDER_H_
#define _RENDERER_BUILDER_H_

#include "RendererBuilder.fwd.h"
#include "Renderer.fwd.h"
#include "Renderer.h"
#include "../Window/Window.h"

COLIBRI_BEGIN

class RendererBuilder
{

private:
	static const constexpr long init_bit_window = 1L;
	long initBits = 1L; // Heighes bit value * 2 - 1

	// TODO: Performance impact of storing shared pointer copies?
	std::shared_ptr<Window> _window;

	void check();
	std::string formatRequiredAttributesMessage();

public:
	RendererBuilder();

	std::unique_ptr<Renderer> build();
	RendererBuilder window(const std::shared_ptr<Window>& window);
};

COLIBRI_END

#endif // _RENDERER_BUILDER_H_
