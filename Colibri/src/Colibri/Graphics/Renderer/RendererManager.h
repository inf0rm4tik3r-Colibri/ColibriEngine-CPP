#pragma once

#ifndef _RENDERER_MANAGER_H_
#define _RENDERER_MANAGER_H_

#include "../Window/Window.h"
#include "Renderer.h"

COLIBRI_BEGIN

class RendererManager
{
private:
	std::map<Window*, std::shared_ptr<Renderer>> renderers;

public:
	void waitAllIdle() const;

	void setRenderer(std::shared_ptr<Renderer> renderer);
	Renderer* getRenderer(Window* window);
};

COLIBRI_END

#endif // _RENDERER_MANAGER_H_
