#include "pch.h"
#include "VulkanRenderer.h"
#include "../../Platform/Vulkan/VulkanIndexBuffer.h"
#include "../../Platform/Vulkan/VulkanVertexBuffer.h"
#include "../Shader/UniformBufferDescription.h"

COLIBRI_BEGIN

VulkanRenderer::VulkanRenderer(std::shared_ptr<Window>& baseWindow)
	: Renderer(),
	window(std::dynamic_pointer_cast<VulkanWindow>(baseWindow)),
	renderPass(std::make_shared<VulkanRenderPass>(window->getSwapchain())),
	graphicsCommandBufferPool(std::make_shared<VulkanCommandBufferPool>(Vulkan::getPhysicalDevice()->getQueueFamilies().getGraphicsFamily().value())),
	commandBuffers(graphicsCommandBufferPool->createCommandBuffers(static_cast<uint32_t>(window->getSwapchain().getVkImageViews().size()))),
	uniformDescriptorPool(std::make_unique<VulkanUniformDescriptorPool>(window->getSwapchain().getVkImageViews().size()))
{
	LOG_INFO << "Created Vulkan renderer.";

	window->getSwapchain().createFramebuffers(renderPass);

	// Recover from swapchain recreation.
	window->swapchainRecreationFinished.subscribe(
		[this]()
		{
			LOG_INFO << "Vulkan renderer recovers from swapchain recreation.";
			// The render pass must be recreated.
			// The old render pass may still be alive and referenced by others but its now degenerate.
			renderPass.reset(new VulkanRenderPass(window->getSwapchain()));

			// Command buffers must be recreated.
			commandBuffers.clear();
			commandBuffers = graphicsCommandBufferPool->createCommandBuffers(
				static_cast<uint32_t>(window->getSwapchain().getVkImageViews().size())
			);

			// Uniform descriptor pool must be recreated.
			// Descriptor sets should be freed automatically by destroying the old pool, but we are explicit about this.
			uniformDescriptorSets.clear();
			uniformDescriptorPool.reset(new VulkanUniformDescriptorPool(window->getSwapchain().getVkImageViews().size()));

			// Pipeline state (viewport, scissor, ...) must be corrected to reflect current swapchain state. 
			// --> We use dynamic states to do this during rendering.

			// The swapchain framebuffers must be recreated.
			window->getSwapchain().createFramebuffers(renderPass);
			LOG_INFO << "Vulkan renderer recovered successfuly.";
		}
	);

	LOG_INFO << "Vulkan renderer subscribed for recreation.";

	// Create semaphores.
	VkSemaphoreCreateInfo semaphoreInfo {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	VkFenceCreateInfo fenceInfo {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT; // Create fences in the signaled state, as if we rendered an image with them!

	imageAvailableSemaphores.resize(maxFramesInFlight);
	renderFinishedSemaphores.resize(maxFramesInFlight);
	inFlightFences.resize(maxFramesInFlight);
	for (size_t i = 0; i < maxFramesInFlight; i++) {
		if (vkCreateSemaphore(Vulkan::getLogicalDevice()->getVkDevice(), &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS ||
			vkCreateSemaphore(Vulkan::getLogicalDevice()->getVkDevice(), &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS ||
			vkCreateFence(Vulkan::getLogicalDevice()->getVkDevice(), &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS) {

			throw COLIBRI_EXCEPTION("Failed to create synchronization objects for a frame!");
		}
	}
	imagesInFlight.resize(window->getSwapchain().getVkImages().size(), VK_NULL_HANDLE); // Initially, no image is used -> no fence.

	LOG_INFO << "Created Vulkan renderer semaphores and fences.";
}

VulkanRenderer::~VulkanRenderer()
{
	vkDeviceWaitIdle(Vulkan::getLogicalDevice()->getVkDevice());
	for (size_t i = 0; i < maxFramesInFlight; i++) {
		vkDestroySemaphore(Vulkan::getLogicalDevice()->getVkDevice(), renderFinishedSemaphores[i], nullptr);
		vkDestroySemaphore(Vulkan::getLogicalDevice()->getVkDevice(), imageAvailableSemaphores[i], nullptr);
		vkDestroyFence(Vulkan::getLogicalDevice()->getVkDevice(), inFlightFences[i], nullptr);
	}
	LOG_INFO << "Destroyed Vulkan renderer semaphores.";
	LOG_INFO << "Destroyed Vulkan renderer.";
}

void VulkanRenderer::render(const EntityComponentSystem& ecs)
{
	uint32_t imageIndex;
	if (!beginFrame(&imageIndex))
	{
		return;
	}

	// The command buffer used for the current thread.
	VulkanCommandBuffer& commandBuffer = commandBuffers[imageIndex];

	// Begin a render pass.
	VkRenderPassBeginInfo renderPassInfo {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	assert(!renderPass->isDegenerate());
	renderPassInfo.renderPass = renderPass->getVkRenderPass();
	renderPassInfo.framebuffer = window->getSwapchain().getVkFramebuffers()[imageIndex];
	renderPassInfo.renderArea.offset = { 0, 0 };
	renderPassInfo.renderArea.extent = window->getSwapchain().getExtent(); // Should match size of attachments for best performance.
	VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f }; // Black, 100% opacity.
	renderPassInfo.clearValueCount = 1;
	renderPassInfo.pClearValues = &clearColor;

	/*
	 * VK_SUBPASS_CONTENTS_INLINE: The render pass commands will be embedded in the primary command buffer itself and no secondary command buffers will be executed.
	 * VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS: The render pass commands will be executed from secondary command buffers.
	 */
	vkCmdBeginRenderPass(commandBuffer.getVkCommandBuffer(), &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);


	// --- FOR EACH ENTITY: Collect unique pipelines and check uniform buffer presence! ------------
	// Collect all unique pipelines:
	std::unordered_set<const VulkanGraphicsPipeline*> pipelines;
	for (const Entity& entity : ecs.getEntities())
	{
		pipelines.insert(dynamic_cast<const VulkanGraphicsPipeline*>(entity.getPipeline()));
	}
	// Collect all unique uniform descriptor set layouts:
	std::unordered_set<const VulkanUniformDescriptorSetLayout*> uniformDescriptorSetLayouts;
	for (const VulkanGraphicsPipeline* pipeline : pipelines)
	{
		for (const VulkanUniformDescriptorSetLayout& uniformDescriptorSetLayout : pipeline->getUniformDescriptorSetLayouts())
		{
			uniformDescriptorSetLayouts.insert(&uniformDescriptorSetLayout);
		}
	}
	// Collect all unique uniform buffer descriptions:
	std::unordered_set<const UniformBufferDescription*> uniformBufferDescriptions;
	for (const VulkanGraphicsPipeline* pipeline : pipelines)
	{
		for (const std::shared_ptr<ShaderDescription>& shaderDescription : pipeline->getShaderDescriptions())
		{
			for (const std::shared_ptr<UniformBufferDescription>& ubd : shaderDescription->getUniformBufferDescriptions())
			{
				uniformBufferDescriptions.insert(ubd.get());
			}
		}
	}
	// Ensure that all required uniform buffers exist and create them if necessary.
	for (const UniformBufferDescription* uniformBufferDescription : uniformBufferDescriptions)
	{
		size_t swapSize = window->getSwapchain().getVkImages().size();
		uint64_t ubdId = uniformBufferDescription->getId();
		// Bucket does not exist.
		if (uniformBuffers.find(ubdId) == uniformBuffers.end())
		{
			uniformBuffers[ubdId] = std::vector<std::shared_ptr<VulkanUniformBuffer>>();
		}
		std::vector<std::shared_ptr<VulkanUniformBuffer>>& ubs = uniformBuffers.at(ubdId);
		if (ubs.size() != swapSize)
		{
			ubs.resize(swapSize);
		}
		if (ubs[imageIndex] == nullptr)
		{
			ubs[imageIndex] = std::make_unique<VulkanUniformBuffer>(uniformBufferDescription->getSizeInBytes(), true);
		}
	}
	// TODO: clear alls keys of uniformBuffers which did not get visited. The buffers are no longer needed. Maybe only every x seconds.

	// Ensure that all required uniform descriptor sets exist and create them if necessary.
	for (const VulkanUniformDescriptorSetLayout* uniformDescriptorSetLayout : uniformDescriptorSetLayouts)
	{
		size_t swapSize = window->getSwapchain().getVkImages().size();
		uint64_t ubdId = uniformDescriptorSetLayout->getUniformBufferDescriptionId();
		// Bucket does not exist.
		if (uniformDescriptorSets.find(ubdId) == uniformDescriptorSets.end())
		{
			uniformDescriptorSets[ubdId] = std::vector<std::shared_ptr<VulkanDescriptorSet>>();
		}
		std::vector<std::shared_ptr<VulkanDescriptorSet>>& dSets = uniformDescriptorSets.at(ubdId);
		if (dSets.size() != swapSize)
		{
			dSets.resize(swapSize);
		}
		if (dSets[imageIndex] == nullptr)
		{
			// TODO: Create all necessary descriptor sets at once.
			dSets[imageIndex] = std::make_shared<VulkanDescriptorSet>(std::move(
				uniformDescriptorPool->allocateDescriptorSet(*uniformDescriptorSetLayout)
			));
			// Set up the descriptor set to bind the actual uniform buffer.
			// TODO: Move code to VulkanDescriptorSet.
			VkDescriptorBufferInfo bufferInfo {};
			bufferInfo.buffer = uniformBuffers[ubdId][imageIndex]->getVkBuffer();
			bufferInfo.offset = 0; // TODO: Partial buffer update
			bufferInfo.range = uniformBuffers[ubdId][imageIndex]->getSizeInBytes(); //sizeof(UniformBufferObject);

			VkWriteDescriptorSet descriptorWrite {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = dSets[imageIndex]->getVkDescriptorSet();
			descriptorWrite.dstBinding = uniformDescriptorSetLayout->getVkDescriptorSetLayoutBinding().binding;
			descriptorWrite.dstArrayElement = 0; // TODO: use
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &bufferInfo;
			descriptorWrite.pImageInfo = nullptr; // Optional
			descriptorWrite.pTexelBufferView = nullptr; // Optional
			vkUpdateDescriptorSets(Vulkan::getLogicalDevice()->getVkDevice(), 1, &descriptorWrite, 0, nullptr);
		}
	}

	// --- FOR EACH ENTITY: DRAW IT! ------------

	for (const Entity& entity : ecs.getEntities())
	{
		const VulkanGraphicsPipeline* pipeline = dynamic_cast<const VulkanGraphicsPipeline*>(entity.getPipeline());

		// Bind the pipeline.
		// TODO: Do not do this if this pipeline is already set!
		pipeline->bind(commandBuffer);

		// Set the viewport to the current swapchain extent to fill the output images, which are also that size.
		VkViewport viewport {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float)window->getSwapchain().getExtent().width;
		viewport.height = (float)window->getSwapchain().getExtent().height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		vkCmdSetViewport(commandBuffer.getVkCommandBuffer(), 0, 1, &viewport);

		// Set the scissort region to the current swapchain extent to fill the output images, which are also that size.
		VkRect2D scissor {};
		scissor.offset = { 0, 0 };
		scissor.extent = { window->getSwapchain().getExtent().width, window->getSwapchain().getExtent().height };
		vkCmdSetScissor(commandBuffer.getVkCommandBuffer(), 0, 1, &scissor);

		// Bind the vertex buffer neccesarry for the next draw.
		VkBuffer vertexBuffers[] = { dynamic_cast<const VulkanVertexBuffer&>(entity.getModel()->getVertexBuffer()).getVkBuffer() };
		VkDeviceSize offsets[] = { 0 };
		vkCmdBindVertexBuffers(commandBuffer.getVkCommandBuffer(), 0, 1, vertexBuffers, offsets);

		// Bind the index buffer neccesarry for the next draw.
		vkCmdBindIndexBuffer(
			commandBuffer.getVkCommandBuffer(),
			dynamic_cast<const VulkanIndexBuffer&>(entity.getModel()->getIndexBuffer()).getVkBuffer(),
			0, 
			VK_INDEX_TYPE_UINT32
		);

		// Update all uniform buffers specified by the shaders of the activated pipeline.
		for (const std::shared_ptr<ShaderDescription>& shaderDescription : pipeline->getShaderDescriptions())
		{
			for (const std::shared_ptr<UniformBufferDescription>& ubd : shaderDescription->getUniformBufferDescriptions())
			{
				VulkanUniformBuffer* uniformBuffer = uniformBuffers[ubd->getId()][imageIndex].get();
				void* data;
				uniformBuffer->mapMemory(&data);
				ubd->fillBuffer(data, uniformBuffer->getSizeInBytes(), entity);
				uniformBuffer->unmapMemory();

				// Bind all necessary descriptor sets for the current pipeline!
				vkCmdBindDescriptorSets(
					commandBuffer.getVkCommandBuffer(), 
					VK_PIPELINE_BIND_POINT_GRAPHICS, 
					pipeline->getVkPipelineLayout(),
					0, // first set
					1, // descriptor set count
					&uniformDescriptorSets[ubd->getId()][imageIndex]->getVkDescriptorSet(),
					0, // dynamic offset count
					nullptr
				);
			}
		}

		// Draw some geometry...
		/*
		vkCmdDraw(
			commandBuffers[imageIndex].getVkCommandBuffer(),
			static_cast<uint32_t>(entity.getModel().getVertexCount()), 
			1, // instance count
			0, // first vertex
			0  // first instance
		);
		*/
		vkCmdDrawIndexed(
			commandBuffer.getVkCommandBuffer(),
			static_cast<uint32_t>(entity.getModel()->getIndexCount()),
			1, // instance count
			0, // first index
			0, // vertex offset
			0  // first instance
		);
	}

	// ------------------------------------------



	// End the render pass.
	vkCmdEndRenderPass(commandBuffers[imageIndex].getVkCommandBuffer());

	endFrame(imageIndex);
}

bool VulkanRenderer::beginFrame(uint32_t* imageIndex)
{
	vkWaitForFences(Vulkan::getLogicalDevice()->getVkDevice(), 1, &inFlightFences[currentFrame], VK_TRUE, UINT64_MAX);

	// Acquire an image from the swapchain.
	auto result = vkAcquireNextImageKHR(
		Vulkan::getLogicalDevice()->getVkDevice(), 
		window->getSwapchain().getVkSwapchain(), 
		UINT64_MAX, 
		imageAvailableSemaphores[currentFrame], 
		VK_NULL_HANDLE, 
		imageIndex
	);
	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		LOG_ERROR << "Acquired swapchain image is out of data! Recreatig the swapchain...";
		window->recreateSwapchain();
		// The swapchin is now reconstructed. But the rest of this frames rendering execution must be skipped.
		return false;
	}
	if (result != VK_SUCCESS)
	{
		if (result == VK_SUBOPTIMAL_KHR)
		{
			LOG_WARN << "Acquired swapchain image is suboptimal! Continuing...";
		}
		else
		{
			throw COLIBRI_EXCEPTION("Failed to acquire swapchain image!");
		}
	}

	// Check if a previous frame is using this image (i.e. there is its fence to wait on).
	if (imagesInFlight[*imageIndex] != VK_NULL_HANDLE) {
		vkWaitForFences(Vulkan::getLogicalDevice()->getVkDevice(), 1, &imagesInFlight[*imageIndex], VK_TRUE, UINT64_MAX);
	}

	// Mark the image as now being in use by this frame
	imagesInFlight[*imageIndex] = inFlightFences[currentFrame];

	// Retrieve the command buffer for the image of the current frame and start its recording.
	commandBuffers[*imageIndex].startRecording();

	return true;
}

void VulkanRenderer::endFrame(uint32_t imageIndex)
{
	// Submit the command buffer.
	commandBuffers[imageIndex].stopRecording();

	VkSubmitInfo submitInfo {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore executeCommandsWaitSemaphores[] = { imageAvailableSemaphores[currentFrame] };
	VkPipelineStageFlags executeCommandsWaitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	//VkPipelineStageFlags executeCommandsWaitStages[] = { VK_PIPELINE_STAGE_TRANSFER_BIT };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = executeCommandsWaitSemaphores;
	submitInfo.pWaitDstStageMask = executeCommandsWaitStages;

	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers[imageIndex].getVkCommandBuffer();

	VkSemaphore executeCommandsSignalSemaphores[] = { renderFinishedSemaphores[currentFrame] };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = executeCommandsSignalSemaphores;

	vkResetFences(Vulkan::getLogicalDevice()->getVkDevice(), 1, &inFlightFences[currentFrame]);
	if (vkQueueSubmit(Vulkan::getLogicalDevice()->getGraphicsQueue(), 1, &submitInfo, inFlightFences[currentFrame]) != VK_SUCCESS)
	{
		throw COLIBRI_EXCEPTION("Failed to submit Vulkan draw command buffer!");
	}

	// Present image.
	VkSemaphore presentSignalSemaphores[] = { renderFinishedSemaphores[currentFrame] };
	VkPresentInfoKHR presentInfo {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = presentSignalSemaphores;
	VkSwapchainKHR swapChains[] = { window->getSwapchain().getVkSwapchain() };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &swapChains[0];
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr; // Not really necessary when only using a single swapchain.

	auto result = vkQueuePresentKHR(Vulkan::getLogicalDevice()->getPresentQueue(), &presentInfo);
	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		LOG_ERROR << "Presented swapchain image is out of data! Recreatig the swapchain...";
		window->recreateSwapchain();
	}
	else if (result == VK_SUBOPTIMAL_KHR)
	{
		LOG_ERROR << "Presented swapchain image is suboptimal! Recreatig the swapchain...";
		window->recreateSwapchain();
	}
	else if (result != VK_SUCCESS)
	{
		throw COLIBRI_EXCEPTION("Failed to present to Vulkan queue!");
	}

	currentFrame = (currentFrame + 1) % maxFramesInFlight;
}

void VulkanRenderer::waitIdle()
{
	vkDeviceWaitIdle(Vulkan::getLogicalDevice()->getVkDevice());
}

Window* VulkanRenderer::getWindow() const
{
	return window.get();
}

const std::shared_ptr<VulkanWindow>& VulkanRenderer::getVulkanWindow() const
{
	return window;
}

const std::shared_ptr<VulkanRenderPass>& VulkanRenderer::getRenderPass() const
{
	return renderPass;
}


COLIBRI_END
