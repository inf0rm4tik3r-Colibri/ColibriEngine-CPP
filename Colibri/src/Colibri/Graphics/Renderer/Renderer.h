#pragma once

#ifndef _RENDERER_H_
#define _RENDERER_H_

#include "../../Components/EntityComponentSystem.fwd.h"
#include "../../Components/EntityComponentSystem.h"
#include "../Window/Window.h"
#include "Renderer.fwd.h"
#include "RendererBuilder.fwd.h"
#include "RendererBuilder.h"

COLIBRI_BEGIN

class Renderer
{
public:
	virtual ~Renderer();

	static RendererBuilder builder();

	virtual void render(const EntityComponentSystem& ecs) = 0;
	virtual void waitIdle() = 0;

	virtual Window* getWindow() const = 0;
};

COLIBRI_END

#endif // _RENDERER_H_
