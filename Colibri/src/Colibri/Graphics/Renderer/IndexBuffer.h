#pragma once

#ifndef _INDEX_BUFFER_H_
#define _INDEX_BUFFER_H_

#include "../Model/Vertex.h"

COLIBRI_BEGIN

class IndexBuffer
{
public:
	static std::unique_ptr<IndexBuffer> create(size_t size, size_t sizePerIndex);

	virtual ~IndexBuffer();

	virtual void fill(const std::initializer_list<uint32_t>& indices) = 0;
};

COLIBRI_END

#endif // _INDEX_BUFFER_H_
