#include "pch.h"
#include "RendererBuilder.h"
#include "../../Core/Engine.h"
#include "VulkanRenderer.h"

COLIBRI_BEGIN

void RendererBuilder::check()
{
	if (initBits != 0) {
		throw ILLEGAL_STATE_EXCEPTION(formatRequiredAttributesMessage());
	}
}

std::string RendererBuilder::formatRequiredAttributesMessage() {
	std::vector<std::string> attributes {};
	if ((initBits & init_bit_window) != 0) attributes.push_back("window");
	return std::string("Cannot build renderer, some of required attributes are not set ")
		.append(string::toString(attributes, ", "));
}

RendererBuilder::RendererBuilder()
{
	// Default initialization.
}

std::unique_ptr<Renderer> RendererBuilder::build()
{
#ifdef COLIBRI_DEBUG
	check();
#endif
	switch (Engine::getConfig().getGraphicsApi())
	{
		case GraphicsApi::vulkan:
			return std::make_unique<VulkanRenderer>(_window);
		case GraphicsApi::opengl:
			LOG_ERROR << "OpenGL is not yet supported.";
			throw UNSUPPORTED_OPERATION_EXCEPTION("Cannot create renderer with unsupported graphics api: OpenGL.");
			break;
		default:
			throw COLIBRI_EXCEPTION("Unknown graphics api.");
	}
}

RendererBuilder RendererBuilder::window(const std::shared_ptr<Window>& window)
{
	_window = window;
	initBits &= ~init_bit_window;
	return *this;
}

COLIBRI_END
