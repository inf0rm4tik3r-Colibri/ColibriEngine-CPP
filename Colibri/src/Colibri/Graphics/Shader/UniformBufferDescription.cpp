#include "pch.h"
#include "UniformBufferDescription.h"

COLIBRI_BEGIN

UniformBufferDescription::UniformBufferDescription(const UniformBufferBinding& binding,
												   const size_t sizeInBytes,
												   const std::function<void(void* buffer, size_t sizeInBytes, const Entity& entity)>& fillBuffer)
	: id(IDS::genRand64()), 
	  binding(binding), 
	  sizeInBytes(sizeInBytes), 
	  fillBuffer(fillBuffer)
{
}

UniformBufferDescriptionBuilder UniformBufferDescription::builder()
{
	return UniformBufferDescriptionBuilder();
}

inline bool operator==(const UniformBufferDescription& a, const UniformBufferDescription& b)
{
	return a.getId() == b.getId();
}

COLIBRI_END
