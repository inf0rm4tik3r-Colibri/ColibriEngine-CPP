#include "pch.h"
#include "ShaderDescription.h"

COLIBRI_BEGIN

ShaderDescription::ShaderDescription(const ShaderType type,
                                     const std::vector<std::string>& sources,
                                     const std::vector<std::shared_ptr<UniformBufferDescription>>& uniformBufferDescriptions,
                                     const std::string& entryPoint)
    : type(type), 
      sources(sources), 
      uniformBufferDescriptions(uniformBufferDescriptions), 
      entryPoint(entryPoint)
{
}

ShaderDescriptionBuilder ShaderDescription::builder()
{
    return ShaderDescriptionBuilder();
}

const ShaderType ShaderDescription::getType() const
{
    return type;
}

const std::vector<std::string>& ShaderDescription::getSources() const
{
    return sources;
}

const std::vector<std::shared_ptr<UniformBufferDescription>>& ShaderDescription::getUniformBufferDescriptions() const
{
    return uniformBufferDescriptions;
}

const std::string& ShaderDescription::getEntryPoint() const
{
    return entryPoint;
}

COLIBRI_END
