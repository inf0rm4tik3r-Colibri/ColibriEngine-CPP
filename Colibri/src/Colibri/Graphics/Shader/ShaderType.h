#pragma once

#ifndef _SHADER_TYPE_H_
#define _SHADER_TYPE_H_

COLIBRI_BEGIN

enum class ShaderType
{
	vertex,
	compute,
	tesselationControl,
	tesselationEvaluation,
	fragment
};

COLIBRI_END

#endif // _SHADER_TYPE_H_
