#include "pch.h"
#include "ShaderDescriptionBuilder.h"
#include "../../Exception/Exceptions.h"
#include "../../Core/Engine.h"
#include "../../Platform/Vulkan/VulkanWindow.h"

COLIBRI_BEGIN

void ShaderDescriptionBuilder::check()
{
	if (initBits != 0) {
		throw ILLEGAL_STATE_EXCEPTION(formatRequiredAttributesMessage());
	}
}

std::string ShaderDescriptionBuilder::formatRequiredAttributesMessage() {
	std::vector<std::string> attributes {};
	if ((initBits & init_bit_type) != 0) attributes.push_back("type");
	if ((initBits & init_bit_sources) != 0) attributes.push_back("sources");
	if (_sources.empty()) attributes.push_back("sources");
	return std::string("Cannot build shader description, some of required attributes are not set ")
		.append(string::toString(attributes, ", "));
}

ShaderDescriptionBuilder::ShaderDescriptionBuilder()
{
	// Default initialization.
	_entryPoint = "main";
}

std::unique_ptr<ShaderDescription> ShaderDescriptionBuilder::build()
{
#ifdef COLIBRI_DEBUG
	check();
#endif
	return std::make_unique<ShaderDescription>(_type, _sources, _ubds, _entryPoint);
}

ShaderDescriptionBuilder ShaderDescriptionBuilder::type(const ShaderType& type)
{
	_type = type;
	initBits &= ~init_bit_type;
	return *this;
}

ShaderDescriptionBuilder ShaderDescriptionBuilder::addSource(const char* sourceLocation)
{
	_sources.push_back(sourceLocation);
	initBits &= ~init_bit_sources;
	return *this;
}

ShaderDescriptionBuilder ShaderDescriptionBuilder::addSource(const std::string& sourceLocation)
{
	_sources.push_back(sourceLocation);
	initBits &= ~init_bit_sources;
	return *this;
}

ShaderDescriptionBuilder ShaderDescriptionBuilder::addUniformBufferDescription(const std::shared_ptr<UniformBufferDescription>& ubd)
{
	_ubds.push_back(ubd);
	return *this;
}

ShaderDescriptionBuilder ShaderDescriptionBuilder::entryPoint(const char* entryPoint)
{
	_entryPoint = entryPoint;
	return *this;
}

ShaderDescriptionBuilder ShaderDescriptionBuilder::entryPoint(const std::string& entryPoint)
{
	_entryPoint = entryPoint;
	return *this;
}

COLIBRI_END
