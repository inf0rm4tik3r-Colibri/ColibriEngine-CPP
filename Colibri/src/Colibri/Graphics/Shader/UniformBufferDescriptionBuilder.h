#pragma once

#ifndef _UNIFORM_BUFFER_DESCRIPTION_BUILDER_H_
#define _UNIFORM_BUFFER_DESCRIPTION_BUILDER_H_

#include "UniformBufferDescriptionBuilder.fwd.h"
#include "UniformBufferDescription.fwd.h"
#include "UniformBufferDescription.h"
#include "UniformBufferBinding.h"
#include "../../Components/Entity.h"

COLIBRI_BEGIN

class UniformBufferDescriptionBuilder
{
private:
	static const constexpr long init_bit_binding = 1L;
	static const constexpr long init_bit_size = 2L;
	static const constexpr long init_bit_fill_buffer = 4L;
	long initBits = 7L; // Heighes bit value * 2 - 1

	const UniformBufferBinding* _binding;
	size_t _size;
	std::function<void(void* buffer, size_t sizeInBytes, const Entity& entity)> _fillBuffer;

	void check();
	std::string formatRequiredAttributesMessage();

public:
	UniformBufferDescriptionBuilder();

	std::unique_ptr<UniformBufferDescription> build();
	UniformBufferDescriptionBuilder binding(const UniformBufferBinding& binding);
	UniformBufferDescriptionBuilder sizeInBytes(const size_t size);
	UniformBufferDescriptionBuilder fillBuffer(const std::function<void(void* buffer, size_t sizeInBytes, const Entity& entity)>& fillBuffer);
};

COLIBRI_END

#endif // _UNIFORM_BUFFER_DESCRIPTION_BUILDER_H_
