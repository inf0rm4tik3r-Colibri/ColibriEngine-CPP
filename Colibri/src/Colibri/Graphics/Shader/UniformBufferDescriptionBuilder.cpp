#include "pch.h"
#include "UniformBufferDescriptionBuilder.h"
#include "../../Exception/Exceptions.h"
#include "../../Core/Engine.h"
#include "../../Platform/Vulkan/VulkanWindow.h"

COLIBRI_BEGIN

void UniformBufferDescriptionBuilder::check()
{
	if (initBits != 0) {
		throw ILLEGAL_STATE_EXCEPTION(formatRequiredAttributesMessage());
	}
}

std::string UniformBufferDescriptionBuilder::formatRequiredAttributesMessage() {
	std::vector<std::string> attributes {};
	if ((initBits & init_bit_binding) != 0) attributes.push_back("binding");
	if ((initBits & init_bit_size) != 0) attributes.push_back("size in bytes");
	if ((initBits & init_bit_fill_buffer) != 0) attributes.push_back("buffer fill function");
	return std::string("Cannot build uniform buffer description, some of required attributes are not set ")
		.append(string::toString(attributes, ", "));
}

UniformBufferDescriptionBuilder::UniformBufferDescriptionBuilder()
{
	// Default initialization.
}

std::unique_ptr<UniformBufferDescription> UniformBufferDescriptionBuilder::build()
{
#ifdef COLIBRI_DEBUG
	check();
#endif
	return std::make_unique<UniformBufferDescription>(*_binding, _size, _fillBuffer);
}

UniformBufferDescriptionBuilder UniformBufferDescriptionBuilder::binding(const UniformBufferBinding& binding)
{
	_binding = &binding;
	initBits &= ~init_bit_binding;
	return *this;
}

UniformBufferDescriptionBuilder UniformBufferDescriptionBuilder::sizeInBytes(const size_t size)
{
	_size = size;
	initBits &= ~init_bit_size;
	return *this;
}

UniformBufferDescriptionBuilder UniformBufferDescriptionBuilder::fillBuffer(const std::function<void(void* buffer, size_t sizeInBytes, const Entity& entity)>& fillBuffer)
{
	_fillBuffer = fillBuffer;
	initBits &= ~init_bit_fill_buffer;
	return *this;
}

COLIBRI_END
