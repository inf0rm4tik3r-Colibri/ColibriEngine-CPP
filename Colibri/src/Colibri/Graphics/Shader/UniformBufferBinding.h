#pragma once

#ifndef _UNIFORM_BUFFER_BINDING_H_
#define _UNIFORM_BUFFER_BINDING_H_

#include "ShaderStage.h"

COLIBRI_BEGIN

class UniformBufferBinding
{
public:
	/* The binding point of the uniform buffer. */
	const uint32_t binding;

	/* How many instances of the layout should exist. */
	const uint32_t count;

	/* In which this uniform buffer binding is available. */
	const std::vector<ShaderStage> shaderStages;

public:
	UniformBufferBinding(uint32_t binding, uint32_t count, ShaderStage shaderStage);
	UniformBufferBinding(uint32_t binding, uint32_t count, std::array<ShaderStage, 2> shaderStages);
	UniformBufferBinding(uint32_t binding, uint32_t count, std::array<ShaderStage, 3> shaderStages);
	UniformBufferBinding(uint32_t binding, uint32_t count, std::vector<ShaderStage>& shaderStages);
};

COLIBRI_END

#endif // _UNIFORM_BUFFER_BINDING_H_
