#include "pch.h"
#include "UniformBufferBinding.h"

COLIBRI_BEGIN

UniformBufferBinding::UniformBufferBinding(uint32_t binding, uint32_t count, ShaderStage shaderStage)
	: binding(binding), count(count), shaderStages({ shaderStage })
{
}

UniformBufferBinding::UniformBufferBinding(uint32_t binding, uint32_t count, std::array<ShaderStage, 2> shaderStages)
	: binding(binding), count(count), shaderStages({ shaderStages[0], shaderStages[1] })
{
}

UniformBufferBinding::UniformBufferBinding(uint32_t binding, uint32_t count, std::array<ShaderStage, 3> shaderStages)
	: binding(binding), count(count), shaderStages({ shaderStages[0], shaderStages[1], shaderStages[2] })
{
}

UniformBufferBinding::UniformBufferBinding(uint32_t binding, uint32_t count, std::vector<ShaderStage>& shaderStages)
	: binding(binding), count(count), shaderStages({ shaderStages.begin(), shaderStages.end() })
{
}

COLIBRI_END
