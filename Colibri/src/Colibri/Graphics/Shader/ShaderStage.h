#pragma once

#ifndef _SHADER_STAGE_H_
#define _SHADER_STAGE_H_

COLIBRI_BEGIN

enum class ShaderStage
{
	vertex,
	compute,
	tesselationControl,
	tesselationEvaluation,
	fragment
};

COLIBRI_END

#endif // _SHADER_STAGE_H_
