#pragma once

#ifndef _SHADER_DESCRIPTION_H_
#define _SHADER_DESCRIPTION_H_

#include "UniformBufferDescription.h"
#include "ShaderType.h"
#include "ShaderDescription.fwd.h"
#include "ShaderDescriptionBuilder.fwd.h"
#include "ShaderDescriptionBuilder.h"

COLIBRI_BEGIN

class ShaderDescription
{
private:
	const ShaderType type;
	const std::vector<std::string> sources;
	const std::vector<std::shared_ptr<UniformBufferDescription>> uniformBufferDescriptions;
	const std::string entryPoint;

public:
	ShaderDescription(const ShaderType type,
					  const std::vector<std::string>& sources,
					  const std::vector<std::shared_ptr<UniformBufferDescription>>& uniformBufferDescriptions,
					  const std::string& entryPoint);

	static ShaderDescriptionBuilder builder();

	const ShaderType getType() const;
	const std::vector<std::string>& getSources() const;
	const std::vector<std::shared_ptr<UniformBufferDescription>>& getUniformBufferDescriptions() const;
	const std::string& getEntryPoint() const;
};

COLIBRI_END

#endif // _SHADER_DESCRIPTION_H_

