#pragma once

#ifndef _SHADER_DESCRIPTION_BUILDER_H_
#define _SHADER_DESCRIPTION_BUILDER_H_

#include "ShaderDescriptionBuilder.fwd.h"
#include "ShaderDescription.fwd.h"
#include "ShaderDescription.h"
#include "ShaderType.h"
#include "UniformBufferDescription.h"

COLIBRI_BEGIN

class ShaderDescriptionBuilder
{
private:
	static const constexpr long init_bit_type = 1L;
	static const constexpr long init_bit_sources = 2L;
	long initBits = 3L; // Heighes bit value * 2 - 1

	ShaderType _type;
	std::vector<std::string> _sources;
	std::vector<std::shared_ptr<UniformBufferDescription>> _ubds;
	std::string _entryPoint;

	void check();
	std::string formatRequiredAttributesMessage();

public:
	ShaderDescriptionBuilder();

	std::unique_ptr<ShaderDescription> build();
	ShaderDescriptionBuilder type(const ShaderType& type);
	ShaderDescriptionBuilder addSource(const char* sourceLocation);
	ShaderDescriptionBuilder addSource(const std::string& sourceLocation);
	ShaderDescriptionBuilder addUniformBufferDescription(const std::shared_ptr<UniformBufferDescription>& ubd);
	ShaderDescriptionBuilder entryPoint(const char* entryPoint);
	ShaderDescriptionBuilder entryPoint(const std::string& entryPoint);
};

COLIBRI_END

#endif // _SHADER_DESCRIPTION_BUILDER_H_
