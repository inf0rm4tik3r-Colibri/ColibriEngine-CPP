#pragma once

#ifndef _UNIFORM_BUFFER_DESCRIPTION_H_
#define _UNIFORM_BUFFER_DESCRIPTION_H_

#include "UniformBuffer.h"
#include "UniformBufferDescription.fwd.h"
#include "UniformBufferDescriptionBuilder.fwd.h"
#include "UniformBufferDescriptionBuilder.h"
#include "UniformBufferBinding.h"
#include "../../Components/Entity.fwd.h"
#include "../../Components/Entity.h"

COLIBRI_BEGIN

class UniformBufferDescription
{
private:
	const uint64_t id;
	const UniformBufferBinding binding;
	const size_t sizeInBytes;

public:
	const std::function<void(void* buffer, size_t sizeInBytes, const Entity& entity)> fillBuffer;

public:
	UniformBufferDescription(const UniformBufferBinding& binding,
							 const size_t sizeInBytes,
							 const std::function<void(void* buffer, size_t sizeInBytes, const Entity& entity)>& fillBuffer);

	UniformBufferDescription(const UniformBufferDescription& other) = delete;
	UniformBufferDescription(UniformBufferDescription&& other) = default;

	UniformBufferDescription& operator=(const UniformBufferDescription& other) = delete;
	UniformBufferDescription& operator=(UniformBufferDescription&& other) = default;

	inline friend bool operator==(const UniformBufferDescription& a,
								  const UniformBufferDescription& b);

	static UniformBufferDescriptionBuilder builder();

	inline uint64_t getId() const { return id; }
	inline const UniformBufferBinding& getBinding() const { return binding; }
	inline size_t getSizeInBytes() const { return sizeInBytes; }
};

COLIBRI_END

#endif // _UNIFORM_BUFFER_DESCRIPTION_H_
