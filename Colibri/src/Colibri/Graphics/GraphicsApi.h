#pragma once

#ifndef _GRAPHICS_API_H_
#define _GRAPHICS_API_H_

#include "../StaticConfig.h"
#include <boost/assign/list_of.hpp>
#include <boost/unordered_map.hpp>
#include <string>

COLIBRI_BEGIN

enum class GraphicsApi
{
	vulkan,
	opengl
};

const boost::unordered_map<GraphicsApi, const std::string> GraphicsApiToString = boost::assign::map_list_of
(GraphicsApi::vulkan, "Vulkan")
(GraphicsApi::opengl, "OpenGL");

const boost::unordered_map<std::string, GraphicsApi> GraphicsApiFromString = boost::assign::map_list_of
("Vulkan", GraphicsApi::vulkan)
("OpenGL", GraphicsApi::opengl);

/*
template <GraphicsApi n> struct GraphicsApiString { static const char* const value; };
template <GraphicsApi n> const char* const GraphicsApiString<n>::value = "error";

template <> struct GraphicsApiString<GraphicsApi::vulkan> { static const char* const value; };
const char* const GraphicsApiString<GraphicsApi::vulkan>::value = "Vulkan";

template <> struct GraphicsApiString<GraphicsApi::opengl> { static const char* const value; };
const char* const GraphicsApiString<GraphicsApi::opengl>::value = "OpenGL";
*/

COLIBRI_END

#endif // _GRAPHICS_API_H_
