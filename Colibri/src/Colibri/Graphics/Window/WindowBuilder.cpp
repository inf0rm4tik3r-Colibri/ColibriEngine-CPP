#include "pch.h"
#include "WindowBuilder.h"
#include "../../Core/Engine.h"
#include "../../Platform/Vulkan/VulkanWindow.h"

COLIBRI_BEGIN

void WindowBuilder::check()
{
	if (initBits != 0) {
		throw ILLEGAL_STATE_EXCEPTION(formatRequiredAttributesMessage());
	}
}

std::string WindowBuilder::formatRequiredAttributesMessage() {
	std::vector<std::string> attributes {};
	if ((initBits & init_bit_width) != 0) attributes.push_back("width");
	if ((initBits & init_bit_height) != 0) attributes.push_back("height");
	if ((initBits & init_bit_title) != 0) attributes.push_back("title");
	return std::string("Cannot build Window, some of required attributes are not set ")
		.append(string::toString(attributes, ", "));
}

std::unique_ptr<Window> WindowBuilder::build()
{
#ifdef COLIBRI_DEBUG
	check();
#endif
	switch (Engine::getConfig().getGraphicsApi())
	{
		case GraphicsApi::vulkan:
			return std::make_unique<VulkanWindow>(_width, _height, _title);
		case GraphicsApi::opengl:
			LOG_ERROR << "OpenGL is not yet supported.";
			throw UNSUPPORTED_OPERATION_EXCEPTION("Cannot create window with unsupported graphics api: OpenGL.");
			break;
		default:
			throw COLIBRI_EXCEPTION("Unknown graphics api.");
	}
}

WindowBuilder WindowBuilder::width(unsigned int width)
{
	_width = width;
	initBits &= ~init_bit_width;
	return *this;
}

WindowBuilder WindowBuilder::height(unsigned int height)
{
	_height = height;
	initBits &= ~init_bit_height;
	return *this;
}

WindowBuilder WindowBuilder::title(const char* title)
{
	_title = std::string(title);
	initBits &= ~init_bit_title;
	return *this;
}

WindowBuilder WindowBuilder::title(const std::string& title)
{
	_title = title;
	initBits &= ~init_bit_title;
	return *this;
}

COLIBRI_END
