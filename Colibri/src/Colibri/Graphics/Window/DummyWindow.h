#pragma once

#ifndef _DUMMY_WINDOW_H_
#define _DUMMY_WINDOW_H_

#include "Window.h"

COLIBRI_BEGIN

class DummyWindow : public Window
{
public:
	DummyWindow()
		: Window(1280, 720, "Dummy")
	{
	}
};

COLIBRI_END

#endif // _DUMMY_WINDOW_H_
