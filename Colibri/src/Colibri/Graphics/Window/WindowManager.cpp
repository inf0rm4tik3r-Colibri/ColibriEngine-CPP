#include "pch.h"
#include "WindowManager.h"

COLIBRI_BEGIN

void WindowManager::initializeGLFW()
{
	glfwInit();
}

void WindowManager::terminateGLFW()
{
	glfwTerminate();
}

WindowManager::WindowManager() noexcept
{
	LOG_INFO << "Created window manager.";
}

WindowManager::~WindowManager() noexcept
{
	LOG_INFO << "Destroyed window manager.";
}

void WindowManager::addWindow(const std::shared_ptr<Window>& window)
{
	if (std::find(windows.begin(), windows.end(), window) != windows.end())
	{
		throw ILLEGAL_ARGUMENT_EXCEPTION(
			"This window is already managed and can therefore not be added again."
		);
	}
	windows.push_back(window);

	windowRenderStopRequested[window.get()] = false;
	windowFramebufferNotRenderable[window.get()] = false;

	window->stopRendering.subscribe(
		[&](bool stopRendering) {
			LOG_INFO << "Reneder stop request by window: " << stopRendering;
			windowRenderStopRequested[window.get()] = stopRendering;
		}
	);

	window->frameBufferResize.subscribe(
		[&](int width, int height) {
			if (width == 0 || height == 0)
			{
				LOG_INFO << "There is noting to render with a framebuffer of size: w=" << width << ", h=" << height << ". Pausing rendering...";
				windowFramebufferNotRenderable[window.get()] = true;
			}
			else
			{
				windowFramebufferNotRenderable[window.get()] = false;
			}
		}
	);
}

void WindowManager::setActiveWindow(const std::shared_ptr<Window>& window)
{
	if (std::find(windows.begin(), windows.end(), window) == windows.end())
	{
		throw ILLEGAL_ARGUMENT_EXCEPTION(
			"This window is not managed and can therefore not be set active. Call addWindow with the given window before calling setActive."
		);
	}
	activeWindow = window;
}

Window* WindowManager::getActiveWindow() const
{
	return activeWindow.get();
}

bool WindowManager::isWindowSuitableForRendering(Window* window) const
{
	return !(windowRenderStopRequested.at(window) || windowFramebufferNotRenderable.at(window));
}

COLIBRI_END
