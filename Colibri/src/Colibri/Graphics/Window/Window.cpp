#include "pch.h"
#include "Window.h"

COLIBRI_BEGIN

Window::Window(unsigned int width, unsigned int height, const std::string& title) noexcept
	: width(width), height(height), title(title)
{
	LOG_INFO << "Creating GLFW window: \"" << title << "\"";
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	// glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
	
	// Let the created GLFW window track a pointer to this engine window instance.
	// The provided pointer to "this" is alter to used to access member functions of this instance in callbacks.
	glfwSetWindowUserPointer(window, this);

	// Regsiter callbacks.
	glfwSetFramebufferSizeCallback(
		window,
		[](GLFWwindow* window, int width, int height)
		{
			reinterpret_cast<Window*>(glfwGetWindowUserPointer(window))->frameBufferResize.next(width, height);
		}
	);
}

Window::~Window() noexcept
{
	if (window != nullptr)
	{
		// Clean up callbacks.
		glfwSetFramebufferSizeCallback(window, nullptr);

		LOG_INFO << "Destroying window: " << window;
		glfwDestroyWindow(window);
	}
}

Window::Window(Window&& other) noexcept
	: window(std::move(other.window)),
	width(std::move(other.width)),
	height(std::move(other.height)),
	title(std::move(other.title))
{
	other.window = nullptr;
}

Window& Window::operator=(Window&& other) noexcept
{
	window = std::move(other.window);
	width = std::move(other.width);
	height = std::move(other.height);
	title = std::move(other.title);
	other.window = nullptr;
	return *this;
}

WindowBuilder Window::builder()
{
	return WindowBuilder();
}

void Window::close()
{
	this->~Window();
}

bool Window::shouldClose()
{
	return glfwWindowShouldClose(window);
}

std::string Window::getTitle() const {
	return title;
}

void Window::setTitle(const std::string& title)
{
	glfwSetWindowTitle(window, title.c_str());
}

GLFWwindow& Window::getHandle() const
{
	return *window;
}

COLIBRI_END
