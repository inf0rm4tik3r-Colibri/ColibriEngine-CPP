#pragma once

#ifndef _WINDOW_H_
#define _WINDOW_H_

#include "../../Platform/PlatformLibraries.h"
#include "../../Observer/Pattern.h"
#include "Window.fwd.h"
#include "WindowBuilder.fwd.h"
#include "WindowBuilder.h"

COLIBRI_BEGIN

class Window
{

private:
	GLFWwindow* window;
	unsigned int width;
	unsigned int height;
	std::string title;

public:
	Subject<bool> stopRendering; // Indicates if rendering should be stopped(true) or started(false).
	Subject<int, int> frameBufferResize; // Data: width, height

public:
	Window(unsigned int width, unsigned int height, const std::string& title) noexcept;
	virtual ~Window() noexcept;
	Window(const Window& other) = delete;
	Window(Window&& other) noexcept;
	Window& operator=(const Window& other) = delete;
	Window& operator=(Window&& other) noexcept;

	static WindowBuilder builder();

	void close();
	bool shouldClose();
	std::string getTitle() const;
	void setTitle(const std::string& title);
	GLFWwindow& getHandle() const;

protected:
	// virtual void onFramebufferResize(int width, int height) = 0;


private:
	Window() noexcept;
};

COLIBRI_END

#endif // _WINDOW_H_
