#pragma once

#ifndef _WINDOW_BUILDER_H_
#define _WINDOW_BUILDER_H_

#include "WindowBuilder.fwd.h"
#include "Window.fwd.h"
#include "Window.h"

COLIBRI_BEGIN

class WindowBuilder
{
private:
	static const constexpr long init_bit_width   = 1L;
	static const constexpr long init_bit_height  = 2L;
	static const constexpr long init_bit_title   = 4L;
	long initBits = 7L; // Heighes bit value * 2 - 1

	unsigned int _width;
	unsigned int _height;
	std::string _title;

	void check();
	std::string formatRequiredAttributesMessage();

public:
	std::unique_ptr<Window> build();
	WindowBuilder width(unsigned int width);
	WindowBuilder height(unsigned int height);
	WindowBuilder title(const char* title);
	WindowBuilder title(const std::string& title);
};

COLIBRI_END

#endif // _WINDOW_BUILDER_H_
