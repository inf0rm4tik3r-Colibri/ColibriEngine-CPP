#pragma once

#ifndef _WINDOW_MANAGER_H_
#define _WINDOW_MANAGER_H_

#include "Window.h"

COLIBRI_BEGIN

class WindowManager
{
private:
	std::vector<std::shared_ptr<Window>> windows;
	// std::vector<std::shared_ptr<Window>> windowFocusHistory;
	std::shared_ptr<Window> activeWindow;

	std::map<Window*, bool> windowRenderStopRequested;
	std::map<Window*, bool> windowFramebufferNotRenderable;

public:
	static void initializeGLFW();
	static void terminateGLFW();

	WindowManager() noexcept;
	~WindowManager() noexcept;

	WindowManager(const WindowManager& other) = delete;
	WindowManager(WindowManager&& rhs) = delete;
	WindowManager& operator=(const WindowManager& other) = delete;
	WindowManager& operator=(WindowManager&& rhs) = delete;

	void addWindow(const std::shared_ptr<Window>& window);
	void setActiveWindow(const std::shared_ptr<Window>& window);
	Window* getActiveWindow() const;

	bool isWindowSuitableForRendering(Window* window) const;
};

COLIBRI_END

#endif // _WINDOW_MANAGER_H_
