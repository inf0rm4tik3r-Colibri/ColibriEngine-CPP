#pragma once

#ifndef _STATIC_CONFIG_H_
#define _STATIC_CONFIG_H_

// System file separator.
#if defined(__unix__)
#define FILE_SEPARATOR "/"
#elif defined(_MSC_VER)
#define FILE_SEPARATOR "\\"
#else
#define FILE_SEPARATOR "/"
#endif

// SRC root folder.
#if defined(__unix__)
#define SRC_ROOT "src/"
#elif defined(_MSC_VER)
#define SRC_ROOT "src\\"
#else
#define SRC_ROOT "src/"
#endif

// The namespace of the engine.
#define COLIBRI_NAMESPACE Colibri

#define COLIBRI_BEGIN namespace COLIBRI_NAMESPACE {
#define COLIBRI_END }

// Allows "in debug mode" check at runtime.
#ifdef NDEBUG
#define DEBUG_MODE false
#else
#define DEBUG_MODE true
#endif // NDEBUG

// Default Vulkan logging.
#define VULKAN_LOG_VERBOSE false
#define VULKAN_LOG_GENERAL false
#define VULKAN_LOG_INFO    false
#define VULKAN_LOG_WARN    true
#define VULKAN_LOG_ERROR   true

#endif // _STATIC_CONFIG_H_
