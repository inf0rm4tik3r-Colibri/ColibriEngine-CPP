#pragma once

class Timer
{
private:
	// If the timer is not started, the tick() function will do nothing.
	bool started;
	
	// Current point in time.
	std::chrono::steady_clock::time_point current, last;
	
	// Time differences: The last delte (difference between current and last), 
	// the maximum, and the time elapsed since the last reset.
	std::chrono::duration<long long, std::nano> delta, maxDelta, elapsed;
	
	// Increments with every call to the tick() function.
	unsigned long long ticks;

public:
	Timer()
	{
		reset();
	}

	void reset()
	{
		last = current = std::chrono::high_resolution_clock::now();
		resetElapsed();
		resetDelta();
		resetMaxDelta();
		resetTicks();
		start();
	}

	void resetElapsed()
	{
		elapsed = std::chrono::steady_clock::duration(0);
	}

	void resetDelta()
	{
		delta = std::chrono::steady_clock::duration(0);
	}

	void resetMaxDelta()
	{
		maxDelta = std::chrono::steady_clock::duration(0);
	}

	void resetTicks()
	{
		ticks = 0;
	}

	void start()
	{
		started = true;
	}

	void stop()
	{
		started = false;
	}

	void tick() {
		if (started)
		{
			current = std::chrono::high_resolution_clock::now();

			delta = current - last;
			maxDelta = std::max(maxDelta, delta);

			elapsed += delta;

			last = current;
			ticks++; // Catch overflow?
		}
	}

	unsigned long long getTicks() const
	{
		return ticks;
	}

	// HELPER

	double nanosecondsToSeconds(long long nanoseconds) const
	{
		return nanoseconds / 1'000'000'000.0;
	}

	double nanosecondsToMilliseconds(long long nanoseconds) const
	{
		return nanoseconds / 1'000'000.0;
	}

	// ELAPSED

	long long getElapsedNs() const
	{
		return elapsed.count();
	}

	double getElapsedMs() const
	{
		return nanosecondsToMilliseconds(getElapsedNs());
	}

	double getElapsedS() const
	{
		return nanosecondsToSeconds(getElapsedNs());
	}

	// LAST DELTA

	long long getDeltaNs() const
	{
		return delta.count();
	}

	double getDeltaMs() const
	{
		return nanosecondsToMilliseconds(getDeltaNs());
	}

	double getDeltaS() const
	{
		return nanosecondsToSeconds(getDeltaNs());
	}

	// AVG DELTA

	long long getAvgDeltaNs() const
	{
		return getElapsedNs() / getTicks();
	}

	double getAvgDeltaMs() const
	{
		return nanosecondsToMilliseconds(getAvgDeltaNs());
	}

	double getAvgDeltaS() const
	{
		return nanosecondsToSeconds(getAvgDeltaNs());
	}

	// MAX DELTA

	long long getMaxDeltaNs() const
	{
		return maxDelta.count();
	}

	double getMaxDeltaMs() const
	{
		return nanosecondsToMilliseconds(getMaxDeltaNs());
	}

	double getMaxDeltaS() const
	{
		return nanosecondsToSeconds(getMaxDeltaNs());
	}

};
