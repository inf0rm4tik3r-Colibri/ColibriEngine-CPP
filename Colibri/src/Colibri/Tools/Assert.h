#pragma once

#ifndef _ASSERT_H_
#define _ASSERT_H_

#define ASSERT_MSG(expr, msg) assert(( (void)(msg), (expr) ))

#endif // _ASSERT_H_
