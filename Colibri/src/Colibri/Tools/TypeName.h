#pragma once

#ifndef _TYPE_NAME_H_
#define _TYPE_NAME_H_

COLIBRI_BEGIN

// -----------------------------------------------------------
// Default implementation.
// -----------------------------------------------------------
template <typename T>
struct TypeName
{
    static constexpr const char* get()
    {
        return typeid(T).name();
    }
    static constexpr const char* getShort()
    {
        return typeid(T).name()[0];
    }
};

// -----------------------------------------------------------
// A specialization for each type of those you want to support
// and don't like the string returned by typeid!
// -----------------------------------------------------------

template <>
struct TypeName<bool>
{
    static constexpr const char* get()
    {
        return "bool";
    }
    static constexpr const char* getShort()
    {
        return "b";
    }
};

template <>
struct TypeName<char>
{
    static constexpr const char* get()
    {
        return "char";
    }
    static constexpr const char* getShort()
    {
        return "c";
    }
};

template <>
struct TypeName<int>
{
    static constexpr const char* get()
    {
        return "int";
    }
    static constexpr const char* getShort()
    {
        return "i";
    }
};

template <>
struct TypeName<long>
{
    static constexpr const char* get()
    {
        return "long";
    }
    static constexpr const char* getShort()
    {
        return "l";
    }
};

template <>
struct TypeName<float>
{
    static constexpr const char* get()
    {
        return "float";
    }
    static constexpr const char* getShort()
    {
        return "f";
    }
};

template <>
struct TypeName<double>
{
    static constexpr const char* get()
    {
        return "double";
    }
    static constexpr const char* getShort()
    {
        return "d";
    }
};

COLIBRI_END

#endif // _TYPE_NAME_H_
