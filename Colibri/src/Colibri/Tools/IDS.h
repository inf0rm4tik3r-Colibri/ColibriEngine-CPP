#pragma once

#ifndef _IDS_H_
#define _IDS_H_

namespace IDS
{

inline uint_fast32_t genRand32() {
	static thread_local std::mt19937 generator(std::random_device {}());
	return generator();
}

inline uint_fast64_t genRand64() {
	return (uint_fast64_t)(genRand32()) << 32 | genRand32();
}

inline boost::uuids::uuid genUuid()
{
	static thread_local boost::uuids::random_generator uuidGenerator;
	uuidGenerator();
}

};

#endif // _IDS_H_
