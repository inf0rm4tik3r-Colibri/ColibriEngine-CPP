#pragma once

#ifndef _LOGGER_H_
#define _LOGGER_H_

COLIBRI_BEGIN

class Logger
{
public:
	enum class LogLevel : int
	{
		trace = 0,
		debug = 1,
		info = 2,
		warn = 3,
		error = 4
	};
	static constexpr const char* LOG_LEVEL_NAMES[5] = 
	{
		"trace", "debug", "info", "warn", "error"
	};
	static constexpr const char* LOG_LEVEL_NAMES_CAPITALIZED[5] = 
	{
		"TRACE", "DEBUG", "INFO", "WARN", "ERROR"
	};
	static constexpr const int LOG_LEVEL_NAME_LENGTH[5] =
	{
		5, 5, 4, 4, 5
	};
	static const constexpr LogLevel logLevelFromString(const char* string)
	{
		for (int i = 0; i < 5; i++)
		{
			if (strcmp(LOG_LEVEL_NAMES[i], string) == 0)
			{
				return static_cast<LogLevel>(i);
			}
		}
		throw COLIBRI_EXCEPTION("Unknown log level name encountered!");
	}
	static const constexpr char* logLevelString(LogLevel logLevel)
	{
		return LOG_LEVEL_NAMES[static_cast<int>(logLevel)];
	}
	static const constexpr char* logLevelStringCapitalized(LogLevel logLevel)
	{
		return LOG_LEVEL_NAMES_CAPITALIZED[static_cast<int>(logLevel)];
	}
	static const constexpr int logLevelStringLength(LogLevel logLevel)
	{
		return LOG_LEVEL_NAME_LENGTH[static_cast<int>(logLevel)];
	}
	static const constexpr bool isMinLevelNotReached(LogLevel logLevel, LogLevel minLogLevel)
	{
		return static_cast<int>(logLevel) < static_cast<int>(minLogLevel);
	}

	Logger();

	Logger& registerThread(std::thread::id threadId, const std::string& threadName);
	Logger& setLogLevel(LogLevel logLevel);
	Logger& setMinLogLevel(LogLevel logLevel);
	Logger& setFile(const char* file);
	Logger& setLine(int line);
	Logger& startNewLine();

	Logger& trace();
	Logger& trace(const char* file, int line);
	Logger& debug();
	Logger& debug(const char* file, int line);
	Logger& info();
	Logger& info(const char* file, int line);
	Logger& warn();
	Logger& warn(const char* file, int line);
	Logger& error();
	Logger& error(const char* file, int line);

	friend Logger& operator<<(Logger& logger, const std::string& message);
	friend Logger& operator<<(Logger& logger, const char* message);
	friend Logger& operator<<(Logger& logger, const std::vector<const char*>& messages);
	friend Logger& operator<<(Logger& logger, const std::vector<std::string>& messages);
	friend Logger& operator<<(Logger& logger, const char** messages);
	friend Logger& operator<<(Logger& logger, const std::tuple<const char**, size_t>& messages);
	friend Logger& operator<<(Logger& logger, const std::chrono::time_point<std::chrono::steady_clock>& time);
	friend Logger& operator<<(Logger& logger, const std::chrono::steady_clock::duration& duration);
	friend Logger& operator<<(Logger& logger, const unsigned long value);
	friend Logger& operator<<(Logger& logger, const long long value);
	friend Logger& operator<<(Logger& logger, const unsigned long long value);
	friend Logger& operator<<(Logger& logger, const int value);
	friend Logger& operator<<(Logger& logger, const unsigned int value);
	friend Logger& operator<<(Logger& logger, const bool value);
	friend Logger& operator<<(Logger& logger, const float value);
	friend Logger& operator<<(Logger& logger, const double value);
	friend Logger& operator<<(Logger& logger, const boost::stacktrace::stacktrace& stacktrace);

private:
	// Col: date		Typical data: "2020-06-02"			Alignment: left
	// Col: time		Typical data: "20:55:06.1234"		Alignment: left
	// Col: uptime		Typical data: "(99h 12m 3.123s)"	Alignment: right
	// Col: thread		Typical data: "[main]"				Alignment: left
	// Col: file		Typical data: "Foo.cpp"				Alignment: right
	// Col: line		Typical data: ":346"				Alignment: left
	// Col: level		Typical data: "WARN"				Alignment: right

	//   date       time          (       uptime ) [thread                                         file:line      level| message
	// \n2020-06-02 20:55:06.1234  99h 12m 3.123s  [main         ]                             Main.cpp:346     WARN| A brown fox ...
	// 0 1                        26              42               59                                 94    98      107     

	static const constexpr size_t nl_start_length     = 110;
	static const constexpr size_t nl_date_time_offset = 1;
	static const constexpr size_t nl_date_time_size   = 24;
	static const constexpr size_t nl_uptime_offset    = 26;
	static const constexpr size_t nl_uptime_size      = 16;
	static const constexpr size_t nl_thread_offset    = 42;
	static const constexpr size_t nl_thread_size      = 17;
	static const constexpr size_t nl_file_offset      = 59;
	static const constexpr size_t nl_file_size        = 35;
	static const constexpr size_t nl_line_offset      = 94;
	static const constexpr size_t nl_line_size        = 6;
	static const constexpr size_t nl_level_offset     = 100;
	static const constexpr size_t nl_level_size       = 9;

	// DATA
	const std::chrono::time_point<std::chrono::steady_clock> start;
	LogLevel logLevel = LogLevel::info;
	LogLevel minLogLevel = LogLevel::trace; // The minimum level which is still logged.
	const char* file = nullptr;
	int line = -1;
	std::map<std::thread::id, std::string> threadNames;

	void placeDateTime(std::string& buff);
	void placeUptime(std::string& buff);
	void placeThread(std::string& buff);
	void placeFile(std::string& buff);
	void placeLine(std::string& buff);
	void placeLevel(std::string& buff);
};

namespace log {

/**
* The default logger.
*/
extern Logger logger;

Logger& trace();
Logger& trace(const char* file, int line);
Logger& debug();
Logger& debug(const char* file, int line);
Logger& info();
Logger& info(const char* file, int line);
Logger& warn();
Logger& warn(const char* file, int line);
Logger& error();
Logger& error(const char* file, int line);

} // namespace


#ifdef COLIBRI_NOT_DIST
#define LOG_ENABLED true
#else
#define LOG_ENABLED false
#endif

#define LOG log::logger
#define LOG_REPEAT log::logger.startNewLine()

// Stripping non-error logging in distribution builds.
#define LOG_TRACE if constexpr (!LOG_ENABLED) {} else log::logger.trace(__FILE__, __LINE__)
#define LOG_DEBUG if constexpr (!LOG_ENABLED) {} else log::logger.debug(__FILE__, __LINE__)
#define LOG_INFO if constexpr (!LOG_ENABLED) {} else log::logger.info(__FILE__, __LINE__)
#define LOG_WARN if constexpr (!LOG_ENABLED) {} else log::logger.warn(__FILE__, __LINE__)
#define LOG_ERROR log::logger.error(__FILE__, __LINE__)


COLIBRI_END

#endif // _LOGGER_H_
