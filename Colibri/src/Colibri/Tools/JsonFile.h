#pragma once

#ifndef _JSON_FILE_H_
#define _JSON_FILE_H_

COLIBRI_BEGIN

class JsonFile
{
private:
	File file;

public:
	JsonFile(const std::string& location,
			 bool createDirectories = false,
			 File::CreateFileBehaviour createFileBehaviour = File::CreateFileBehaviour::dont);

	nlohmann::json load();
	void save(const nlohmann::json& json);
	void savePretty(const nlohmann::json& json);

	const std::filesystem::path& getPath() const;
	const File& getFile() const;
};

COLIBRI_END

#endif // _JSON_FILE_H_
