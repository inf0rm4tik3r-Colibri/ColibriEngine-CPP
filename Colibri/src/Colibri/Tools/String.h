#pragma once

#ifndef _STRING_H_
#define _STRING_H_

COLIBRI_BEGIN

namespace string
{

inline void strncopy_xp(char* dest, size_t destSize, const char* source, size_t count)
{
#if defined(__unix__)
    strncpy_r(dest, destSize, source, count);
#elif defined(_MSC_VER)
    strncpy_s(dest, destSize, source, count);
#else
    strncpy(dest, source, count);
#endif
}

const constexpr char* fileSeparator();
std::string fileName(const char* file);
std::string relativeFileName(const char* file, const std::string& toPathElement);
std::string join(const std::vector<std::string>& strings, const char* delimiter = ", ");
std::string toString(std::vector<std::string>& strings, const char* delimiter = ", ");

}

COLIBRI_END

#endif // _TOOLS_H_
