#include "pch.h"

COLIBRI_BEGIN

File::File(const std::string& pathString,
		   bool createDirectories,
		   CreateFileBehaviour createFileBehaviour)
	: path(std::filesystem::path(pathString))
{
	LOG_TRACE << "Using file \"" << path.string() << "\"";
	
	create(createDirectories, createFileBehaviour);
}

bool File::exists() const
{
	return std::filesystem::exists(path);
}

bool File::isRegularFile() const
{
	return std::filesystem::is_regular_file(path);
}

void File::create(bool createDirectories, CreateFileBehaviour createFileBehaviour)
{
	if (createDirectories)
	{
		std::filesystem::create_directories(std::filesystem::path(path).remove_filename());
	}
	if (createFileBehaviour == CreateFileBehaviour::dont)
	{
		return;
	}
	if (createFileBehaviour == CreateFileBehaviour::always)
	{
		fstream = std::fstream(path, std::ios::out | std::ios::trunc);
		if (!fstream.is_open()) {
			throw COLIBRI_EXCEPTION("Failed to create file!");
		}
		fstream.close();
	}
	else if (createFileBehaviour == CreateFileBehaviour::ifNotPresent && !std::filesystem::exists(path))
	{
		fstream = std::fstream(path, std::ios::out);
		fstream.close();
	}
	if (!isRegularFile())
	{
		LOG_ERROR << "\"" << path.string() << "\" does not point to a regular file!";
		throw std::runtime_error("Not a regular file!");
	}
}

std::string File::read()
{
	LOG_TRACE << "Reading file \"" << path.string() << "\"";
	fstream = std::fstream(path, std::ios::in);
	if (!fstream.is_open()) {
		throw COLIBRI_EXCEPTION("Failed to open file!");
	}
	fstream.clear();                 // Clear fail and eof bits.
	fstream.seekg(0, std::ios::beg); // Back to the start!
	std::stringstream buffer;
	buffer << fstream.rdbuf();
	std::string content = buffer.str();
	fstream.close();
	return content;
}

std::string File::readFast()
{
	LOG_TRACE << "Reading file \"" << path.string() << "\"";
	fstream = std::fstream(path, std::ios::in | std::ios::ate);
	if (!fstream.is_open()) {
		throw COLIBRI_EXCEPTION("Failed to open file!");
	}
	// fstream.clear();                 // Clear fail and eof bits.
	// fstream.seekg(0, std::ios::end); // Start -> End.
	size_t size = fstream.tellg();
	std::string buffer(size, ' ');
	fstream.seekg(0);
	fstream.read(buffer.data(), size);
	fstream.close();
	return buffer;
}

std::vector<char> File::readBinary()
{
	LOG_TRACE << "Reading file \"" << path.string() << "\" as binary.";
	fstream = std::fstream(path, std::ios::in | std::ios::ate | std::ios::binary);
	if (!fstream.is_open()) {
		throw COLIBRI_EXCEPTION("Failed to open file!");
	}
	// fstream.clear();                 // Clear fail and eof bits.
	// fstream.seekg(0, std::ios::end); // Start -> End.
	size_t size = fstream.tellg();
	std::vector<char> buffer(size);
	fstream.seekg(0);
	fstream.read(buffer.data(), size);
	fstream.close();
	return buffer;
}

void File::writeAppend(const std::string& data)
{
	LOG_TRACE << "Appending to file \"" << path.string() << "\"";
	fstream = std::fstream(path, std::ios::out | std::ios::app);
	if (!fstream.is_open()) {
		throw COLIBRI_EXCEPTION("Failed to open file!");
	}
	fstream << data;
	fstream.close();
}

void File::writeOver(const std::string& data)
{
	LOG_TRACE << "Overwriting file \"" << path.string() << "\"";
	fstream = std::fstream(path, std::ios::out | std::ios::trunc);
	if (!fstream.is_open()) {
		throw COLIBRI_EXCEPTION("Failed to open file!");
	}
	fstream << data;
	fstream.close();
}

const std::filesystem::path& File::getPath() const
{
	return path;
}

COLIBRI_END
