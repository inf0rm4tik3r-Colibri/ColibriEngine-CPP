#pragma once

#ifndef _SAFETY_H_
#define _SAFETY_H_

COLIBRI_BEGIN

namespace Safety
{

inline void stacktraceDumpSignalHandlerAction(int signum)
{
    signal(signum, SIG_DFL);
    boost::stacktrace::safe_dump_to("./backtrace.dump");
    raise(SIGABRT);
}

inline void registerSignalHandler()
{
    signal(SIGSEGV, &stacktraceDumpSignalHandlerAction);
    signal(SIGABRT, &stacktraceDumpSignalHandlerAction);
}

inline void checkDump()
{
    if (boost::filesystem::exists("./backtrace.dump"))
    {
        std::ifstream ifs("./backtrace.dump");
        boost::stacktrace::stacktrace st = boost::stacktrace::stacktrace::from_dump(ifs);
        LOG_ERROR << "Previous run crashed:";
        LOG_ERROR << st;

        // cleaning up
        ifs.close();
        boost::filesystem::remove("./backtrace.dump");
    }
}

}

COLIBRI_END

#endif // _SAFETY_H_
