#pragma once

#ifndef _TIME_H_
#define _TIME_H_

COLIBRI_BEGIN

namespace time
{

inline std::tm localtime_xp(std::time_t timer)
{
    std::tm bt {};
#if defined(__unix__)
    localtime_r(&timer, &bt);
#elif defined(_MSC_VER)
    localtime_s(&bt, &timer);
#else
    static std::mutex mtx;
    std::lock_guard<std::mutex> lock(mtx);
    bt = *std::localtime(&timer);
#endif
    return bt;
}

// default = "YYYY-MM-DD HH:MM:SS"
inline std::string timeStamp(const std::string& fmt = "%F %T")
{
    auto bt = localtime_xp(std::time(0));
    char buf[64];
    return { buf, std::strftime(buf, sizeof(buf), fmt.c_str(), &bt) };
}

// default = "YYYY-MM-DD HH:MM:SS"
inline void writeTimeStamp(char* buf, unsigned int bufWriteMax, const std::string& fmt = "%F %T")
{
    auto bt = localtime_xp(std::time(0));
    char temp[24];
    std::strftime(temp, 24, fmt.c_str(), &bt);
    string::strncopy_xp(buf, bufWriteMax, temp, bufWriteMax);
}

}

COLIBRI_END

#endif // _TIME_H_
