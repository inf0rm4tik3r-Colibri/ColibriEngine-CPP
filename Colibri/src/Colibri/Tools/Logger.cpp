#include "pch.h"

COLIBRI_BEGIN

namespace log {

// Instantiation of the default logger which is globally available.
Logger logger;

Logger& trace()
{
	return logger.trace();
}

Logger& trace(const char* file, int line)
{
	return logger.trace(file, line);
}

Logger& debug()
{
	return logger.debug();
}

Logger& debug(const char* file, int line)
{
	return logger.debug(file, line);
}

Logger& info()
{
	return logger.info();
}

Logger& info(const char* file, int line)
{
	return logger.info(file, line);
}

Logger& warn()
{
	return logger.warn();
}

Logger& warn(const char* file, int line)
{
	return logger.warn(file, line);
}

Logger& error()
{
	return logger.error();
}

Logger& error(const char* file, int line)
{
	return logger.error(file, line);
}

} // namespace: log

Logger::Logger() : start(std::chrono::high_resolution_clock::now()),
                   logLevel(LogLevel::info),
	               minLogLevel(LogLevel::info),
	               file(nullptr),
	               line(-1)
{
}

Logger& Logger::registerThread(std::thread::id threadId, const std::string& threadName)
{
	threadNames.emplace(threadId, threadName);
	return *this;
}

Logger& Logger::setLogLevel(LogLevel logLevel)
{
	this->logLevel = logLevel;
	return *this;
}

Logger& Logger::setMinLogLevel(LogLevel minLogLevel)
{
	this->minLogLevel = minLogLevel;
	return *this;
}

Logger& Logger::setFile(const char* file)
{
	this->file = file;
	return *this;
}

Logger& Logger::setLine(const int line)
{
	this->line = line;
	return *this;
}

Logger& Logger::startNewLine()
{
	if (isMinLevelNotReached(logLevel, minLogLevel))
	{
		return *this;
	}
	std::string buff(nl_start_length, ' ');
	buff.replace(0, 1, "\n");
	placeDateTime(buff);
	placeUptime(buff);
	placeThread(buff);
	placeFile(buff);
	placeLine(buff);
	placeLevel(buff);
	return *this << buff;
}

void Logger::placeDateTime(std::string& buff)
{
	std::string time = time::timeStamp();
	buff.replace(nl_date_time_offset, time.size(), time);
}

void Logger::placeUptime(std::string& buff)
{
	auto ms = std::to_string((std::chrono::high_resolution_clock::now() - start).count() / 1000000L);
	buff.replace(nl_uptime_offset, ms.size(), ms);
}

void Logger::placeThread(std::string& buff)
{
	auto threadNameEntry = threadNames.find(std::this_thread::get_id());
	if (threadNameEntry != threadNames.end()) {
		auto name = threadNameEntry->second;
		buff.replace(nl_thread_offset, 1, "[");
		buff.replace(nl_thread_offset + 1, std::min(name.size(), nl_thread_size - 3), name);
		buff.replace(nl_thread_offset + nl_thread_size - 3, 1, "]");
	}
	else {
		buff.replace(nl_thread_offset, std::min(static_cast<size_t>(11), nl_thread_size), "[undefined]");
	}
}

void Logger::placeFile(std::string& buff)
{
	if (file != nullptr)
	{
		std::string fileInfo = string::relativeFileName(file, SRC_ROOT);
		size_t size = fileInfo.size();
		size_t diff = std::max(static_cast<int>(size) - static_cast<int>(nl_file_size), 0);
		size -= diff;
		buff.replace(nl_file_offset + nl_file_size - size, size, fileInfo.c_str() + diff);
	}
}

void Logger::placeLine(std::string& buff)
{
	if (line > 0)
	{
		auto lineString = std::to_string(line);
		buff.replace(nl_line_offset, 1, ":");
		buff.replace(nl_line_offset + 1, std::min(nl_line_size - 1, lineString.size()), lineString);
	}
}

void Logger::placeLevel(std::string& buff)
{
	size_t size = logLevelStringLength(logLevel);
	size = std::min(size, nl_level_size - 2);
	buff.replace(nl_level_offset + nl_level_size - size - 2, size, logLevelStringCapitalized(logLevel));
	buff.replace(nl_level_offset + nl_level_size - 2, 2, "| ");
}

Logger& Logger::trace()
{
	setLogLevel(LogLevel::trace);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(nullptr).setLine(-1).startNewLine();
}

Logger& Logger::trace(const char* file, int line)
{
	setLogLevel(LogLevel::trace);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(file).setLine(line).startNewLine();
}

Logger& Logger::debug()
{
	setLogLevel(LogLevel::debug);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(nullptr).setLine(-1).startNewLine();
}

Logger& Logger::debug(const char* file, int line)
{
	setLogLevel(LogLevel::debug);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(file).setLine(line).startNewLine();
}

Logger& Logger::info()
{
	setLogLevel(LogLevel::info);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(nullptr).setLine(-1).startNewLine();
}

Logger& Logger::info(const char* file, int line)
{
	setLogLevel(LogLevel::info);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(file).setLine(line).startNewLine();
}

Logger& Logger::warn()
{
	setLogLevel(LogLevel::warn);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(nullptr).setLine(-1).startNewLine();
}

Logger& Logger::warn(const char* file, int line)
{
	setLogLevel(LogLevel::warn);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(file).setLine(line).startNewLine();
}

Logger& Logger::error()
{
	setLogLevel(LogLevel::error);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(nullptr).setLine(-1).startNewLine();
}

Logger& Logger::error(const char* file, int line)
{
	setLogLevel(LogLevel::error);
	return isMinLevelNotReached(logLevel, minLogLevel) ? *this : setFile(file).setLine(line).startNewLine();
}


/* << OVERLOADS */

Logger& operator<<(Logger& logger, const std::string& message)
{
	if (logger.isMinLevelNotReached(logger.logLevel, logger.minLogLevel))
	{
		return logger;
	}
	std::cout << message;
	return logger;
}

Logger& operator<<(Logger& logger, const char* message)
{
	if (logger.isMinLevelNotReached(logger.logLevel, logger.minLogLevel))
	{
		return logger;
	}
	std::cout << message;
	return logger;
}

Logger& operator<<(Logger& logger, const std::vector<const char*>& messages)
{
	if (logger.isMinLevelNotReached(logger.logLevel, logger.minLogLevel))
	{
		return logger;
	}
	size_t size = messages.size();
	for (unsigned int i = 0; i < size - 1; i++) {
		std::cout << messages[i] << ", ";
	}
	std::cout << messages[size - 1];
	return logger;
}

Logger& operator<<(Logger& logger, const std::vector<std::string>& messages)
{
	if (logger.isMinLevelNotReached(logger.logLevel, logger.minLogLevel))
	{
		return logger;
	}
	size_t size = messages.size();
	for (unsigned int i = 0; i < size - 1; i++) {
		std::cout << messages[i] << ", ";
	}
	std::cout << messages[size - 1];
	return logger;
}

Logger& operator<<(Logger& logger, const char** messages)
{
	if (logger.isMinLevelNotReached(logger.logLevel, logger.minLogLevel))
	{
		return logger;
	}
	const char** ptr = messages;
	for (const char* message = *ptr; message; message = *++ptr) {
		std::cout << message;
	}
	return logger;
}

Logger& operator<<(Logger& logger, const std::tuple<const char**, size_t>& messages)
{
	if (logger.isMinLevelNotReached(logger.logLevel, logger.minLogLevel))
	{
		return logger;
	}
	const char** ptr = std::get<0>(messages);
	size_t size = std::get<1>(messages);
	for (size_t i = 0; i < size - 1; i++) {
		logger << *ptr + i << ", ";
	}
	logger << *ptr + size;
	return logger;
}

Logger& operator<<(Logger& logger, const std::chrono::time_point<std::chrono::steady_clock>& time)
{
	return logger << time.time_since_epoch();
}

Logger& operator<<(Logger& logger, const std::chrono::steady_clock::duration& duration)
{
	return logger << duration.count() / 1000000;
}

Logger& operator<<(Logger& logger, const unsigned long value)
{
	return logger << std::to_string(value);
}

Logger& operator<<(Logger& logger, const long long value)
{
	return logger << std::to_string(value);
}

Logger& operator<<(Logger& logger, const unsigned long long value)
{
	return logger << std::to_string(value);
}

Logger& operator<<(Logger& logger, const int value)
{
	return logger << std::to_string(value);
}

Logger& operator<<(Logger& logger, const unsigned int value)
{
	return logger << std::to_string(value);
}

Logger& operator<<(Logger& logger, const bool value)
{
	return logger << (value == TRUE ? "TRUE" : "FALSE");
}

Logger& operator<<(Logger& logger, const float value)
{
	return logger << std::to_string(value);
}

Logger& operator<<(Logger& logger, const double value)
{
	return logger << std::to_string(value);
}

Logger& operator<<(Logger& logger, const boost::stacktrace::stacktrace& stacktrace)
{
	logger << "--- Stacktrace: \n";
	logger << boost::stacktrace::to_string(stacktrace);
	return logger;
}

COLIBRI_END
