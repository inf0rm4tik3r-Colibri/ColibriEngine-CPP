#include "pch.h"

COLIBRI_BEGIN

namespace string
{

const constexpr char* string::fileSeparator()
{
#if defined(__unix__)
	return "/";
#elif defined(_MSC_VER)
	return "\\";
#else
	return "/";
#endif
}

std::string string::fileName(const char* file)
{
	std::string filestring(file);
	size_t offset = filestring.find_last_of(fileSeparator());
	return offset == std::string::npos
		? file
		: filestring.substr(offset + 1, filestring.length() - offset);
}

std::string string::relativeFileName(const char* file, const std::string& toPathElement)
{
	std::string filestring(file);
	size_t offset = filestring.find(toPathElement);
	return offset == std::string::npos
		? file
		: filestring.substr(offset + toPathElement.length(), filestring.length() - offset);
}

std::string string::join(const std::vector<std::string>& strings, const char* delimiter)
{
	size_t size = strings.size();
	if (size == 0)
	{
		return std::string();
	}

	// Determine length of new string.
	size_t resSize = 0;
	for (const std::string& string : strings) {
		resSize += string.size();
	}
	size_t delimiterLenght = std::string(delimiter).size();
	resSize += delimiterLenght * (size - 1);

	// Create memory.
	std::string result;
	result.resize(resSize);

	// Fill memory with strings and delimiters.
	size_t offset = 0;
	for (int i = 0; i < size - 1; i++) {
		size_t currSize = strings[i].size();
		result.replace(offset, currSize, strings[i].c_str());
		result.replace(offset + currSize, delimiterLenght, delimiter);
		offset += currSize + delimiterLenght;
	}
	size_t lastSize = strings[size - 1].size();
	result.replace(offset, lastSize, strings[size - 1].c_str());

	return result;
}

std::string string::toString(std::vector<std::string>& strings, const char* delimiter)
{
	return std::string("[").append(join(strings, delimiter)).append("]");
}

}

COLIBRI_END
