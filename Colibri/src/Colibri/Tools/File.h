#pragma once

#ifndef _FILE_H_
#define _FILE_H_

COLIBRI_BEGIN

class File
{

private:
	std::filesystem::path path;
	std::fstream fstream;

public:
	enum class OpenMode {
		append, truncate
	};

	enum class CreateFileBehaviour {
		always, ifNotPresent, dont
	};

	File(const std::string& pathString, 
		 bool createDirectories = false, 
		 CreateFileBehaviour createFileBehaviour = CreateFileBehaviour::dont);

	bool exists() const;
	bool isRegularFile() const;
	void create(bool createDirectories = true, CreateFileBehaviour createFileBehaviour = CreateFileBehaviour::ifNotPresent);

	std::string read();
	std::string readFast();
	std::vector<char> readBinary();
	void writeAppend(const std::string& data);
	void writeOver(const std::string& data);

	const std::filesystem::path& getPath() const;
};

COLIBRI_END

#endif // _FILE_H_
