#pragma once

#ifndef _ARRAY_ACCESS_PROXY_H_
#define _ARRAY_ACCESS_PROXY_H_

COLIBRI_BEGIN

template<typename T>
class ArrayAccessProxy
{
public:
	ArrayAccessProxy<T>(T* array) : array(array)
	{ }

	T& operator[](int index)
	{
		return array[index];
	}
	T& operator[](int index) const
	{
		return array[index];
	}
private:
	T* array;
};

COLIBRI_END

#endif // _ARRAY_ACCESS_PROXY_H_
