#pragma once

#ifndef _NUMBERS_H_
#define _NUMBERS_H_

COLIBRI_BEGIN

namespace Number
{

// Generic solution.
template <class T>
inline int numDigits(T number)
{
    int digits = 0;
    if (number < 0) digits = 1; // Remove this line if '-' counts as a digit.
    while (number) {
        number /= 10;
        digits++;
    }
    return digits;
}

// Partial specialization optimization for 32-bit numbers.
template<>
inline int numDigits(int32_t x)
{
    if (x == INT_MIN) return 10 + 1;
    if (x < 0) return numDigits(-x) + 1;

    if (x >= 10000) {
        if (x >= 10000000) {
            if (x >= 100000000) {
                if (x >= 1000000000)
                    return 10;
                return 9;
            }
            return 8;
        }
        if (x >= 100000) {
            if (x >= 1000000)
                return 7;
            return 6;
        }
        return 5;
    }
    if (x >= 100) {
        if (x >= 1000)
            return 4;
        return 3;
    }
    if (x >= 10)
        return 2;
    return 1;
}

}

COLIBRI_END

#endif // _NUMBERS_H_
