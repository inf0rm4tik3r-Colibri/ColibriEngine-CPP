#include "pch.h"
#include "JsonFile.h"

COLIBRI_BEGIN

JsonFile::JsonFile(const std::string& location,
				   bool createDirectories,
				   File::CreateFileBehaviour createFileBehaviour)
	: file(File(location, createDirectories, createFileBehaviour))
{
}

nlohmann::json JsonFile::load()
{
	std::string content = file.readFast();
	try
	{
		nlohmann::json parsed = nlohmann::json::parse(content);
		return parsed;
	}
	catch (nlohmann::json::parse_error exception)
	{
		LOG_WARN << "Was unable to parse content of file \"" << file.getPath().string() << "\" as JSON. "
			<< "Continuing with an empty JSON object. Details: " << exception.what();
		return nlohmann::json {};
	}
}

void JsonFile::save(const nlohmann::json& json)
{
	file.writeOver(json.dump());
}

void JsonFile::savePretty(const nlohmann::json& json)
{
	file.writeOver(json.dump(1));
}

const std::filesystem::path& JsonFile::getPath() const
{
	return file.getPath();
}

const File& JsonFile::getFile() const
{
	return file;
}

COLIBRI_END
