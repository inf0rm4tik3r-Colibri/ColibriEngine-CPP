#pragma once

#ifndef _ENTITY_H_
#define _ENTITY_H_

#include "Entity.fwd.h"
#include "../Graphics/Model/Model.h"
#include "../Graphics/Renderer/GraphicsPipeline.fwd.h"
#include "../Graphics/Renderer/GraphicsPipeline.h"

COLIBRI_BEGIN

/*
 An entity represents a general "thing" in the engine.
 This could be a renderable entity which carries its own transform to be positioned in the world.

 TODO: Traits?
 */
class Entity
{
private:
	uint64_t id;

	// Transform transform;
	std::shared_ptr<Model> model;
	std::shared_ptr<GraphicsPipeline> pipeline;

public:
	Entity(const std::shared_ptr<Model>& model, 
		   const std::shared_ptr<GraphicsPipeline>& pipeline) noexcept;
	~Entity() noexcept;

	uint64_t getId() const;
	const Model* getModel() const;
	const GraphicsPipeline* getPipeline() const;
};

COLIBRI_END

#endif // _ENTITY_H_
