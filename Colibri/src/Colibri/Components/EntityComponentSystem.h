#pragma once

#include "Entity.fwd.h"
#include "Entity.h"

COLIBRI_BEGIN

class EntityComponentSystem
{
private:
	std::vector<Entity> entities;

public:
	EntityComponentSystem() noexcept;
	~EntityComponentSystem() noexcept;

	void clear();
	void addEntity(const Entity& entity);

	const std::vector<Entity>& getEntities() const;
};

COLIBRI_END
