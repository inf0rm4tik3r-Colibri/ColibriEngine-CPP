#include "pch.h"
#include "Entity.h"

COLIBRI_BEGIN

Entity::Entity(const std::shared_ptr<Model>& model,
			   const std::shared_ptr<GraphicsPipeline>& pipeline) noexcept
	: id(IDS::genRand64()), model(model), pipeline(pipeline)
{
	LOG_INFO << "Created entity.";
}

Entity::~Entity() noexcept
{
	LOG_INFO << "Destroyed entity.";
}

uint64_t Entity::getId() const
{
	return id;
}

const Model* Entity::getModel() const
{
	return model.get();
}

const GraphicsPipeline* Entity::getPipeline() const
{
	return pipeline.get();
}

COLIBRI_END
