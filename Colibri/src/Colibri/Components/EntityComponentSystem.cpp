#include "pch.h"
#include "EntityComponentSystem.h"

COLIBRI_BEGIN

EntityComponentSystem::EntityComponentSystem() noexcept
{
}

EntityComponentSystem::~EntityComponentSystem() noexcept
{
}

void EntityComponentSystem::clear()
{
	entities.clear();
}

void EntityComponentSystem::addEntity(const Entity& entity)
{
	entities.push_back(entity);
}

const std::vector<Entity>& EntityComponentSystem::getEntities() const
{
	return entities;
}

COLIBRI_END
