#pragma once

#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include "ApplicationInfo.h"
#include "Engine.fwd.h"
#include "Engine.h"

COLIBRI_BEGIN

class Application
{
public:
	ApplicationInfo appInfo;

	std::unique_ptr<Engine> engine;

	Application(ApplicationInfo appInfo);
	virtual ~Application();

	virtual void initialize() = 0;
	virtual void onUpdate() = 0;
	virtual void terminate() = 0;

	int entryPoint(); // Called by the entrypoint of Colibri.
};

COLIBRI_END

#endif // _APPLICATION_H_
