#pragma once

#ifndef _APPLICATION_INFO_H_
#define _APPLICATION_INFO_H_

COLIBRI_BEGIN

struct ApplicationInfo
{
	const std::string appName;
	const unsigned int appVersionMajor;
	const unsigned int appVersionMinor;
	const unsigned int appVersionPatch;
};

COLIBRI_END

#endif // _APPLICATION_INFO_H_
