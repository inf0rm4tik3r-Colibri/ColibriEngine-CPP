#pragma once

#ifndef _ENTRY_POINT_H_
#define _ENTRY_POINT_H_

#include "Application.h"

COLIBRI_BEGIN

/*
 * Client applications must provide an implementation of this function.
 */
extern Application* createApplication();

COLIBRI_END

/*
 * Main entry point.
 */
int main(int argc, int** argv)
{
	using namespace COLIBRI_NAMESPACE;
	Application* app = createApplication();
	try
	{
		int exitCode = app->entryPoint();
		delete app;
		LOG_INFO << "Programm finished.\n";
		system("pause");
		return exitCode;
	}
	catch (const ColibriException& exception)
	{
		LOG_ERROR << "Generic Colibri::Exception catched:";
		LOG_ERROR << exception.getName();
		LOG_ERROR << exception.what();
		LOG_ERROR << exception.getStacktrace();
		return EXIT_FAILURE;
	}
}

#endif // _ENTRY_POINT_H_
