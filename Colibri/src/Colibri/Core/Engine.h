#pragma once

#ifndef _ENGINE_H_
#define _ENGINE_H_

#include "../Config/ColibriInitialConfig.h"
#include "../Config/ColibriConfig.h"
#include "../Components/EntityComponentSystem.h"
#include "../Graphics/Renderer/RendererManager.h"
#include "../Graphics/Window/WindowManager.h"
#include "ApplicationInfo.h"
#include "Application.fwd.h"
#include "Application.h"

COLIBRI_BEGIN

class Engine {

public:
	// VERSION
	const static int ColibriVersionMajor = 0;
	const static int ColibriVersionMinor = 0;
	const static int ColibriVersionPatch = 1;

	const static std::string& EngineName() {
		static std::string engineName = std::string("Colibri");
		return engineName;
	}

	const static std::string& EngineVersion() {
		static std::string versionString = std::string("")
			+ std::to_string(ColibriVersionMajor) + "."
			+ std::to_string(ColibriVersionMinor) + "."
			+ std::to_string(ColibriVersionPatch);
		return versionString;
	}
	// -------

	// Public static data and function.
	static ColibriInitialConfig initialConfig;
	static ColibriConfig& getConfig();

	static bool warmedUp;
	static void warmUp(ColibriInitialConfig initialConfig, ApplicationInfo appInfo);
	static void coolDown();

private:
	bool shouldTerminate = false;

	/*
	 * The main configuration of the engine. 
	 * Loaded from disk on initialization.
	 * Saved to disk on termination. // TODO!
	 */
	static std::unique_ptr<ColibriConfig> config;
	
	/*
	 * This timer is the engines main clock. It is ticked every frame to calculate a new delta time.
	 */
	Timer timer;

	/*
	 * Indication if the window requested that rendering should not take place.
	 * This can be requested if the window is currently in the process of recreating important parts of it.
	 */
	bool renderStopRequestedByWindow = false;

	/*
	 * Indication if the windows framebuffer is of a size which does not allow rendering.
	 * This might be set if the user minimizes the window, which leads to a zero sized framebuffer.
	 */
	bool renderStopRequestedByZeroSizedFramebuffer = false;

	WindowManager windowManager;

	RendererManager rendererManager;

	/*
	 * Central ECS.
	 */
	EntityComponentSystem ecs;


public: // Instance
	Engine() noexcept;
	~Engine() noexcept;

	int start(Application* app);
	void input();
	void update();
	void render();
	void tick();

	void cleanUp();
	
	void setShouldTerminate(bool shouldTerminate);
	inline bool getShouldTerminate() const;

	const Timer& getTimer() const;
	WindowManager& getWindowManager();
	RendererManager& getRendererManager();
	EntityComponentSystem& getEcs();
};

COLIBRI_END

#endif // _ENGINE_H_
