#include "pch.h"
#include "Application.fwd.h"
#include "Application.h"

COLIBRI_BEGIN

Application::Application(ApplicationInfo appInfo)
	: appInfo(appInfo)
{
}

Application::~Application()
{
}

int Application::entryPoint()
{
	ColibriInitialConfig config;
	config.configLocation = ".\\userdata\\config.json";

	Engine::warmUp(config, appInfo);

	engine = std::make_unique<Engine>();

	// Initialize the application.
	initialize();

	// Execute the engine and let the engine clean up behind.
	int executionResult = engine->start(this);
	engine->cleanUp();
	
	// Let the application terminate / free all resources.
	terminate();

	// Desctruct the engine 
	engine.reset();

	Engine::coolDown();

	return executionResult;
}

COLIBRI_END
