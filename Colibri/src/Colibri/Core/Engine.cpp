#include "pch.h"
#include "Engine.fwd.h"
#include "Engine.h"
#include "../Graphics/Window/Window.h"
#include "../Graphics/Window/WindowManager.h"
#include "../Graphics/Renderer/Renderer.h"
#include "../Platform/Vulkan/Vulkan.h"

COLIBRI_BEGIN

bool Engine::warmedUp = false;
ColibriInitialConfig Engine::initialConfig = ColibriInitialConfig {};
std::unique_ptr<ColibriConfig> Engine::config = nullptr;

void Engine::warmUp(ColibriInitialConfig initialEngineConfig, ApplicationInfo appInfo)
{
	log::logger.registerThread(std::this_thread::get_id(), "main");
	Safety::registerSignalHandler();
	Safety::checkDump();

	LOG_INFO << "----------------------------------";
	LOG_INFO << "ColibriEngine is warming up...";
	LOG_INFO << "----------------------------------";
	LOG_INFO << Engine::EngineName() << " - v" << Engine::EngineVersion();

	if (warmedUp) {
		LOG_ERROR << "Already hot! Returning...";
		return;
	}

	initialConfig = initialEngineConfig;
	config = std::make_unique<ColibriConfig>(initialConfig.configLocation);

	log::logger.setMinLogLevel(Engine::getConfig().getMinLogLevel());

	WindowManager::initializeGLFW();

	switch (Engine::getConfig().getGraphicsApi())
	{
		case GraphicsApi::vulkan:
			Vulkan::initialize(appInfo);
			break;
		case GraphicsApi::opengl:
			LOG_ERROR << "OpenGL is not yet supported.";
			throw COLIBRI_EXCEPTION("Cannot continue with unsupported graphics api.");
			break;
		default:
			throw COLIBRI_EXCEPTION("Unknown graohics api.");
	}
	
	warmedUp = true;

	LOG_INFO << "----------------------------------";
	LOG_INFO << "HOT!";
	LOG_INFO << "----------------------------------";
}

void Engine::coolDown()
{
	LOG_INFO << "----------------------------------";
	LOG_INFO << "ColibriEngine is cooling down...";
	LOG_INFO << "----------------------------------";

	if (!warmedUp) {
		LOG_ERROR << "Was never warmed up! Returning...";
		return;
	}
	Vulkan::terminate();
	WindowManager::terminateGLFW();
	warmedUp = false;

	LOG_INFO << "----------------------------------";
	LOG_INFO << "FROZE!";
	LOG_INFO << "----------------------------------";
}

Engine::Engine() noexcept
{
}

Engine::~Engine() noexcept
{
}

int Engine::start(Application* app)
{
	while (!shouldTerminate) {
		input();
		update();
		render();
		tick();
	}
	return EXIT_SUCCESS;
}

void Engine::input()
{
	glfwPollEvents();
}

void Engine::update()
{
	if (windowManager.getActiveWindow()->shouldClose())
	{
		shouldTerminate = true;
	}
}

void Engine::render()
{
	Window* activeWindow = windowManager.getActiveWindow();
	if (activeWindow == nullptr)
	{
		LOG_WARN << "The Colibri engine needs an active window to render to!";
	}
	else
	{
		if (!windowManager.isWindowSuitableForRendering(activeWindow))
		{
			return;
		}
		Renderer* renderer = rendererManager.getRenderer(activeWindow);
		if (renderer == nullptr)
		{
			LOG_WARN << "Unable to render to active window. No renderer is set!";
		}
		else
		{
			renderer->render(ecs);
		}
	}
}

void Engine::tick()
{
	// One frame passed. We should increment our tick and calculate timing deltas for the next frame.
	timer.tick();
	if (timer.getElapsedS() > 1.0)
	{
		LOG_DEBUG << "FPS: " << timer.getTicks() << "   avgDelta: " << timer.getAvgDeltaMs() << "ms   maxDelta: " << timer.getMaxDeltaMs() << "ms";
		timer.resetElapsed();
		timer.resetTicks();
		timer.resetMaxDelta();
	}
}

void Engine::cleanUp()
{
	rendererManager.waitAllIdle();
	ecs.clear();
}

ColibriConfig& Engine::getConfig()
{
	return *config.get();
}

void Engine::setShouldTerminate(bool shouldTerminate)
{
	this->shouldTerminate = shouldTerminate;
}

bool Engine::getShouldTerminate() const
{
	return shouldTerminate;
}

const Timer& Engine::getTimer() const {
	return timer;
}

WindowManager& Engine::getWindowManager() {
	return windowManager;
}

RendererManager& Engine::getRendererManager() {
	return rendererManager;
}

EntityComponentSystem& Engine::getEcs() {
	return ecs;
}

COLIBRI_END
