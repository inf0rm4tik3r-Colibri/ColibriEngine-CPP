#pragma once

#ifndef _COLIBRI_CONFIG_H_
#define _COLIBRI_CONFIG_H_

#include "JsonConfig.h"
#include "../Graphics/GraphicsApi.h"

COLIBRI_BEGIN

class ColibriConfig : protected JsonConfig
{
private:
	// CORE
	unsigned int version;

	// LOGGING
	Logger::LogLevel minLogLevel;

	// GRAPHICS
	GraphicsApi graphicsApi;
	int resolutionX;
	int resolutionY;
	bool vSync;

	// ...

public:
	ColibriConfig(std::string location, bool saveOnChange = false);

	bool isValidJson();
	void load();
	void save();
	void initDefault();
	void initFromJson(nlohmann::json data);

	Logger::LogLevel getMinLogLevel();
	GraphicsApi getGraphicsApi();
	int getResolutionX();
	int getResolutionY();
	bool isVSync();

	ColibriConfig& setMinLogLevel(Logger::LogLevel minLogLevel);
	ColibriConfig& setGraphicsApi(GraphicsApi graphicsApi);
	ColibriConfig& setResolutionX(int resolutionX);
	ColibriConfig& setResolutionY(int resolutionY);
	ColibriConfig& setVSync(bool vSync);
};

COLIBRI_END

#endif // _COLIBRI_CONFIG_H_
