#pragma once

#ifndef _CONFIG_H_
#define _CONFIG_H_

COLIBRI_BEGIN

class JsonConfig
{
public:
	JsonFile backingFile;
	nlohmann::json data;

	JsonConfig(std::string location, bool createDirectories, File::CreateFileBehaviour createFileBehaviour)
		: backingFile(JsonFile(location, createDirectories, createFileBehaviour))
	{
	}

	virtual bool isValidJson() = 0;

	void load()
	{
		data = backingFile.load();
	}

	void save()
	{
		backingFile.savePretty(data);
	}
};

COLIBRI_END

#endif // _CONFIG_H_
