#include "pch.h"
#include "ColibriConfig.h"
#include "../Exception/Exceptions.h"

COLIBRI_BEGIN

ColibriConfig::ColibriConfig(std::string location, bool saveOnChange)
	: JsonConfig(location, true, File::CreateFileBehaviour::ifNotPresent)
{
	// Load config from underlying data source.
	load();
}

bool ColibriConfig::isValidJson()
{
	return data.is_object()
		&& data.contains("logging")
		&& data.at("logging").contains("minLogLevel")
		&& data.contains("graphics")
		&& data.at("graphics").contains("api")
		&& data.at("graphics").contains("resolutionX")
		&& data.at("graphics").contains("resolutionY")
		&& data.at("graphics").contains("vSync");
}

void ColibriConfig::load()
{
	// Load the configuration from disk.
	JsonConfig::load();
	
	// Default config:
	if (!this->isValidJson())
	{
		LOG_WARN << "Found invlaid FluxEngine config at \""
			<< backingFile.getPath().string() 
			<< "\". Recreating default configuration.";
		data = {};
		initDefault();
		save();
	} 
	else
	{
		// Init from json data.
		initFromJson(data);
	}
}

void ColibriConfig::save()
{
	// Copy values to json data.
	data["version"] = version;
	data["logging"]["minLogLevel"] = Logger::logLevelString(minLogLevel);
	data["graphics"]["api"] = GraphicsApiToString.at(graphicsApi);
	data["graphics"]["resolutionX"] = resolutionX;
	data["graphics"]["resolutionY"] = resolutionY;
	data["graphics"]["vSync"] = vSync;

	// Save json data to disk.
	JsonConfig::save();
}

void ColibriConfig::initDefault()
{
	// Init default config from file.
	JsonFile defaultConfig {".\\res\\DefaultConfig.json"};
	if (!defaultConfig.getFile().exists() || !defaultConfig.getFile().isRegularFile())
	{
		throw COLIBRI_EXCEPTION("Unable to find default config!");
	}
	data = defaultConfig.load();
	initFromJson(data);
	if (!this->isValidJson())
	{
		LOG_ERROR << "Default configuration is invalid.";
	}
	LOG_INFO << "Initialized default configuration.";
}

void ColibriConfig::initFromJson(nlohmann::json json)
{
	version = json["version"].get<unsigned int>();
	minLogLevel = Logger::logLevelFromString(json["logging"]["minLogLevel"].get<std::string>().c_str());
	graphicsApi = GraphicsApiFromString.at(json["graphics"]["api"].get<std::string>());
	resolutionX = json["graphics"]["resolutionX"].get<int>();
	resolutionY = json["graphics"]["resolutionY"].get<int>();
	vSync = json["graphics"]["vSync"].get<bool>();

	LOG_INFO << "Initialized configuration from json: " << json.dump(1);
}

Logger::LogLevel ColibriConfig::getMinLogLevel()
{
	return minLogLevel;
}

GraphicsApi ColibriConfig::getGraphicsApi()
{
	return graphicsApi;
}

int ColibriConfig::getResolutionX()
{
	return resolutionX;
}

int ColibriConfig::getResolutionY()
{
	return resolutionY;
}

bool ColibriConfig::isVSync()
{
	return vSync;
}

ColibriConfig& ColibriConfig::setMinLogLevel(Logger::LogLevel minLogLevel)
{
	this->minLogLevel = minLogLevel;
	return *this;
}

ColibriConfig& ColibriConfig::setGraphicsApi(GraphicsApi graphicsApi)
{
	this->graphicsApi = graphicsApi;
	return *this;
}

ColibriConfig& ColibriConfig::setResolutionX(int resolutionX)
{
	this->resolutionX = resolutionX;
	return *this;
}

ColibriConfig& ColibriConfig::setResolutionY(int resolutionY)
{
	this->resolutionY = resolutionY;
	return *this;
}

ColibriConfig& ColibriConfig::setVSync(bool vSync)
{
	this->vSync = vSync;
	return *this;
}

COLIBRI_END
