//
// pch.h
// Header for standard system include files.
//

#pragma once

#ifndef _COLIBRI_PCH_H_
#define _COLIBRI_PCH_H_

// STD library
#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <climits>
#include <cmath>
#include <csignal>
#include <cstdint>
#include <ctime>
#include <filesystem>
#include <functional>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <optional>
#include <random>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// Json
#include <nlohmann/json.hpp>

// Boost
#include <boost/exception/all.hpp>
#define NOMINMAX
#define BOOST_STACKTRACE_NO_NOMINMAX
#include <boost/stacktrace.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/filesystem.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/unordered_map.hpp>

// COLIBRI CORE
#include "src/Colibri/StaticConfig.h"

// Exceptions
#include "src/Colibri/Exception/Exceptions.h" // redundant...
#include "src/Colibri/Exception/ColibriException.h"
#include "src/Colibri/Exception/IllegalStateException.h"
#include "src/Colibri/Exception/UnsupportedOperationException.h"

// Tools
#include "src/Colibri/Tools/ArrayAccessProxy.h"
#include "src/Colibri/Tools/Assert.h"
#include "src/Colibri/Tools/File.h"
#include "src/Colibri/Tools/IDS.h"
#include "src/Colibri/Tools/JsonFile.h"
#include "src/Colibri/Tools/Logger.h"
#include "src/Colibri/Tools/Numbers.h"
#include "src/Colibri/Tools/Safety.h"
#include "src/Colibri/Tools/String.h"
#include "src/Colibri/Tools/Time.h"
#include "src/Colibri/Tools/Timer.h"
#include "src/Colibri/Tools/TypeName.h"

// Math
#include "src/Colibri/Math/StaticVector.h"
#include "src/Colibri/Math/StaticMatrix.h"
#include "src/Colibri/Math/Quaternion.h"
#include "src/Colibri/Math/Math.h"
#include "src/Colibri/Math/MathTypes.h"

#endif // _COLIBRI_PCH_H_
