#include "pch.h"

#include "Colibri/Observer/Pattern.h"

using namespace COLIBRI_NAMESPACE;

TEST(SubjectTest, CallingNextOnSubject)
{
	Subject<int, int> s;
	auto observer = s.subscribe(
		[](int next, int next2)
		{
			ASSERT_EQ(2, next);
			ASSERT_EQ(3, next2);
		}
	);
	s.next(2, 3);
	s.unsubscribe(observer);
}
