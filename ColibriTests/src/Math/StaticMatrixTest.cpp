#include "pch.h"

using namespace COLIBRI_NAMESPACE;

TEST(StaticMatrixTests, Set)
{
	Mat2i mat;
	mat.set({
		1, 2,
		3, 4
	});
	Mat2i expected
	{
		1, 2, 
		3, 4
	};
	ASSERT_EQ(expected, mat);
}

TEST(StaticMatrixTests, Zero)
{
	Mat3i expected
	{
		0, 0, 0,
		0, 0, 0,
		0, 0, 0
	};
	ASSERT_EQ(expected, Mat3i::makeZero());
	ASSERT_EQ(expected, Mat3i().initZero());
}

TEST(StaticMatrixTests, Identity)
{
	Mat3i expected
	{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1
	};
	ASSERT_EQ(expected, Mat3i::makeIdentity());
	ASSERT_EQ(expected, Mat3i().initIdentity());
}

TEST(StaticMatrixTests, Bias)
{
	Mat4f expected
	{
		0.5, 0,   0,   0.5,
		0,   0.5, 0,   0.5,
		0,   0,   0.5, 0.5,
		0,   0,   0,   1
	};
	ASSERT_EQ(expected, Mat4f::makeBias());
	ASSERT_EQ(expected, Mat4f().initBias());
}

TEST(StaticMatrixTests, Translation)
{
	Mat4f expected
	{
		1, 0, 0, 1,
		0, 1, 0, 2,
		0, 0, 1, 3,
		0, 0, 0, 1
	};
	ASSERT_EQ(expected, Mat4f::makeTranslation({ 1, 2, 3 }));
	ASSERT_EQ(expected, Mat4f().initTranslation({ 1, 2, 3 }));
}

TEST(StaticMatrixTests, RotationAroundX)
{
	Mat3d rot = Mat3d().initRotationX( 45.0 );
	Vec3d vec { 0, 1, 0 };
	Vec3d rotated = rot * vec;
	// Expected is a clockwise rotation about 45 degrees around x
	// where z points to the viewer (RH-coordinate-system).
	Vec3d expected { 0, 0.707107, -0.707107 };
	for (int i = 0; i < 3; i++)
	{
		ASSERT_TRUE(std::abs(expected[i] - rotated[i]) < 0.001);
	}
}

TEST(StaticMatrixTests, RotationAroundY)
{
	Mat3d rot = Mat3d().initRotationY( 45.0 );
	Vec3d vec { 1, 0, 0 };
	Vec3d rotated = rot * vec;
	// Expected is a clockwise rotation about 45 degrees around y
	// where z points to the viewer (RH-coordinate-system).
	Vec3d expected { 0.707107, 0, 0.707107 };
	for (int i = 0; i < 3; i++)
	{
		ASSERT_TRUE(std::abs(expected[i] - rotated[i]) < 0.001);
	}
}

TEST(StaticMatrixTests, RotationAroundZ)
{
	Mat3d rot = Mat3d().initRotationZ( 45.0 );
	Vec3d vec { 0, 1, 0 };
	Vec3d rotated = rot * vec;
	// Expected is a clockwise rotation about 45 degrees around z
	// where z points to the viewer (RH-coordinate-system).
	Vec3d expected { -0.707107, 0.707107, 0 };
	for (int i = 0; i < 3; i++)
	{
		ASSERT_TRUE(std::abs(expected[i] - rotated[i]) < 0.001);
	}
}

TEST(StaticMatrixTests, RotationAroundXYZ)
{
	Mat3d rotX = Mat3d().initRotationX( 25.0 );
	Mat3d rotY = Mat3d().initRotationY( 40.0 );
	Mat3d rotZ = Mat3d().initRotationZ( 75.0 );
	Mat3d rotXYZ = Mat3d().initRotation( 25.0, 40.0, 75.0 );
	Mat3d expected = rotX * rotY * rotZ;
	ASSERT_EQ(expected, rotXYZ);
	for (int c = 0; c < 3; c++)
	{
		for (int r = 0; r < 3; r++)
		{
			ASSERT_TRUE(std::abs(expected[c][r] - rotXYZ[c][r]) < 0.001);
		}
	}

	Vec3d vec { 0, 1, 0 };
	Vec3d rotated = rotXYZ * vec;
	Vec3d expectedRotatedVec { -0.739942, -0.0278277, -0.672095 };
	for (int i = 0; i < 3; i++)
	{
		ASSERT_TRUE(std::abs(expectedRotatedVec[i] - rotated[i]) < 0.001);
	}
}

TEST(StaticMatrixTests, ColumnAccessByVectorReferenceYieldsCorrectAddresses)
{
	Mat4f mat;
	Vec4f& col1 = mat[0];
	ASSERT_TRUE(reinterpret_cast<float(*)>(&col1) == &mat.data[0][0]);
	Vec4f& col2 = mat[1];
	ASSERT_TRUE(reinterpret_cast<float(*)>(&col2) == &mat.data[1][0]);
	Vec4f& col3 = mat[2];
	ASSERT_TRUE(reinterpret_cast<float(*)>(&col3) == &mat.data[2][0]);
	Vec4f& col4 = mat[3];
	ASSERT_TRUE(reinterpret_cast<float(*)>(&col4) == &mat.data[3][0]);
}

TEST(StaticMatrixTests, ColumnAccessByVectorReference)
{
	Mat4f mat;
	Vec4f& col = mat[1];
	col[0] = 3.1f;
	col[1] = 5.4f;
	col[2] = -2.0f;
	col[3] = 9.123f;
	ASSERT_EQ(3.1f, mat[1][0]);
	ASSERT_EQ(5.4f, mat[1][1]);
	ASSERT_EQ(-2.0f, mat[1][2]);
	ASSERT_EQ(9.123f, mat[1][3]);
}

TEST(StaticMatrixTests, Equality)
{
	Mat3i mat1
	{
		 1,  2,  4,
		 3, -1,  6,
		-1,  5, -2
	};
	Mat3i mat2
	{
		 2, -2,  3,
		 1,  5, -5,
		-3,  2, -1
	};
	Mat3i mat3
	{
		 2, -2,  3,
		 1,  5, -5,
		-3,  2, -1
	};
	ASSERT_FALSE(mat1 == mat3);
	ASSERT_TRUE(mat1 != mat3);
	ASSERT_TRUE(mat2 == mat3);
	ASSERT_FALSE(mat2 != mat3);
}

TEST(StaticMatrixTests, MultiplicationNonSquare)
{
	StaticMatrixI<2, 3> mat1;
	// Column 1
	mat1[0][0] = 1;
	mat1[0][1] = 0;
	// Column 2
	mat1[1][0] = 3;
	mat1[1][1] = -1;
	// Column 3
	mat1[2][0] = -2;
	mat1[2][1] = 4;

	StaticMatrixI<3, 2> mat2;
	// Column 1
	mat2[0][0] = 2;
	mat2[0][1] = 1;
	mat2[0][2] = -3;
	// Column 2
	mat2[1][0] = -2;
	mat2[1][1] = 5;
	mat2[1][2] = 4;

	Mat2i result = mat1 * mat2;
	ASSERT_EQ( 11, result[0][0]);
	ASSERT_EQ(-13, result[0][1]);
	ASSERT_EQ(  5, result[1][0]);
	ASSERT_EQ( 11, result[1][1]);
}

TEST(StaticMatrixTests, MultiplicationSquare2)
{
	Mat2i mat1
	{
		 1,  3,
		 2, -1
	};
	Mat2i mat2
	{
		 2, -2,
		 1,  5
	};
	Mat2i result = mat1 * mat2;
	Mat2i expected
	{
		 5,  13,
		 3,  -9
	};
	ASSERT_EQ(expected, result);
}

TEST(StaticMatrixTests, MultiplicationSquare3)
{
	Mat3i mat1
	{
		 1,  2,  4,
		 3, -1,  6,
		-1,  5, -2
	};
	Mat3i mat2
	{
		 2, -2,  3,
		 1,  5, -5,
		-3,  2, -1
	};
	Mat3i result = mat1 * mat2;
	Mat3i expected
	{
		 -8,  16, -11,
		-13,   1,   8,
		  9,  23, -26
	};
	ASSERT_EQ(expected, result);
}

TEST(StaticMatrixTests, MultiplicationSquare4)
{
	Mat4i mat1
	{
		 1,  2,  4,  2,
		 3, -1,  6, -5,
		-1,  5, -2,  4,
		 2, -7,  4,  7
	};
	Mat4i mat2
	{
		 2, -2,  3, -8,
		 1,  5, -5,  2,
		-3,  2, -1,  3,
		 6, -4,  5,  4
	};
	Mat4i result = mat1 * mat2;
	Mat4i expected
	{
		  4,   8,  -1,  16,
		-43,  21, -17, -28,
		 33,   7,  -6,  28,
		 27, -59,  72,  10
	};
	ASSERT_EQ(expected, result);
}

TEST(StaticMatrixTests, VectorMultiplication2D)
{
	Mat2i mat {
		1,  3,
		0, -1
	};
	Vec2i vec { 2, 3 };
	ASSERT_EQ((Vec2i { 11, -3 }), mat * vec);
}

TEST(StaticMatrixTests, VectorMultiplication3D)
{
	Mat3i mat
	{
		 1,  2,  4,
		 3, -1,  6,
		-1,  5, -2
	};
	Vec3i vec { 2, 3, -8 };
	ASSERT_EQ((Vec3i { -24, -45, 29 }), mat * vec);
}

TEST(StaticMatrixTests, VectorMultiplication4D)
{
	Mat4i mat
	{
		 1,  2,  4,  2,
		 3, -1,  6, -5,
		-1,  5, -2,  4,
		 2, -7,  4,  7
	};
	Vec4i vec { 2, 3, -8, 2 };
	ASSERT_EQ((Vec4i { -20, -55, 37, -35 }), mat * vec);
}

TEST(StaticMatrixTests, Transpose)
{
	StaticMatrixI<3, 2> mat;
	mat.set({
		1, 2, 
		3, 4, 
		5, 6
	});
	StaticMatrixI<2, 3> expected;
	expected.set({
		1, 3, 5,
		2, 4, 6
	});
	ASSERT_EQ(expected, mat.transpose());
}

TEST(StaticMatrixTests, Submatrix)
{
	Mat3i mat
	{
		1, 2, 3,
		4, 5, 6,
		7, 8, 9
	};
	Mat2i sub00
	{
		5, 6,
		8, 9
	};
	ASSERT_EQ(sub00, mat.submatrix(0, 0));
	Mat2i sub10
	{
		4, 6,
		7, 9
	};
	ASSERT_EQ(sub10, mat.submatrix(1, 0));
	Mat2i sub20
	{
		4, 5,
		7, 8
	};
	ASSERT_EQ(sub20, mat.submatrix(2, 0));
	Mat2i sub01
	{
		2, 3,
		8, 9
	};
	ASSERT_EQ(sub01, mat.submatrix(0, 1));
	Mat2i sub11
	{
		1, 3,
		7, 9
	};
	ASSERT_EQ(sub11, mat.submatrix(1, 1));
	Mat2i sub21
	{
		1, 2,
		7, 8
	};
	ASSERT_EQ(sub21, mat.submatrix(2, 1));
	Mat2i sub02
	{
		2, 3,
		5, 6
	};
	ASSERT_EQ(sub02, mat.submatrix(0, 2));
	Mat2i sub12
	{
		1, 3,
		4, 6
	};
	ASSERT_EQ(sub12, mat.submatrix(1, 2));
	Mat2i sub22
	{
		1, 2,
		4, 5
	};
	ASSERT_EQ(sub22, mat.submatrix(2, 2));
}

TEST(StaticMatrixTests, Determinant2x2)
{
	Mat2f mat
	{
		-1.5,  6,
		 5,   -2.25
	};
	double det = mat.determinant();
	ASSERT_EQ(-26.625, det);
}

TEST(StaticMatrixTests, Determinant3x3)
{
	Mat3i mat
	{
		 1,  2,  4,
		 3, -1,  6,
		-1,  5, -2
	};
	double det = mat.determinant();
	ASSERT_EQ(28.0, det);
}

TEST(StaticMatrixTests, Determinant4x4)
{
	Mat4f mat
	{
		 6,  2,  4,  2,
		 3,  9,  6, -5,
		-8,  5, -2,  4,
		 2, -7,  4,  7
	};
	double det = mat.determinant();
	ASSERT_EQ(-3888.0, det);
}

TEST(StaticMatrixTests, Determinant5x5)
{
	StaticMatrixI<5, 5> mat;
	mat.set({
		 6,  2,  4,  2,  4,
		 3,  9,  6, -5,  3,
		-8,  5, -2,  4, -3,
		 2, -7,  4,  7,  9,
		 2,  6,  8,  7,  5
	});
	double det = mat.determinant();
	ASSERT_EQ(15316, det);
}

TEST(StaticMatrixTests, Inverse1x1)
{
	Mat1f mat {	5.0f };
	Mat1f expectedInverse {	1.0f / 5.0f };
	ASSERT_EQ(expectedInverse, mat.inverse());
}

TEST(StaticMatrixTests, Inverse2x2)
{
	Mat2f mat
	{
		2, 3,
		4, 7
	};
	Mat2f expectedInverse { 
		 3.5f, -1.5f,
		-2.0f,  1
	};
	ASSERT_EQ(expectedInverse, mat.inverse());
}

TEST(StaticMatrixTests, Inverse3x3)
{
	Mat3f mat
	{
		 1,  2,  4,
		 3, -1,  6,
		-1,  5, -2
	};
	Mat3f expectedInverse
	{
		-1.0f,  6.0f /  7.0f,  4.0f /  7.0f,
		 0.0f,  1.0f / 14.0f,  3.0f / 14.0f,
		 0.5f, -1.0f /  4.0f, -1.0f /  4.0f
	};
	Mat3f inverse = mat.inverse();
	for (int c = 0; c < 3; c++) {
		for (int r = 0; r < 3; r++) {
			ASSERT_TRUE(std::abs(expectedInverse[c][r] - inverse[c][r]) < 0.001f);
		}
	}
}

TEST(StaticMatrixTests, Inverse4x4)
{
	Mat4d mat
	{
		 6,  2,  4,  2,
		 3,  9,  6, -5,
		-8,  5, -2,  4,
		 2, -7,  4,  7
	};
	Mat4d expectedInverse
	{
		  113.0 /  648.0,  -25.0 /  324.0,  -7.0 / 162.0,  -13.0 / 162.0,
		  217.0 / 1944.0,   -5.0 /  972.0,  31.0 / 486.0,  -35.0 / 486.0,
		 -535.0 / 3888.0,  299.0 / 1944.0,  -7.0 / 972.0,  149.0 / 972.0,
		   91.0 /  648.0,  -23.0 /  324.0,  13.0 / 162.0,    1.0 / 162.0
	};
	Mat4d inverse = mat.inverse();
	for (int c = 0; c < 4; c++) {
		for (int r = 0; r < 4; r++) {
			ASSERT_TRUE(std::abs(expectedInverse[c][r] - inverse[c][r]) < 0.001);
		}
	}
}

TEST(StaticMatrixTests, GetMajorDiagonal)
{
	Mat4i mat
	{
		 1,  2,  4,  2,
		 3, -1,  6, -5,
		-1,  5, -2,  4,
		 2, -7,  4,  7
	};
	Vec4i expectedDiagonal { 1, -1, -2, 7 };
	ASSERT_EQ(expectedDiagonal, mat.getMajorDiagonal());
}

TEST(StaticMatrixTests, GetColumn)
{
	Mat4i mat
	{
		 1,  2,  4,  2,
		 3, -1,  6, -5,
		-1,  5, -2,  4,
		 2, -7,  4,  7
	};
	ASSERT_EQ((Vec4i { 1,  3, -1,  2 }), mat.getColumn(0));
	ASSERT_EQ((Vec4i { 2, -1,  5, -7 }), mat.getColumn(1));
	ASSERT_EQ((Vec4i { 4,  6, -2,  4 }), mat.getColumn(2));
	ASSERT_EQ((Vec4i { 2, -5,  4,  7 }), mat.getColumn(3));
}

TEST(StaticMatrixTests, GetRow)
{
	Mat4i mat
	{
		 1,  2,  4,  2,
		 3, -1,  6, -5,
		-1,  5, -2,  4,
		 2, -7,  4,  7
	};
	ASSERT_EQ((Vec4i {  1,  2,  4,  2 }), mat.getRow(0));
	ASSERT_EQ((Vec4i {  3, -1,  6, -5 }), mat.getRow(1));
	ASSERT_EQ((Vec4i { -1,  5, -2,  4 }), mat.getRow(2));
	ASSERT_EQ((Vec4i {  2, -7,  4,  7 }), mat.getRow(3));
}

TEST(StaticMatrixTests, SetColumn)
{
	Mat4i mat
	{
		 1,  2,  4,  2,
		 3, -1,  6, -5,
		-1,  5, -2,  4,
		 2, -7,  4,  7
	};
	mat.setColumn(0, {  1,  2,  4,  2 });
	mat.setColumn(1, {  3, -1,  6, -5 });
	mat.setColumn(2, { -1,  5, -2,  4 });
	mat.setColumn(3, {  2, -7,  4,  7 });
	ASSERT_EQ((Vec4i { 1,  2,  4,  2 }), mat.getColumn(0));
	ASSERT_EQ((Vec4i { 3, -1,  6, -5 }), mat.getColumn(1));
	ASSERT_EQ((Vec4i { -1,  5, -2,  4 }), mat.getColumn(2));
	ASSERT_EQ((Vec4i { 2, -7,  4,  7 }), mat.getColumn(3));
}

TEST(StaticMatrixTests, SetRow)
{
	Mat4i mat
	{
		 1,  2,  4,  2,
		 3, -1,  6, -5,
		-1,  5, -2,  4,
		 2, -7,  4,  7
	};
	mat.setRow(0, { 1,  3, -1,  2 });
	mat.setRow(1, { 2, -1,  5, -7 });
	mat.setRow(2, { 4,  6, -2,  4 });
	mat.setRow(3, { 2, -5,  4,  7 });
	ASSERT_EQ((Vec4i { 1,  3, -1,  2 }), mat.getRow(0));
	ASSERT_EQ((Vec4i { 2, -1,  5, -7 }), mat.getRow(1));
	ASSERT_EQ((Vec4i { 4,  6, -2,  4 }), mat.getRow(2));
	ASSERT_EQ((Vec4i { 2, -5,  4,  7 }), mat.getRow(3));
}
