#include "pch.h"

using namespace COLIBRI_NAMESPACE;

TEST(MathTest, degreesToRadians)
{
	double radians = 1.0;
	double degrees = Math::toDegrees(radians);
	ASSERT_TRUE(degrees > 57.2957);
	ASSERT_TRUE(degrees < 57.2959);
}

TEST(MathTest, radiansToDegrees)
{
	double degrees = 90.0;
	double radians = Math::toRadians(degrees);
	ASSERT_TRUE(radians > 1.5707);
	ASSERT_TRUE(radians < 1.5709);
}

TEST(MathTest, wrapAround)
{
	ASSERT_EQ(4, Math::wrapAround(0, 1, 2, 4));
	ASSERT_EQ(2, Math::wrapAround(1, 1, 2, 4));
	ASSERT_EQ(3, Math::wrapAround(2, 1, 2, 4));
	ASSERT_EQ(4, Math::wrapAround(3, 1, 2, 4));
	ASSERT_EQ(2, Math::wrapAround(4, 1, 2, 4));
	ASSERT_EQ(3, Math::wrapAround(5, 1, 2, 4));
	ASSERT_EQ(4, Math::wrapAround(6, 1, 2, 4));
}

TEST(MathTest, wrapAroundDelta2)
{
	ASSERT_EQ(2, Math::wrapAround(0, 2, 2, 8));
	ASSERT_EQ(3, Math::wrapAround(1, 2, 2, 8));
	ASSERT_EQ(4, Math::wrapAround(2, 2, 2, 8));
	ASSERT_EQ(5, Math::wrapAround(3, 2, 2, 8));
	ASSERT_EQ(6, Math::wrapAround(4, 2, 2, 8));
	ASSERT_EQ(7, Math::wrapAround(5, 2, 2, 8));
	ASSERT_EQ(8, Math::wrapAround(6, 2, 2, 8));
	ASSERT_EQ(2, Math::wrapAround(7, 2, 2, 8));
	ASSERT_EQ(3, Math::wrapAround(8, 2, 2, 8));
	ASSERT_EQ(4, Math::wrapAround(9, 2, 2, 8));
}