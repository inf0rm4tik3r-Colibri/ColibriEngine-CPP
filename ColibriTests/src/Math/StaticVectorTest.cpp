#include "pch.h"

using namespace COLIBRI_NAMESPACE;

TEST(StaticVectorTests, SwizzleConstruct3DFromXY)
{
	const Vec2i xy { 1, 2 };
	const Vec3i v { xy, 3 };
	ASSERT_EQ(1, v[0]);
	ASSERT_EQ(2, v[1]);
	ASSERT_EQ(3, v[2]);
}

TEST(StaticVectorTests, SwizzleConstruct3DFromYZ)
{
	const Vec2i yz { 2, 3 };
	const Vec3i v { 1, yz };
	ASSERT_EQ(1, v[0]);
	ASSERT_EQ(2, v[1]);
	ASSERT_EQ(3, v[2]);
}

TEST(StaticVectorTests, SwizzleConstruct4DFromXYZ)
{
	const Vec3i xyz { 1, 2, 3 };
	const Vec4i v { xyz, 4 };
	ASSERT_EQ(1, v[0]);
	ASSERT_EQ(2, v[1]);
	ASSERT_EQ(3, v[2]);
	ASSERT_EQ(4, v[3]);
}

TEST(StaticVectorTests, SwizzleConstruct4DFromYZW)
{
	const Vec3i yzw { 2, 3, 4 };
	const Vec4i v { 1, yzw };
	ASSERT_EQ(1, v[0]);
	ASSERT_EQ(2, v[1]);
	ASSERT_EQ(3, v[2]);
	ASSERT_EQ(4, v[3]);
}

TEST(StaticVectorTests, SwizzleConstruct4DFromXYZW)
{
	const Vec2i xy { 1, 2 };
	const Vec2i zw { 3, 4 };
	const Vec4i v { xy, zw };
	ASSERT_EQ(1, v[0]);
	ASSERT_EQ(2, v[1]);
	ASSERT_EQ(3, v[2]);
	ASSERT_EQ(4, v[3]);
}

TEST(StaticVectorTests, Construction3i)
{
	const Vec3i v { 1, 2, 3 };
	ASSERT_EQ(1, v[0]);
	ASSERT_EQ(2, v[1]);
	ASSERT_EQ(3, v[2]);
}

TEST(StaticVectorTests, ConstElementAccess)
{
	const Vec3i v { 1, 2, 3 };
	auto x = v[0];
	ASSERT_EQ(1, x);
}

TEST(StaticVectorTests, ElementAccess)
{
	Vec3i v { 1, 2, 3 };
	auto x = v[2];
	ASSERT_EQ(3, x);
}

TEST(StaticVectorTests, ElementAccessAndWrite)
{
	Vec3i v { 1, 2, 3 };
	auto a = v[2];
	a = 10;
	ASSERT_EQ(3, v[2]);
	auto& b = v[2];
	b = 10;
	ASSERT_EQ(10, v[2]);
}

TEST(StaticVectorTests, ElementAccessAndWriteDirect)
{
	Vec3i v { 1, 2, 3 };
	v[2] = 10;
	ASSERT_EQ(10, v[2]);
}

TEST(StaticVectorTests, ElementAccessGetter)
{
	Vec4i v { 1, 2, 3, 4 };
	ASSERT_EQ(1, v.x());
	ASSERT_EQ(2, v.y());
	ASSERT_EQ(3, v.z());
	ASSERT_EQ(4, v.w());
}

TEST(StaticVectorTests, ElementAccessAlternativeGetter)
{
	Vec4i v { 1, 2, 3, 4 };
	ASSERT_EQ(1, v.r());
	ASSERT_EQ(2, v.g());
	ASSERT_EQ(3, v.b());
	ASSERT_EQ(4, v.a());
}

TEST(StaticVectorTests, Swizzle3DXY)
{
	Vec3i v { 1, 2, 3 };
	Vec2i s = v.xy();
	ASSERT_EQ(1, s.x());
	ASSERT_EQ(2, s.y());
}

TEST(StaticVectorTests, Swizzle3DYZ)
{
	Vec3i v { 1, 2, 3 };
	Vec2i s = v.yz();
	ASSERT_EQ(2, s.x());
	ASSERT_EQ(3, s.y());
}

TEST(StaticVectorTests, Swizzle3DXZ)
{
	Vec3i v { 1, 2, 3 };
	Vec2i s = v.xz();
	ASSERT_EQ(1, s.x());
	ASSERT_EQ(3, s.y());
}

TEST(StaticVectorTests, Swizzle4DXYZ)
{
	Vec4i v { 1, 2, 3, 4 };
	Vec3i s = v.xyz();
	ASSERT_EQ(1, s.x());
	ASSERT_EQ(2, s.y());
	ASSERT_EQ(3, s.z());
}

TEST(StaticVectorTests, Swizzle4DRGB)
{
	Vec4i v { 1, 2, 3, 4 };
	Vec3i s = v.rgb();
	ASSERT_EQ(1, s.r());
	ASSERT_EQ(2, s.g());
	ASSERT_EQ(3, s.b());
}

TEST(StaticVectorTests, Equality1D)
{
	Vec1i v1 { 1 };
	Vec1i v2 { 1 };
	ASSERT_TRUE(v1 == v2);
}

TEST(StaticVectorTests, Inequality1D)
{
	Vec1i v1 { 1 };
	Vec1i v2 { 2 };
	ASSERT_TRUE(v1 != v2);
}

TEST(StaticVectorTests, Equality2D)
{
	Vec2i v1 { 1, 2 };
	Vec2i v2 { 1, 2 };
	ASSERT_TRUE(v1 == v2);
}

TEST(StaticVectorTests, Inequality2D)
{
	Vec2i v1 { 1, 2 };
	Vec2i v2 { 1, 4 };
	ASSERT_TRUE(v1 != v2);
}

TEST(StaticVectorTests, Equality3D)
{
	Vec3i v1 { 1, 2, -3 };
	Vec3i v2 { 1, 2, -3 };
	ASSERT_TRUE(v1 == v2);
}

TEST(StaticVectorTests, Inequality3D)
{
	Vec3i v1 { 1, 2, -3 };
	Vec3i v2 { -4, 2, -3 };
	ASSERT_TRUE(v1 != v2);
}

TEST(StaticVectorTests, Equality4D)
{
	Vec4i v1 { 1, 2, -3, 4 };
	Vec4i v2 { 1, 2, -3, 4 };
	ASSERT_TRUE(v1 == v2);
}

TEST(StaticVectorTests, Inequality4D)
{
	Vec4i v1 { 1, 2, -3, 4 };
	Vec4i v2 { -4, 2, -3, 5 };
	ASSERT_TRUE(v1 != v2);
}

TEST(StaticVectorTests, GreaterOrEqual)
{
	Vec3i v1 { 1, 2, 3 };
	Vec3i v2 { 0, 2, 3 };
	Vec3i v3 { 1, 1, 3 };
	Vec3i v4 { 2, 2, 3 };
	ASSERT_FALSE(v1 > v1);
	ASSERT_TRUE(v1 >= v1);
	ASSERT_TRUE(v1 > v2);
	ASSERT_TRUE(v1 > v3);
	ASSERT_FALSE(v1 > v4);
}

TEST(StaticVectorTests, LessOrEqual)
{
	Vec3i v1 { 1, 2, 3 };
	Vec3i v2 { 0, 2, 3 };
	Vec3i v3 { 1, 1, 3 };
	Vec3i v4 { 2, 2, 3 };
	ASSERT_FALSE(v1 < v1);
	ASSERT_TRUE(v1 <= v1);
	ASSERT_FALSE(v1 < v2);
	ASSERT_FALSE(v1 < v3);
	ASSERT_TRUE(v1 < v4);
}

TEST(StaticVectorTests, Add)
{
	Vec3i v1 { 1, 1, 1 };
	Vec3i v2 { -3, 4, 2 };
	Vec3i r = v1 + v2;
	ASSERT_EQ(-2, r[0]);
	ASSERT_EQ(5, r[1]);
	ASSERT_EQ(3, r[2]);
}

TEST(StaticVectorTests, AddFactor)
{
	Vec3i v { -3, 4, 2 };
	Vec3i r = v + 3;
	ASSERT_EQ(0, r[0]);
	ASSERT_EQ(7, r[1]);
	ASSERT_EQ(5, r[2]);
}

TEST(StaticVectorTests, Subtract)
{
	Vec3i v1 { 1, 1, 1 };
	Vec3i v2 { -3, 4, 2 };
	Vec3i r = v1 - v2;
	ASSERT_EQ(4, r[0]);
	ASSERT_EQ(-3, r[1]);
	ASSERT_EQ(-1, r[2]);
}

TEST(StaticVectorTests, SubtractFactor)
{
	Vec3i v { -3, 4, 2 };
	Vec3i sum = v - 3;
	ASSERT_EQ(-6, sum[0]);
	ASSERT_EQ(1, sum[1]);
	ASSERT_EQ(-1, sum[2]);
}

TEST(StaticVectorTests, Multiply)
{
	Vec3i v1 { 2, 3, 4 };
	Vec3i v2 { 1, -2, 3 };
	Vec3i r = v1 * v2;
	ASSERT_EQ(2, r[0]);
	ASSERT_EQ(-6, r[1]);
	ASSERT_EQ(12, r[2]);
}

TEST(StaticVectorTests, MultiplyFactor)
{
	Vec3i v { -3, 4, 2 };
	Vec3i sum = v * 3;
	ASSERT_EQ(-9, sum[0]);
	ASSERT_EQ(12, sum[1]);
	ASSERT_EQ(6, sum[2]);
}

TEST(StaticVectorTests, Divide)
{
	Vec3i v1 { 2, 3, 12 };
	Vec3i v2 { 1, -2, 3 };
	Vec3i r = v1 / v2;
	ASSERT_EQ(2, r[0]);
	ASSERT_EQ(-1, r[1]);
	ASSERT_EQ(4, r[2]);
}

TEST(StaticVectorTests, DivideFactor)
{
	Vec3i v { -3, 4, 2 };
	Vec3i sum = v / 3;
	ASSERT_EQ(-1, sum[0]);
	ASSERT_EQ(1, sum[1]);
	ASSERT_EQ(0, sum[2]);
}

TEST(StaticVectorTests, DotProductOn3dVector)
{
	Vec3f vec1 { 1.0f, 2.0f, 3.0f };
	Vec3f vec2 { -5.0f, 4.0f, 5.0f };
	ASSERT_EQ(18.0f, vec1.dot(vec2));
}

TEST(StaticVectorTests, DotProductOn4dVector)
{
	Vec4i vec4i1 { 1, 2, 3, 3 };
	Vec4i vec4i2 { -5, 4, 5, 6 };
	ASSERT_EQ(36, vec4i1.dot(vec4i2));
}

TEST(StaticVectorTests, CrossOnUnitAxis)
{
	Vec3d unitx { 1.0, 0.0, 0.0 };
	Vec3d unity { 0.0, 1.0, 0.0 };
	Vec3d unitz = Vec3d::cross(unitx, unity);

	ASSERT_EQ(0.0, unitz.x());
	ASSERT_EQ(0.0, unitz.y());
	ASSERT_EQ(1.0, unitz.z());
}

TEST(StaticVectorTests, Angle)
{
	Vec3d unitx { 1.0, 0.0, 0.0 };
	Vec3d unity { 0.0, 1.0, 0.0 };
	Vec3d diagonal { 0.5, 0.5, 0.0 };
	ASSERT_EQ(90.0, unitx.angle(unity));
	ASSERT_TRUE(45.1 > unitx.angle(diagonal));
	ASSERT_TRUE(44.9 < unitx.angle(diagonal));
}

TEST(StaticVectorTests, LerpFree)
{
	Vec3d v1 { 1.0, 1.0, 1.0 };
	Vec3d v2 { 2.0, 2.0, 2.0 };
	Vec3d intermediate = v1.lerpFree(v2, 0.5);
	ASSERT_EQ(1.5, intermediate.x());
	ASSERT_EQ(1.5, intermediate.y());
	ASSERT_EQ(1.5, intermediate.z());
}

TEST(StaticVectorTests, Minmax)
{
	Vec4i minmaxTest { 3, 65, 8, -32 };
	int min = minmaxTest.min();
	int max = minmaxTest.max();
	ASSERT_EQ(-32, min);
	ASSERT_EQ(65, max);
}

TEST(StaticVectorTests, Project)
{
	Vec3f a { 2, 1, 0 };
	Vec3f b { 3, 0, 0 };
	Vec3f r = a.prorject(b);
	ASSERT_EQ(2.0f, r.x());
	ASSERT_EQ(0.0f, r.y());
	ASSERT_EQ(0.0f, r.z());
}

TEST(StaticVectorTests, Reject)
{
	Vec3f a { 2, 1, 0 };
	Vec3f b { 3, 0, 0 };
	Vec3f r = a.reject(b);
	std::cout << r;
	ASSERT_EQ(0.0f, r.x());
	ASSERT_EQ(1.0f, r.y());
	ASSERT_EQ(0.0f, r.z());
}

TEST(StaticVectorTests, Reflect)
{
	Vec3f a { -1, 1, 0 };
	Vec3f b { 1, 0, 0 };
	Vec3f r = a.reflect(b);
	std::cout << r;
	ASSERT_EQ(1.0f, r.x());
	ASSERT_EQ(1.0f, r.y());
	ASSERT_EQ(0.0f, r.z());
}

/*
TEST(StaticVectorTests, Refract)
{
	Vec3f a { 1, -0.5, 0 };
	Vec3f b { 1, 0, 0 };
	Vec3f r = a.refract(b.normalize(), Math::REFRACTION_WATER);
	TestLogger::WriteMessage(r.toString().c_str());
	ASSERT_EQ(0.0f, r.x());
	ASSERT_EQ(0.0f, r.y());
	ASSERT_EQ(0.0f, r.z());
}
*/

TEST(StaticVectorTests, Set)
{
	Vec3i v;
	v.set({ 1, 2, 3 });
	ASSERT_EQ(1, v.x());
	ASSERT_EQ(2, v.y());
	ASSERT_EQ(3, v.z());
}
